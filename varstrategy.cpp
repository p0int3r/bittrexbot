#include "varstrategy.hpp"

VARStrategy::VARStrategy(): VARStrategy(QString(),QString(),trader::Strategy::FrequencyLevel::NONE) {}


VARStrategy::VARStrategy(QString name,QString description,trader::Strategy::FrequencyLevel frequencyLevel)
{
    this->setName(name);
    this->setDescription(description);
    this->setFrequencyLevel(frequencyLevel);
}


VARStrategy::~VARStrategy() {}


void VARStrategy::preprocess(const QString &market,const QObject *object)
{
    Q_UNUSED(market)
    Q_UNUSED(object)

    // NOTE: emit signal at the end.
    emit this->preprocessed(market,object);
}


void VARStrategy::strategise(const QString &market,const QObject *object)
{
    Q_UNUSED(market)
    Q_UNUSED(object)

    // NOTE: emit signal at the end.
    emit this->strategised(market,object);
}
