#include "risktree.hpp"



RiskTree::RiskTree(QWidget *parent): QTreeWidget(parent) { this->initSignalsAndSlots(); }

RiskTree::~RiskTree() {}

void RiskTree::initSignalsAndSlots()
{
    this->connect(this,&RiskTree::itemChanged,this,&RiskTree::receiveItemChanged);
    this->connect(this,&RiskTree::singleSelectionsChanged,this,&RiskTree::filterSingleSelections);
}


void RiskTree::receiveItemChanged(QTreeWidgetItem *item,int column)
{
    if (column!=0) { return; }
    if (item->parent()==nullptr) { return; }
    QString risk=item->text(column);
    QString riskCategoryStr=item->parent()->text(column);
    RiskTree::RiskCategory riskCategory=RiskTree::RiskCategory::NONE;
    if (riskCategoryStr==tr("Stop Loss")) { riskCategory=RiskTree::RiskCategory::STOP_LOSS; }
    if (riskCategoryStr==tr("Take Profit")) { riskCategory=RiskTree::RiskCategory::TAKE_PROFIT; }
    if (item->checkState(column)==Qt::Unchecked) { emit this->riskDeselected(riskCategory,risk); }
    if (item->checkState(column)==Qt::Checked)
    {
        emit this->riskSelected(riskCategory,risk);
        emit this->singleSelectionsChanged(item,column);
    }
}

void RiskTree::filterSingleSelections(QTreeWidgetItem *item,int column)
{
    if (column!=0) { return; }
    if (item->parent()==nullptr) { return; }
    QString parent_name=item->parent()->text(column);
    if (parent_name!=tr("Stop Loss") && parent_name!=tr("Take Profit")) { return; }

    for (int i=0;i<item->parent()->childCount();i++)
    {
        QTreeWidgetItem *child_item=item->parent()->child(i);
        if (child_item==item) { continue; }
        bool isChecked=(child_item->checkState(column)==Qt::Checked);
        if (isChecked==true) { child_item->setCheckState(column,Qt::Unchecked); }
    }
}

void RiskTree::enableItems()
{
    for (int i=0;i<this->topLevelItemCount();i++)
    {
        QTreeWidgetItem *current_top_item=this->topLevelItem(i);
        current_top_item->setFlags(current_top_item->flags() | Qt::ItemIsEnabled);
    }
}

void RiskTree::disableItems()
{
    for (int i=0;i<this->topLevelItemCount();i++)
    {
        QTreeWidgetItem *current_top_item=this->topLevelItem(i);
        current_top_item->setFlags(current_top_item->flags() & ~Qt::ItemIsEnabled);
    }
}
