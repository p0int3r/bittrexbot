#ifndef VARSTRATEGY_HPP
#define VARSTRATEGY_HPP

#include <QObject>
#include <QString>
#include "strategy.hpp"

// TODO: implement the Vector Autoregression.

class VARStrategy: public trader::Strategy
{
    public:
        VARStrategy();
        VARStrategy(QString name,QString description,
            trader::Strategy::FrequencyLevel frequencyLevel);
        ~VARStrategy();

    public slots:
        void preprocess(const QString &market,const QObject *object);
        void strategise(const QString &market,const QObject *object);
};

#endif // VARSTRATEGY_HPP
