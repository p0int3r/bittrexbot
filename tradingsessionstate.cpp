#include "mainwindow.hpp"
#include "tradingsessionstate.hpp"
#include "filterrulestrategy.hpp"
#include "takeprofitrisk.hpp"
#include "stoplossrisk.hpp"
#include "bittrexasset.hpp"
#include "bittrexanalyser.hpp"
#include "bittrexexecution.hpp"
#include <QJsonDocument>
#include <QFile>
#include <QDir>
#include <QJsonArray>
#include <QJsonObject>
#include <QStandardPaths>
#include <QDebug>


TradingSessionState::TradingSessionState(QObject *parent): QObject(parent)
{
    this->setTrader(nullptr);
    this->setBittrexRestApiHandler(nullptr);
    bool exists_restoration_file=this->existsRestorationFile();
    if (exists_restoration_file==false) { this->createRestorationFile(); }
    if (exists_restoration_file==true) { this->loadRestorationFile(); }
}

trader::Trader *TradingSessionState::getTrader() const { return this->trader; }

void TradingSessionState::setTrader(trader::Trader *value)
{
    this->trader=value;
    emit this->traderChanged(this->trader);
}

QJsonObject TradingSessionState::getStateObject() const { return this->stateObject; }

void TradingSessionState::setStateObject(const QJsonObject &value)
{
    this->stateObject=value;
    emit this->stateObjectChanged(this->stateObject);
}

QString TradingSessionState::getRestorationFilePath() const { return this->restorationFilePath; }

void TradingSessionState::setRestorationFilePath(const QString &value)
{
    this->restorationFilePath=value;
    emit this->restorationFilePathChanged(this->restorationFilePath);
}

TradingSessionState::State TradingSessionState::getState() const { return this->state; }

void TradingSessionState::setState(const TradingSessionState::State &value)
{
    this->state=value;
    emit this->stateChanged(this->state);
}

bittrexapi::v3::BittrexRestApiHandler *TradingSessionState::getBittrexRestApiHandler() const { return this->bittrexRestApiHandler; }

void TradingSessionState::setBittrexRestApiHandler(bittrexapi::v3::BittrexRestApiHandler *value)
{
    this->bittrexRestApiHandler=value;
    emit this->bittrexRestApiHandlerChanged(this->bittrexRestApiHandler);
}

bool TradingSessionState::existsRestorationFile()
{
    QDir home_directory=QDir::home();
    QString restoration_file_path=home_directory.path()+QString("/")+QString(".bittrexbot/trading_session_state.json");
    bool exists_restoration_file=QFile::exists(restoration_file_path);
    return exists_restoration_file;
}


bool TradingSessionState::createRestorationFile()
{
    QDir home_directory=QDir::home();
    home_directory.mkdir(".bittrexbot");
    QString restoration_file_path=home_directory.path()+QString("/")+QString(".bittrexbot/trading_session_state.json");
    QFile restoration_file=QFile(restoration_file_path);
    bool create_restoration_file=restoration_file.open(QFile::WriteOnly);
    restoration_file.close();
    this->setRestorationFilePath(restoration_file_path);
    this->setState(TradingSessionState::State::COMPLETED);
    return  create_restoration_file;
}

bool TradingSessionState::loadRestorationFile()
{
    QDir home_directory=QDir::home();
    QString restoration_file_path=home_directory.path()+QString("/")+QString(".bittrexbot/trading_session_state.json");
    this->setRestorationFilePath(restoration_file_path);
    this->setState(TradingSessionState::State::COMPLETED);
    return true;
}


void TradingSessionState::save()
{
    QJsonObject object;
    MainWindow *main_window=qobject_cast<MainWindow *>(this->parent());
    trader::PortfolioHash::iterator portfolio_iterator;
    trader::PortfolioManager *portfolio_manager=this->getTrader()->getPortfolioManager();
    QJsonArray portfolios_array;

    for (portfolio_iterator=portfolio_manager->getPortfolios()->begin();portfolio_iterator!=portfolio_manager->getPortfolios()->end();portfolio_iterator++)
    {
        QJsonObject current_portfolio_object;
        trader::Portfolio *current_portfolio=portfolio_iterator.value();
        current_portfolio_object.insert("name",current_portfolio->getName());
        current_portfolio_object.insert("baseCurrency",current_portfolio->getBaseCurrency());
        current_portfolio_object.insert("initialCapital",current_portfolio->getInitialCapital());
        current_portfolio_object.insert("portfolioId",current_portfolio->getPortfolioId());
        portfolios_array.append(current_portfolio_object);
    }

    trader::AssetHash::iterator asset_iterator;
    trader::AssetManager *asset_manager=this->getTrader()->getAssetManager();
    QJsonArray assets_array;

    for (asset_iterator=asset_manager->getAssets()->begin();asset_iterator!=asset_manager->getAssets()->end();asset_iterator++)
    {
        QJsonObject current_asset_object;
        BittrexAsset *current_asset=static_cast<BittrexAsset *>(asset_iterator.value());
        current_asset_object.insert("market",current_asset->getMarket());
        current_asset_object.insert("allocatedCapital",current_asset->getAllocatedCapital());
        current_asset_object.insert("minimumTradeSize",current_asset->getMinimumTradeSize());
        current_asset_object.insert("capitalPercentage",current_asset->getCapitalPercentage());
        current_asset_object.insert("tradingRounds",current_asset->getTradingRounds());
        current_asset_object.insert("currentRound",current_asset->getCurrentRound());
        current_asset_object.insert("frequencyMilliseconds",current_asset->getFrequencyMilliseconds());
        current_asset_object.insert("precision",current_asset->getPrecision());
        assets_array.append(current_asset_object);
    }

    trader::StrategyHash::iterator strategy_iterator;
    trader::StrategyManager *strategy_manager=this->getTrader()->getStrategyManager();
    QJsonArray strategies_array;

    for (strategy_iterator=strategy_manager->getStrategies()->begin();strategy_iterator!=strategy_manager->getStrategies()->end();strategy_iterator++)
    {
        QJsonObject current_strategy_object;
        trader::Strategy *current_strategy=strategy_iterator.value();
        current_strategy_object.insert("name",current_strategy->getName());
        current_strategy_object.insert("description",current_strategy->getDescription());
        current_strategy_object.insert("frequencyLevel",static_cast<int>(current_strategy->getFrequencyLevel()));
        QString class_name=current_strategy->metaObject()->className();

        if (class_name==QString("FilterRuleStrategy"))
        {
            FilterRuleStrategy *filter_rule_strategy=static_cast<FilterRuleStrategy *>(current_strategy);
            current_strategy_object.insert("filterRule",filter_rule_strategy->getFilterRule());
            current_strategy_object.insert("backwardSteps",filter_rule_strategy->getBackwardSteps());
            current_strategy_object.insert("className",class_name);
        }

        strategies_array.append(current_strategy_object);
    }

    trader::RiskHash::iterator risk_iterator;
    trader::RiskManager *risk_manager=this->getTrader()->getRiskManager();
    QJsonArray risks_array;

    for (risk_iterator=risk_manager->getRisks()->begin();risk_iterator!=risk_manager->getRisks()->end();risk_iterator++)
    {
        QJsonObject current_risk_object;
        trader::Risk *current_risk=risk_iterator.value();
        current_risk_object.insert("name",current_risk->getName());
        current_risk_object.insert("description",current_risk->getDescription());
        current_risk_object.insert("riskIndicator",static_cast<int>(current_risk->getRiskIndicator()));
        QString class_name=current_risk->metaObject()->className();
        current_risk_object.insert("className",class_name);

        if (class_name==QString("TakeProfitRisk"))
        {
            TakeProfitRisk *take_profit_risk=static_cast<TakeProfitRisk *>(current_risk);
            current_risk_object.insert("takeProfit",take_profit_risk->getTakeProfit());
        }
        else if (class_name==QString("StopLossRisk"))
        {
            StopLossRisk *stop_loss_risk=static_cast<StopLossRisk *>(current_risk);
            current_risk_object.insert("stopLoss",stop_loss_risk->getStopLoss());
        }

        risks_array.append(current_risk_object);
    }


    trader::OrderHash::iterator order_iterator;
    trader::OrderManager *order_manager=this->getTrader()->getOrderManager();
    QJsonArray open_orders_array; QJsonArray closed_orders_array;

    for (order_iterator=order_manager->getOpenOrders()->begin();order_iterator!=order_manager->getOpenOrders()->end();order_iterator++)
    {
        QJsonObject current_open_order_object;
        trader::Order *current_open_order=order_iterator.value();
        current_open_order_object.insert("orderId",current_open_order->getOrderId());
        current_open_order_object.insert("exchangeOrderId",current_open_order->getExchangeOrderId());
        current_open_order_object.insert("status",static_cast<int>(current_open_order->getStatus()));
        current_open_order_object.insert("type",static_cast<int>(current_open_order->getType()));
        current_open_order_object.insert("exchange",static_cast<int>(current_open_order->getExchange()));
        current_open_order_object.insert("market",current_open_order->getMarket());
        current_open_order_object.insert("quantity",current_open_order->getQuantity());
        current_open_order_object.insert("price",current_open_order->getPrice());
        current_open_order_object.insert("data",current_open_order->getData());
        current_open_order_object.insert("timeStamp",current_open_order->getTimeStamp().toString());
        current_open_order_object.insert("opened",current_open_order->getOpened().toString());
        current_open_order_object.insert("closed",current_open_order->getClosed().toString());
        open_orders_array.append(current_open_order_object);
    }

    for (order_iterator=order_manager->getClosedOrders()->begin();order_iterator!=order_manager->getClosedOrders()->end();order_iterator++)
    {
        QJsonObject current_closed_order_object;
        trader::Order *current_closed_order=order_iterator.value();
        current_closed_order_object.insert("orderId",current_closed_order->getOrderId());
        current_closed_order_object.insert("exchangeOrderId",current_closed_order->getExchangeOrderId());
        current_closed_order_object.insert("status",static_cast<int>(current_closed_order->getStatus()));
        current_closed_order_object.insert("type",static_cast<int>(current_closed_order->getType()));
        current_closed_order_object.insert("exchange",static_cast<int>(current_closed_order->getExchange()));
        current_closed_order_object.insert("market",current_closed_order->getMarket());
        current_closed_order_object.insert("quantity",current_closed_order->getQuantity());
        current_closed_order_object.insert("price",current_closed_order->getPrice());
        current_closed_order_object.insert("data",current_closed_order->getData());
        current_closed_order_object.insert("timeStamp",current_closed_order->getTimeStamp().toString());
        current_closed_order_object.insert("opened",current_closed_order->getOpened().toString());
        current_closed_order_object.insert("closed",current_closed_order->getClosed().toString());
        closed_orders_array.append(current_closed_order_object);
    }

    object.insert("portfolios",portfolios_array);
    object.insert("state",static_cast<int>(this->getState()));
    object.insert("assets",assets_array);
    object.insert("strategies",strategies_array);
    object.insert("risks",risks_array);
    object.insert("openOrders",open_orders_array);
    object.insert("closedOrders",closed_orders_array);
    object.insert("publicKey",this->getBittrexRestApiHandler()->getPublicKey());
    object.insert("privateKey",this->getBittrexRestApiHandler()->getPrivateKey());
    object.insert("tradingMode",static_cast<int>(main_window->getTradingMode()));

    QJsonDocument document(object);
    QFile json_file=QFile(this->getRestorationFilePath());
    json_file.open(QFile::WriteOnly);
    json_file.write(document.toJson());
    json_file.close();
    this->setStateObject(object);
}


void TradingSessionState::restore()
{
    MainWindow *main_window=qobject_cast<MainWindow *>(this->parent());
    QFile json_file=QFile(this->getRestorationFilePath());
    json_file.open(QFile::ReadOnly);
    QJsonDocument document=QJsonDocument::fromJson(json_file.readAll());
    json_file.close();
    QJsonObject object=document.object();

    this->getBittrexRestApiHandler()->setPublicKey(object.value("publicKey").toString());
    this->getBittrexRestApiHandler()->setPrivateKey(object.value("privateKey").toString());
    main_window->setTradingMode(static_cast<MainWindow::TradingMode>(object.value("tradingMode").toInt()));

    trader::PortfolioManager *portfolio_manager=this->getTrader()->getPortfolioManager();
    QJsonArray::iterator portfolio_iterator;
    QJsonArray portfolios_array=object.value("portfolios").toArray();

    for (portfolio_iterator=portfolios_array.begin();portfolio_iterator!=portfolios_array.end();portfolio_iterator++)
    {
        QJsonObject current_portfolio_object=portfolio_iterator->toObject();
        trader::Portfolio *current_portfolio=portfolio_manager->getPortfolio(current_portfolio_object.value("name").toString());
        current_portfolio->setName(current_portfolio_object.value("name").toString());
        current_portfolio->setBaseCurrency(current_portfolio_object.value("baseCurrency").toString());
        current_portfolio->setPortfolioId(current_portfolio_object.value("portfolioId").toString());
        current_portfolio->setInitialCapital(current_portfolio_object.value("initialCapital").toDouble());
        current_portfolio->setCurrentCapital(current_portfolio_object.value("initialCapital").toDouble());
        current_portfolio->setEquity(current_portfolio_object.value("initialCapital").toDouble());
        current_portfolio->setRealisedPnL(0.0);
        current_portfolio->setUnrealisedPnL(0.0);
        current_portfolio->setReturnOnInvestment(0.0);
    }

    trader::AssetManager *asset_manager=this->getTrader()->getAssetManager();
    trader::AnalyserManager *analyser_manager=this->getTrader()->getAnalyserManager();
    QJsonArray::iterator asset_iterator;
    QJsonArray assets_array=object.value("assets").toArray();

    for (asset_iterator=assets_array.begin();asset_iterator!=assets_array.end();asset_iterator++)
    {
        QJsonObject current_asset_object=asset_iterator->toObject();
        BittrexAsset *current_asset=new BittrexAsset(this->getBittrexRestApiHandler());
        current_asset->setMarket(current_asset_object.value("market").toString());
        current_asset->setAllocatedCapital(current_asset_object.value("allocatedCapital").toDouble());
        current_asset->setMinimumTradeSize(current_asset_object.value("minimumTradeSize").toDouble());
        current_asset->setCapitalPercentage(current_asset_object.value("capitalPercentage").toDouble());
        current_asset->setTradingRounds(current_asset_object.value("tradingRounds").toInt());
        current_asset->setCurrentRound(current_asset_object.value("currentRound").toInt());
        current_asset->setFrequencyMilliseconds(current_asset_object.value("frequencyMilliseconds").toInt());
        current_asset->setPrecision(current_asset_object.value("precision").toDouble());
        bool result=asset_manager->addAsset(current_asset->getMarket(),current_asset);
        current_asset->start();
        if (result==false) { delete current_asset; continue; }
        BittrexAnalyser *bittrex_analyser=new BittrexAnalyser();
        analyser_manager->addAnalyser(current_asset->getMarket(),bittrex_analyser);
    }

    trader::StrategyManager *strategy_manager=this->getTrader()->getStrategyManager();
    QJsonArray::iterator strategy_iterator;
    QJsonArray strategies_array=object.value("strategies").toArray();

    for (strategy_iterator=strategies_array.begin();strategy_iterator!=strategies_array.end();strategy_iterator++)
    {
        QJsonObject current_strategy_object=strategy_iterator->toObject();
        QString class_name=current_strategy_object.value("className").toString();

        if (class_name==QString("FilterRuleStrategy"))
        {
            FilterRuleStrategy *filter_rule_strategy=new FilterRuleStrategy();
            filter_rule_strategy->setName(current_strategy_object.value("name").toString());
            filter_rule_strategy->setDescription(current_strategy_object.value("description").toString());
            filter_rule_strategy->setFrequencyLevel(static_cast<trader::Strategy::FrequencyLevel>(current_strategy_object.value("frequencyLevel").toInt()));
            filter_rule_strategy->setFilterRule(current_strategy_object.value("filterRule").toDouble());
            filter_rule_strategy->setBackwardSteps(current_strategy_object.value("backwardSteps").toInt());
            strategy_manager->addStrategy(filter_rule_strategy->getName(),filter_rule_strategy);
        }
    }

    trader::RiskManager *risk_manager=this->getTrader()->getRiskManager();
    QJsonArray::iterator risk_iterator;
    QJsonArray risks_array=object.value("risks").toArray();

    for (risk_iterator=risks_array.begin();risk_iterator!=risks_array.end();risk_iterator++)
    {
        QJsonObject current_risk_object=risk_iterator->toObject();
        QString class_name=current_risk_object.value("className").toString();

        if (class_name==QString("TakeProfitRisk"))
        {
            TakeProfitRisk *take_profit_risk=new TakeProfitRisk();
            take_profit_risk->setName(current_risk_object.value("name").toString());
            take_profit_risk->setDescription(current_risk_object.value("description").toString());
            take_profit_risk->setRiskIndicator(static_cast<trader::Risk::RiskIndicator>(current_risk_object.value("riskIndicator").toInt()));
            take_profit_risk->setTakeProfit(current_risk_object.value("takeProfit").toDouble());
            risk_manager->addRisk(take_profit_risk->getName(),take_profit_risk);
        }
        else if (class_name==QString("StopLossRisk"))
        {
            StopLossRisk *stop_loss_risk=new StopLossRisk();
            stop_loss_risk->setName(current_risk_object.value("name").toString());
            stop_loss_risk->setDescription(current_risk_object.value("description").toString());
            stop_loss_risk->setRiskIndicator(static_cast<trader::Risk::RiskIndicator>(current_risk_object.value("riskIndicator").toInt()));
            stop_loss_risk->setStopLoss(current_risk_object.value("stopLoss").toDouble());
            risk_manager->addRisk(stop_loss_risk->getName(),stop_loss_risk);
        }
    }

    trader::OrderManager *order_manager=this->getTrader()->getOrderManager();
    trader::ExecutionManager *execution_manager=this->getTrader()->getExecutionManager();
    QJsonArray::iterator order_iterator;
    QJsonArray open_orders_array=object.value("openOrders").toArray();
    QJsonArray closed_orders_array=object.value("closedOrders").toArray();

    std::function<bool(const QJsonValue &,const QJsonValue &)> sort_by_type_ascending_fn;
    std::function<bool(const QJsonValue &,const QJsonValue &)> sort_by_date_ascending_fn;

    sort_by_type_ascending_fn=[=](const QJsonValue &v1,const QJsonValue &v2) -> bool
    {
        QJsonObject v1_object=v1.toObject();
        QJsonObject v2_object=v2.toObject();
        int v1_object_order_type=v1_object.value("type").toInt();
        int v2_object_order_type=v2_object.value("type").toInt();
        return v1_object_order_type<v2_object_order_type;
    };

    sort_by_date_ascending_fn=[=](const QJsonValue &v1,const QJsonValue &v2) -> bool
    {
        QJsonObject v1_object=v1.toObject();
        QJsonObject v2_object=v2.toObject();
        QDateTime v1_object_order_time_stamp=QDateTime::fromString(v1_object.value("timeStamp").toString());
        QDateTime v2_object_order_time_stamp=QDateTime::fromString(v2_object.value("timeStamp").toString());
        return v1_object_order_time_stamp<v2_object_order_time_stamp;
    };

    std::sort(open_orders_array.begin(),open_orders_array.end(),sort_by_type_ascending_fn);
    std::sort(open_orders_array.begin(),open_orders_array.end(),sort_by_date_ascending_fn);

    std::sort(closed_orders_array.begin(),closed_orders_array.end(),sort_by_type_ascending_fn);
    std::sort(closed_orders_array.begin(),closed_orders_array.end(),sort_by_date_ascending_fn);

    for (order_iterator=closed_orders_array.begin();order_iterator!=closed_orders_array.end();order_iterator++)
    {
        QJsonObject current_closed_order_object=order_iterator->toObject();
        trader::Order *current_closed_order=new trader::Order();
        current_closed_order->setOrderId(current_closed_order_object.value("orderId").toString());
        current_closed_order->setExchangeOrderId(current_closed_order_object.value("exchangeOrderId").toString());
        current_closed_order->setStatus(static_cast<trader::Order::Status>(current_closed_order_object.value("status").toInt()));
        current_closed_order->setType(static_cast<trader::Order::Type>(current_closed_order_object.value("type").toInt()));
        current_closed_order->setExchange(static_cast<trader::Order::Exchange>(current_closed_order_object.value("exchange").toInt()));
        current_closed_order->setMarket(current_closed_order_object.value("market").toString());
        current_closed_order->setQuantity(current_closed_order_object.value("quantity").toDouble());
        current_closed_order->setPrice(current_closed_order_object.value("price").toDouble());
        current_closed_order->setData(current_closed_order_object.value("data").toObject());
        current_closed_order->setTimeStamp(QDateTime::fromString(current_closed_order_object.value("timeStamp").toString()));
        current_closed_order->setOpened(QDateTime::fromString(current_closed_order_object.value("opened").toString()));
        current_closed_order->setClosed(QDateTime::fromString(current_closed_order_object.value("closed").toString()));
        this->getTrader()->getOrderManager()->getOpenOrders()->insert(current_closed_order->getOrderId(),current_closed_order);
        this->getTrader()->getOrderManager()->getOpenMarketOrders()->insert(current_closed_order->getMarket(),current_closed_order);
        order_manager->close(current_closed_order->getOrderId(),current_closed_order);
    }

    for (order_iterator=open_orders_array.begin();order_iterator!=open_orders_array.end();order_iterator++)
    {
        QJsonObject current_open_order_object=order_iterator->toObject();
        trader::Order *current_open_order=new trader::Order();
        current_open_order->setOrderId(current_open_order_object.value("orderId").toString());
        current_open_order->setExchangeOrderId(current_open_order_object.value("exchangeOrderId").toString());
        current_open_order->setStatus(static_cast<trader::Order::Status>(current_open_order_object.value("status").toInt()));
        current_open_order->setType(static_cast<trader::Order::Type>(current_open_order_object.value("type").toInt()));
        current_open_order->setExchange(static_cast<trader::Order::Exchange>(current_open_order_object.value("exchange").toInt()));
        current_open_order->setMarket(current_open_order_object.value("market").toString());
        current_open_order->setQuantity(current_open_order_object.value("quantity").toDouble());
        current_open_order->setPrice(current_open_order_object.value("price").toDouble());
        current_open_order->setData(current_open_order_object.value("data").toObject());
        current_open_order->setTimeStamp(QDateTime::fromString(current_open_order_object.value("timeStamp").toString()));
        current_open_order->setOpened(QDateTime::fromString(current_open_order_object.value("opened").toString()));
        current_open_order->setClosed(QDateTime::fromString(current_open_order_object.value("closed").toString()));
        this->getTrader()->getOrderManager()->getSuggestedOrders()->insert(current_open_order->getOrderId(),current_open_order);
        this->getTrader()->getOrderManager()->getSuggestedMarketOrders()->insert(current_open_order->getMarket(),current_open_order);
        BittrexExecution *bittrex_execution=new BittrexExecution(this->getBittrexRestApiHandler());

        if (main_window->getTradingMode()==MainWindow::TradingMode::LIVE_TRADING)
        {
            bittrex_execution->setTradingMode(BittrexExecution::TradingMode::LIVE_TRADING);
            main_window->showGeneralInformation("bittrexbot version: v2.0, bittrex API v3.0, trading mode: live trading");
        }
        if (main_window->getTradingMode()==MainWindow::TradingMode::PAPER_TRADING)
        {
            bittrex_execution->setTradingMode(BittrexExecution::TradingMode::PAPER_TRADING);
            main_window->showGeneralInformation("bittrexbot version: v2.0, bittrex API v3.0, trading mode: paper trading");
        }

        bittrex_execution->setOrder(current_open_order);
        bittrex_execution->setFrequencyMilliseconds(12000);
        bittrex_execution->setCancelAfterMilliseconds(900000);
        execution_manager->addExecution(current_open_order->getOrderId(),bittrex_execution);
        emit bittrex_execution->placed();
        emit bittrex_execution->opened();
    }
}


bool TradingSessionState::check()
{
    bool check_state=true;
    QFile json_file=QFile(this->getRestorationFilePath());
    json_file.open(QFile::ReadOnly);
    if (json_file.size()==0) { return check_state; }
    QJsonDocument document=QJsonDocument::fromJson(json_file.readAll());
    json_file.close();
    TradingSessionState::State state=static_cast<TradingSessionState::State>(document.object().value("state").toInt());
    if (state==TradingSessionState::State::COMPLETED) { check_state=true; }
    else if (state==TradingSessionState::State::CRASHED) { check_state=false; }
    return check_state;
}


inline void swap(QJsonValueRef v1, QJsonValueRef v2)
{
    QJsonValue temp(v1);
    v1=QJsonValue(v2);
    v2=temp;
}
