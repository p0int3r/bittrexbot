#ifndef ARIMASTRATEGY_HPP
#define ARIMASTRATEGY_HPP

#include <QObject>
#include <QString>
#include "strategy.hpp"

// TODO: Implement the autoregressive integrated moving average.

class ARIMAStrategy: public trader::Strategy
{
    Q_OBJECT

    public:
        ARIMAStrategy();
        ARIMAStrategy(QString name,QString description,
            trader::Strategy::FrequencyLevel frequencyLevel);
        ~ARIMAStrategy();

    public slots:
        void preprocess(const QString &market,const QObject *object) override;
        void strategise(const QString &market,const QObject *object) override;
};

#endif // ARIMASTRATEGY_HPP
