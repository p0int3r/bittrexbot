#ifndef TAKEPROFITRISKFACTORY_HPP
#define TAKEPROFITRISKFACTORY_HPP

#include <QObject>
#include <QString>
#include "risk.hpp"


class TakeProfitRiskFactory: public QObject
{
    Q_OBJECT

    public:
        explicit TakeProfitRiskFactory(QObject *parent=nullptr);
        ~TakeProfitRiskFactory();

    signals:

    public slots:
        trader::Risk *createTakeProfitRisk(double takeProfit);
        trader::Risk *createTakeProfitRisk(QString name);
        QString translateTakeProfitRiskToSourceLanguage(QString name);
};

#endif // TAKEPROFITRISKFACTORY_HPP
