#ifndef ARSTRATEGY_HPP
#define ARSTRATEGY_HPP

#include <QObject>
#include <QString>
#include "strategy.hpp"

// TODO: Implement the autoregression time series model.

class ARStrategy: public trader::Strategy
{
    Q_OBJECT

    public:
        ARStrategy();
        ARStrategy(QString name,QString description,
            trader::Strategy::FrequencyLevel frequencyLevel);
        ~ARStrategy();

    public slots:
        void preprocess(const QString &market,const QObject *object) override;
        void strategise(const QString &market,const QObject *object) override;
};

#endif // ARSTRATEGY_HPP
