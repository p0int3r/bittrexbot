#include "bittrexexecution.hpp"
#include <QRandomGenerator>


BittrexExecution::BittrexExecution(): BittrexExecution(nullptr) {}

BittrexExecution::BittrexExecution(bittrexapi::v3::BittrexRestApiHandler *bittrexRestApiHandler): trader::Execution()
{
    this->setBittrexRestApiHandler(bittrexRestApiHandler);
    this->initializeSignalsAndSlots();
}

bittrexapi::v3::BittrexRestApiHandler *BittrexExecution::getBittrexRestApiHandler() const { return this->bittrexRestApiHandler; }

void BittrexExecution::setBittrexRestApiHandler(bittrexapi::v3::BittrexRestApiHandler *value) { this->bittrexRestApiHandler=value; }

BittrexExecution::TradingMode BittrexExecution::getTradingMode() const { return this->tradingMode; }

void BittrexExecution::setTradingMode(const BittrexExecution::TradingMode &value) { this->tradingMode=value; }

void BittrexExecution::initializeSignalsAndSlots()
{
    this->connect(this,&BittrexExecution::mockOrderGenerated,this,&BittrexExecution::recondition);
}

void BittrexExecution::recondition(const QString &orderId,const QJsonObject &data)
{
    this->getOrder()->setData(data);
    QString order_status=data.value("status").toString();
    double quantity_filled=this->getOrder()->getData().value("fillQuantity").toString().toDouble();
    this->getOrder()->setQuantityFilled(quantity_filled);

    if (order_status==QString("CLOSED"))
    {
        this->getOrder()->setClosed(QDateTime::currentDateTime());
        emit this->closed();
    }

    emit this->reconditioned(orderId,this->getOrder());
}


void BittrexExecution::place()
{
    this->getOrder()->setOpened(QDateTime::currentDateTime());
    trader::Order::Type orderType=this->getOrder()->getType();

    if (this->getTradingMode()==BittrexExecution::TradingMode::LIVE_TRADING)
    {
        if (orderType==trader::Order::Type::LIMIT_BUY)
        {
            QString orderId=this->getOrder()->getOrderId();
            QString market=this->getOrder()->getMarket();
            double quantity=this->getOrder()->getQuantity();
            double rate=this->getOrder()->getPrice();
            QString timeInForce=QString("GOOD_TIL_CANCELLED");
            QJsonObject order_object=QJsonObject();
            order_object.insert("marketSymbol",market);
            order_object.insert("direction","BUY");
            order_object.insert("type","LIMIT");
            order_object.insert("quantity",quantity);
            order_object.insert("limit",rate);
            order_object.insert("timeInForce",timeInForce);
            this->getBittrexRestApiHandler()->placeOrder(orderId,order_object);
        }
        else if (orderType==trader::Order::Type::LIMIT_SELL)
        {
            QString orderId=this->getOrder()->getOrderId();
            QString market=this->getOrder()->getMarket();
            double quantity=this->getOrder()->getQuantity();
            double rate=this->getOrder()->getPrice();
            QString timeInForce=QString("GOOD_TIL_CANCELLED");
            QJsonObject order_object=QJsonObject();
            order_object.insert("marketSymbol",market);
            order_object.insert("direction","SELL");
            order_object.insert("type","LIMIT");
            order_object.insert("quantity",quantity);
            order_object.insert("limit",rate);
            order_object.insert("timeInForce",timeInForce);
            this->getBittrexRestApiHandler()->placeOrder(orderId,order_object);
        }
    }

    emit this->placed();
}


void BittrexExecution::update()
{
    if (this->getTradingMode()==BittrexExecution::TradingMode::LIVE_TRADING)
    {
        QString uuid=this->getOrder()->getExchangeOrderId();
        QString orderId=this->getOrder()->getOrderId();
        this->getBittrexRestApiHandler()->queryOrder(orderId,uuid);
    }

    if (this->getTradingMode()==BittrexExecution::TradingMode::PAPER_TRADING)
    {
        this->mockOrderGenerator();
        emit this->opened();
    }

    emit this->updated();
}


void BittrexExecution::cancel()
{
    if (this->getTradingMode()==BittrexExecution::TradingMode::LIVE_TRADING)
    {
        if (this->getOrder()->getData().isEmpty()) { return; }
        QString exchangeOrderId=this->getOrder()->getExchangeOrderId();
        QString orderId=this->getOrder()->getOrderId();
        this->getBittrexRestApiHandler()->cancelOrder(orderId,exchangeOrderId);
    }

    if (this->getTradingMode()==BittrexExecution::TradingMode::PAPER_TRADING)
    {
        emit this->canceled();
    }
}


void BittrexExecution::stop()
{
    this->getTimer()->stop();
    emit this->stopped();
}


void BittrexExecution::mockOrderGenerator()
{
    QJsonObject object=QJsonObject();
    QString id=QUuid::createUuid().toString();
    object.insert("id",id);
    if (this->getOrder()->getType()==trader::Order::Type::LIMIT_BUY) { object.insert("direction","BUY"); }
    if (this->getOrder()->getType()==trader::Order::Type::LIMIT_SELL) { object.insert("direction","SELL"); }
    object.insert("type","LIMIT");
    object.insert("quantity",QString::number(this->getOrder()->getQuantity()));
    object.insert("fillQuantity",QString::number(this->getOrder()->getQuantity()));
    object.insert("limit",QString::number(this->getOrder()->getPrice()));
    object.insert("commission",QString::number(this->getOrder()->getPrice()*this->getOrder()->getQuantity()*0.020));
    quint32 random_number=QRandomGenerator::global()->generate()%10;
    QDateTime opened=QDateTime::currentDateTime();
    object.insert("createdAt",opened.toString("dd-MM-yyyyThh:mm:ss.zzz"));
    object.insert("status","OPEN");

    if (random_number==0)
    {
        object.insert("closedAt",QDateTime::currentDateTime().toString("dd-MM-yyyyThh:mm:ss.zzz"));
        object.insert("status","CLOSED");
    }

    this->getOrder()->setData(object);
    emit this->mockOrderGenerated(this->getOrder()->getOrderId(),this->getOrder()->getData());
}
