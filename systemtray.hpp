#ifndef SYSTEMTRAY_HPP
#define SYSTEMTRAY_HPP

#include <QObject>
#include <QAction>
#include <QSystemTrayIcon>


class SystemTray: public QObject
{
    Q_OBJECT

    private:
        QSystemTrayIcon *trayIcon=nullptr;

    public:
        explicit SystemTray(QObject *parent=nullptr);
        QSystemTrayIcon *getTrayIcon() const;
        void setTrayIcon(QSystemTrayIcon *value);

    signals:
        void signalIconActivated();
        void signalShow();
        void signalQuit();

    private slots:
        void iconActivated(QSystemTrayIcon::ActivationReason reason);

    public slots:
        void hideIconTray();

};

#endif // SYSTEMTRAY_HPP
