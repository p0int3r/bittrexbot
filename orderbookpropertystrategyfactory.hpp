#ifndef ORDERBOOKPROPERTYSTRATEGYFACTORY_HPP
#define ORDERBOOKPROPERTYSTRATEGYFACTORY_HPP

#include <QObject>
#include <QString>
#include "strategy.hpp"

class OrderBookPropertyStrategyFactory: public QObject
{
    Q_OBJECT

    public:

        enum class OrderBookProperty
        {
            DEPTH_IMBALANCE,
            VOLUME_IMBALANCE,
            NONE
        }; Q_ENUM(OrderBookProperty)

    public:
        explicit OrderBookPropertyStrategyFactory(QObject *parent=nullptr);
        ~OrderBookPropertyStrategyFactory();

    signals:

    public slots:
        trader::Strategy *createOrderBookPropertyStrategy(OrderBookPropertyStrategyFactory::OrderBookProperty orderBookProperty);
        trader::Strategy *createOrderBookPropertyStrategy(QString name);
};

#endif // ORDERBOOKPROPERTYSTRATEGYFACTORY_HPP
