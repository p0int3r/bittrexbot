#ifndef ARMASTRATEGY_HPP
#define ARMASTRATEGY_HPP

#include <QObject>
#include <QString>
#include "strategy.hpp"

// TODO: Implement the autoregressive moving average.

class ARMAStrategy: public trader::Strategy
{
    Q_OBJECT

    public:
        ARMAStrategy();
        ARMAStrategy(QString name,QString description,
            trader::Strategy::FrequencyLevel frequencyLevel);
        ~ARMAStrategy();


    public slots:
        void preprocess(const QString &market,const QObject *object) override;
        void strategise(const QString &market,const QObject *object) override;
};

#endif // ARMASTRATEGY_HPP
