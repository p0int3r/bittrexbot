#ifndef STOPLOSSRISK_HPP
#define STOPLOSSRISK_HPP

#include <QObject>
#include <QString>
#include "risk.hpp"

class StopLossRisk: public trader::Risk
{
    Q_OBJECT
    Q_PROPERTY(double stopLoss READ getStopLoss WRITE setStopLoss NOTIFY stopLossChanged)

    private:
        double stopLoss;

    public:
        StopLossRisk();
        StopLossRisk(QString name,QString description,
            trader::Risk::RiskIndicator riskIndicator);
        ~StopLossRisk();
        double getStopLoss() const;
        void setStopLoss(double value);

    signals:
        void stopLossChanged(const double &stopLoss);

    public slots:
        void preprocess(const QString &market,const QObject *object) override;
        void assess(const QString &market,const QObject *object) override;
};

#endif // STOPLOSSRISK_HPP
