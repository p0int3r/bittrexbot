#ifndef OPENORDERTABLE_HPP
#define OPENORDERTABLE_HPP

#include <QObject>
#include <QWidget>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QMap>
#include "order.hpp"


class OpenOrderTable: public QTableWidget
{
    Q_OBJECT

    public:

        enum class ColorMode
        {
            LIGHT,
            DARK
        }; Q_ENUM(ColorMode)

    private:
        OpenOrderTable::ColorMode colorMode;
        QMap<QString,QList<QTableWidgetItem *>> *map=nullptr;

    public:
        explicit OpenOrderTable(QWidget *parent=nullptr);
        ~OpenOrderTable();
        OpenOrderTable::ColorMode getColorMode() const;
        void setColorMode(const OpenOrderTable::ColorMode &value);
        QMap<QString,QList<QTableWidgetItem *>> *getMap() const;
        void setMap(QMap<QString,QList<QTableWidgetItem *>> *value);
        void initializeSignalsAndSlots();

    signals:
        void tableItemsModified(const QString &orderId,const trader::Order *order);
        void tableItemsInserted(const QString &orderId,const trader::Order *order);
        void tableItemsCanceled(const QString &orderId,const trader::Order *order);
        void tableItemsRemoved(const QString &orderId,const trader::Order *order);

    public slots:
        void changeToLightMode();
        void changeToDarkMode();
        void insertTableItems(const QString &orderId,const trader::Order *order);
        void cancelTableItems(const QString &orderId,const trader::Order *order);
        void removeTableItems(const QString &orderId,const trader::Order *order);
        void modifyTableItems(const QString &orderId,const trader::Order *order);
};

#endif // OPENORDERTABLE_HPP
