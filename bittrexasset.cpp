#include "bittrexasset.hpp"


BittrexAsset::BittrexAsset(bittrexapi::v3::BittrexRestApiHandler *bittrexRestApiHandler): trader::Asset()
{
    this->setBittrexRestApiHandler(bittrexRestApiHandler);
    this->setMinimumTradeSize(0.0);
    this->setCapitalPercentage(1.0);
    this->setTradingRounds(1);
    this->setPrecision(0);
}

int BittrexAsset::getPrecision() const { return this->precision; }

void BittrexAsset::setPrecision(int value)
{
    this->precision=value;
    emit this->precisionChanged(this->precision);
}

double BittrexAsset::getMinimumTradeSize() const { return this->minimumTradeSize; }

void BittrexAsset::setMinimumTradeSize(double value)
{
    this->minimumTradeSize=value;
    emit this->minimumTradeSizeChanged(this->minimumTradeSize);
}


bittrexapi::v3::BittrexRestApiHandler *BittrexAsset::getBittrexRestApiHandler() const { return this->bittrexRestApiHandler; }

void BittrexAsset::setBittrexRestApiHandler(bittrexapi::v3::BittrexRestApiHandler *value)
{
    this->bittrexRestApiHandler=value;
    emit this->bittrexRestApiHandlerChanged(this->bittrexRestApiHandler);
}

void BittrexAsset::start()
{
    QString market_name=this->getMarket();
    this->getBittrexRestApiHandler()->queryMarketTicker(market_name);
    this->getBittrexRestApiHandler()->queryMarketOrderBook(market_name,500);
    this->getBittrexRestApiHandler()->queryMarketTrades(market_name);
    this->getBittrexRestApiHandler()->queryMarketSummary(market_name);
    this->getBittrexRestApiHandler()->queryMarket(market_name);
    this->getTimer()->start(this->getFrequencyMilliseconds());
    emit this->started();
}

void BittrexAsset::update()
{
    QString market_name=this->getMarket();
    this->getBittrexRestApiHandler()->queryMarketTicker(market_name);
    this->getBittrexRestApiHandler()->queryMarketOrderBook(market_name,500);
    this->getBittrexRestApiHandler()->queryMarketTrades(market_name);
    this->getBittrexRestApiHandler()->queryMarketSummary(market_name);
    emit this->updated();
}


void BittrexAsset::stop()
{
    this->getTimer()->stop();
    emit this->stopped();
}
