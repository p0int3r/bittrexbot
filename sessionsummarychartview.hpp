#ifndef SESSIONSUMMARYCHARTVIEW_HPP
#define SESSIONSUMMARYCHARTVIEW_HPP

#include <QObject>
#include <QtCharts>
#include <QGraphicsView>


class FullScreenChartView: public QChartView
{
    public:
        explicit FullScreenChartView(QWidget *parent=nullptr);

    public slots:
        void changeToLightMode();
        void changeToDarkMode();

    protected:
        void mouseDoubleClickEvent(QMouseEvent *event) override;
};


class SessionSummaryChartView: public QChartView
{
    private:
        FullScreenChartView fullScreenChartView;

    public:
        explicit SessionSummaryChartView(QWidget *parent=nullptr);
        FullScreenChartView &getFullScreenChartView();

    public slots:
        void changeToLightMode();
        void changeToDarkMode();

    protected:
        void mouseDoubleClickEvent(QMouseEvent *event) override;
};


#endif // SESSIONSUMMARYCHARTVIEW_HPP
