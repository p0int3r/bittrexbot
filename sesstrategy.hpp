#ifndef SESSTRATEGY_HPP
#define SESSTRATEGY_HPP

#include <QObject>
#include <QString>
#include "strategy.hpp"

// TODO: implement Simple Exponential Smoothing.

class SESStrategy: public trader::Strategy
{
    Q_OBJECT

    public:
        SESStrategy();
        SESStrategy(QString name,QString description,
            trader::Strategy::FrequencyLevel frequencyLevel);
        ~SESStrategy();

    public slots:
        void preprocess(const QString &market,const QObject *object);
        void strategise(const QString &market,const QObject *object);
};

#endif // SESSTRATEGY_HPP
