#ifndef CLOSEDORDERTABLE_HPP
#define CLOSEDORDERTABLE_HPP

#include <QObject>
#include <QWidget>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QMap>
#include "order.hpp"


class ClosedOrderTable: public QTableWidget
{
    Q_OBJECT

    public:

        enum class ColorMode
        {
            LIGHT,
            DARK
        }; Q_ENUM(ColorMode)

    private:
        ClosedOrderTable::ColorMode colorMode;
        QMap<QString,QList<QTableWidgetItem *>> *map=nullptr;

    public:
        explicit ClosedOrderTable(QWidget *parent=nullptr);
        ~ClosedOrderTable();
        ClosedOrderTable::ColorMode getColorMode() const;
        void setColorMode(const ClosedOrderTable::ColorMode &value);
        QMap<QString,QList<QTableWidgetItem *>> *getMap() const;
        void setMap(QMap<QString,QList<QTableWidgetItem *>> *value);
        void initializeSignalsAndSlots();

    signals:
        void tableItemsModified(const QString &market,const trader::Order *order);
        void tableItemsInserted(const QString &orderId,const trader::Order *order);
        void tableItemsCanceled(const QString &orderId,const trader::Order *order);
        void tableItemsRemoved(const QString &orderId,const trader::Order *order);

    public slots:
        void changeToLightMode();
        void changeToDarkMode();
        void insertTableItems(const QString &orderId,const trader::Order *order);
        void cancelTableItems(const QString &orderId,const trader::Order *order);
        void removeTableItems(const QString &orderId,const trader::Order *order);
        void modifyTableItems(const QString &orderId,const trader::Order *order);
};

#endif // CLOSEDORDERTABLE_HPP
