#include "timeseriesstrategyfactory.hpp"
#include "arstrategy.hpp"
#include "armastrategy.hpp"
#include "mastrategy.hpp"
#include "arimastrategy.hpp"
#include "sarimastrategy.hpp"
#include "sarimaxstrategy.hpp"
#include "varstrategy.hpp"
#include "varmastrategy.hpp"
#include "varmaxstrategy.hpp"
#include "sesstrategy.hpp"
#include "hwesstrategy.hpp"


TimeSeriesStrategyFactory::TimeSeriesStrategyFactory(QObject *parent): QObject(parent) {}

TimeSeriesStrategyFactory::~TimeSeriesStrategyFactory() {}


trader::Strategy *TimeSeriesStrategyFactory::createTimeSeriesStrategy(QString name)
{
    trader::Strategy *timeSeriesStrategy=nullptr;

    if (name==QString("AR"))
    {
        QString name=QString("AR");
        QString description=QString("Autoregression");
        trader::Strategy::FrequencyLevel frequencyLevel=trader::Strategy::FrequencyLevel::MEDIUM;
        timeSeriesStrategy=new ARStrategy(name,description,frequencyLevel);
        return timeSeriesStrategy;
    }
    else if (name==QString("MA"))
    {
        QString name=QString("MA");
        QString description=QString("Moving Average");
        trader::Strategy::FrequencyLevel frequencyLevel=trader::Strategy::FrequencyLevel::MEDIUM;
        timeSeriesStrategy=new MAStrategy(name,description,frequencyLevel);
        return timeSeriesStrategy;
    }
    else if (name==QString("ARMA"))
    {
        QString name=QString("ARMA");
        QString description=QString("Autoregressive Moving Average");
        trader::Strategy::FrequencyLevel frequencyLevel=trader::Strategy::FrequencyLevel::MEDIUM;
        timeSeriesStrategy=new ARMAStrategy(name,description,frequencyLevel);
        return timeSeriesStrategy;
    }
    else if (name==QString("ARIMA"))
    {
        QString name=QString("ARIMA");
        QString description=QString("Autoregressive Integrated Moving Average");
        trader::Strategy::FrequencyLevel frequencyLevel=trader::Strategy::FrequencyLevel::MEDIUM;
        timeSeriesStrategy=new ARIMAStrategy(name,description,frequencyLevel);
        return timeSeriesStrategy;
    }
    else if (name==QString("SARIMA"))
    {
        QString name=QString("SARIMA");
        QString description=QString("Seasonal Autoregressive Integrated Moving-Average");
        trader::Strategy::FrequencyLevel frequencyLevel=trader::Strategy::FrequencyLevel::MEDIUM;
        timeSeriesStrategy=new SARIMAStrategy(name,description,frequencyLevel);
    }
    else if (name==QString("SARIMAX"))
    {
        QString name=QString("SARIMAX");
        QString description=QString("Seasonal Autoregressive Integrated Moving-Average with Exogenous Regressors");
        trader::Strategy::FrequencyLevel frequencyLevel=trader::Strategy::FrequencyLevel::MEDIUM;
        timeSeriesStrategy=new SARIMAXStrategy(name,description,frequencyLevel);
    }
    else if (name==QString("VAR"))
    {
        QString name=QString("VAR");
        QString description=QString("Vector Autoregression");
        trader::Strategy::FrequencyLevel frequencyLevel=trader::Strategy::FrequencyLevel::MEDIUM;
        timeSeriesStrategy=new VARStrategy(name,description,frequencyLevel);
    }
    else if (name==QString("VARMA"))
    {
        QString name=QString("VARMA");
        QString description=QString("Vector Autoregression Moving-Average");
        trader::Strategy::FrequencyLevel frequencyLevel=trader::Strategy::FrequencyLevel::MEDIUM;
        timeSeriesStrategy=new VARMAStrategy(name,description,frequencyLevel);
    }
    else if (name==QString("VARMAX"))
    {
        QString name=QString("VARMAX");
        QString description=QString("Vector Autoregression Moving-Average with Exogenous Regressors");
        trader::Strategy::FrequencyLevel frequencyLevel=trader::Strategy::FrequencyLevel::MEDIUM;
        timeSeriesStrategy=new VARMAXStrategy(name,description,frequencyLevel);
    }
    else if (name==QString("SES"))
    {
        QString name=QString("SES");
        QString description=QString("Simple Exponential Smoothing");
        trader::Strategy::FrequencyLevel frequencyLevel=trader::Strategy::FrequencyLevel::MEDIUM;
        timeSeriesStrategy=new SESStrategy(name,description,frequencyLevel);
    }
    else if (name==QString("HWES"))
    {
        QString name=QString("HWES");
        QString description=QString("Holt Winter?s Exponential Smoothing");
        trader::Strategy::FrequencyLevel frequencyLevel=trader::Strategy::FrequencyLevel::MEDIUM;
        timeSeriesStrategy=new HWESStrategy(name,description,frequencyLevel);
    }

    return timeSeriesStrategy;
}


trader::Strategy *TimeSeriesStrategyFactory::createTimeSeriesStrategy(TimeSeriesStrategyFactory::TimeSeries timeSeries)
{
    trader::Strategy *timeSeriesStrategy=nullptr;
    if (timeSeries==TimeSeriesStrategyFactory::TimeSeries::AR) { timeSeriesStrategy=this->createTimeSeriesStrategy(QString("AR")); }
    else if (timeSeries==TimeSeriesStrategyFactory::TimeSeries::MA) { timeSeriesStrategy=this->createTimeSeriesStrategy(QString("MA")); }
    else if (timeSeries==TimeSeriesStrategyFactory::TimeSeries::ARMA) { timeSeriesStrategy=this->createTimeSeriesStrategy(QString("ARMA")); }
    else if (timeSeries==TimeSeriesStrategyFactory::TimeSeries::ARIMA) { timeSeriesStrategy=this->createTimeSeriesStrategy(QString("ARIMA")); }
    else if (timeSeries==TimeSeriesStrategyFactory::TimeSeries::SARIMA) { timeSeriesStrategy=this->createTimeSeriesStrategy(QString("SARIMA")); }
    else if (timeSeries==TimeSeriesStrategyFactory::TimeSeries::SARIMAX) { timeSeriesStrategy=this->createTimeSeriesStrategy(QString("SARIMAX")); }
    else if (timeSeries==TimeSeriesStrategyFactory::TimeSeries::VAR) { timeSeriesStrategy=this->createTimeSeriesStrategy(QString("VAR")); }
    else if (timeSeries==TimeSeriesStrategyFactory::TimeSeries::VARMA) { timeSeriesStrategy=this->createTimeSeriesStrategy(QString("VARMA")); }
    else if (timeSeries==TimeSeriesStrategyFactory::TimeSeries::VARMAX) { timeSeriesStrategy=this->createTimeSeriesStrategy(QString("VARMAX")); }
    else if (timeSeries==TimeSeriesStrategyFactory::TimeSeries::SES) { timeSeriesStrategy=this->createTimeSeriesStrategy(QString("SES")); }
    else if (timeSeries==TimeSeriesStrategyFactory::TimeSeries::HWES) { timeSeriesStrategy=this->createTimeSeriesStrategy(QString("HWES")); }
    return timeSeriesStrategy;
}

