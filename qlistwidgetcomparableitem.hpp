#ifndef QLISTWIDGETCOMPARABLEITEM_HPP
#define QLISTWIDGETCOMPARABLEITEM_HPP

#include <QListWidget>
#include <QListWidgetItem>
#include "marketlist.hpp"
#include "marketlistitem.hpp"


class QListWidgetComparableItem: public QListWidgetItem
{
    public:
        explicit QListWidgetComparableItem(QListWidget *parent=nullptr): QListWidgetItem(parent) {}

        bool operator <(const QListWidgetItem &rhs) const
        {
            bool result=false;
            const QListWidgetItem *lhs=static_cast<const QListWidgetItem *>(this);
            const QListWidgetComparableItem *item=dynamic_cast<const QListWidgetComparableItem *>(&rhs);
            MarketList *market_list=static_cast<MarketList *>(item->listWidget());
            MarketListItem *market_list_item_lhs=static_cast<MarketListItem *>(market_list->itemWidget(const_cast<QListWidgetItem *>(lhs)));
            MarketListItem *market_list_item_rhs=static_cast<MarketListItem *>(market_list->itemWidget(const_cast<QListWidgetItem *>(&rhs)));
            MarketList::SortBy sortBy=market_list->getSortBy();

            if (sortBy==MarketList::SortBy::VOLUME)
            {
                double volume_lhs=market_list_item_lhs->getQuoteVolume().toDouble();
                double volume_rhs=market_list_item_rhs->getQuoteVolume().toDouble();
                result=volume_lhs<volume_rhs;
            }

            if (sortBy==MarketList::SortBy::CHANGE)
            {
                QString change_str_lhs=market_list_item_lhs->getPercentChange().split("%")[0];
                QString change_str_rhs=market_list_item_rhs->getPercentChange().split("%")[0];
                double change_lhs=change_str_lhs.toDouble();
                double change_rhs=change_str_rhs.toDouble();
                result=change_lhs<change_rhs;
            }

            if (sortBy==MarketList::SortBy::LAST_PRICE)
            {
                double last_price_lhs=market_list_item_lhs->getLastPrice().toDouble();
                double last_price_rhs=market_list_item_rhs->getLastPrice().toDouble();
                result=last_price_lhs<last_price_rhs;
            }

            return result;
        }
};

#endif // QLISTWIDGETCOMPARABLEITEM_HPP
