#ifndef VARMAXSTRATEGY_HPP
#define VARMAXSTRATEGY_HPP

#include <QObject>
#include <QString>
#include "strategy.hpp"

// TODO: implement the Vector Autoregression Moving-Average with Exogenous Regressors

class VARMAXStrategy: public trader::Strategy
{
    Q_OBJECT

    public:
        VARMAXStrategy();
        VARMAXStrategy(QString name,QString description,
            trader::Strategy::FrequencyLevel frequencyLevel);
        ~VARMAXStrategy();

    public slots:
        void preprocess(const QString &market,const QObject *object);
        void strategise(const QString &market,const QObject *object);
};

#endif // VARMAXSTRATEGY_HPP
