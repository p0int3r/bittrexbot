#ifndef RISKTREE_HPP
#define RISKTREE_HPP

#include <QWidget>
#include <QString>
#include <QTreeWidget>


class RiskTree: public QTreeWidget
{
    Q_OBJECT

    public:

        enum class RiskCategory
        {
            STOP_LOSS,
            TAKE_PROFIT,
            NONE
        }; Q_ENUM(RiskCategory)

    public:
        explicit RiskTree(QWidget *parent=nullptr);
        ~RiskTree();
        void initSignalsAndSlots();

    signals:
        void singleSelectionsChanged(QTreeWidgetItem *item,int column);
        void riskSelected(const RiskTree::RiskCategory &riskCategory,const QString &risk);
        void riskDeselected(const RiskTree::RiskCategory &riskCategory,const QString &risk);

    public slots:
        void receiveItemChanged(QTreeWidgetItem *item,int column);
        void filterSingleSelections(QTreeWidgetItem *item,int column);
        void enableItems();
        void disableItems();
};

#endif // RISKTREE_HPP
