#ifndef BITTREXSIZER_HPP
#define BITTREXSIZER_HPP

#include <QObject>
#include "filterer.hpp"
#include "trader.hpp"

class BittrexSizer: public trader::Sizer
{
    Q_OBJECT

    private:
        trader::Trader *trader=nullptr;

    public:
        BittrexSizer();
        BittrexSizer(trader::Trader *trader);
        ~BittrexSizer();
        trader::Trader *getTrader() const;
        void setTrader(trader::Trader *value);

    signals:

    public slots:
        void size(const QString &market,const QObject *object) override;
};

#endif // BITTREXSIZER_HPP
