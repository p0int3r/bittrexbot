#ifndef BITTREXEXECUTION_HPP
#define BITTREXEXECUTION_HPP

#include <QObject>
#include "execution.hpp"
#include "v3_bittrexrestapihandler.hpp"


class BittrexExecution: public trader::Execution
{
    Q_OBJECT

    public:

        enum class TradingMode
        {
            LIVE_TRADING,
            PAPER_TRADING
        }; Q_ENUM(TradingMode)


    private:
        BittrexExecution::TradingMode tradingMode;
        bittrexapi::v3::BittrexRestApiHandler *bittrexRestApiHandler=nullptr;

    public:
        BittrexExecution();
        BittrexExecution(bittrexapi::v3::BittrexRestApiHandler *bittrexRestApiHandler=nullptr);
        bittrexapi::v3::BittrexRestApiHandler *getBittrexRestApiHandler() const;
        void setBittrexRestApiHandler(bittrexapi::v3::BittrexRestApiHandler *value);
        BittrexExecution::TradingMode getTradingMode() const;
        void setTradingMode(const BittrexExecution::TradingMode &value);
        void initializeSignalsAndSlots();

    signals:
        void mockOrderGenerated(const QString &orderId,const QJsonObject &data);
        void reconditioned(const QString &orderId,const trader::Order *order);

    public slots:
        void mockOrderGenerator();
        void recondition(const QString &orderId,const QJsonObject &data);
        void place() override;
        void update() override;
        void cancel() override;
        void stop() override;
};

#endif // BITTREXEXECUTION_HPP
