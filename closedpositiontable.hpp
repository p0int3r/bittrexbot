#ifndef CLOSEDPOSITIONTABLE_HPP
#define CLOSEDPOSITIONTABLE_HPP

#include <QObject>
#include <QWidget>
#include <QTableWidgetItem>
#include <QMap>
#include "position.hpp"


class ClosedPositionTable: public QTableWidget
{
    Q_OBJECT

    public:

        enum class ColorMode
        {
            LIGHT,
            DARK
        }; Q_ENUM(ColorMode)

    private:
        ClosedPositionTable::ColorMode colorMode;
        QMap<QString,QList<QTableWidgetItem *>> *map=nullptr;

    public:
        ClosedPositionTable(QWidget *parent=nullptr);
        ~ClosedPositionTable();
        ClosedPositionTable::ColorMode getColorMode() const;
        void setColorMode(const ClosedPositionTable::ColorMode &value);
        QMap<QString,QList<QTableWidgetItem *>> *getMap() const;
        void setMap(QMap<QString,QList<QTableWidgetItem *>> *value);
        void initializeSignalsAndSlots();

    signals:
        void tableItemsModified(const QString &market,const trader::Position *position);
        void tableItemsInserted(const QString &market,const trader::Position *position);
        void tableItemsCanceled(const QString &market,const trader::Position *position);
        void tableItemsRemoved(const QString &market,const trader::Position *position);

    public slots:
        void changeToLightMode();
        void changeToDarkMode();
        void insertTableItems(const QString &market,const trader::Position *position);
        void cancelTableItems(const QString &market,const trader::Position *position);
        void removeTableItems(const QString &market,const trader::Position *position);
        void modifyTableItems(const QString &market,const trader::Position *position);
};

#endif // CLOSEDPOSITIONTABLE_HPP
