#include "systemtray.hpp"
#include <QMenu>
#include <QtGlobal>


SystemTray::SystemTray(QObject *parent): QObject(parent)
{
    QMenu *tray_icon_menu=new QMenu();
    tray_icon_menu->setObjectName("systemTrayMenu");
    QAction *open_action=new QAction(tr("Open"),parent);
    open_action->setData(QVariant("Open"));
    QAction *quit_action=new QAction(tr("Quit"),parent);
    quit_action->setData(QVariant("Quit"));
    this->connect(open_action,&QAction::triggered,this,&SystemTray::signalShow);
    this->connect(quit_action,&QAction::triggered,this,&SystemTray::signalQuit);
    tray_icon_menu->addAction(open_action);
    tray_icon_menu->addAction(quit_action);
    this->setTrayIcon(new QSystemTrayIcon());
    this->getTrayIcon()->setContextMenu(tray_icon_menu);
    this->getTrayIcon()->setIcon(QIcon(QString(":/icons/light_mode/bittrexbot_logo.png")));
    this->getTrayIcon()->show();
    this->getTrayIcon()->setToolTip(QString("BittrexBot"));
    this->connect(this->trayIcon,QOverload<QSystemTrayIcon::ActivationReason>::of(&QSystemTrayIcon::activated),this,&SystemTray::iconActivated);
}


QSystemTrayIcon *SystemTray::getTrayIcon() const { return this->trayIcon; }

void SystemTray::setTrayIcon(QSystemTrayIcon *value) { this->trayIcon=value; }

void SystemTray::hideIconTray() { this->getTrayIcon()->hide(); }

void SystemTray::iconActivated(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason)
    {
        case QSystemTrayIcon::Trigger:
            emit this->signalIconActivated();
            break;
        default:
            break;
    }
}
