#ifndef MARKETLIST_HPP
#define MARKETLIST_HPP

#include <QWidget>
#include <QListWidget>
#include <QMap>
#include <QJsonObject>
#include <QListWidgetItem>

class MarketList: public QListWidget
{
    Q_OBJECT

    public:

        enum class SortBy
        {
            VOLUME,
            CHANGE,
            LAST_PRICE
        }; Q_ENUM(SortBy)

        enum class ColorMode
        {
            LIGHT,
            DARK
        }; Q_ENUM(ColorMode)

    private:
        MarketList::SortBy sortBy;
        MarketList::ColorMode colorMode;
        QMap<QString,QListWidgetItem *> *map=nullptr;

    public:
        explicit MarketList(QWidget *parent=nullptr);
        ~MarketList();
        QMap<QString,QListWidgetItem *> *getMap() const;
        void setMap(QMap<QString,QListWidgetItem *> *value);
        SortBy getSortBy() const;
        void setSortBy(const SortBy &value);
        MarketList::ColorMode getColorMode() const;
        void setColorMode(const MarketList::ColorMode &value);

    signals:
        void listItemModified();

    public slots:
        void changeToLightMode();
        void changeToDarkMode();
        void modifyListItem(const QString &market,const QJsonObject &data);
};

#endif // MARKETLIST_HPP
