#ifndef VARMASTRATEGY_HPP
#define VARMASTRATEGY_HPP

#include <QObject>
#include <QString>
#include "strategy.hpp"


// TODO: implement the Vector Autoregression Moving-Average.

class VARMAStrategy: public trader::Strategy
{
    Q_OBJECT

    public:
        VARMAStrategy();
        VARMAStrategy(QString name,QString description,
            trader::Strategy::FrequencyLevel frequencyLevel);
        ~VARMAStrategy();

    public slots:
        void preprocess(const QString &market,const QObject *object);
        void strategise(const QString &market,const QObject *object);
};

#endif // VARMASTRATEGY_HPP
