#ifndef HWESSTRATEGY_HPP
#define HWESSTRATEGY_HPP

#include <QObject>
#include <QString>
#include "strategy.hpp"

// TODO: implement Holt Winter?s Exponential Smoothing.

class HWESStrategy: public trader::Strategy
{
    Q_OBJECT

    public:
        HWESStrategy();
        HWESStrategy(QString name,QString description,
            trader::Strategy::FrequencyLevel frequencyLevel);
        ~HWESStrategy();

    public slots:
        void preprocess(const QString &market,const QObject *object);
        void strategise(const QString &market,const QObject *object);
};

#endif // HWESSTRATEGY_HPP
