#include "filterrulestrategyfactory.hpp"
#include "filterrulestrategy.hpp"

FilterRuleStrategyFactory::FilterRuleStrategyFactory(QObject *parent): QObject(parent) {}

FilterRuleStrategyFactory::~FilterRuleStrategyFactory() {}


trader::Strategy *FilterRuleStrategyFactory::createFilterRuleStrategy(QString name,int backwardSteps)
{
    FilterRuleStrategy *filterRuleStrategy=nullptr;

    if (name==QString("Trigger-buy at 0.01%"))
    {
        double filterRule=0.01;
        QString description=QString("Triggers a limit buy order at 0.01% momentum percentage increase");
        trader::Strategy::FrequencyLevel frequencyLevel=trader::Strategy::FrequencyLevel::HIGH;
        filterRuleStrategy=new FilterRuleStrategy(QString("Trigger-buy at 0.01%"),description,frequencyLevel);
        filterRuleStrategy->setFilterRule(filterRule);
        filterRuleStrategy->setBackwardSteps(backwardSteps);
    }
    else if (name==QString("Trigger-buy at 0.02%"))
    {
        double filterRule=0.02;
        QString description=QString("Triggers a limit buy order at 0.02% momentum percentage increase");
        trader::Strategy::FrequencyLevel frequencyLevel=trader::Strategy::FrequencyLevel::HIGH;
        filterRuleStrategy=new FilterRuleStrategy(QString("Trigger-buy at 0.02%"),description,frequencyLevel);
        filterRuleStrategy->setFilterRule(filterRule);
        filterRuleStrategy->setBackwardSteps(backwardSteps);
    }
    else if (name==QString("Trigger-buy at 0.03%"))
    {
        double filterRule=0.03;
        QString description=QString("Triggers a limit buy order at 0.03% momentum percentage increase");
        trader::Strategy::FrequencyLevel frequencyLevel=trader::Strategy::FrequencyLevel::HIGH;
        filterRuleStrategy=new FilterRuleStrategy(QString("Trigger-buy at 0.03%"),description,frequencyLevel);
        filterRuleStrategy->setFilterRule(filterRule);
        filterRuleStrategy->setBackwardSteps(backwardSteps);
    }
    else if (name==QString("Trigger-buy at 0.04%"))
    {
        double filterRule=0.04;
        QString description=QString("Triggers a limit buy order at 0.04% momentum percentage increase");
        trader::Strategy::FrequencyLevel frequencyLevel=trader::Strategy::FrequencyLevel::HIGH;
        filterRuleStrategy=new FilterRuleStrategy(QString("Trigger-buy at 0.04%"),description,frequencyLevel);
        filterRuleStrategy->setFilterRule(filterRule);
        filterRuleStrategy->setBackwardSteps(backwardSteps);
    }
    else if (name==QString("Trigger-buy at 0.05%"))
    {
        double filterRule=0.05;
        QString description=QString("Triggers a limit buy order at 0.05% momentum percentage increase");
        trader::Strategy::FrequencyLevel frequencyLevel=trader::Strategy::FrequencyLevel::HIGH;
        filterRuleStrategy=new FilterRuleStrategy(QString("Trigger-buy at 0.05%"),description,frequencyLevel);
        filterRuleStrategy->setFilterRule(filterRule);
        filterRuleStrategy->setBackwardSteps(backwardSteps);
    }
    else if (name==QString("Trigger-buy at 0.06%"))
    {
        double filterRule=0.06;
        QString description=QString("Triggers a limit buy order at 0.06% momentum percentage increase");
        trader::Strategy::FrequencyLevel frequencyLevel=trader::Strategy::FrequencyLevel::HIGH;
        filterRuleStrategy=new FilterRuleStrategy(QString("Trigger-buy at 0.06%"),description,frequencyLevel);
        filterRuleStrategy->setFilterRule(filterRule);
        filterRuleStrategy->setBackwardSteps(backwardSteps);
    }
    else if (name==QString("Trigger-buy at 0.07%"))
    {
        double filterRule=0.07;
        QString description=QString("Triggers a limit buy order at 0.07% momentum percentage increase");
        trader::Strategy::FrequencyLevel frequencyLevel=trader::Strategy::FrequencyLevel::HIGH;
        filterRuleStrategy=new FilterRuleStrategy(QString("Trigger-buy at 0.07%"),description,frequencyLevel);
        filterRuleStrategy->setFilterRule(filterRule);
        filterRuleStrategy->setBackwardSteps(backwardSteps);
    }
    else if (name==QString("Trigger-buy at 0.08%"))
    {
        double filterRule=0.08;
        QString description=QString("Triggers a limit buy order at 0.08% momentum percentage increase");
        trader::Strategy::FrequencyLevel frequencyLevel=trader::Strategy::FrequencyLevel::HIGH;
        filterRuleStrategy=new FilterRuleStrategy(QString("Trigger-buy at 0.08%"),description,frequencyLevel);
        filterRuleStrategy->setFilterRule(filterRule);
        filterRuleStrategy->setBackwardSteps(backwardSteps);
    }
    else if (name==QString("Trigger-buy at 0.09%"))
    {
        double filterRule=0.09;
        QString description=QString("Triggers a limit buy order at 0.09% momentum percentage increase");
        trader::Strategy::FrequencyLevel frequencyLevel=trader::Strategy::FrequencyLevel::HIGH;
        filterRuleStrategy=new FilterRuleStrategy(QString("Trigger-buy at 0.09%"),description,frequencyLevel);
        filterRuleStrategy->setFilterRule(filterRule);
        filterRuleStrategy->setBackwardSteps(backwardSteps);
    }
    else if (name==QString("Trigger-buy at 0.1%"))
    {
        double filterRule=0.1;
        QString description=QString("Triggers a limit buy order at 0.1% momentum percentage increase");
        trader::Strategy::FrequencyLevel frequencyLevel=trader::Strategy::FrequencyLevel::HIGH;
        filterRuleStrategy=new FilterRuleStrategy(QString("Trigger-buy at 0.1%"),description,frequencyLevel);
        filterRuleStrategy->setFilterRule(filterRule);
        filterRuleStrategy->setBackwardSteps(backwardSteps);
    }
    else if (name==QString("Trigger-buy at 0.2%"))
    {
        double filterRule=0.2;
        QString description=QString("Triggers a limit buy order at 0.2% momentum percentage increase");
        trader::Strategy::FrequencyLevel frequencyLevel=trader::Strategy::FrequencyLevel::HIGH;
        filterRuleStrategy=new FilterRuleStrategy(QString("Trigger-buy at 0.2%"),description,frequencyLevel);
        filterRuleStrategy->setFilterRule(filterRule);
        filterRuleStrategy->setBackwardSteps(backwardSteps);
    }
    else if (name==QString("Trigger-buy at 0.3%"))
    {
        double filterRule=0.3;
        QString description=QString("Triggers a limit buy order at 0.3% momentum percentage increase");
        trader::Strategy::FrequencyLevel frequencyLevel=trader::Strategy::FrequencyLevel::HIGH;
        filterRuleStrategy=new FilterRuleStrategy(QString("Trigger-buy at 0.3%"),description,frequencyLevel);
        filterRuleStrategy->setFilterRule(filterRule);
        filterRuleStrategy->setBackwardSteps(backwardSteps);
    }
    else if (name==QString("Trigger-buy at 0.4%"))
    {
        double filterRule=0.4;
        QString description=QString("Triggers a limit buy order at 0.4% momentum percentage increase");
        trader::Strategy::FrequencyLevel frequencyLevel=trader::Strategy::FrequencyLevel::HIGH;
        filterRuleStrategy=new FilterRuleStrategy(QString("Trigger-buy at 0.4%"),description,frequencyLevel);
        filterRuleStrategy->setFilterRule(filterRule);
        filterRuleStrategy->setBackwardSteps(backwardSteps);
    }
    else if (name==QString("Trigger-buy at 0.5%"))
    {
        double filterRule=0.5;
        QString description=QString("Triggers a limit buy order at 0.5% momentum percentage increase");
        trader::Strategy::FrequencyLevel frequencyLevel=trader::Strategy::FrequencyLevel::HIGH;
        filterRuleStrategy=new FilterRuleStrategy(QString("Trigger-buy at 0.5%"),description,frequencyLevel);
        filterRuleStrategy->setFilterRule(filterRule);
        filterRuleStrategy->setBackwardSteps(backwardSteps);
    }
    else if (name==QString("Trigger-buy at 0.6%"))
    {
        double filterRule=0.6;
        QString description=QString("Triggers a limit buy order at 0.6% momentum percentage increase");
        trader::Strategy::FrequencyLevel frequencyLevel=trader::Strategy::FrequencyLevel::HIGH;
        filterRuleStrategy=new FilterRuleStrategy(QString("Trigger-buy at 0.6%"),description,frequencyLevel);
        filterRuleStrategy->setFilterRule(filterRule);
        filterRuleStrategy->setBackwardSteps(backwardSteps);
    }
    else if (name==QString("Trigger-buy at 0.7%"))
    {
        double filterRule=0.7;
        QString description=QString("Triggers a limit buy order at 0.7% momentum percentage increase");
        trader::Strategy::FrequencyLevel frequencyLevel=trader::Strategy::FrequencyLevel::HIGH;
        filterRuleStrategy=new FilterRuleStrategy(QString("Trigger-buy at 0.7%"),description,frequencyLevel);
        filterRuleStrategy->setFilterRule(filterRule);
        filterRuleStrategy->setBackwardSteps(backwardSteps);
    }
    else if (name==QString("Trigger-buy at 0.8%"))
    {
        double filterRule=0.8;
        QString description=QString("Triggers a limit buy order at 0.8% momentum percentage increase");
        trader::Strategy::FrequencyLevel frequencyLevel=trader::Strategy::FrequencyLevel::HIGH;
        filterRuleStrategy=new FilterRuleStrategy(QString("Trigger-buy at 0.8%"),description,frequencyLevel);
        filterRuleStrategy->setFilterRule(filterRule);
        filterRuleStrategy->setBackwardSteps(backwardSteps);
    }
    else if (name==QString("Trigger-buy at 0.9%"))
    {
        double filterRule=0.9;
        QString description=QString("Triggers a limit buy order at 0.9% momentum percentage increase");
        trader::Strategy::FrequencyLevel frequencyLevel=trader::Strategy::FrequencyLevel::HIGH;
        filterRuleStrategy=new FilterRuleStrategy(QString("Trigger-buy at 0.9%"),description,frequencyLevel);
        filterRuleStrategy->setFilterRule(filterRule);
        filterRuleStrategy->setBackwardSteps(backwardSteps);
    }

    return filterRuleStrategy;
}


trader::Strategy *FilterRuleStrategyFactory::createFilterRuleStrategy(double filterRule,int backwardSteps)
{
    FilterRuleStrategy *filterRuleStrategy=nullptr;
    QString name=QString("Trigger-buy at ")+QString::number(filterRule)+QString("%");
    QString description=QString("Triggers a limit buy order at ")+QString::number(filterRule)+QString("% momentum percentage increase");
    trader::Strategy::FrequencyLevel frequencyLevel=trader::Strategy::FrequencyLevel::NONE;
    if (filterRule<=3.0) { frequencyLevel=trader::Strategy::FrequencyLevel::HIGH; }
    if (filterRule>3.0 && filterRule<=5.0) { frequencyLevel=trader::Strategy::FrequencyLevel::MEDIUM; }
    if (filterRule>5.0){ frequencyLevel=trader::Strategy::FrequencyLevel::LOW; }
    filterRuleStrategy=new FilterRuleStrategy(name,description,frequencyLevel);
    filterRuleStrategy->setFilterRule(filterRule);
    filterRuleStrategy->setBackwardSteps(backwardSteps);
    return filterRuleStrategy;
}


QString FilterRuleStrategyFactory::translateFilterRuleStrategyToSourceLanguageceLanguage(QString name)
{
    QString translated_strategy_source_language=QString();

    if (name==tr("Trigger-buy at 0.01%"))
    {
        translated_strategy_source_language=QString("Trigger-buy at 0.01%");
    }
    else if (name==tr("Trigger-buy at 0.02%"))
    {
        translated_strategy_source_language=QString("Trigger-buy at 0.02%");
    }
    else if (name==tr("Trigger-buy at 0.03%"))
    {
        translated_strategy_source_language=QString("Trigger-buy at 0.03%");
    }
    else if (name==tr("Trigger-buy at 0.04%"))
    {
        translated_strategy_source_language=QString("Trigger-buy at 0.04%");
    }
    else if (name==tr("Trigger-buy at 0.05%"))
    {
        translated_strategy_source_language=QString("Trigger-buy at 0.05%");
    }
    else if (name==tr("Trigger-buy at 0.06%"))
    {
        translated_strategy_source_language=QString("Trigger-buy at 0.06%");
    }
    else if (name==tr("Trigger-buy at 0.07%"))
    {
        translated_strategy_source_language=QString("Trigger-buy at 0.07%");
    }
    else if (name==tr("Trigger-buy at 0.08%"))
    {
        translated_strategy_source_language=QString("Trigger-buy at 0.08%");
    }
    else if (name==tr("Trigger-buy at 0.09%"))
    {
        translated_strategy_source_language=QString("Trigger-buy at 0.09%");
    }
    else if (name==tr("Trigger-buy at 0.1%"))
    {
        translated_strategy_source_language=QString("Trigger-buy at 0.1%");
    }
    else if (name==tr("Trigger-buy at 0.2%"))
    {
        translated_strategy_source_language=QString("Trigger-buy at 0.2%");
    }
    else if (name==tr("Trigger-buy at 0.3%"))
    {
        translated_strategy_source_language=QString("Trigger-buy at 0.3%");
    }
    else if (name==tr("Trigger-buy at 0.4%"))
    {
        translated_strategy_source_language=QString("Trigger-buy at 0.4%");
    }
    else if (name==tr("Trigger-buy at 0.5%"))
    {
        translated_strategy_source_language=QString("Trigger-buy at 0.5%");
    }
    else if (name==tr("Trigger-buy at 0.6%"))
    {
        translated_strategy_source_language=QString("Trigger-buy at 0.6%");
    }
    else if (name==tr("Trigger-buy at 0.7%"))
    {
        translated_strategy_source_language=QString("Trigger-buy at 0.7%");
    }
    else if (name==tr("Trigger-buy at 0.8%"))
    {
        translated_strategy_source_language=QString("Trigger-buy at 0.8%");
    }
    else if (name==tr("Trigger-buy at 0.9%"))
    {
        translated_strategy_source_language=QString("Trigger-buy at 0.9%");
    }

    return translated_strategy_source_language;
}
