#-------------------------------------------------
#
# Project created by QtCreator 2019-07-21T01:53:26
#
#-------------------------------------------------

QT += core gui network printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets charts

TARGET = BittrexBot
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++17

QMAKE_CXXFLAGS += -fopenmp
QMAKE_LFLAGS +=  -fopenmp

TRANSLATIONS = \
    translations/bittrexbot_english.ts \
    translations//bittrexbot_chinese.ts \
    translations/bittrexbot_hindi.ts \
    translations/bittrexbot_spanish.ts \
    translations/bittrexbot_arabic.ts \
    translations/bittrexbot_portuguese.ts \
    translations/bittrexbot_russian.ts \
    translations/bittrexbot_japanese.ts \
    translations/bittrexbot_malay.ts \
    translations/bittrexbot_german.ts \
    translations/bittrexbot_javanese.ts \
    translations/bittrexbot_korean.ts \
    translations/bittrexbot_italian.ts \

SOURCES += \
    applicationsettings.cpp \
    arimastrategy.cpp \
    armastrategy.cpp \
    arstrategy.cpp \
    aspectratiopixmaplabel.cpp \
    bittrexanalyser.cpp \
    bittrexasset.cpp \
    bittrexexecution.cpp \
    bittrexfilterer.cpp \
    bittrexsizer.cpp \
    closedordertable.cpp \
    closedpositiontable.cpp \
    dialogmanager.cpp \
    filterrulestrategy.cpp \
    filterrulestrategyfactory.cpp \
    hwesstrategy.cpp \
    languageselectordialog.cpp \
    main.cpp \
    mainwindow.cpp \
    marketlist.cpp \
    marketlistitem.cpp \
    markettable.cpp \
    mastrategy.cpp \
    openordertable.cpp \
    openpositiontable.cpp \
    orderbookpropertystrategyfactory.cpp \
    qcustomplot.cpp \
    risktree.cpp \
    sarimastrategy.cpp \
    sarimaxstrategy.cpp \
    selectedmarkettable.cpp \
    sessionsummarychartview.cpp \
    sesstrategy.cpp \
    stoplossrisk.cpp \
    stoplossriskfactory.cpp \
    strategytree.cpp \
    systemtray.cpp \
    takeprofitrisk.cpp \
    takeprofitriskfactory.cpp \
    timeseriesstrategyfactory.cpp \
    tradingsessionstate.cpp \
    userinterfacelogger.cpp \
    varmastrategy.cpp \
    varmaxstrategy.cpp \
    varstrategy.cpp \

HEADERS += \
    applicationsettings.hpp \
    arimastrategy.hpp \
    armastrategy.hpp \
    arstrategy.hpp \
    aspectratiopixmaplabel.hpp \
    bittrexanalyser.hpp \
    bittrexasset.hpp \
    bittrexexecution.hpp \
    bittrexfilterer.hpp \
    bittrexsizer.hpp \
    closedordertable.hpp \
    closedpositiontable.hpp \
    dialogmanager.hpp \
    filterrulestrategy.hpp \
    filterrulestrategyfactory.hpp \
    hwesstrategy.hpp \
    languageselectordialog.hpp \
    mainwindow.hpp \
    marketlist.hpp \
    marketlistitem.hpp \
    markettable.hpp \
    mastrategy.hpp \
    openordertable.hpp \
    openpositiontable.hpp \
    orderbookpropertystrategyfactory.hpp \
    qcustomplot.hpp \
    qlistwidgetcomparableitem.hpp \
    qtablenumericitem.hpp \
    risktree.hpp \
    sarimastrategy.hpp \
    sarimaxstrategy.hpp \
    selectedmarkettable.hpp \
    sessionsummarychartview.hpp \
    sesstrategy.hpp \
    stoplossrisk.hpp \
    stoplossriskfactory.hpp \
    strategytree.hpp \
    systemtray.hpp \
    takeprofitrisk.hpp \
    takeprofitriskfactory.hpp \
    timeseriesstrategyfactory.hpp \
    tradingsessionstate.hpp \
    userinterfacelogger.hpp \
    varmastrategy.hpp \
    varmaxstrategy.hpp \
    varstrategy.hpp \

FORMS += \
    languageselectordialog.ui \
    mainwindow.ui \
    marketlistitem.ui \


# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target


RESOURCES += \
    resources.qrc


android {
#    ANDROID_EXTRA_LIBS = /home/p0int3r/Documents/C++/QtProjects/android_openssl/arm64/libcrypto_1_1.so /home/p0int3r/Documents/C++/QtProjects/android_openssl/arm64/libssl_1_1.so

#    INCLUDEPATH += $$PWD/../Boost-for-Android/build/out/arm64-v8a/include/boost-1_70
#    DEPENDPATH += $$PWD/../Boost-for-Android/build/out/arm64-v8a/include/boost-1_70

#    win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../build-cryptotrader-Android_for_armeabi_v7a_arm64_v8a_x86_x86_64_Clang_Qt_5_14_1_for_Android-Debug/bittrexapi/release/ -lbittrexapi
#    else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../build-cryptotrader-Android_for_armeabi_v7a_arm64_v8a_x86_x86_64_Clang_Qt_5_14_1_for_Android-Debug/bittrexapi/debug/ -lbittrexapi
#    else:unix: LIBS += -L$$PWD/../build-cryptotrader-Android_for_armeabi_v7a_arm64_v8a_x86_x86_64_Clang_Qt_5_14_1_for_Android-Debug/bittrexapi/ -lbittrexapi

#    INCLUDEPATH += $$PWD/../cryptotrader/bittrexapi
#    DEPENDPATH += $$PWD/../cryptotrader/bittrexapi

#    win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../build-cryptotrader-Android_for_armeabi_v7a_arm64_v8a_x86_x86_64_Clang_Qt_5_14_1_for_Android-Debug/bittrexapi/release/libbittrexapi.a
#    else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../build-cryptotrader-Android_for_armeabi_v7a_arm64_v8a_x86_x86_64_Clang_Qt_5_14_1_for_Android-Debug/bittrexapi/debug/libbittrexapi.a
#    else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../build-cryptotrader-Android_for_armeabi_v7a_arm64_v8a_x86_x86_64_Clang_Qt_5_14_1_for_Android-Debug/bittrexapi/release/bittrexapi.lib
#    else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../build-cryptotrader-Android_for_armeabi_v7a_arm64_v8a_x86_x86_64_Clang_Qt_5_14_1_for_Android-Debug/bittrexapi/debug/bittrexapi.lib
#    else:unix: PRE_TARGETDEPS += $$PWD/../build-cryptotrader-Android_for_armeabi_v7a_arm64_v8a_x86_x86_64_Clang_Qt_5_14_1_for_Android-Debug/bittrexapi/libbittrexapi.a

#    win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../build-cryptotrader-Android_for_armeabi_v7a_arm64_v8a_x86_x86_64_Clang_Qt_5_14_1_for_Android-Debug/trader/release/ -ltrader
#    else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../build-cryptotrader-Android_for_armeabi_v7a_arm64_v8a_x86_x86_64_Clang_Qt_5_14_1_for_Android-Debug/trader/debug/ -ltrader
#    else:unix: LIBS += -L$$PWD/../build-cryptotrader-Android_for_armeabi_v7a_arm64_v8a_x86_x86_64_Clang_Qt_5_14_1_for_Android-Debug/trader/ -ltrader

#    INCLUDEPATH += $$PWD/../cryptotrader/trader
#    DEPENDPATH += $$PWD/../cryptotrader/trader

#    win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../build-cryptotrader-Android_for_armeabi_v7a_arm64_v8a_x86_x86_64_Clang_Qt_5_14_1_for_Android-Debug/trader/release/libtrader.a
#    else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../build-cryptotrader-Android_for_armeabi_v7a_arm64_v8a_x86_x86_64_Clang_Qt_5_14_1_for_Android-Debug/trader/debug/libtrader.a
#    else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../build-cryptotrader-Android_for_armeabi_v7a_arm64_v8a_x86_x86_64_Clang_Qt_5_14_1_for_Android-Debug/trader/release/trader.lib
#    else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../build-cryptotrader-Android_for_armeabi_v7a_arm64_v8a_x86_x86_64_Clang_Qt_5_14_1_for_Android-Debug/trader/debug/trader.lib
#    else:unix: PRE_TARGETDEPS += $$PWD/../build-cryptotrader-Android_for_armeabi_v7a_arm64_v8a_x86_x86_64_Clang_Qt_5_14_1_for_Android-Debug/trader/libtrader.a
} else {

    INCLUDEPATH += $$PWD/../../../../../../boost/include/boost-1_71
    DEPENDPATH += $$PWD/../../../../../../boost/include/boost-1_71

    win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../build-cryptotrader-Desktop_Qt_5_15_1_MinGW_64_bit-Debug/bittrexapi/release/ -lbittrexapi
    else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../build-cryptotrader-Desktop_Qt_5_15_1_MinGW_64_bit-Debug/bittrexapi/debug/ -lbittrexapi
    else:unix: LIBS += -L$$PWD/../build-cryptotrader-Desktop_Qt_5_15_1_MinGW_64_bit-Debug/bittrexapi/ -lbittrexapi

    INCLUDEPATH += $$PWD/../cryptotrader/bittrexapi
    DEPENDPATH += $$PWD/../cryptotrader/bittrexapi

    win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../build-cryptotrader-Desktop_Qt_5_15_1_MinGW_64_bit-Debug/bittrexapi/release/libbittrexapi.a
    else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../build-cryptotrader-Desktop_Qt_5_15_1_MinGW_64_bit-Debug/bittrexapi/debug/libbittrexapi.a
    else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../build-cryptotrader-Desktop_Qt_5_15_1_MinGW_64_bit-Debug/bittrexapi/release/bittrexapi.lib
    else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../build-cryptotrader-Desktop_Qt_5_15_1_MinGW_64_bit-Debug/bittrexapi/debug/bittrexapi.lib
    else:unix: PRE_TARGETDEPS += $$PWD/../build-cryptotrader-Desktop_Qt_5_15_1_MinGW_64_bit-Debug/bittrexapi/libbittrexapi.a

    win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../build-cryptotrader-Desktop_Qt_5_15_1_MinGW_64_bit-Debug/trader/release/ -ltrader
    else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../build-cryptotrader-Desktop_Qt_5_15_1_MinGW_64_bit-Debug/trader/debug/ -ltrader
    else:unix: LIBS += -L$$PWD/../build-cryptotrader-Desktop_Qt_5_15_1_MinGW_64_bit-Debug/trader/ -ltrader

    INCLUDEPATH += $$PWD/../cryptotrader/trader
    DEPENDPATH += $$PWD/../cryptotrader/trader

    win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../build-cryptotrader-Desktop_Qt_5_15_1_MinGW_64_bit-Debug/trader/release/libtrader.a
    else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../build-cryptotrader-Desktop_Qt_5_15_1_MinGW_64_bit-Debug/trader/debug/libtrader.a
    else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../build-cryptotrader-Desktop_Qt_5_15_1_MinGW_64_bit-Debug/trader/release/trader.lib
    else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../build-cryptotrader-Desktop_Qt_5_15_1_MinGW_64_bit-Debug/trader/debug/trader.lib
    else:unix: PRE_TARGETDEPS += $$PWD/../build-cryptotrader-Desktop_Qt_5_15_1_MinGW_64_bit-Debug/trader/libtrader.a
}

