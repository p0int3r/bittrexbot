#include "closedordertable.hpp"
#include "qtablenumericitem.hpp"


ClosedOrderTable::ClosedOrderTable(QWidget *parent): QTableWidget(parent)
{
    this->setMap(new QMap<QString,QList<QTableWidgetItem *>>());
}

ClosedOrderTable::~ClosedOrderTable() { delete this->map; }

void ClosedOrderTable::initializeSignalsAndSlots()
{
    // TODO: implement.
}

ClosedOrderTable::ColorMode ClosedOrderTable::getColorMode() const { return this->colorMode; }

void ClosedOrderTable::setColorMode(const ClosedOrderTable::ColorMode &value) { this->colorMode=value; }

QMap<QString, QList<QTableWidgetItem *> > *ClosedOrderTable::getMap() const { return this->map; }

void ClosedOrderTable::setMap(QMap<QString, QList<QTableWidgetItem *> > *value) { this->map=value; }


void ClosedOrderTable::changeToLightMode()
{
    this->setColorMode(ClosedOrderTable::ColorMode::LIGHT);
    QMap<QString,QList<QTableWidgetItem *>>::iterator iterator;
    for (iterator=this->getMap()->begin();iterator!=this->getMap()->end();iterator++)
    {
        QList<QTableWidgetItem *> tableWidgetList=iterator.value();
        QTableWidgetItem *sideItem=tableWidgetList.at(3);
        QTableWidgetItem *priceItem=tableWidgetList.at(5);
        QTableWidgetItem *quantityItem=tableWidgetList.at(6);
        QTableWidgetItem *filledItem=tableWidgetList.at(7);
        if (sideItem->text()==QString("BUY")) { sideItem->setForeground(QColor("green")); }
        if (sideItem->text()==QString("SELL")) { sideItem->setForeground(QColor("red")); }
        priceItem->setForeground(QColor("purple"));
        quantityItem->setForeground(QColor(0,85,127));
        filledItem->setForeground(QColor("SaddleBrown"));
    }
}


void ClosedOrderTable::changeToDarkMode()
{
    this->setColorMode(ClosedOrderTable::ColorMode::DARK);
    QMap<QString,QList<QTableWidgetItem *>>::iterator iterator;
    for (iterator=this->getMap()->begin();iterator!=this->getMap()->end();iterator++)
    {
        QList<QTableWidgetItem *> tableWidgetList=iterator.value();
        QTableWidgetItem *sideItem=tableWidgetList.at(3);
        QTableWidgetItem *priceItem=tableWidgetList.at(5);
        QTableWidgetItem *quantityItem=tableWidgetList.at(6);
        QTableWidgetItem *filledItem=tableWidgetList.at(7);
        if (sideItem->text()==QString("BUY")) { sideItem->setForeground(QColor("lime")); }
        if (sideItem->text()==QString("SELL")) { sideItem->setForeground(QColor("orange")); }
        priceItem->setForeground(QColor("#ffddf4"));
        quantityItem->setForeground(QColor("cyan"));
        filledItem->setForeground(QColor("YellowGreen"));
    }
}


void ClosedOrderTable::insertTableItems(const QString &orderId,const trader::Order *order)
{
    int currentRow=0;
    this->insertRow(currentRow);
    QList<QTableWidgetItem *> tableWidgetList=QList<QTableWidgetItem *>();
    QTableWidgetItem *closedItem=new QTableWidgetItem();
    closedItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    QString closed=order->getClosed().toString();
    closedItem->setText(closed);
    closedItem->setToolTip(closed);
    this->setItem(currentRow,0,closedItem);

    QTableWidgetItem *openedItem=new QTableWidgetItem();
    openedItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    QString opened=order->getOpened().toString();
    openedItem->setText(opened);
    openedItem->setToolTip(opened);
    this->setItem(currentRow,1,openedItem);

    QTableWidgetItem *marketItem=new QTableWidgetItem();
    marketItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    QString market=order->getMarket();
    marketItem->setText(market);
    marketItem->setToolTip(market);
    this->setItem(currentRow,2,marketItem);

    QTableWidgetItem *sideItem=new QTableWidgetItem();
    sideItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    QString side=QString();
    QString foregroundColor=QString();
    if (order->getType()==trader::Order::Type::LIMIT_BUY)
    {
        side=QString("BUY");
        if (this->getColorMode()==ClosedOrderTable::ColorMode::LIGHT) { foregroundColor=QString("green"); }
        if (this->getColorMode()==ClosedOrderTable::ColorMode::DARK) { foregroundColor=QString("lime"); }
    }
    if (order->getType()==trader::Order::Type::LIMIT_SELL)
    {
        side=QString("SELL");
        if (this->getColorMode()==ClosedOrderTable::ColorMode::LIGHT) { foregroundColor=QString("red"); }
        if (this->getColorMode()==ClosedOrderTable::ColorMode::DARK) { foregroundColor=QString("orange"); }
    }
    sideItem->setForeground(QColor(foregroundColor));
    sideItem->setText(side);
    sideItem->setToolTip(side);
    this->setItem(currentRow,3,sideItem);

    QTableWidgetItem *typeItem=new QTableWidgetItem();
    typeItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    QString type=QString("Limit");
    typeItem->setText(type);
    typeItem->setToolTip(type);
    this->setItem(currentRow,4,typeItem);

    QTableNumericItem *priceItem=new QTableNumericItem();
    priceItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double price=order->getPrice();
    if (this->getColorMode()==ClosedOrderTable::ColorMode::LIGHT) { priceItem->setForeground(QColor("purple")); }
    if (this->getColorMode()==ClosedOrderTable::ColorMode::DARK) { priceItem->setForeground(QColor("#ffddf4")); }
    priceItem->setText(QString::number(price,'g'));
    priceItem->setToolTip(QString::number(price,'f',8));
    this->setItem(currentRow,5,priceItem);

    QTableNumericItem *quantityItem=new QTableNumericItem();
    quantityItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double quantity=order->getQuantity();
    if (this->getColorMode()==ClosedOrderTable::ColorMode::LIGHT) { quantityItem->setForeground(QColor(0,85,127)); }
    if (this->getColorMode()==ClosedOrderTable::ColorMode::DARK) { quantityItem->setForeground(QColor("cyan")); }
    quantityItem->setText(QString::number(quantity,'g'));
    quantityItem->setToolTip(QString::number(quantity,'f',8));
    this->setItem(currentRow,6,quantityItem);

    QTableNumericItem *filledItem=new QTableNumericItem();
    filledItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double filled=100.0; filledItem->setText(QString::number(filled,'f',3));
    filledItem->setToolTip(QString::number(filled,'f',8));
    if (this->getColorMode()==ClosedOrderTable::ColorMode::LIGHT) { filledItem->setForeground(QColor("SaddleBrown")); }
    if (this->getColorMode()==ClosedOrderTable::ColorMode::DARK) { filledItem->setForeground(QColor("YellowGreen")); }
    this->setItem(currentRow,7,filledItem);

    QTableNumericItem *estTotalItem=new QTableNumericItem();
    estTotalItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double commissionPaid=order->getData().value("commission").toDouble();
    double estTotal=(quantity*price)+commissionPaid;
    if (order->getType()==trader::Order::Type::LIMIT_BUY) { estTotal=-1.0*estTotal; }
    if (order->getType()==trader::Order::Type::LIMIT_SELL) { estTotal=1.0*estTotal; }
    estTotalItem->setText(QString::number(estTotal,'g'));
    estTotalItem->setToolTip(QString::number(estTotal,'f',8));
    this->setItem(currentRow,8,estTotalItem);

    tableWidgetList.insert(0,closedItem);
    tableWidgetList.insert(1,openedItem);
    tableWidgetList.insert(2,marketItem);
    tableWidgetList.insert(3,sideItem);
    tableWidgetList.insert(4,typeItem);
    tableWidgetList.insert(5,priceItem);
    tableWidgetList.insert(6,quantityItem);
    tableWidgetList.insert(7,filledItem);
    tableWidgetList.insert(8,estTotalItem);
    this->getMap()->insert(orderId,tableWidgetList);

    emit this->tableItemsInserted(orderId,order);
}

void ClosedOrderTable::cancelTableItems(const QString &orderId,const trader::Order *order)
{
    QList<QTableWidgetItem *> tableWidgetList=this->getMap()->value(orderId);
    if (tableWidgetList.length()==0) { return; }
    int current_row=tableWidgetList.at(0)->row();
    this->removeRow(current_row);
    this->getMap()->remove(orderId);
    emit this->tableItemsCanceled(orderId,order);
}

void ClosedOrderTable::removeTableItems(const QString &orderId,const trader::Order *order)
{
    QList<QTableWidgetItem *> tableWidgetList=this->getMap()->value(orderId);
    if (tableWidgetList.length()==0) { return; }
    int current_row=tableWidgetList.at(0)->row();
    this->removeRow(current_row);
    this->getMap()->remove(orderId);
    emit this->tableItemsRemoved(orderId,order);
}



void ClosedOrderTable::modifyTableItems(const QString &orderId,const trader::Order *order)
{
    QList<QTableWidgetItem *> tableWidgetList=this->getMap()->value(orderId);
    if (tableWidgetList.length()==0) {return; }
    QTableWidgetItem *filledItem=static_cast<QTableNumericItem *>(tableWidgetList.at(7));
    QJsonObject order_object=order->getData();
    double filled=100.000;
    filledItem->setText(QString::number(filled*100.0,'f',3));
    emit this->tableItemsRemoved(orderId,order);
}
