#ifndef STOPLOSSRISKFACTORY_HPP
#define STOPLOSSRISKFACTORY_HPP

#include <QObject>
#include <QString>
#include "risk.hpp"


class StopLossRiskFactory: public QObject
{
    Q_OBJECT

    public:
        explicit StopLossRiskFactory(QObject *parent=nullptr);
        ~StopLossRiskFactory();

    signals:

    public slots:
        trader::Risk *createStopLossRisk(double stopLoss);
        trader::Risk *createStopLossRisk(QString name);
        QString translateStopLossRiskToSourceLanguage(QString name);
};

#endif // STOPLOSSRISKFACTORY_HPP
