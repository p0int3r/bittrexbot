#ifndef SARIMAXSTRATEGY_HPP
#define SARIMAXSTRATEGY_HPP

#include <QObject>
#include <QString>
#include "strategy.hpp"


// TODO: implement the Seasonal Autoregressive Integrated Moving-Average with Exogenous Regressors.

class SARIMAXStrategy: public trader::Strategy
{
    Q_OBJECT

    public:
        SARIMAXStrategy();
        SARIMAXStrategy(QString name,QString description,
            trader::Strategy::FrequencyLevel frequencyLevel);
        ~SARIMAXStrategy();

    public slots:
        void preprocess(const QString &market,const QObject *object);
        void strategise(const QString &market,const QObject *object);
};

#endif // SARIMAXSTRATEGY_HPP
