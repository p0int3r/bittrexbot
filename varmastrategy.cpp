#include "varmastrategy.hpp"



VARMAStrategy::VARMAStrategy(): VARMAStrategy(QString(),QString(),trader::Strategy::FrequencyLevel::NONE) {}

VARMAStrategy::VARMAStrategy(QString name,QString description,trader::Strategy::FrequencyLevel frequencyLevel)
{
    this->setName(name);
    this->setDescription(description);
    this->setFrequencyLevel(frequencyLevel);
}


VARMAStrategy::~VARMAStrategy() {}

void VARMAStrategy::preprocess(const QString &market,const QObject *object)
{
    Q_UNUSED(market)
    Q_UNUSED(object)

    // NOTE: emit signal at the end.
    emit this->preprocessed(market,object);
}


void VARMAStrategy::strategise(const QString &market,const QObject *object)
{
    Q_UNUSED(market)
    Q_UNUSED(object)

    // NOTE: emit signal at the end.
    emit this->strategised(market,object);
}
