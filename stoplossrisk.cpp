#include "stoplossrisk.hpp"
#include "position.hpp"
#include "order.hpp"
#include <QDebug>

StopLossRisk::StopLossRisk(): StopLossRisk(QString(),QString(),trader::Risk::RiskIndicator::NONE) {}

StopLossRisk::StopLossRisk(QString name,QString description,trader::Risk::RiskIndicator riskIndicator)
{
    this->setName(name);
    this->setDescription(description);
    this->setRiskIndicator(riskIndicator);
}


StopLossRisk::~StopLossRisk() {}

double StopLossRisk::getStopLoss() const { return this->stopLoss; }

void StopLossRisk::setStopLoss(double value) { this->stopLoss=value; }

void StopLossRisk::preprocess(const QString &market,const QObject *object)
{
    trader::Position *position=const_cast<trader::Position *>(static_cast<const trader::Position *>(object));
    double unrealised_pnl=position->getUnrealisedPnL();
    double cost_basis=position->getCostBasis();
    double middle_price=position->getMiddlePrice();
    double quantity=position->getBuys();
    double commission=quantity*middle_price*0.0020;
    double stop_loss_percentage=(unrealised_pnl-commission)/cost_basis;
    bool detected_stop_loss=(stop_loss_percentage<=(-this->getStopLoss()));
    if (detected_stop_loss==true) { emit this->preprocessed(market,object); }
}

void StopLossRisk::assess(const QString &market,const QObject *object)
{
    const trader::Position *position=static_cast<const trader::Position *>(object);
    double middle_price=position->getMiddlePrice();
    double volume=position->getQuantity();
    trader::Order *order=new trader::Order();
    order->setMarket(market);
    order->setQuantity(volume);
    order->setPrice(middle_price);
    order->setType(trader::Order::Type::LIMIT_SELL);
    order->setExchange(trader::Order::Exchange::BITTREX);
    order->setStatus(trader::Order::Status::SUGGESTED);
    emit this->assessed(market,order);
}
