#ifndef LANGUAGESELECTORDIALOG_HPP
#define LANGUAGESELECTORDIALOG_HPP

#include <QDialog>
#include <QHash>
#include <QTranslator>

namespace Ui { class LanguageSelectorDialog; }

class LanguageSelectorDialog: public QDialog
{
    Q_OBJECT

    private:
        QTranslator *activeTranslator=nullptr;
        QHash<QString,QTranslator *> translators;
        Ui::LanguageSelectorDialog *ui=nullptr;

    public:
        explicit LanguageSelectorDialog(QWidget *parent=nullptr);
        ~LanguageSelectorDialog();
        QTranslator *getActiveTranslator() const;
        void setActiveTranslator(QTranslator *value);

    signals:
        void languageChanged(const QString &language);

    public slots:
        void changeLanguage(const QString &language);

    private slots:
        void on_chineseCheckBox_clicked();
        void on_englishCheckBox_clicked();
        void on_hindiCheckBox_clicked();
        void on_spanishCheckBox_clicked();
        void on_arabicCheckBox_clicked();
        void on_portugueseCheckBox_clicked();
        void on_russianCheckBox_clicked();
        void on_japaneseCheckBox_clicked();
        void on_malayCheckBox_clicked();
        void on_germanCheckBox_clicked();
        void on_javaneseCheckBox_clicked();
        void on_koreanCheckBox_clicked();
        void on_italianCheckBox_clicked();
        void on_closeButton_clicked();
};

#endif // LANGUAGESELECTORDIALOG_HPP
