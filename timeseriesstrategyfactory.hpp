#ifndef TIMESERIESSTRATEGYFACTORY_HPP
#define TIMESERIESSTRATEGYFACTORY_HPP

#include <QObject>
#include <QString>
#include "strategy.hpp"

class TimeSeriesStrategyFactory: public QObject
{
    Q_OBJECT

    public:

        enum class TimeSeries
        {
            AR,
            MA,
            ARMA,
            ARIMA,
            SARIMA,
            SARIMAX,
            VAR,
            VARMA,
            VARMAX,
            SES,
            HWES
        }; Q_ENUM(TimeSeries)


    public:
        explicit TimeSeriesStrategyFactory(QObject *parent=nullptr);
        ~TimeSeriesStrategyFactory();

    signals:

    public slots:
        trader::Strategy *createTimeSeriesStrategy(TimeSeriesStrategyFactory::TimeSeries timeSeries);
        trader::Strategy *createTimeSeriesStrategy(QString name);
};

#endif // TIMESERIESSTRATEGYFACTORY_HPP
