#include "arimastrategy.hpp"


ARIMAStrategy::ARIMAStrategy(): ARIMAStrategy(QString(),QString(),trader::Strategy::FrequencyLevel::NONE) {}

ARIMAStrategy::ARIMAStrategy(QString name,QString description,trader::Strategy::FrequencyLevel frequencyLevel)
{
    this->setName(name);
    this->setDescription(description);
    this->setFrequencyLevel(frequencyLevel);
}


ARIMAStrategy::~ARIMAStrategy() {}

void ARIMAStrategy::preprocess(const QString &market,const QObject *object)
{
    Q_UNUSED(market)
    Q_UNUSED(object)

    // NOTE: emit signal at the end.
    emit this->preprocessed(market,object);
}



void ARIMAStrategy::strategise(const QString &market,const QObject *object)
{
    Q_UNUSED(market)
    Q_UNUSED(object)

    // NOTE: emit signal at the end.
    emit this->strategised(market,object);
}

