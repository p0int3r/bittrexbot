#include "marketlistitem.hpp"
#include "ui_marketlistitem.h"



MarketListItem::MarketListItem(QWidget *parent): QWidget(parent), ui(new Ui::MarketListItem)
{
    this->ui->setupUi(this);
    this->setMarketData(QJsonObject());
    this->setMarketSummaryData(QJsonObject());
    this->setMarketTickerData(QJsonObject());
    this->initSignalsAndSlots();
}

MarketListItem::~MarketListItem()
{
    delete this->ui;
}

void MarketListItem::initSignalsAndSlots()
{
    this->connect(this,&MarketListItem::marketNameChanged,this,[=](const QString &marketName) { this->ui->marketName->setText(marketName); });
    this->connect(this,&MarketListItem::percentChangeChanged,this,[=](const QString &percentChange) { this->ui->percentChange->setText(percentChange); });
    this->connect(this,&MarketListItem::quoteVolumeChanged,this,[=](const QString &quoteVolume) { this->ui->quoteVolume->setText(quoteVolume); });
    this->connect(this,&MarketListItem::lastPriceChanged,this,[=](const QString &lastPrice) { this->ui->lastPrice->setText(lastPrice); });
}

QString MarketListItem::getMarketName() const { return this->marketName; }

void MarketListItem::setMarketName(const QString &value)
{
    this->marketName=value;
    emit this->marketNameChanged(this->marketName);
}

QString MarketListItem::getPercentChange() const { return this->percentChange; }

void MarketListItem::setPercentChange(const QString &value)
{
    this->percentChange=value;
    this->percentChangeChanged(this->percentChange);
}

QString MarketListItem::getQuoteVolume() const { return this->quoteVolume; }

void MarketListItem::setQuoteVolume(const QString &value)
{
    this->quoteVolume=value;
    emit this->quoteVolumeChanged(this->quoteVolume);
}

QString MarketListItem::getLastPrice() const { return this->lastPrice; }

void MarketListItem::setLastPrice(const QString &value)
{
    this->lastPrice=value;
    emit this->lastPriceChanged(this->lastPrice);
}

QJsonObject MarketListItem::getMarketData() const { return this->marketData; }

void MarketListItem::setMarketData(const QJsonObject &value) { this->marketData=value; }

QJsonObject MarketListItem::getMarketSummaryData() const { return this->marketSummaryData; }

void MarketListItem::setMarketSummaryData(const QJsonObject &value) { this->marketSummaryData=value; }

QJsonObject MarketListItem::getMarketTickerData() const { return this->marketTickerData; }

void MarketListItem::setMarketTickerData(const QJsonObject &value) { this->marketTickerData=value; }

Ui::MarketListItem *MarketListItem::getUi() const { return this->ui; }

void MarketListItem::setUi(Ui::MarketListItem *value) { this->ui=value; }

void MarketListItem::changeColor(const QString &color) { this->ui->percentChange->setStyleSheet("color:"+color); }
