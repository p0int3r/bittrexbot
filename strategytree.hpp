#ifndef STRATEGYTREE_HPP
#define STRATEGYTREE_HPP

#include <QWidget>
#include <QString>
#include <QTreeWidget>

class StrategyTree: public QTreeWidget
{
    Q_OBJECT

    public:

        enum class StrategyCategory
        {
            TIME_SERIES,
            NEURAL_NETWORKS,
            ORDER_BOOK_PROPERTIES,
            MARKOV_MODELS,
            FILTER_RULE,
            NONE
        }; Q_ENUM(StrategyCategory)

    public:
        explicit StrategyTree(QWidget *parent=nullptr);
        ~StrategyTree();
        void initSignalsAndSlots();

    signals:
        void singleSelectionsChanged(QTreeWidgetItem *item,int column);
        void strategySelected(const StrategyTree::StrategyCategory &strategyCategory,const QString &strategy);
        void strategyDeselected(const StrategyTree::StrategyCategory &strategyCategory,const QString &strategy);

    public slots:
        void receiveItemChanged(QTreeWidgetItem *item,int column);
        void filterSingleSelections(QTreeWidgetItem *item,int column);
        void enableItems();
        void disableItems();
};

#endif // STRATEGYTREE_HPP
