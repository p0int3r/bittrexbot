#include "mastrategy.hpp"


MAStrategy::MAStrategy(): MAStrategy(QString(),QString(),trader::Strategy::FrequencyLevel::NONE) {}

MAStrategy::MAStrategy(QString name,QString description,trader::Strategy::FrequencyLevel frequencyLevel)
{
    this->setName(name);
    this->setDescription(description);
    this->setFrequencyLevel(frequencyLevel);
}

MAStrategy::~MAStrategy() {}

void MAStrategy::preprocess(const QString &market,const QObject *object)
{
    Q_UNUSED(market)
    Q_UNUSED(object)

    // NOTE: emit signal at the end.
    emit this->preprocessed(market,object);
}


void MAStrategy::strategise(const QString &market,const QObject *object)
{
    Q_UNUSED(market)
    Q_UNUSED(object)

    // NOTE: emit signal at the end.
    emit this->strategised(market,object);
}
