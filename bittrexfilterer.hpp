#ifndef BITTREXFILTERER_HPP
#define BITTREXFILTERER_HPP

#include <QObject>
#include "filterer.hpp"
#include "trader.hpp"

class BittrexFilterer: public trader::Filterer
{
    Q_OBJECT

    private:
        trader::Trader *trader=nullptr;

    public:
        BittrexFilterer();
        BittrexFilterer(trader::Trader *trader);
        ~BittrexFilterer() override;
        trader::Trader *getTrader() const;
        void setTrader(trader::Trader *value);

    signals:

    public slots:
        void filter(const QString &market,const QObject *object) override;
};

#endif // BITTREXFILTERER_HPP
