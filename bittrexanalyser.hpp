#ifndef BITTREXANALYSER_HPP
#define BITTREXANALYSER_HPP

#include <QObject>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include "analyser.hpp"
#include <boost/circular_buffer.hpp>

class BittrexAnalyser: public trader::Analyser
{
    Q_OBJECT
    Q_PROPERTY(int numberOpenBuyOrders READ getNumberOpenBuyOrders WRITE setNumberOpenBuyOrders NOTIFY numberOpenBuyOrdersChanged)
    Q_PROPERTY(int numberOpenSellOrders READ getNumberOpenSellOrders WRITE setNumberOpenSellOrders NOTIFY numberOpenSellOrdersChanged)
    Q_PROPERTY(boost::circular_buffer<double> prices READ getPrices WRITE setPrices NOTIFY pricesChanged)

    private:
        int numberOpenBuyOrders;
        int numberOpenSellOrders;
        boost::circular_buffer<double> prices;

    public:
        BittrexAnalyser();
        ~BittrexAnalyser();
        int getNumberOpenBuyOrders() const;
        void setNumberOpenBuyOrders(int value);
        int getNumberOpenSellOrders() const;
        void setNumberOpenSellOrders(int value);
        boost::circular_buffer<double> &getPrices();
        void setPrices(boost::circular_buffer<double> &value);

    signals:
        void numberOpenBuyOrdersChanged(const int &numberOpenBuyOrders);
        void numberOpenSellOrdersChanged(const int &numberOpenSellOrders);
        void pricesChanged(const boost::circular_buffer<double> &prices);
        void marketTickerProcessed(const QString &market,const BittrexAnalyser *analyser);
        void marketTradesProcessed(const QString &market,const BittrexAnalyser *analyser);
        void marketOrderBookProcessed(const QString &market,const BittrexAnalyser *analyser);
        void marketSummaryProcessed(const QString &market,const BittrexAnalyser *analyser);

    public slots:
        void processMarketTicker(const QString &market,const QJsonObject &data);
        void processMarketTrades(const QString &market,const QJsonArray &data);
        void processMarketOrderBook(const QString &market,const QJsonObject &data);
        void processMarketSummary(const QString &market,const QJsonObject &data);
};

#endif // BITTREXANALYSER_HPP
