#include "bittrexanalyser.hpp"
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/max.hpp>
#include <boost/accumulators/statistics/min.hpp>
#include <boost/accumulators/statistics/variance.hpp>
#include <limits>

BittrexAnalyser::BittrexAnalyser(): trader::Analyser()
{
    boost::circular_buffer<double>::capacity_type i;
    this->setNumberOpenBuyOrders(0.0);
    this->setNumberOpenSellOrders(0.0);
    this->getPrices().set_capacity(100000);

    for (i=0;i<this->getPrices().capacity();i++)
    {
        double infinity=std::numeric_limits<double>::infinity();
        this->getPrices().push_back(infinity);
    }
}

BittrexAnalyser::~BittrexAnalyser() {}


int BittrexAnalyser::getNumberOpenBuyOrders() const { return this->numberOpenBuyOrders; }

void BittrexAnalyser::setNumberOpenBuyOrders(int value)
{
    this->numberOpenBuyOrders=value;
    emit this->numberOpenBuyOrdersChanged(this->numberOpenBuyOrders);
}

int BittrexAnalyser::getNumberOpenSellOrders() const { return this->numberOpenSellOrders; }

void BittrexAnalyser::setNumberOpenSellOrders(int value)
{
    this->numberOpenSellOrders=value;
    emit this->numberOpenSellOrdersChanged(this->numberOpenSellOrders);
}

boost::circular_buffer<double> &BittrexAnalyser::getPrices() { return this->prices; }

void BittrexAnalyser::setPrices(boost::circular_buffer<double> &value)
{
    this->prices=value;
    emit this->pricesChanged(this->prices);
}

void BittrexAnalyser::processMarketTicker(const QString &market,const QJsonObject &data)
{
    double bid=data.value("bidRate").toString().toDouble();
    double ask=data.value("askRate").toString().toDouble();
    double last=data.value("lastTradeRate").toString().toDouble();
    double middle_price=(bid+ask)/2.0;
    double spread=ask-bid;
    double spread_percentage=spread/ask;

    this->getMicroEconomicAnalysis()->setLast(last);
    this->getMicroEconomicAnalysis()->setBid(bid);
    this->getMicroEconomicAnalysis()->setAsk(ask);
    this->getMicroEconomicAnalysis()->setMiddlePrice(middle_price);
    this->getMicroEconomicAnalysis()->setSpread(spread);
    this->getMicroEconomicAnalysis()->setSpreadPercentage(spread_percentage);
    this->getPrices().push_back(last);

    // NOTE: Trigger at the end of the method.
    emit this->marketTickerProcessed(market,this);
}


void BittrexAnalyser::processMarketTrades(const QString &market,const QJsonArray &data)
{
    using namespace boost::accumulators;
    accumulator_set<double,stats<tag::mean,tag::variance,tag::sum>> sell_quantity_accumulator;
    accumulator_set<double,stats<tag::mean,tag::variance,tag::sum>> base_sell_quantity_accumulator;
    accumulator_set<double,stats<tag::mean,tag::variance,tag::sum>> buy_quantity_accumulator;
    accumulator_set<double,stats<tag::mean,tag::variance,tag::sum>> base_buy_quantity_accumulator;
    accumulator_set<double,stats<tag::mean,tag::variance,tag::sum>> sell_price_accumulator;
    accumulator_set<double,stats<tag::mean,tag::variance,tag::sum>> buy_price_accumulator;

    for (QJsonArray::const_iterator i=data.begin();i!=data.end();i++)
    {
        double quantity=i->toObject().value("quantity").toString().toDouble();
        double price=i->toObject().value("rate").toString().toDouble();
        QString orderType=i->toObject().value("takerSide").toString();

        if (orderType==QString("BUY"))
        {
            buy_quantity_accumulator(quantity);
            base_buy_quantity_accumulator(quantity*price);
            buy_price_accumulator(price);
        }

        if (orderType==QString("SELL"))
        {
            sell_quantity_accumulator(quantity);
            base_sell_quantity_accumulator(quantity*price);
            sell_price_accumulator(price);
        }
    }

    double buy_volume=sum(buy_quantity_accumulator);
    double base_buy_volume=sum(base_buy_quantity_accumulator);
    double average_buy_volume=mean(buy_quantity_accumulator);
    double average_base_buy_volume=mean(base_buy_quantity_accumulator);

    double sell_volume=sum(sell_quantity_accumulator);
    double base_sell_volume=sum(base_sell_quantity_accumulator);
    double average_sell_volume=mean(sell_quantity_accumulator);
    double average_base_sell_volume=mean(base_sell_quantity_accumulator);

    this->getMicroEconomicAnalysis()->setBuyVolume(buy_volume);
    this->getMicroEconomicAnalysis()->setBaseBuyVolume(base_buy_volume);
    this->getMicroEconomicAnalysis()->setAverageBuyVolume(average_buy_volume);
    this->getMicroEconomicAnalysis()->setAverageBaseBuyVolume(average_base_buy_volume);

    this->getMicroEconomicAnalysis()->setSellVolume(sell_volume);
    this->getMicroEconomicAnalysis()->setBaseSellVolume(base_sell_volume);
    this->getMicroEconomicAnalysis()->setAverageSellVolume(average_sell_volume);
    this->getMicroEconomicAnalysis()->setAverageBaseSellVolume(average_base_sell_volume);

    // NOTE: Trigger at the end of the method.
    emit this->marketTradesProcessed(market,this);
}


void BittrexAnalyser::processMarketOrderBook(const QString &market,const QJsonObject &data)
{
    using namespace boost::accumulators;

    QJsonArray buys=data.value("bid").toArray();
    accumulator_set<double,stats<tag::mean,tag::variance,tag::sum>> bid_quantity_accumulator;
    accumulator_set<double,stats<tag::mean,tag::variance,tag::sum>> base_bid_quantity_accumulator;
    accumulator_set<double,stats<tag::mean,tag::variance>> bid_price_accumulator;

    for (QJsonArray::iterator i=buys.begin();i!=buys.end();i++)
    {
        double quantity=i->toObject().value("quantity").toString().toDouble();
        double price=i->toObject().value("rate").toString().toDouble();
        bid_quantity_accumulator(quantity);
        base_bid_quantity_accumulator(quantity*price);
        bid_price_accumulator(price);
    }

    double bid_volume=sum(bid_quantity_accumulator);
    double base_bid_volume=sum(base_bid_quantity_accumulator);
    double average_bid_volume=mean(bid_quantity_accumulator);
    double average_base_bid_volume=mean(base_bid_quantity_accumulator);

    this->getMicroEconomicAnalysis()->setBidVolume(bid_volume);
    this->getMicroEconomicAnalysis()->setBaseBidVolume(base_bid_volume);
    this->getMicroEconomicAnalysis()->setAverageBidVolume(average_bid_volume);
    this->getMicroEconomicAnalysis()->setAverageBaseBidVolume(average_base_bid_volume);

    QJsonArray sells=data.value("ask").toArray();
    accumulator_set<double,stats<tag::mean,tag::variance,tag::sum>> ask_quantity_accumulator;
    accumulator_set<double,stats<tag::mean,tag::variance,tag::sum>> base_ask_quantity_accumulator;
    accumulator_set<double,stats<tag::mean,tag::variance>> ask_price_accumulator;

    for (QJsonArray::iterator i=sells.begin();i!=sells.end();i++)
    {
        double quantity=i->toObject().value("quantity").toString().toDouble();
        double price=i->toObject().value("rate").toString().toDouble();
        ask_quantity_accumulator(quantity);
        base_ask_quantity_accumulator(quantity*price);
        ask_price_accumulator(price);
    }

    double ask_volume=sum(ask_quantity_accumulator);
    double base_ask_volume=sum(base_ask_quantity_accumulator);
    double average_ask_volume=mean(ask_quantity_accumulator);
    double average_base_ask_volume=mean(base_ask_quantity_accumulator);

    this->getMicroEconomicAnalysis()->setAskVolume(ask_volume);
    this->getMicroEconomicAnalysis()->setBaseAskVolume(base_ask_volume);
    this->getMicroEconomicAnalysis()->setAverageAskVolume(average_ask_volume);
    this->getMicroEconomicAnalysis()->setAverageBaseAskVolume(average_base_ask_volume);

    // NOTE: Trigger at the end of the method.
    emit this->marketOrderBookProcessed(market,this);
}


void BittrexAnalyser::processMarketSummary(const QString &market,const QJsonObject &data)
{
    double high=data.value("high").toString().toDouble();
    double low=data.value("low").toString().toDouble();
    double volume=data.value("volume").toString().toDouble();
    double base_volume=data.value("quoteVolume").toString().toDouble();
    double percent_change=data.value("percentChange").toString().toDouble();

    double previous_day_price=this->getMicroEconomicAnalysis()->getLast()/(percent_change+1);
    double momentum=this->getMicroEconomicAnalysis()->getLast()-previous_day_price;
    double momentum_percentage=(momentum/previous_day_price);

    double bid_volume=this->getMicroEconomicAnalysis()->getBidVolume();
    double ask_volume=this->getMicroEconomicAnalysis()->getAskVolume();
    double depth_imbalance=(bid_volume-ask_volume)/(bid_volume+ask_volume);

    double buy_volume=this->getMicroEconomicAnalysis()->getBuyVolume();
    double sell_volume=this->getMicroEconomicAnalysis()->getSellVolume();
    double volume_imbalance=(buy_volume-sell_volume)/(buy_volume+sell_volume);

    this->getMicroEconomicAnalysis()->setHigh(high);
    this->getMicroEconomicAnalysis()->setLow(low);
    this->getMicroEconomicAnalysis()->setVolume(volume);
    this->getMicroEconomicAnalysis()->setBaseVolume(base_volume);
    this->getMicroEconomicAnalysis()->setMomentum(momentum);
    this->getMicroEconomicAnalysis()->setMomentumPercentage(momentum_percentage);
    this->getMicroEconomicAnalysis()->setDepthImbalance(depth_imbalance);
    this->getMicroEconomicAnalysis()->setVolumeImbalance(volume_imbalance);

    // NOTE: Trigger at the end of the method.
    emit this->marketSummaryProcessed(market,this);
}
