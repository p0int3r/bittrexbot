#include <QtNetwork>
#include <QNetworkReply>
#include <QMessageAuthenticationCode>
#include <QChart>
#include <QPieSeries>
#include "mainwindow.hpp"
#include "marketlist.hpp"
#include "marketlistitem.hpp"
#include "markettable.hpp"
#include "selectedmarkettable.hpp"
#include "openpositiontable.hpp"
#include "qtablenumericitem.hpp"
#include "bittrexasset.hpp"
#include "bittrexanalyser.hpp"
#include "bittrexfilterer.hpp"
#include "bittrexexecution.hpp"
#include "bittrexsizer.hpp"
#include "qlistwidgetcomparableitem.hpp"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent): QMainWindow(parent), ui(new Ui::MainWindow())
{
    this->ui->setupUi(this);
    this->setDialogManager(new DialogManager(this));
    this->setSystemTray(new SystemTray(this));
    this->setTrader(new trader::Trader(this));
    this->setGlobalTimer(new QTimer(this));
    this->setLanguageSelectorDialog(new LanguageSelectorDialog(this));
    this->setApplicationSettings(new ApplicationSettings(this));
    this->setBittrexRestApiHandler(new bittrexapi::v3::BittrexRestApiHandler(this));
    this->setTimeSeriesStrategyFactory(new TimeSeriesStrategyFactory(this));
    this->setOrderBookPropertyStrategyFactory(new OrderBookPropertyStrategyFactory(this));
    this->setFilterRuleStrategyFactory(new FilterRuleStrategyFactory(this));
    this->setTakeProfitRiskFactory(new TakeProfitRiskFactory(this));
    this->setStopLossRiskFactory(new StopLossRiskFactory(this));
    this->setSessionStatus(MainWindow::SessionStatus::STOPPED);
    this->setTradingMode(MainWindow::TradingMode::PAPER_TRADING);
    this->setTradingSessionState(new TradingSessionState(this));
    this->getTradingSessionState()->setTrader(this->getTrader());
    this->getTradingSessionState()->setBittrexRestApiHandler(this->getBittrexRestApiHandler());
    this->initializeQActions();
    this->initializeQToolBars();
    this->initializeQListWidgets();
    this->initializeQTabWidgets();
    this->initializeQTableWidgets();
    this->initializeQTreeWidgets();
    this->initializeQChartViews();
    this->initializePortfolios();
    this->initializeFilterers();
    this->initializeSizers();
    this->initializeSignalsAndSlots();
    this->restoreSettings();
    this->getBittrexRestApiHandler()->queryMarkets();
    this->getBittrexRestApiHandler()->queryMarketSummaries();
    this->getBittrexRestApiHandler()->queryMarketTickers();
    this->getGlobalTimer()->start(10000);
    this->ui->settingsButton->hide();
    this->ui->analyticsButton->hide();
}

MainWindow::~MainWindow()
{
    delete this->ui;
    delete this->dialogManager;
    delete this->systemTray;
    delete this->languageSelectorDialog;
    delete this->trader;
    delete this->globalTimer;
    delete this->bittrexRestApiHandler;
    delete this->timeSeriesStrategyFactory;
    delete this->orderBookPropertyStrategyFactory;
    delete this->filterRuleStrategyFactory;
    delete this->takeProfitRiskFactory;
    delete this->stopLossRiskFactory;
    delete this->tradingSessionState;
}

MainWindow::SessionStatus MainWindow::getSessionStatus() const { return this->sessionStatus; }

void MainWindow::setSessionStatus(const MainWindow::SessionStatus &value) { this->sessionStatus=value; }

MainWindow::ColorMode MainWindow::getColorMode() const { return this->colorMode; }

void MainWindow::setColorMode(const MainWindow::ColorMode &value) { this->colorMode=value; }

MainWindow::TradingMode MainWindow::getTradingMode() const { return this->tradingMode; }

void MainWindow::setTradingMode(const MainWindow::TradingMode &value) { this->tradingMode=value; }

trader::Trader *MainWindow::getTrader() const { return this->trader; }

void MainWindow::setTrader(trader::Trader *value) { this->trader=value; }

bittrexapi::v3::BittrexRestApiHandler *MainWindow::getBittrexRestApiHandler() const { return this->bittrexRestApiHandler; }

void MainWindow::setBittrexRestApiHandler(bittrexapi::v3::BittrexRestApiHandler *value) { this->bittrexRestApiHandler=value; }

QTimer *MainWindow::getGlobalTimer() const { return this->globalTimer; }

void MainWindow::setGlobalTimer(QTimer *value) { this->globalTimer=value; }

TimeSeriesStrategyFactory *MainWindow::getTimeSeriesStrategyFactory() const { return this->timeSeriesStrategyFactory; }

void MainWindow::setTimeSeriesStrategyFactory(TimeSeriesStrategyFactory *value) { this->timeSeriesStrategyFactory=value; }

OrderBookPropertyStrategyFactory *MainWindow::getOrderBookPropertyStrategyFactory() const { return this->orderBookPropertyStrategyFactory; }

void MainWindow::setOrderBookPropertyStrategyFactory(OrderBookPropertyStrategyFactory *value) { this->orderBookPropertyStrategyFactory=value; }

FilterRuleStrategyFactory *MainWindow::getFilterRuleStrategyFactory() const { return this->filterRuleStrategyFactory; }

void MainWindow::setFilterRuleStrategyFactory(FilterRuleStrategyFactory *value) { this->filterRuleStrategyFactory=value; }

TakeProfitRiskFactory *MainWindow::getTakeProfitRiskFactory() const { return this->takeProfitRiskFactory; }

void MainWindow::setTakeProfitRiskFactory(TakeProfitRiskFactory *value) { this->takeProfitRiskFactory=value; }

StopLossRiskFactory *MainWindow::getStopLossRiskFactory() const { return this->stopLossRiskFactory; }

void MainWindow::setStopLossRiskFactory(StopLossRiskFactory *value) { this->stopLossRiskFactory=value; }

TradingSessionState *MainWindow::getTradingSessionState() const { return this->tradingSessionState; }

void MainWindow::setTradingSessionState(TradingSessionState *value) { this->tradingSessionState=value; }

DialogManager *MainWindow::getDialogManager() const { return this->dialogManager; }

void MainWindow::setDialogManager(DialogManager *value) { this->dialogManager=value; }

SystemTray *MainWindow::getSystemTray() const { return this->systemTray; }

void MainWindow::setSystemTray(SystemTray *value) { this->systemTray=value; }

LanguageSelectorDialog *MainWindow::getLanguageSelectorDialog() const { return this->languageSelectorDialog; }

void MainWindow::setLanguageSelectorDialog(LanguageSelectorDialog *value) { this->languageSelectorDialog=value; }

Ui::MainWindow *MainWindow::getUi() const { return this->ui; }

void MainWindow::setUi(Ui::MainWindow *value) { this->ui=value; }

ApplicationSettings *MainWindow::getApplicationSettings() const { return this->applicationSettings; }

void MainWindow::setApplicationSettings(ApplicationSettings *value) { this->applicationSettings=value; }

void MainWindow::initializeSignalsAndSlots()
{
    if (QSystemTrayIcon::isSystemTrayAvailable()==true)
    {
        this->connect(this->systemTray,&SystemTray::signalIconActivated,this,&QMainWindow::show);
        this->connect(this->systemTray,&SystemTray::signalShow,this,&QMainWindow::show);
        this->connect(this->systemTray,&SystemTray::signalQuit,this,&QMainWindow::close);
    }
    else
    {
        QList<QAction *> actions=this->ui->mainToolBar->actions();
        actions.at(7)->setEnabled(false);
    }

    trader::Portfolio *usd_portfolio=this->getTrader()->getPortfolioManager()->getPortfolio("USD-Portfolio");
    trader::Portfolio *btc_portfolio=this->getTrader()->getPortfolioManager()->getPortfolio("BTC-Portfolio");
    trader::Portfolio *eth_portfolio=this->getTrader()->getPortfolioManager()->getPortfolio("ETH-Portfolio");
    trader::Portfolio *usdt_portfolio=this->getTrader()->getPortfolioManager()->getPortfolio("USDT-Portfolio");
    trader::Portfolio *eur_portfolio=this->getTrader()->getPortfolioManager()->getPortfolio("EUR-Portfolio");

    this->connect(this->languageSelectorDialog,&LanguageSelectorDialog::languageChanged,this->applicationSettings,&ApplicationSettings::setLanguage);

    this->connect(this->ui->selectedUsdMarketsCapital,QOverload<double>::of(&QDoubleSpinBox::valueChanged),usd_portfolio,&trader::Portfolio::setInitialCapital);
    this->connect(this->ui->selectedBtcMarketsCapital,QOverload<double>::of(&QDoubleSpinBox::valueChanged),btc_portfolio,&trader::Portfolio::setInitialCapital);
    this->connect(this->ui->selectedEthMarketsCapital,QOverload<double>::of(&QDoubleSpinBox::valueChanged),eth_portfolio,&trader::Portfolio::setInitialCapital);
    this->connect(this->ui->selectedUsdtMarketsCapital,QOverload<double>::of(&QDoubleSpinBox::valueChanged),usdt_portfolio,&trader::Portfolio::setInitialCapital);
    this->connect(this->ui->selectedEurMarketsCapital,QOverload<double>::of(&QDoubleSpinBox::valueChanged),eur_portfolio,&trader::Portfolio::setInitialCapital);

    this->connect(usd_portfolio,&trader::Portfolio::unrealisedPnLChanged,this,&MainWindow::modifyUsdPortfolioUnrealisedPnL);
    this->connect(usd_portfolio,&trader::Portfolio::realisedPnLChanged,this,&MainWindow::modifyUsdPortfolioRealisedPnL);
    this->connect(usd_portfolio,&trader::Portfolio::initialCapitalChanged,this,&MainWindow::modifyUsdPortfolioInitialCapital);
    this->connect(usd_portfolio,&trader::Portfolio::currentCapitalChanged,this,&MainWindow::modifyUsdPortfolioCurrentCapital);
    this->connect(usd_portfolio,&trader::Portfolio::equityChanged,this,&MainWindow::modifyUsdPortfolioEquity);
    this->connect(usd_portfolio,&trader::Portfolio::returnOnInvestmentChanged,this,&MainWindow::modifyUsdPortfolioSessionSummaryReturnOnInvestment);

    this->connect(usd_portfolio,&trader::Portfolio::positionAdded,this->ui->usdOpenPositionsTable,&OpenPositionTable::insertTableItems);
    this->connect(usd_portfolio,&trader::Portfolio::positionUpdated,this->ui->usdOpenPositionsTable,&OpenPositionTable::modifyTableItems);
    this->connect(usd_portfolio,&trader::Portfolio::positionClosed,this->ui->usdOpenPositionsTable,&OpenPositionTable::removeTableItems);
    this->connect(usd_portfolio,&trader::Portfolio::positionClosed,this->ui->usdClosedPositionsTable,&ClosedPositionTable::insertTableItems);
    this->connect(usd_portfolio,&trader::Portfolio::positionClosed,this,&MainWindow::modifyTradingRound);

    this->connect(btc_portfolio,&trader::Portfolio::unrealisedPnLChanged,this,&MainWindow::modifyBtcPortfolioUnrealisedPnL);
    this->connect(btc_portfolio,&trader::Portfolio::realisedPnLChanged,this,&MainWindow::modifyBtcPortfolioRealisedPnL);
    this->connect(btc_portfolio,&trader::Portfolio::initialCapitalChanged,this,&MainWindow::modifyBtcPortfolioInitialCapital);
    this->connect(btc_portfolio,&trader::Portfolio::currentCapitalChanged,this,&MainWindow::modifyBtcPortfolioCurrentCapital);
    this->connect(btc_portfolio,&trader::Portfolio::equityChanged,this,&MainWindow::modifyBtcPortfolioEquity);
    this->connect(btc_portfolio,&trader::Portfolio::returnOnInvestmentChanged,this,&MainWindow::modifyBtcPortfolioSessionSummaryReturnOnInvestment);

    this->connect(btc_portfolio,&trader::Portfolio::positionAdded,this->ui->btcOpenPositionsTable,&OpenPositionTable::insertTableItems);
    this->connect(btc_portfolio,&trader::Portfolio::positionUpdated,this->ui->btcOpenPositionsTable,&OpenPositionTable::modifyTableItems);
    this->connect(btc_portfolio,&trader::Portfolio::positionClosed,this->ui->btcOpenPositionsTable,&OpenPositionTable::removeTableItems);
    this->connect(btc_portfolio,&trader::Portfolio::positionClosed,this->ui->btcClosedPositionsTable,&ClosedPositionTable::insertTableItems);
    this->connect(btc_portfolio,&trader::Portfolio::positionClosed,this,&MainWindow::modifyTradingRound);

    this->connect(eth_portfolio,&trader::Portfolio::unrealisedPnLChanged,this,&MainWindow::modifyEthPortfolioUnrealisedPnL);
    this->connect(eth_portfolio,&trader::Portfolio::realisedPnLChanged,this,&MainWindow::modifyEthPortfolioRealisedPnL);
    this->connect(eth_portfolio,&trader::Portfolio::initialCapitalChanged,this,&MainWindow::modifyEthPortfolioInitialCapital);
    this->connect(eth_portfolio,&trader::Portfolio::currentCapitalChanged,this,&MainWindow::modifyEthPortfolioCurrentCapital);
    this->connect(eth_portfolio,&trader::Portfolio::equityChanged,this,&MainWindow::modifyEthPortfolioEquity);
    this->connect(eth_portfolio,&trader::Portfolio::returnOnInvestmentChanged,this,&MainWindow::modifyEthPortfolioSessionSummaryReturnOnInvestment);

    this->connect(eth_portfolio,&trader::Portfolio::positionAdded,this->ui->ethOpenPositionsTable,&OpenPositionTable::insertTableItems);
    this->connect(eth_portfolio,&trader::Portfolio::positionUpdated,this->ui->ethOpenPositionsTable,&OpenPositionTable::modifyTableItems);
    this->connect(eth_portfolio,&trader::Portfolio::positionClosed,this->ui->ethOpenPositionsTable,&OpenPositionTable::removeTableItems);
    this->connect(eth_portfolio,&trader::Portfolio::positionClosed,this->ui->ethClosedPositionsTable,&ClosedPositionTable::insertTableItems);
    this->connect(eth_portfolio,&trader::Portfolio::positionClosed,this,&MainWindow::modifyTradingRound);

    this->connect(usdt_portfolio,&trader::Portfolio::unrealisedPnLChanged,this,&MainWindow::modifyUsdtPortfolioUnrealisedPnL);
    this->connect(usdt_portfolio,&trader::Portfolio::realisedPnLChanged,this,&MainWindow::modifyUsdtPortfolioRealisedPnL);
    this->connect(usdt_portfolio,&trader::Portfolio::initialCapitalChanged,this,&MainWindow::modifyUsdtPortfolioInitialCapital);
    this->connect(usdt_portfolio,&trader::Portfolio::currentCapitalChanged,this,&MainWindow::modifyUsdtPortfolioCurrentCapital);
    this->connect(usdt_portfolio,&trader::Portfolio::equityChanged,this,&MainWindow::modifyUsdtPortfolioEquity);
    this->connect(usdt_portfolio,&trader::Portfolio::returnOnInvestmentChanged,this,&MainWindow::modifyUsdtPortfolioSessionSummaryReturnOnInvestment);

    this->connect(usdt_portfolio,&trader::Portfolio::positionAdded,this->ui->usdtOpenPositionsTable,&OpenPositionTable::insertTableItems);
    this->connect(usdt_portfolio,&trader::Portfolio::positionUpdated,this->ui->usdtOpenPositionsTable,&OpenPositionTable::modifyTableItems);
    this->connect(usdt_portfolio,&trader::Portfolio::positionClosed,this->ui->usdtOpenPositionsTable,&OpenPositionTable::removeTableItems);
    this->connect(usdt_portfolio,&trader::Portfolio::positionClosed,this->ui->usdtClosedPositionsTable,&ClosedPositionTable::insertTableItems);
    this->connect(usdt_portfolio,&trader::Portfolio::positionClosed,this,&MainWindow::modifyTradingRound);

    this->connect(eur_portfolio,&trader::Portfolio::unrealisedPnLChanged,this,&MainWindow::modifyEurPortfolioUnrealisedPnL);
    this->connect(eur_portfolio,&trader::Portfolio::realisedPnLChanged,this,&MainWindow::modifyEurPortfolioRealisedPnL);
    this->connect(eur_portfolio,&trader::Portfolio::initialCapitalChanged,this,&MainWindow::modifyEurPortfolioInitialCapital);
    this->connect(eur_portfolio,&trader::Portfolio::currentCapitalChanged,this,&MainWindow::modifyEurPortfolioCurrentCapital);
    this->connect(eur_portfolio,&trader::Portfolio::equityChanged,this,&MainWindow::modifyEurPortfolioEquity);
    this->connect(eur_portfolio,&trader::Portfolio::returnOnInvestmentChanged,this,&MainWindow::modifyEurPortfolioSessionSummaryReturnOnInvestment);

    this->connect(eur_portfolio,&trader::Portfolio::positionAdded,this->ui->eurOpenPositionsTable,&OpenPositionTable::insertTableItems);
    this->connect(eur_portfolio,&trader::Portfolio::positionUpdated,this->ui->eurOpenPositionsTable,&OpenPositionTable::modifyTableItems);
    this->connect(eur_portfolio,&trader::Portfolio::positionClosed,this->ui->eurOpenPositionsTable,&OpenPositionTable::removeTableItems);
    this->connect(eur_portfolio,&trader::Portfolio::positionClosed,this->ui->eurClosedPositionsTable,&ClosedPositionTable::insertTableItems);
    this->connect(eur_portfolio,&trader::Portfolio::positionClosed,this,&MainWindow::modifyTradingRound);

    this->connect(this->globalTimer,&QTimer::timeout,this->bittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryMarketSummaries);
    this->connect(this->globalTimer,&QTimer::timeout,this->bittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryMarketTickers);
    this->connect(this->bittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryMarketsFinished,this,&MainWindow::marketsReceived);
    this->connect(this->bittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryMarketsFinished,this,&MainWindow::checkRestoration);
    this->connect(this->bittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryMarketTickersFinished,this,&MainWindow::marketTickersReceived);
    this->connect(this->bittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryMarketSummariesFinished,this,&MainWindow::marketSummariesReceived);
    this->connect(this->bittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryBalancesFinished,this,&MainWindow::balancesReceived);
    this->connect(this->bittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryMarketTradesFinished,this,&MainWindow::marketTradesReceived);
    this->connect(this->bittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryMarketOrderBookFinished,this,&MainWindow::marketOrderBookReceived);
    this->connect(this->bittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryMarketSummaryFinished,this,&MainWindow::marketSummaryReceived);
    this->connect(this->bittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryMarketTickerFinished,this,&MainWindow::marketTickerReceived);
    this->connect(this->bittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::placeOrderFinished,this,&MainWindow::placeOrderReceived);
    this->connect(this->bittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::cancelOrderFinished,this,&MainWindow::placeCancelOrderReceived);
    this->connect(this->bittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryOrderFinished,this,&MainWindow::queryOrderReceived);
    this->connect(this->bittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryMarketFinished,this,&MainWindow::marketReceived);

    this->connect(this->ui->strategiesTree,&StrategyTree::strategySelected,this,&MainWindow::addStrategySelection);
    this->connect(this->ui->strategiesTree,&StrategyTree::strategyDeselected,this,&MainWindow::removeStrategySelection);

    this->connect(this->ui->risksTree,&RiskTree::riskSelected,this,&MainWindow::addRiskSelection);
    this->connect(this->ui->risksTree,&RiskTree::riskDeselected,this,&MainWindow::removeRiskSelection);

    this->connect(this->ui->usdMarketsTable,&MarketTable::marketSelected,this->ui->selectedUsdMarketsTable,&SelectedMarketTable::insertMarket);
    this->connect(this->ui->selectedUsdMarketsTable,&SelectedMarketTable::marketInserted,this,&MainWindow::addMarketSelection);

    this->connect(this->ui->selectedUsdMarketsTable,&SelectedMarketTable::capitalPercentageSpecified,this,&MainWindow::applyCapitalPercentage);
    this->connect(this->ui->selectedUsdMarketsTable,&SelectedMarketTable::tradingRoundsSpecified,this,&MainWindow::applyTradingRounds);

    this->connect(this->ui->usdMarketsTable,&MarketTable::marketDeselected,this->ui->selectedUsdMarketsTable,&SelectedMarketTable::removeMarket);
    this->connect(this->ui->selectedUsdMarketsTable,&SelectedMarketTable::marketRemoved,this,&MainWindow::removeMarketSelection);

    this->connect(this->ui->btcMarketsTable,&MarketTable::marketSelected,this->ui->selectedBtcMarketsTable,&SelectedMarketTable::insertMarket);
    this->connect(this->ui->selectedBtcMarketsTable,&SelectedMarketTable::marketInserted,this,&MainWindow::addMarketSelection);

    this->connect(this->ui->selectedBtcMarketsTable,&SelectedMarketTable::capitalPercentageSpecified,this,&MainWindow::applyCapitalPercentage);
    this->connect(this->ui->selectedBtcMarketsTable,&SelectedMarketTable::tradingRoundsSpecified,this,&MainWindow::applyTradingRounds);

    this->connect(this->ui->btcMarketsTable,&MarketTable::marketDeselected,this->ui->selectedBtcMarketsTable,&SelectedMarketTable::removeMarket);
    this->connect(this->ui->selectedBtcMarketsTable,&SelectedMarketTable::marketRemoved,this,&MainWindow::removeMarketSelection);

    this->connect(this->ui->ethMarketsTable,&MarketTable::marketSelected,this->ui->selectedEthMarketsTable,&SelectedMarketTable::insertMarket);
    this->connect(this->ui->selectedEthMarketsTable,&SelectedMarketTable::marketInserted,this,&MainWindow::addMarketSelection);

    this->connect(this->ui->selectedEthMarketsTable,&SelectedMarketTable::capitalPercentageSpecified,this,&MainWindow::applyCapitalPercentage);
    this->connect(this->ui->selectedEthMarketsTable,&SelectedMarketTable::tradingRoundsSpecified,this,&MainWindow::applyTradingRounds);

    this->connect(this->ui->ethMarketsTable,&MarketTable::marketDeselected,this->ui->selectedEthMarketsTable,&SelectedMarketTable::removeMarket);
    this->connect(this->ui->selectedEthMarketsTable,&SelectedMarketTable::marketRemoved,this,&MainWindow::removeMarketSelection);

    this->connect(this->ui->usdtMarketsTable,&MarketTable::marketSelected,this->ui->selectedUsdtMarketsTable,&SelectedMarketTable::insertMarket);
    this->connect(this->ui->selectedUsdtMarketsTable,&SelectedMarketTable::marketInserted,this,&MainWindow::addMarketSelection);

    this->connect(this->ui->selectedUsdtMarketsTable,&SelectedMarketTable::capitalPercentageSpecified,this,&MainWindow::applyCapitalPercentage);
    this->connect(this->ui->selectedUsdtMarketsTable,&SelectedMarketTable::tradingRoundsSpecified,this,&MainWindow::applyTradingRounds);

    this->connect(this->ui->usdtMarketsTable,&MarketTable::marketDeselected,this->ui->selectedUsdtMarketsTable,&SelectedMarketTable::removeMarket);
    this->connect(this->ui->selectedUsdtMarketsTable,&SelectedMarketTable::marketRemoved,this,&MainWindow::removeMarketSelection);

    this->connect(this->ui->eurMarketsTable,&MarketTable::marketSelected,this->ui->selectedEurMarketsTable,&SelectedMarketTable::insertMarket);
    this->connect(this->ui->selectedEurMarketsTable,&SelectedMarketTable::marketInserted,this,&MainWindow::addMarketSelection);

    this->connect(this->ui->selectedEurMarketsTable,&SelectedMarketTable::capitalPercentageSpecified,this,&MainWindow::applyCapitalPercentage);
    this->connect(this->ui->selectedEurMarketsTable,&SelectedMarketTable::tradingRoundsSpecified,this,&MainWindow::applyTradingRounds);

    this->connect(this->ui->eurMarketsTable,&MarketTable::marketDeselected,this->ui->selectedEurMarketsTable,&SelectedMarketTable::removeMarket);
    this->connect(this->ui->selectedEurMarketsTable,&SelectedMarketTable::marketRemoved,this,&MainWindow::removeMarketSelection);

    this->connect(this->getTrader()->getOrderManager(),&trader::OrderManager::suggested,this,&MainWindow::executeBittrexExecution);
    this->connect(this->ui->openOrdersTable,&OpenOrderTable::tableItemsCanceled,this,&MainWindow::removeBittrexExecution);
    this->connect(this->ui->openOrdersTable,&OpenOrderTable::tableItemsCanceled,this->getTrader()->getOrderManager(),&trader::OrderManager::cancel);

    this->connect(this->getTrader()->getExecutionManager(),&trader::ExecutionManager::opened,this->getTrader()->getOrderManager(),&trader::OrderManager::open);
    this->connect(this->getTrader()->getExecutionManager(),&trader::ExecutionManager::closed,this->getTrader()->getOrderManager(),&trader::OrderManager::close);
    this->connect(this->getTrader()->getExecutionManager(),&trader::ExecutionManager::canceled,this->getTrader()->getOrderManager(),&trader::OrderManager::cancel);
    this->connect(this->getTrader()->getExecutionManager(),&trader::ExecutionManager::updated,this->getTrader()->getOrderManager(),&trader::OrderManager::update);

    this->connect(this->getTrader()->getOrderManager(),&trader::OrderManager::opened,this->ui->openOrdersTable,&OpenOrderTable::insertTableItems);
    this->connect(this->getTrader()->getOrderManager(),&trader::OrderManager::closed,this->ui->openOrdersTable,&OpenOrderTable::removeTableItems);
    this->connect(this->getTrader()->getOrderManager(),&trader::OrderManager::canceled,this->ui->openOrdersTable,&OpenOrderTable::cancelTableItems);
    this->connect(this->getTrader()->getOrderManager(),&trader::OrderManager::updated,this->ui->openOrdersTable,&OpenOrderTable::modifyTableItems);

    this->connect(this->getTrader()->getOrderManager(),&trader::OrderManager::closed,this,&MainWindow::modifyPortfolio);
    this->connect(this->getTrader()->getOrderManager(),&trader::OrderManager::closed,this->ui->closedOrdersTable,&ClosedOrderTable::insertTableItems);

    this->connect(this,&MainWindow::tradingSessionStarted,this->tradingSessionState,&TradingSessionState::save);
    this->connect(this,&MainWindow::tradingSessionPaused,this->tradingSessionState,&TradingSessionState::save);
    this->connect(this,&MainWindow::tradingSessionResumed,this->tradingSessionState,&TradingSessionState::save);
    this->connect(this,&MainWindow::tradingSessionStopped,this->tradingSessionState,&TradingSessionState::save);
    this->connect(this->getTrader()->getPortfolioManager(),&trader::PortfolioManager::portfolioAdded,this->tradingSessionState,&TradingSessionState::save);
    this->connect(this->getTrader()->getPortfolioManager(),&trader::PortfolioManager::portfolioRemoved,this->tradingSessionState,&TradingSessionState::save);
    this->connect(this->getTrader()->getAssetManager(),&trader::AssetManager::assetAdded,this->tradingSessionState,&TradingSessionState::save);
    this->connect(this->getTrader()->getAssetManager(),&trader::AssetManager::assetRemoved,this->tradingSessionState,&TradingSessionState::save);
    this->connect(this->getTrader()->getStrategyManager(),&trader::StrategyManager::strategyAdded,this->tradingSessionState,&TradingSessionState::save);
    this->connect(this->getTrader()->getStrategyManager(),&trader::StrategyManager::strategyRemoved,this->tradingSessionState,&TradingSessionState::save);
    this->connect(this->getTrader()->getRiskManager(),&trader::RiskManager::riskAdded,this->tradingSessionState,&TradingSessionState::save);
    this->connect(this->getTrader()->getRiskManager(),&trader::RiskManager::riskRemoved,this->tradingSessionState,&TradingSessionState::save);
    this->connect(this->getTrader()->getOrderManager(),&trader::OrderManager::opened,this->tradingSessionState,&TradingSessionState::save);
    this->connect(this->getTrader()->getOrderManager(),&trader::OrderManager::canceled,this->tradingSessionState,&TradingSessionState::save);
    this->connect(this->getTrader()->getOrderManager(),&trader::OrderManager::closed,this->tradingSessionState,&TradingSessionState::save);
}


void MainWindow::initializeLogger()
{
    this->ui->logger->document()->setMaximumBlockCount(1000);
    this->connect(this->bittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryMarketsFinished,this->ui->logger,&UserInterfaceLogger::logQueryMarketsFinishedMessage);
    this->connect(this->bittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryCurrenciesFinished,this->ui->logger,&UserInterfaceLogger::logQueryCurrenciesFinishedMessage);
    this->connect(this->bittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryMarketTickerFinished,this->ui->logger,&UserInterfaceLogger::logQueryMarketTickerFinishedMessage);
    this->connect(this->bittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryMarketSummariesFinished,this->ui->logger,&UserInterfaceLogger::logQueryMarketSummariesFinishedMessage);
    this->connect(this->bittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryMarketSummaryFinished,this->ui->logger,&UserInterfaceLogger::logQueryMarketSummaryFinishedMessage);
    this->connect(this->bittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryMarketOrderBookFinished,this->ui->logger,&UserInterfaceLogger::logQueryMarketOrderBookFinishedMessage);
    this->connect(this->bittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryMarketTradesFinished,this->ui->logger,&UserInterfaceLogger::logQueryMarketTradesFinishedMessage);
    this->connect(this->bittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::placeOrderFinished,this->ui->logger,&UserInterfaceLogger::logPlaceOrderFinishedMessage);
    this->connect(this->bittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::cancelOrderFinished,this->ui->logger,&UserInterfaceLogger::logCancelOrderFinishedMessage);
    this->connect(this->bittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryBalancesFinished,this->ui->logger,&UserInterfaceLogger::logQueryBalancesFinishedMessage);
    this->connect(this->bittrexRestApiHandler,&bittrexapi::v3::BittrexRestApiHandler::queryOrderFinished,this->ui->logger,&UserInterfaceLogger::logQueryOrderFinishedMessage);
}


void MainWindow::initializePortfolios()
{
    QString usd_portfolio_name=QString("USD-Portfolio"), usd_base_currency=QString("USD");
    QString btc_portfolio_name=QString("BTC-Portfolio"), btc_base_currency=QString("BTC");
    QString eth_portfolio_name=QString("ETH-Portfolio"), eth_base_currency=QString("ETH");
    QString usdt_portfolio_name=QString("USDT-Portfolio"), usdt_base_currency=QString("USDT");
    QString eur_portfolio_name=QString("EUR-Portfolio"), eur_base_currency=QString("EUR");

    trader::Portfolio *usd_portfolio=new trader::Portfolio(usd_portfolio_name,usd_base_currency,0.0);
    trader::Portfolio *btc_portfolio=new trader::Portfolio(btc_portfolio_name,btc_base_currency,0.0);
    trader::Portfolio *eth_portfolio=new trader::Portfolio(eth_portfolio_name,eth_base_currency,0.0);
    trader::Portfolio *usdt_portfolio=new trader::Portfolio(usdt_portfolio_name,usdt_base_currency,0.0);
    trader::Portfolio *eur_portfolio=new trader::Portfolio(eur_portfolio_name,eur_base_currency,0.0);

    this->getTrader()->getPortfolioManager()->addPortfolio(usd_portfolio_name,usd_portfolio);
    this->getTrader()->getPortfolioManager()->addPortfolio(btc_portfolio_name,btc_portfolio);
    this->getTrader()->getPortfolioManager()->addPortfolio(eth_portfolio_name,eth_portfolio);
    this->getTrader()->getPortfolioManager()->addPortfolio(usdt_portfolio_name,usdt_portfolio);
    this->getTrader()->getPortfolioManager()->addPortfolio(eur_portfolio_name,eur_portfolio);
}

void MainWindow::initializeFilterers()
{
    trader::Trader *trader=this->getTrader();
    QString filterer_name=QString("Bittrex Filterer");
    BittrexFilterer *bittrex_filterer=new BittrexFilterer(trader);
    trader::FiltererManager *filtererManager=this->getTrader()->getFiltererManager();
    bittrex_filterer->setName(filterer_name);
    bittrex_filterer->setDescription("An order filterer data structure.");
    filtererManager->addFilterer(filterer_name,bittrex_filterer);
}

void MainWindow::initializeSizers()
{
    trader::Trader *trader=this->getTrader();
    QString sizer_name=QString("Bittrex Sizer");
    BittrexSizer *bittrex_sizer=new BittrexSizer(trader);
    trader::SizerManager *sizerManager=this->getTrader()->getSizerManager();
    bittrex_sizer->setName(sizer_name);
    bittrex_sizer->setDescription("A sizer data structure");
    sizerManager->addSizer(sizer_name,bittrex_sizer);
}


void MainWindow::addMarketSelection(const QString &market)
{
    bittrexapi::v3::BittrexRestApiHandler *bittrexRestApiHandler=this->getBittrexRestApiHandler();
    BittrexAsset *bittrex_asset=new BittrexAsset(bittrexRestApiHandler);
    trader::AssetManager *assetManager=this->getTrader()->getAssetManager();
    trader::AnalyserManager *analyserManager=this->getTrader()->getAnalyserManager();
    bittrex_asset->setMarket(market);
    bittrex_asset->setFrequencyMilliseconds(20000);
    bool result=assetManager->addAsset(market,bittrex_asset);
    if (result==false) { delete bittrex_asset; return; }
    bittrex_asset->start();
    BittrexAnalyser *bittrex_analyser=new BittrexAnalyser();
    analyserManager->addAnalyser(market,bittrex_analyser);
}


void MainWindow::removeMarketSelection(const QString &market)
{
    trader::AssetManager *assetManager=this->getTrader()->getAssetManager();
    trader::AnalyserManager *analyserManager=this->getTrader()->getAnalyserManager();
    BittrexAsset *bittrex_asset=static_cast<BittrexAsset *>(assetManager->getAsset(market));
    bittrex_asset->stop();
    assetManager->removeAsset(market);
    analyserManager->removeAnalyser(market);
}


void MainWindow::addStrategySelection(const StrategyTree::StrategyCategory &strategyCategory, const QString &strategy)
{
    QString translated_strategy_source_language;
    trader::Strategy *selected_strategy=nullptr;
    trader::StrategyManager *strategyManager=this->getTrader()->getStrategyManager();
    FilterRuleStrategyFactory *filterRuleStrategyFactory=this->getFilterRuleStrategyFactory();

    if (strategyCategory==StrategyTree::StrategyCategory::FILTER_RULE)
    {
        translated_strategy_source_language=this->getFilterRuleStrategyFactory()->translateFilterRuleStrategyToSourceLanguageceLanguage(strategy);
        selected_strategy=filterRuleStrategyFactory->createFilterRuleStrategy(translated_strategy_source_language,100000);
    }

    strategyManager->addStrategy(translated_strategy_source_language,selected_strategy);
}


void MainWindow::removeStrategySelection(const StrategyTree::StrategyCategory &strategyCategory, const QString &strategy)
{
    QString translated_strategy_source_language;
    TradingSessionState::State trading_session_state=this->getTradingSessionState()->getState();
    if (trading_session_state==TradingSessionState::State::RESTORED) { return; }
    trader::StrategyManager *strategyManager=this->getTrader()->getStrategyManager();

    if (strategyCategory==StrategyTree::StrategyCategory::FILTER_RULE)
    {
        translated_strategy_source_language=this->getFilterRuleStrategyFactory()->translateFilterRuleStrategyToSourceLanguageceLanguage(strategy);
    }

    strategyManager->removeStrategy(translated_strategy_source_language);
}


void MainWindow::addRiskSelection(const RiskTree::RiskCategory &riskCategory, const QString &risk)
{
    QString translated_risk_source_language;
    trader::Risk *selected_risk=nullptr;
    trader::RiskManager *riskManager=this->getTrader()->getRiskManager();
    TakeProfitRiskFactory *takeProfitRiskFactory=this->getTakeProfitRiskFactory();
    StopLossRiskFactory *stopLossRiskFactory=this->getStopLossRiskFactory();

    if (riskCategory==RiskTree::RiskCategory::STOP_LOSS)
    {
        translated_risk_source_language=this->getStopLossRiskFactory()->translateStopLossRiskToSourceLanguage(risk);
        selected_risk=stopLossRiskFactory->createStopLossRisk(translated_risk_source_language);
    }


    if (riskCategory==RiskTree::RiskCategory::TAKE_PROFIT)
    {
        translated_risk_source_language=this->getTakeProfitRiskFactory()->translateTakeProfitRiskToSourceLanguage(risk);
        selected_risk=takeProfitRiskFactory->createTakeProfitRisk(translated_risk_source_language);
    }

    riskManager->addRisk(translated_risk_source_language,selected_risk);
}


void MainWindow::removeRiskSelection(const RiskTree::RiskCategory &riskCategory, const QString &risk)
{
    QString translated_risk_source_language;
    TradingSessionState::State trading_session_state=this->getTradingSessionState()->getState();
    if (trading_session_state==TradingSessionState::State::RESTORED) { return; }
    trader::RiskManager *riskManager=this->getTrader()->getRiskManager();

    if (riskCategory==RiskTree::RiskCategory::STOP_LOSS)
    {
        translated_risk_source_language=this->getStopLossRiskFactory()->translateStopLossRiskToSourceLanguage(risk);
    }

    if (riskCategory==RiskTree::RiskCategory::TAKE_PROFIT)
    {
        translated_risk_source_language=this->getTakeProfitRiskFactory()->translateTakeProfitRiskToSourceLanguage(risk);
    }

    riskManager->removeRisk(translated_risk_source_language);
}


void MainWindow::applyCapitalPercentage(const QString &market, const double &capitalPercentage)
{
    trader::AssetManager *assetManager=this->getTrader()->getAssetManager();
    BittrexAsset *bittrex_asset=static_cast<BittrexAsset *>(assetManager->getAsset(market));
    bittrex_asset->setCapitalPercentage(capitalPercentage);
}

void MainWindow::applyTradingRounds(const QString &market, const int &tradingRounds)
{
    trader::AssetManager *assetManager=this->getTrader()->getAssetManager();
    BittrexAsset *bittrex_asset=static_cast<BittrexAsset *>(assetManager->getAsset(market));
    bittrex_asset->setTradingRounds(tradingRounds);
}


void MainWindow::executeBittrexExecution(const QString &orderId, const trader::Order *order)
{
    BittrexExecution *bittrex_execution=new BittrexExecution(this->bittrexRestApiHandler);
    if (this->getTradingMode()==MainWindow::TradingMode::LIVE_TRADING) { bittrex_execution->setTradingMode(BittrexExecution::TradingMode::LIVE_TRADING); }
    if (this->getTradingMode()==MainWindow::TradingMode::PAPER_TRADING) {bittrex_execution->setTradingMode(BittrexExecution::TradingMode::PAPER_TRADING); }
    trader::Order *current_order=const_cast<trader::Order *>(order);
    bittrex_execution->setOrder(current_order);
    bittrex_execution->setFrequencyMilliseconds(12000);
    bittrex_execution->setCancelAfterMilliseconds(900000);
    this->getTrader()->getExecutionManager()->addExecution(orderId,bittrex_execution);
    bittrex_execution->place();
}


void MainWindow::removeBittrexExecution(const QString &orderId,const trader::Order *order)
{
    Q_UNUSED(order)
    trader::ExecutionManager *executionManager=this->getTrader()->getExecutionManager();
    BittrexExecution *bittrex_execution=static_cast<BittrexExecution *>(executionManager->getExecution(orderId));
    bittrex_execution->cancel();
}


void MainWindow::marketTickerReceived(const QString &market,const QJsonObject &result)
{
    bool doesAnalyserManagerContainMarket=this->getTrader()->getAnalyserManager()->getAnalysers()->contains(market);
    if (doesAnalyserManagerContainMarket==true)
    {
        BittrexAnalyser *bittrexAnalyser=static_cast<BittrexAnalyser *>(this->getTrader()->getAnalyserManager()->getAnalyser(market));
        bittrexAnalyser->processMarketTicker(market,result);
    }

    trader::Portfolio *usd_portfolio=this->getTrader()->getPortfolioManager()->getPortfolio("USD-Portfolio");
    trader::Portfolio *btc_portfolio=this->getTrader()->getPortfolioManager()->getPortfolio("BTC-Portfolio");
    trader::Portfolio *eth_portfolio=this->getTrader()->getPortfolioManager()->getPortfolio("ETH-Portfolio");
    trader::Portfolio *usdt_portfolio=this->getTrader()->getPortfolioManager()->getPortfolio("USDT-Portfolio");
    trader::Portfolio *eur_portfolio=this->getTrader()->getPortfolioManager()->getPortfolio("EUR-Portfolio");

    double bid=result.value("bidRate").toString().toDouble();
    double ask=result.value("askRate").toString().toDouble();

    if (market.contains("-USD")==true && market.contains("-USDT")==false) { usd_portfolio->updatePosition(market,bid,ask); }
    if (market.contains("-BTC")==true) { btc_portfolio->updatePosition(market,bid,ask); }
    if (market.contains("-ETH")==true) { eth_portfolio->updatePosition(market,bid,ask); }
    if (market.contains("-USDT")==true) { usdt_portfolio->updatePosition(market,bid,ask); }
    if (market.contains("-EUR")==true) { eur_portfolio->updatePosition(market,bid,ask); }
}


void MainWindow::modifyUsdPortfolioUnrealisedPnL(const double &unrealisedPnL)
{
    if (this->getColorMode()==MainWindow::ColorMode::LIGHT)
    {
        QString colorStr=QString();
        if (unrealisedPnL>0.0) { colorStr=QString("green"); }
        if (unrealisedPnL==0.0) { colorStr=QString("orange"); }
        if (unrealisedPnL<0.0) { colorStr=QString("red"); }
        this->ui->usdPortfolioUnrealisedPnL->setStyleSheet("color: "+colorStr);
        this->ui->usdPortfolioUnrealisedPnL->display(unrealisedPnL);
    }
    else if (this->getColorMode()==MainWindow::ColorMode::DARK)
    {
        QString colorStr=QString();
        if (unrealisedPnL>0.0) { colorStr=QString("lime"); }
        if (unrealisedPnL==0.0) { colorStr=QString("yellow"); }
        if (unrealisedPnL<0.0) { colorStr=QString("orange"); }
        this->ui->usdPortfolioUnrealisedPnL->setStyleSheet("color: "+colorStr);
        this->ui->usdPortfolioUnrealisedPnL->display(unrealisedPnL);
    }
}

void MainWindow::modifyUsdPortfolioRealisedPnL(const double &realisedPnL)
{
    if (this->getColorMode()==MainWindow::ColorMode::LIGHT)
    {
        QString colorStr=QString();
        if (realisedPnL>0.0) { colorStr=QString("green"); }
        if (realisedPnL==0.0) { colorStr=QString("orange"); }
        if (realisedPnL<0.0) { colorStr=QString("red"); }
        this->ui->usdPortfolioRealisedPnL->setStyleSheet("color: "+colorStr);
        this->ui->usdPortfolioRealisedPnL->display(realisedPnL);
        this->ui->usdPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: "+colorStr);
        this->ui->usdPortfolioSessionSummaryRealisedPnL->display(realisedPnL);
    }
    else if (this->getColorMode()==MainWindow::ColorMode::DARK)
    {
        QString colorStr=QString();
        if (realisedPnL>0.0) { colorStr=QString("lime"); }
        if (realisedPnL==0.0) { colorStr=QString("yellow"); }
        if (realisedPnL<0.0) { colorStr=QString("orange"); }
        this->ui->usdPortfolioRealisedPnL->setStyleSheet("color: "+colorStr);
        this->ui->usdPortfolioRealisedPnL->display(realisedPnL);
        this->ui->usdPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: "+colorStr);
        this->ui->usdPortfolioSessionSummaryRealisedPnL->display(realisedPnL);
    }
}

void MainWindow::modifyUsdPortfolioInitialCapital(const double &initialCapital)
{
    if (this->getColorMode()==MainWindow::ColorMode::LIGHT)
    {
        QString colorStr=QString("blue");
        this->ui->usdPortfolioInitialCapital->setStyleSheet("color: "+colorStr);
        this->ui->usdPortfolioInitialCapital->display(initialCapital);
        this->ui->usdPortfolioSessionSummaryInitialCapital->setStyleSheet("color: "+colorStr);
        this->ui->usdPortfolioSessionSummaryInitialCapital->display(initialCapital);
    }
    else if (this->getColorMode()==MainWindow::ColorMode::DARK)
    {
        QString colorStr=QString("cyan");
        this->ui->usdPortfolioInitialCapital->setStyleSheet("color: "+colorStr);
        this->ui->usdPortfolioInitialCapital->display(initialCapital);
        this->ui->usdPortfolioSessionSummaryInitialCapital->setStyleSheet("color: "+colorStr);
        this->ui->usdPortfolioSessionSummaryInitialCapital->display(initialCapital);
    }
}

void MainWindow::modifyUsdPortfolioCurrentCapital(const double &currentCapital)
{
    if (this->getColorMode()==MainWindow::ColorMode::LIGHT)
    {
        QString colorStr=QString("blue");
        this->ui->usdPortfolioCurrentCapital->setStyleSheet("color: "+colorStr);
        this->ui->usdPortfolioCurrentCapital->display(currentCapital);
        this->ui->usdPortfolioSessionSummaryCurrentCapital->setStyleSheet("color: "+colorStr);
        this->ui->usdPortfolioSessionSummaryCurrentCapital->display(currentCapital);
    }
    else if (this->getColorMode()==MainWindow::ColorMode::DARK)
    {
        QString colorStr=QString("cyan");
        this->ui->usdPortfolioCurrentCapital->setStyleSheet("color: "+colorStr);
        this->ui->usdPortfolioCurrentCapital->display(currentCapital);
        this->ui->usdPortfolioSessionSummaryCurrentCapital->setStyleSheet("color: "+colorStr);
        this->ui->usdPortfolioSessionSummaryCurrentCapital->display(currentCapital);
    }
}

void MainWindow::modifyUsdPortfolioEquity(const double &equity)
{
    if (this->getColorMode()==MainWindow::ColorMode::LIGHT)
    {
        QString colorStr=QString("blue");
        this->ui->usdPortfolioEquity->setStyleSheet("color: "+colorStr);
        this->ui->usdPortfolioEquity->display(equity);
        this->ui->usdPortfolioSessionSummaryTotalEquity->setStyleSheet("color: "+colorStr);
        this->ui->usdPortfolioSessionSummaryTotalEquity->display(equity);
    }
    else if (this->getColorMode()==MainWindow::ColorMode::DARK)
    {
        QString colorStr=QString("cyan");
        this->ui->usdPortfolioEquity->setStyleSheet("color: "+colorStr);
        this->ui->usdPortfolioEquity->display(equity);
        this->ui->usdPortfolioSessionSummaryTotalEquity->setStyleSheet("color: "+colorStr);
        this->ui->usdPortfolioSessionSummaryTotalEquity->display(equity);
    }
}

void MainWindow::modifyUsdPortfolioSessionSummaryReturnOnInvestment(const double &returnOnInvestment)
{
    if (this->getColorMode()==MainWindow::ColorMode::LIGHT)
    {
        QString colorStr=QString();
        if (returnOnInvestment>0.0) { colorStr=QString("green"); }
        if (returnOnInvestment==0.0) { colorStr=QString("orange"); }
        if (returnOnInvestment<0.0) { colorStr=QString("red"); }
        this->ui->usdPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: "+colorStr);
        this->ui->usdPortfolioSessionSummaryReturnOnInvestment->display(returnOnInvestment);
    }
    else if (this->getColorMode()==MainWindow::ColorMode::DARK)
    {
        QString colorStr=QString();
        if (returnOnInvestment>0.0) { colorStr=QString("lime"); }
        if (returnOnInvestment==0.0) { colorStr=QString("yellow"); }
        if (returnOnInvestment<0.0) { colorStr=QString("orange"); }
        this->ui->usdPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: "+colorStr);
        this->ui->usdPortfolioSessionSummaryReturnOnInvestment->display(returnOnInvestment);
    }
}


void MainWindow::modifyBtcPortfolioUnrealisedPnL(const double &unrealisedPnL)
{
    if (this->getColorMode()==MainWindow::ColorMode::LIGHT)
    {
        QString colorStr=QString();
        if (unrealisedPnL>0.0) { colorStr=QString("green"); }
        if (unrealisedPnL==0.0) { colorStr=QString("orange"); }
        if (unrealisedPnL<0.0) { colorStr=QString("red"); }
        QPalette pallete=this->ui->btcPortfolioUnrealisedPnL->palette();
        this->ui->btcPortfolioUnrealisedPnL->setStyleSheet("color: "+colorStr);
        this->ui->btcPortfolioUnrealisedPnL->display(unrealisedPnL);
    }
    else if (this->getColorMode()==MainWindow::ColorMode::DARK)
    {
        QString colorStr=QString();
        if (unrealisedPnL>0.0) { colorStr=QString("lime"); }
        if (unrealisedPnL==0.0) { colorStr=QString("yellow"); }
        if (unrealisedPnL<0.0) { colorStr=QString("orange"); }
        QPalette pallete=this->ui->btcPortfolioUnrealisedPnL->palette();
        this->ui->btcPortfolioUnrealisedPnL->setStyleSheet("color: "+colorStr);
        this->ui->btcPortfolioUnrealisedPnL->display(unrealisedPnL);
    }
}


void MainWindow::modifyBtcPortfolioRealisedPnL(const double &realisedPnL)
{
    if (this->getColorMode()==MainWindow::ColorMode::LIGHT)
    {
        QString colorStr=QString();
        if (realisedPnL>0.0) { colorStr=QString("green"); }
        if (realisedPnL==0.0) { colorStr=QString("orange"); }
        if (realisedPnL<0.0) { colorStr=QString("red"); }
        this->ui->btcPortfolioRealisedPnL->setStyleSheet("color: "+colorStr);
        this->ui->btcPortfolioRealisedPnL->display(realisedPnL);
        this->ui->btcPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: "+colorStr);
        this->ui->btcPortfolioSessionSummaryRealisedPnL->display(realisedPnL);
    }
    else if (this->getColorMode()==MainWindow::ColorMode::DARK)
    {
        QString colorStr=QString();
        if (realisedPnL>0.0) { colorStr=QString("lime"); }
        if (realisedPnL==0.0) { colorStr=QString("yellow"); }
        if (realisedPnL<0.0) { colorStr=QString("orange"); }
        this->ui->btcPortfolioRealisedPnL->setStyleSheet("color: "+colorStr);
        this->ui->btcPortfolioRealisedPnL->display(realisedPnL);
        this->ui->btcPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: "+colorStr);
        this->ui->btcPortfolioSessionSummaryRealisedPnL->display(realisedPnL);
    }
}


void MainWindow::modifyBtcPortfolioInitialCapital(const double &initialCapital)
{
    if (this->getColorMode()==MainWindow::ColorMode::LIGHT)
    {
        QString colorStr=QString("blue");
        this->ui->btcPortfolioInitialCapital->setStyleSheet("color: "+colorStr);
        this->ui->btcPortfolioInitialCapital->display(initialCapital);
        this->ui->btcPortfolioSessionSummaryInitialCapital->setStyleSheet("color: "+colorStr);
        this->ui->btcPortfolioSessionSummaryInitialCapital->display(initialCapital);
    }
    else if (this->getColorMode()==MainWindow::ColorMode::DARK)
    {
        QString colorStr=QString("cyan");
        this->ui->btcPortfolioInitialCapital->setStyleSheet("color: "+colorStr);
        this->ui->btcPortfolioInitialCapital->display(initialCapital);
        this->ui->btcPortfolioSessionSummaryInitialCapital->setStyleSheet("color: "+colorStr);
        this->ui->btcPortfolioSessionSummaryInitialCapital->display(initialCapital);
    }
}

void MainWindow::modifyBtcPortfolioCurrentCapital(const double &currentCapital)
{
    if (this->getColorMode()==MainWindow::ColorMode::LIGHT)
    {
        QString colorStr=QString("blue");
        this->ui->btcPortfolioCurrentCapital->setStyleSheet("color: "+colorStr);
        this->ui->btcPortfolioCurrentCapital->display(currentCapital);
        this->ui->btcPortfolioSessionSummaryCurrentCapital->setStyleSheet("color: "+colorStr);
        this->ui->btcPortfolioSessionSummaryCurrentCapital->display(currentCapital);
    }
    else if (this->getColorMode()==MainWindow::ColorMode::DARK)
    {
        QString colorStr=QString("cyan");
        this->ui->btcPortfolioCurrentCapital->setStyleSheet("color: "+colorStr);
        this->ui->btcPortfolioCurrentCapital->display(currentCapital);
        this->ui->btcPortfolioSessionSummaryCurrentCapital->setStyleSheet("color: "+colorStr);
        this->ui->btcPortfolioSessionSummaryCurrentCapital->display(currentCapital);
    }
}

void MainWindow::modifyBtcPortfolioEquity(const double &equity)
{
    if (this->getColorMode()==MainWindow::ColorMode::LIGHT)
    {
        QString colorStr=QString("blue");
        this->ui->btcPortfolioEquity->setStyleSheet("color: "+colorStr);
        this->ui->btcPortfolioEquity->display(equity);
        this->ui->btcPortfolioSessionSummaryTotalEquity->setStyleSheet("color: "+colorStr);
        this->ui->btcPortfolioSessionSummaryTotalEquity->display(equity);
    }
    else if (this->getColorMode()==MainWindow::ColorMode::DARK)
    {
        QString colorStr=QString("cyan");
        this->ui->btcPortfolioEquity->setStyleSheet("color: "+colorStr);
        this->ui->btcPortfolioEquity->display(equity);
        this->ui->btcPortfolioSessionSummaryTotalEquity->setStyleSheet("color: "+colorStr);
        this->ui->btcPortfolioSessionSummaryTotalEquity->display(equity);
    }
}

void MainWindow::modifyBtcPortfolioSessionSummaryReturnOnInvestment(const double &returnOnInvestment)
{
    if (this->getColorMode()==MainWindow::ColorMode::LIGHT)
    {
        QString colorStr=QString();
        if (returnOnInvestment>0.0) { colorStr=QString("green"); }
        if (returnOnInvestment==0.0) { colorStr=QString("orange"); }
        if (returnOnInvestment<0.0) { colorStr=QString("red"); }
        this->ui->btcPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: "+colorStr);
        this->ui->btcPortfolioSessionSummaryReturnOnInvestment->display(returnOnInvestment);
    }
    else if (this->getColorMode()==MainWindow::ColorMode::DARK)
    {
        QString colorStr=QString();
        if (returnOnInvestment>0.0) { colorStr=QString("lime"); }
        if (returnOnInvestment==0.0) { colorStr=QString("yellow"); }
        if (returnOnInvestment<0.0) { colorStr=QString("orange"); }
        this->ui->btcPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: "+colorStr);
        this->ui->btcPortfolioSessionSummaryReturnOnInvestment->display(returnOnInvestment);
    }
}


void MainWindow::modifyEthPortfolioUnrealisedPnL(const double &unrealisedPnL)
{
    if (this->getColorMode()==MainWindow::ColorMode::LIGHT)
    {
        QString colorStr=QString();
        if (unrealisedPnL>0.0) { colorStr=QString("green"); }
        if (unrealisedPnL==0.0) { colorStr=QString("orange"); }
        if (unrealisedPnL<0.0) { colorStr=QString("red"); }
        this->ui->ethPortfolioUnrealisedPnL->setStyleSheet("color: "+colorStr);
        this->ui->ethPortfolioUnrealisedPnL->display(unrealisedPnL);
    }
    else if (this->getColorMode()==MainWindow::ColorMode::DARK)
    {
        QString colorStr=QString();
        if (unrealisedPnL>0.0) { colorStr=QString("lime"); }
        if (unrealisedPnL==0.0) { colorStr=QString("yellow"); }
        if (unrealisedPnL<0.0) { colorStr=QString("orange"); }
        this->ui->ethPortfolioUnrealisedPnL->setStyleSheet("color: "+colorStr);
        this->ui->ethPortfolioUnrealisedPnL->display(unrealisedPnL);
    }
}


void MainWindow::modifyEthPortfolioRealisedPnL(const double &realisedPnL)
{
    if (this->getColorMode()==MainWindow::ColorMode::LIGHT)
    {
        QString colorStr=QString();
        if (realisedPnL>0.0) { colorStr=QString("green"); }
        if (realisedPnL==0.0) { colorStr=QString("orange"); }
        if (realisedPnL<0.0) { colorStr=QString("red"); }
        this->ui->ethPortfolioRealisedPnL->setStyleSheet("color: "+colorStr);
        this->ui->ethPortfolioRealisedPnL->display(realisedPnL);
        this->ui->ethPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: "+colorStr);
        this->ui->ethPortfolioSessionSummaryRealisedPnL->display(realisedPnL);
    }
    else if (this->getColorMode()==MainWindow::ColorMode::DARK)
    {
        QString colorStr=QString();
        if (realisedPnL>0.0) { colorStr=QString("lime"); }
        if (realisedPnL==0.0) { colorStr=QString("yellow"); }
        if (realisedPnL<0.0) { colorStr=QString("orange"); }
        this->ui->ethPortfolioRealisedPnL->setStyleSheet("color: "+colorStr);
        this->ui->ethPortfolioRealisedPnL->display(realisedPnL);
        this->ui->ethPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: "+colorStr);
        this->ui->ethPortfolioSessionSummaryRealisedPnL->display(realisedPnL);
    }
}


void MainWindow::modifyEthPortfolioInitialCapital(const double &initialCapital)
{
    if (this->getColorMode()==MainWindow::ColorMode::LIGHT)
    {
        QString colorStr=QString("blue");
        this->ui->ethPortfolioInitialCapital->setStyleSheet("color: "+colorStr);
        this->ui->ethPortfolioInitialCapital->display(initialCapital);
        this->ui->ethPortfolioSessionSummaryInitialCapital->setStyleSheet("color: "+colorStr);
        this->ui->ethPortfolioSessionSummaryInitialCapital->display(initialCapital);
    }
    else if (this->getColorMode()==MainWindow::ColorMode::DARK)
    {
        QString colorStr=QString("cyan");
        this->ui->ethPortfolioInitialCapital->setStyleSheet("color: "+colorStr);
        this->ui->ethPortfolioInitialCapital->display(initialCapital);
        this->ui->ethPortfolioSessionSummaryInitialCapital->setStyleSheet("color: "+colorStr);
        this->ui->ethPortfolioSessionSummaryInitialCapital->display(initialCapital);
    }
}


void MainWindow::modifyEthPortfolioCurrentCapital(const double &currentCapital)
{
    if (this->getColorMode()==MainWindow::ColorMode::LIGHT)
    {
        QString colorStr=QString("blue");
        this->ui->ethPortfolioCurrentCapital->setStyleSheet("color: "+colorStr);
        this->ui->ethPortfolioCurrentCapital->display(currentCapital);
        this->ui->ethPortfolioSessionSummaryCurrentCapital->setStyleSheet("color: "+colorStr);
        this->ui->ethPortfolioSessionSummaryCurrentCapital->display(currentCapital);
    }
    else if (this->getColorMode()==MainWindow::ColorMode::DARK)
    {
        QString colorStr=QString("cyan");
        this->ui->ethPortfolioCurrentCapital->setStyleSheet("color: "+colorStr);
        this->ui->ethPortfolioCurrentCapital->display(currentCapital);
        this->ui->ethPortfolioSessionSummaryCurrentCapital->setStyleSheet("color: "+colorStr);
        this->ui->ethPortfolioSessionSummaryCurrentCapital->display(currentCapital);
    }
}


void MainWindow::modifyEthPortfolioEquity(const double &equity)
{
    if (this->getColorMode()==MainWindow::ColorMode::LIGHT)
    {
        QString colorStr=QString("blue");
        this->ui->ethPortfolioEquity->setStyleSheet("color: "+colorStr);
        this->ui->ethPortfolioEquity->display(equity);
        this->ui->ethPortfolioSessionSummaryTotalEquity->setStyleSheet("color: "+colorStr);
        this->ui->ethPortfolioSessionSummaryTotalEquity->display(equity);
    }
    else if (this->getColorMode()==MainWindow::ColorMode::DARK)
    {
        QString colorStr=QString("cyan");
        this->ui->ethPortfolioEquity->setStyleSheet("color: "+colorStr);
        this->ui->ethPortfolioEquity->display(equity);
        this->ui->ethPortfolioSessionSummaryTotalEquity->setStyleSheet("color: "+colorStr);
        this->ui->ethPortfolioSessionSummaryTotalEquity->display(equity);
    }
}


void MainWindow::modifyEthPortfolioSessionSummaryReturnOnInvestment(const double &returnOnInvestment)
{
    if (this->getColorMode()==MainWindow::ColorMode::LIGHT)
    {
        QString colorStr=QString();
        if (returnOnInvestment>0.0) { colorStr=QString("green"); }
        if (returnOnInvestment==0.0) { colorStr=QString("orange"); }
        if (returnOnInvestment<0.0) { colorStr=QString("red"); }
        this->ui->ethPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: "+colorStr);
        this->ui->ethPortfolioSessionSummaryReturnOnInvestment->display(returnOnInvestment);
    }
    else if (this->getColorMode()==MainWindow::ColorMode::DARK)
    {
        QString colorStr=QString();
        if (returnOnInvestment>0.0) { colorStr=QString("lime"); }
        if (returnOnInvestment==0.0) { colorStr=QString("yellow"); }
        if (returnOnInvestment<0.0) { colorStr=QString("orange"); }
        this->ui->ethPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: "+colorStr);
        this->ui->ethPortfolioSessionSummaryReturnOnInvestment->display(returnOnInvestment);
    }
}


void MainWindow::modifyUsdtPortfolioUnrealisedPnL(const double &unrealisedPnL)
{
    if (this->getColorMode()==MainWindow::ColorMode::LIGHT)
    {
        QString colorStr=QString();
        if (unrealisedPnL>0.0) { colorStr=QString("green"); }
        if (unrealisedPnL==0.0) { colorStr=QString("orange"); }
        if (unrealisedPnL<0.0) { colorStr=QString("red"); }
        this->ui->usdtPortfolioUnrealisedPnL->setStyleSheet("color: "+colorStr);
        this->ui->usdtPortfolioUnrealisedPnL->display(unrealisedPnL);
    }
    else if (this->getColorMode()==MainWindow::ColorMode::DARK)
    {
        QString colorStr=QString();
        if (unrealisedPnL>0.0) { colorStr=QString("lime"); }
        if (unrealisedPnL==0.0) { colorStr=QString("yellow"); }
        if (unrealisedPnL<0.0) { colorStr=QString("orange"); }
        this->ui->usdtPortfolioUnrealisedPnL->setStyleSheet("color: "+colorStr);
        this->ui->usdtPortfolioUnrealisedPnL->display(unrealisedPnL);
    }
}

void MainWindow::modifyUsdtPortfolioRealisedPnL(const double &realisedPnL)
{
    if (this->getColorMode()==MainWindow::ColorMode::LIGHT)
    {
        QString colorStr=QString();
        if (realisedPnL>0.0) { colorStr=QString("green"); }
        if (realisedPnL==0.0) { colorStr=QString("orange"); }
        if (realisedPnL<0.0) { colorStr=QString("red"); }
        this->ui->usdtPortfolioRealisedPnL->setStyleSheet("color :"+colorStr);
        this->ui->usdtPortfolioRealisedPnL->display(realisedPnL);
        this->ui->usdtPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: "+colorStr);
        this->ui->usdtPortfolioSessionSummaryRealisedPnL->display(realisedPnL);
    }
    else if (this->getColorMode()==MainWindow::ColorMode::DARK)
    {
        QString colorStr=QString();
        if (realisedPnL>0.0) { colorStr=QString("lime"); }
        if (realisedPnL==0.0) { colorStr=QString("yellow"); }
        if (realisedPnL<0.0) { colorStr=QString("orange"); }
        this->ui->usdtPortfolioRealisedPnL->setStyleSheet("color :"+colorStr);
        this->ui->usdtPortfolioRealisedPnL->display(realisedPnL);
        this->ui->usdtPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: "+colorStr);
        this->ui->usdtPortfolioSessionSummaryRealisedPnL->display(realisedPnL);
    }
}


void MainWindow::modifyUsdtPortfolioInitialCapital(const double &initialCapital)
{
    if (this->getColorMode()==MainWindow::ColorMode::LIGHT)
    {
        QString colorStr=QString("blue");
        this->ui->usdtPortfolioInitialCapital->setStyleSheet("color: "+colorStr);
        this->ui->usdtPortfolioInitialCapital->display(initialCapital);
        this->ui->usdtPortfolioSessionSummaryInitialCapital->setStyleSheet("color: "+colorStr);
        this->ui->usdtPortfolioSessionSummaryInitialCapital->display(initialCapital);
    }
    else if (this->getColorMode()==MainWindow::ColorMode::DARK)
    {
        QString colorStr=QString("cyan");
        this->ui->usdtPortfolioInitialCapital->setStyleSheet("color: "+colorStr);
        this->ui->usdtPortfolioInitialCapital->display(initialCapital);
        this->ui->usdtPortfolioSessionSummaryInitialCapital->setStyleSheet("color: "+colorStr);
        this->ui->usdtPortfolioSessionSummaryInitialCapital->display(initialCapital);
    }
}


void MainWindow::modifyUsdtPortfolioCurrentCapital(const double &currentCapital)
{
    if (this->getColorMode()==MainWindow::ColorMode::LIGHT)
    {
        QString colorStr=QString("blue");
        this->ui->usdtPortfolioCurrentCapital->setStyleSheet("color: "+colorStr);
        this->ui->usdtPortfolioCurrentCapital->display(currentCapital);
        this->ui->usdtPortfolioSessionSummaryCurrentCapital->setStyleSheet("color: "+colorStr);
        this->ui->usdtPortfolioSessionSummaryCurrentCapital->display(currentCapital);
    }
    else if (this->getColorMode()==MainWindow::ColorMode::DARK)
    {
        QString colorStr=QString("cyan");
        this->ui->usdtPortfolioCurrentCapital->setStyleSheet("color: "+colorStr);
        this->ui->usdtPortfolioCurrentCapital->display(currentCapital);
        this->ui->usdtPortfolioSessionSummaryCurrentCapital->setStyleSheet("color: "+colorStr);
        this->ui->usdtPortfolioSessionSummaryCurrentCapital->display(currentCapital);
    }
}

void MainWindow::modifyUsdtPortfolioEquity(const double &equity)
{
    if (this->getColorMode()==MainWindow::ColorMode::LIGHT)
    {
        QString colorStr=QString("blue");
        this->ui->usdtPortfolioEquity->setStyleSheet("color: "+colorStr);
        this->ui->usdtPortfolioEquity->display(equity);
        this->ui->usdtPortfolioSessionSummaryTotalEquity->setStyleSheet("color: "+colorStr);
        this->ui->usdtPortfolioSessionSummaryTotalEquity->display(equity);
    }
    else if (this->getColorMode()==MainWindow::ColorMode::DARK)
    {
        QString colorStr=QString("cyan");
        this->ui->usdtPortfolioEquity->setStyleSheet("color: "+colorStr);
        this->ui->usdtPortfolioEquity->display(equity);
        this->ui->usdtPortfolioSessionSummaryTotalEquity->setStyleSheet("color: "+colorStr);
        this->ui->usdtPortfolioSessionSummaryTotalEquity->display(equity);
    }
}

void MainWindow::modifyUsdtPortfolioSessionSummaryReturnOnInvestment(const double &returnOnInvestment)
{
    if (this->getColorMode()==MainWindow::ColorMode::LIGHT)
    {
        QString colorStr=QString();
        if (returnOnInvestment>0.0) { colorStr=QString("green"); }
        if (returnOnInvestment==0.0) { colorStr=QString("orange"); }
        if (returnOnInvestment<0.0) { colorStr=QString("red"); }
        this->ui->usdtPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: "+colorStr);
        this->ui->usdtPortfolioSessionSummaryReturnOnInvestment->display(returnOnInvestment);
    }
    else if (this->getColorMode()==MainWindow::ColorMode::DARK)
    {
        QString colorStr=QString();
        if (returnOnInvestment>0.0) { colorStr=QString("lime"); }
        if (returnOnInvestment==0.0) { colorStr=QString("yellow"); }
        if (returnOnInvestment<0.0) { colorStr=QString("orange"); }
        this->ui->usdtPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: "+colorStr);
        this->ui->usdtPortfolioSessionSummaryReturnOnInvestment->display(returnOnInvestment);
    }
}

void MainWindow::modifyEurPortfolioUnrealisedPnL(const double &unrealisedPnL)
{
    if (this->getColorMode()==MainWindow::ColorMode::LIGHT)
    {
        QString colorStr=QString();
        if (unrealisedPnL>0.0) { colorStr=QString("green"); }
        if (unrealisedPnL==0.0) { colorStr=QString("orange"); }
        if (unrealisedPnL<0.0) { colorStr=QString("red"); }
        this->ui->eurPortfolioUnrealisedPnL->setStyleSheet("color: "+colorStr);
        this->ui->eurPortfolioUnrealisedPnL->display(unrealisedPnL);
    }
    else if (this->getColorMode()==MainWindow::ColorMode::DARK)
    {
        QString colorStr=QString();
        if (unrealisedPnL>0.0) { colorStr=QString("lime"); }
        if (unrealisedPnL==0.0) { colorStr=QString("yellow"); }
        if (unrealisedPnL<0.0) { colorStr=QString("orange"); }
        this->ui->eurPortfolioUnrealisedPnL->setStyleSheet("color: "+colorStr);
        this->ui->eurPortfolioUnrealisedPnL->display(unrealisedPnL);
    }
}

void MainWindow::modifyEurPortfolioRealisedPnL(const double &realisedPnL)
{
    if (this->getColorMode()==MainWindow::ColorMode::LIGHT)
    {
        QString colorStr=QString();
        if (realisedPnL>0.0) { colorStr=QString("green"); }
        if (realisedPnL==0.0) { colorStr=QString("orange"); }
        if (realisedPnL<0.0) { colorStr=QString("red"); }
        this->ui->eurPortfolioRealisedPnL->setStyleSheet("color :"+colorStr);
        this->ui->eurPortfolioRealisedPnL->display(realisedPnL);
        this->ui->eurPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: "+colorStr);
        this->ui->eurPortfolioSessionSummaryRealisedPnL->display(realisedPnL);
    }
    else if (this->getColorMode()==MainWindow::ColorMode::DARK)
    {
        QString colorStr=QString();
        if (realisedPnL>0.0) { colorStr=QString("lime"); }
        if (realisedPnL==0.0) { colorStr=QString("yellow"); }
        if (realisedPnL<0.0) { colorStr=QString("orange"); }
        this->ui->eurPortfolioRealisedPnL->setStyleSheet("color :"+colorStr);
        this->ui->eurPortfolioRealisedPnL->display(realisedPnL);
        this->ui->eurPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: "+colorStr);
        this->ui->eurPortfolioSessionSummaryRealisedPnL->display(realisedPnL);
    }
}

void MainWindow::modifyEurPortfolioInitialCapital(const double &initialCapital)
{
    if (this->getColorMode()==MainWindow::ColorMode::LIGHT)
    {
        QString colorStr=QString("blue");
        this->ui->eurPortfolioInitialCapital->setStyleSheet("color: "+colorStr);
        this->ui->eurPortfolioInitialCapital->display(initialCapital);
        this->ui->eurPortfolioSessionSummaryInitialCapital->setStyleSheet("color: "+colorStr);
        this->ui->eurPortfolioSessionSummaryInitialCapital->display(initialCapital);
    }
    else if (this->getColorMode()==MainWindow::ColorMode::DARK)
    {
        QString colorStr=QString("cyan");
        this->ui->eurPortfolioInitialCapital->setStyleSheet("color: "+colorStr);
        this->ui->eurPortfolioInitialCapital->display(initialCapital);
        this->ui->eurPortfolioSessionSummaryInitialCapital->setStyleSheet("color: "+colorStr);
        this->ui->eurPortfolioSessionSummaryInitialCapital->display(initialCapital);
    }
}

void MainWindow::modifyEurPortfolioCurrentCapital(const double &currentCapital)
{
    if (this->getColorMode()==MainWindow::ColorMode::LIGHT)
    {
        QString colorStr=QString("blue");
        this->ui->eurPortfolioCurrentCapital->setStyleSheet("color: "+colorStr);
        this->ui->eurPortfolioCurrentCapital->display(currentCapital);
        this->ui->eurPortfolioSessionSummaryCurrentCapital->setStyleSheet("color: "+colorStr);
        this->ui->eurPortfolioSessionSummaryCurrentCapital->display(currentCapital);
    }
    else if (this->getColorMode()==MainWindow::ColorMode::DARK)
    {
        QString colorStr=QString("cyan");
        this->ui->eurPortfolioCurrentCapital->setStyleSheet("color: "+colorStr);
        this->ui->eurPortfolioCurrentCapital->display(currentCapital);
        this->ui->eurPortfolioSessionSummaryCurrentCapital->setStyleSheet("color: "+colorStr);
        this->ui->eurPortfolioSessionSummaryCurrentCapital->display(currentCapital);
    }
}

void MainWindow::modifyEurPortfolioEquity(const double &equity)
{
    if (this->getColorMode()==MainWindow::ColorMode::LIGHT)
    {
        QString colorStr=QString("blue");
        this->ui->eurPortfolioEquity->setStyleSheet("color: "+colorStr);
        this->ui->eurPortfolioEquity->display(equity);
        this->ui->eurPortfolioSessionSummaryTotalEquity->setStyleSheet("color: "+colorStr);
        this->ui->eurPortfolioSessionSummaryTotalEquity->display(equity);
    }
    else if (this->getColorMode()==MainWindow::ColorMode::DARK)
    {
        QString colorStr=QString("cyan");
        this->ui->eurPortfolioEquity->setStyleSheet("color: "+colorStr);
        this->ui->eurPortfolioEquity->display(equity);
        this->ui->eurPortfolioSessionSummaryTotalEquity->setStyleSheet("color: "+colorStr);
        this->ui->eurPortfolioSessionSummaryTotalEquity->display(equity);
    }
}

void MainWindow::modifyEurPortfolioSessionSummaryReturnOnInvestment(const double &returnOnInvestment)
{
    if (this->getColorMode()==MainWindow::ColorMode::LIGHT)
    {
        QString colorStr=QString();
        if (returnOnInvestment>0.0) { colorStr=QString("green"); }
        if (returnOnInvestment==0.0) { colorStr=QString("orange"); }
        if (returnOnInvestment<0.0) { colorStr=QString("red"); }
        this->ui->eurPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: "+colorStr);
        this->ui->eurPortfolioSessionSummaryReturnOnInvestment->display(returnOnInvestment);
    }
    else if (this->getColorMode()==MainWindow::ColorMode::DARK)
    {
        QString colorStr=QString();
        if (returnOnInvestment>0.0) { colorStr=QString("lime"); }
        if (returnOnInvestment==0.0) { colorStr=QString("yellow"); }
        if (returnOnInvestment<0.0) { colorStr=QString("orange"); }
        this->ui->eurPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: "+colorStr);
        this->ui->eurPortfolioSessionSummaryReturnOnInvestment->display(returnOnInvestment);
    }
}

void MainWindow::modifyPortfolio(const QString &orderId,const trader::Order *order)
{
    Q_UNUSED(orderId)
    QString market=order->getMarket();
    trader::Position::Action action=trader::Position::Action::NONE;
    trader::Order::Type orderType=order->getType();
    if (orderType==trader::Order::Type::LIMIT_BUY) { action=trader::Position::Action::BOUGHT; }
    if (orderType==trader::Order::Type::LIMIT_SELL) { action=trader::Position::Action::SOLD; }
    double quantity=order->getData().value("quantity").toString().toDouble();
    double price=order->getData().value("limit").toString().toDouble();
    double commission=order->getData().value("commission").toString().toDouble();
    BittrexAnalyser *bittrex_analyser=static_cast<BittrexAnalyser *>(this->getTrader()->getAnalyserManager()->getAnalyser(market));
    double bid=bittrex_analyser->getMicroEconomicAnalysis()->getBid();
    double ask=bittrex_analyser->getMicroEconomicAnalysis()->getAsk();

    trader::Portfolio *usd_portfolio=this->getTrader()->getPortfolioManager()->getPortfolio("USD-Portfolio");
    trader::Portfolio *btc_portfolio=this->getTrader()->getPortfolioManager()->getPortfolio("BTC-Portfolio");
    trader::Portfolio *eth_portfolio=this->getTrader()->getPortfolioManager()->getPortfolio("ETH-Portfolio");
    trader::Portfolio *usdt_portfolio=this->getTrader()->getPortfolioManager()->getPortfolio("USDT-Portfolio");
    trader::Portfolio *eur_portfolio=this->getTrader()->getPortfolioManager()->getPortfolio("EUR-Portfolio");

    if (market.contains("-USD")==true && market.contains("-USDT")==false) { usd_portfolio->transactPosition(market,action,quantity,price,commission,bid,ask); }
    if (market.contains("-BTC")==true) { btc_portfolio->transactPosition(market,action,quantity,price,commission,bid,ask); }
    if (market.contains("-ETH")==true) { eth_portfolio->transactPosition(market,action,quantity,price,commission,bid,ask); }
    if (market.contains("-USDT")==true) { usdt_portfolio->transactPosition(market,action,quantity,price,commission,bid,ask); }
    if (market.contains("-EUR")==true) { eur_portfolio->transactPosition(market,action,quantity,price,commission,bid,ask); }
}


void MainWindow::modifyTradingRound(const QString &market,const trader::Position *position)
{
    Q_UNUSED(position)
    BittrexAsset *bittrex_asset=static_cast<BittrexAsset *>(this->getTrader()->getAssetManager()->getAsset(market));
    int current_trading_round=bittrex_asset->getCurrentRound();
    bittrex_asset->setCurrentRound(current_trading_round+1);
    this->checkAllTradingRounds();
}

void MainWindow::checkAllTradingRounds()
{
    bool areTradingRoundsCompleted=true;
    trader::AssetHash::iterator iterator;
    trader::AssetManager *assetManager=this->getTrader()->getAssetManager();

    for (iterator=assetManager->getAssets()->begin();iterator!=assetManager->getAssets()->end();iterator++)
    {
        int current_trading_round=iterator.value()->getCurrentRound();
        int trading_rounds=iterator.value()->getTradingRounds();
        if (current_trading_round<trading_rounds) { areTradingRoundsCompleted=false; break; }
    }

    if (areTradingRoundsCompleted==true) { this->stopTradingSession(); }
}

void MainWindow::startTradingSession()
{
    MainWindow::SessionStatus session_status=this->getSessionStatus();

    if (session_status==MainWindow::SessionStatus::STOPPED)
    {
        trader::RiskHash::iterator risk_iterator;
        trader::AnalyserHash::iterator analyser_iterator;
        trader::StrategyHash::iterator strategy_iterator;
        trader::PortfolioHash::iterator portfolio_iterator;
        trader::AnalyserManager *analyserManager=this->getTrader()->getAnalyserManager();
        trader::StrategyManager *strategyManager=this->getTrader()->getStrategyManager();
        trader::OrderManager *orderManager=this->getTrader()->getOrderManager();
        trader::RiskManager *riskManager=this->getTrader()->getRiskManager();
        trader::PortfolioManager *portfolioManager=this->getTrader()->getPortfolioManager();

        for (analyser_iterator=analyserManager->getAnalysers()->begin();analyser_iterator!=analyserManager->getAnalysers()->end();analyser_iterator++)
        {
            BittrexAnalyser *current_bittrex_analyser=static_cast<BittrexAnalyser *>(analyser_iterator.value());
            for (strategy_iterator=strategyManager->getStrategies()->begin();strategy_iterator!=strategyManager->getStrategies()->end();strategy_iterator++)
            {
                trader::Strategy *current_strategy=strategy_iterator.value();
                this->connect(current_bittrex_analyser,&BittrexAnalyser::marketTickerProcessed,current_strategy,&trader::Strategy::preprocess);
                this->connect(current_bittrex_analyser,&BittrexAnalyser::marketTradesProcessed,current_strategy,&trader::Strategy::preprocess);
                this->connect(current_bittrex_analyser,&BittrexAnalyser::marketOrderBookProcessed,current_strategy,&trader::Strategy::preprocess);
                this->connect(current_bittrex_analyser,&BittrexAnalyser::marketSummaryProcessed,current_strategy,&trader::Strategy::preprocess);
            }
        }

        for (portfolio_iterator=portfolioManager->getPortfolios()->begin();portfolio_iterator!=portfolioManager->getPortfolios()->end();portfolio_iterator++)
        {
            trader::Portfolio *current_portfolio=portfolio_iterator.value();
            for (risk_iterator=riskManager->getRisks()->begin();risk_iterator!=riskManager->getRisks()->end();risk_iterator++)
            {
                trader::Risk *current_risk=risk_iterator.value();
                this->connect(current_portfolio,&trader::Portfolio::positionUpdated,current_risk,&trader::Risk::preprocess);
            }
        }

        trader::Filterer *bittrex_filterer=this->getTrader()->getFiltererManager()->getFilterer("Bittrex Filterer");

        for (strategy_iterator=strategyManager->getStrategies()->begin();strategy_iterator!=strategyManager->getStrategies()->end();strategy_iterator++)
        {
            trader::Strategy *current_strategy=strategy_iterator.value();
            this->connect(current_strategy,&trader::Strategy::strategised,bittrex_filterer,&trader::Filterer::filter);
        }

        for (risk_iterator=riskManager->getRisks()->begin();risk_iterator!=riskManager->getRisks()->end();risk_iterator++)
        {
            trader::Risk *current_risk=risk_iterator.value();
            this->connect(current_risk,&trader::Risk::assessed,bittrex_filterer,&trader::Filterer::filter);
        }

        this->connect(this->ui->usdOpenPositionsTable,&OpenPositionTable::positionLiquified,bittrex_filterer,&trader::Filterer::filter);
        this->connect(this->ui->btcOpenPositionsTable,&OpenPositionTable::positionLiquified,bittrex_filterer,&trader::Filterer::filter);
        this->connect(this->ui->ethOpenPositionsTable,&OpenPositionTable::positionLiquified,bittrex_filterer,&trader::Filterer::filter);
        this->connect(this->ui->usdtOpenPositionsTable,&OpenPositionTable::positionLiquified,bittrex_filterer,&trader::Filterer::filter);
        this->connect(this->ui->eurOpenPositionsTable,&OpenPositionTable::positionLiquified,bittrex_filterer,&trader::Filterer::filter);


        trader::Sizer *bittrex_sizer=this->getTrader()->getSizerManager()->getSizer("Bittrex Sizer");
        this->connect(bittrex_filterer,&trader::Filterer::accepted,bittrex_sizer,&trader::Sizer::size);
        this->connect(bittrex_sizer,&trader::Sizer::resized,orderManager,&trader::OrderManager::suggest);
        this->setSessionStatus(MainWindow::SessionStatus::EXECUTING);

        QList<QAction *> actions=this->ui->mainToolBar->actions();
        actions.at(0)->setEnabled(true);
        actions.at(0)->setChecked(true);
        actions.at(1)->setEnabled(true);
        actions.at(2)->setEnabled(true);

        this->ui->executionPagesWidget->setCurrentIndex(1);
        this->ui->homeButton->setDisabled(true);
        this->ui->executeSessionButton->setDisabled(true);
        this->ui->checkupButton->setDisabled(true);
        this->disableMarketsPageCheckableWidgets();
        this->disableTraderPageCheckableWidgets();
        this->disableExecutionPageCheckableWidgets();
        this->populateSessionSummaryChartViews();
        this->getTradingSessionState()->setState(TradingSessionState::State::CRASHED);
        this->getTradingSessionState()->save();
        emit this->tradingSessionStarted();
    }
}


void MainWindow::resumeTradingSession()
{
    MainWindow::SessionStatus session_status=this->getSessionStatus();
    if (session_status==MainWindow::SessionStatus::PAUSED)
    {
        trader::Sizer *bittrex_sizer=this->getTrader()->getSizerManager()->getSizer("Bittrex Sizer");
        trader::OrderManager *orderManager=this->getTrader()->getOrderManager();
        this->connect(bittrex_sizer,&trader::Sizer::resized,orderManager,&trader::OrderManager::suggest);
        this->setSessionStatus(MainWindow::SessionStatus::EXECUTING);
        emit this->tradingSessionResumed();
    }
}

void MainWindow::pauseTradingSession()
{
    MainWindow::SessionStatus session_status=this->getSessionStatus();
    if (session_status==MainWindow::SessionStatus::EXECUTING)
    {
        trader::Sizer *bittrex_sizer=this->getTrader()->getSizerManager()->getSizer("Bittrex Sizer");
        trader::OrderManager *orderManager=this->getTrader()->getOrderManager();
        this->disconnect(bittrex_sizer,&trader::Sizer::resized,orderManager,&trader::OrderManager::suggest);
        this->setSessionStatus(MainWindow::SessionStatus::PAUSED);
        emit this->tradingSessionPaused();
    }
}


void MainWindow::stopTradingSessionConfirmation()
{
    MainWindow::SessionStatus session_status=this->getSessionStatus();
    QString message=tr("The current trading session is still running!");
    QString informative_message=tr("Are you sure you want to stop execution?");
    QString descriptive_message=tr("Stopping the current session while executing ");
    descriptive_message+=tr("can leave you exposed with open positions. It is highly ");
    descriptive_message+=tr(" advised to wait until the session completes.");
    bool ratification_result=this->getDialogManager()->showRatificationMessageBox(message,informative_message,descriptive_message);

    if (ratification_result==false)
    {
        QList<QAction *> actions=this->ui->mainToolBar->actions();
        if (session_status==MainWindow::SessionStatus::EXECUTING) { actions.at(0)->setChecked(true); }
        if (session_status==MainWindow::SessionStatus::PAUSED) { actions.at(1)->setChecked(true); }
        return;
    }
    else { this->stopTradingSession(); }
}


void MainWindow::stopTradingSession()
{
    MainWindow::SessionStatus session_status=this->getSessionStatus();
    if (session_status==MainWindow::SessionStatus::EXECUTING || session_status==MainWindow::SessionStatus::PAUSED)
    {
        trader::AssetHash::iterator asset_iterator;
        trader::RiskHash::iterator risk_iterator;
        trader::AnalyserHash::iterator analyser_iterator;
        trader::StrategyHash::iterator strategy_iterator;
        trader::PortfolioHash::iterator portfolio_iterator;
        trader::AssetManager *assetManager=this->getTrader()->getAssetManager();
        trader::AnalyserManager *analyserManager=this->getTrader()->getAnalyserManager();
        trader::StrategyManager *strategyManager=this->getTrader()->getStrategyManager();
        trader::OrderManager *orderManager=this->getTrader()->getOrderManager();
        trader::RiskManager *riskManager=this->getTrader()->getRiskManager();
        trader::PortfolioManager *portfolioManager=this->getTrader()->getPortfolioManager();

        for (asset_iterator=assetManager->getAssets()->begin();asset_iterator!=assetManager->getAssets()->end();asset_iterator++)
        {
            BittrexAsset *current_bittrex_asset=static_cast<BittrexAsset *>(asset_iterator.value());
            current_bittrex_asset->setCurrentRound(0);
        }

        for (analyser_iterator=analyserManager->getAnalysers()->begin();analyser_iterator!=analyserManager->getAnalysers()->end();analyser_iterator++)
        {
            BittrexAnalyser *current_bittrex_analyser=static_cast<BittrexAnalyser *>(analyser_iterator.value());
            for (strategy_iterator=strategyManager->getStrategies()->begin();strategy_iterator!=strategyManager->getStrategies()->end();strategy_iterator++)
            {
                trader::Strategy *current_strategy=strategy_iterator.value();
                this->disconnect(current_bittrex_analyser,&BittrexAnalyser::marketTickerProcessed,current_strategy,&trader::Strategy::preprocess);
                this->disconnect(current_bittrex_analyser,&BittrexAnalyser::marketTradesProcessed,current_strategy,&trader::Strategy::preprocess);
                this->disconnect(current_bittrex_analyser,&BittrexAnalyser::marketOrderBookProcessed,current_strategy,&trader::Strategy::preprocess);
                this->disconnect(current_bittrex_analyser,&BittrexAnalyser::marketSummaryProcessed,current_strategy,&trader::Strategy::preprocess);
            }
        }

        for (portfolio_iterator=portfolioManager->getPortfolios()->begin();portfolio_iterator!=portfolioManager->getPortfolios()->end();portfolio_iterator++)
        {
            trader::Portfolio *current_portfolio=portfolio_iterator.value();
            for (risk_iterator=riskManager->getRisks()->begin();risk_iterator!=riskManager->getRisks()->end();risk_iterator++)
            {
                trader::Risk *current_risk=risk_iterator.value();
                this->disconnect(current_portfolio,&trader::Portfolio::positionUpdated,current_risk,&trader::Risk::preprocess);
            }
        }

        trader::Filterer *bittrex_filterer=this->getTrader()->getFiltererManager()->getFilterer("Bittrex Filterer");

        for (strategy_iterator=strategyManager->getStrategies()->begin();strategy_iterator!=strategyManager->getStrategies()->end();strategy_iterator++)
        {
            trader::Strategy *current_strategy=strategy_iterator.value();
            this->disconnect(current_strategy,&trader::Strategy::strategised,bittrex_filterer,&trader::Filterer::filter);
        }

        for (risk_iterator=riskManager->getRisks()->begin();risk_iterator!=riskManager->getRisks()->end();risk_iterator++)
        {
            trader::Risk *current_risk=risk_iterator.value();
            this->disconnect(current_risk,&trader::Risk::assessed,bittrex_filterer,&trader::Filterer::filter);
        }

        this->disconnect(this->ui->usdOpenPositionsTable,&OpenPositionTable::positionLiquified,bittrex_filterer,&trader::Filterer::filter);
        this->disconnect(this->ui->btcOpenPositionsTable,&OpenPositionTable::positionLiquified,bittrex_filterer,&trader::Filterer::filter);
        this->disconnect(this->ui->ethOpenPositionsTable,&OpenPositionTable::positionLiquified,bittrex_filterer,&trader::Filterer::filter);
        this->disconnect(this->ui->usdtOpenPositionsTable,&OpenPositionTable::positionLiquified,bittrex_filterer,&trader::Filterer::filter);
        this->disconnect(this->ui->eurOpenPositionsTable,&OpenPositionTable::positionLiquified,bittrex_filterer,&trader::Filterer::filter);

        trader::Sizer *bittrex_sizer=this->getTrader()->getSizerManager()->getSizer("Bittrex Sizer");
        this->disconnect(bittrex_filterer,&trader::Filterer::accepted,bittrex_sizer,&trader::Sizer::size);
        this->disconnect(bittrex_sizer,&trader::Sizer::resized,orderManager,&trader::OrderManager::suggest);
        this->setSessionStatus(MainWindow::SessionStatus::STOPPED);

        QList<QAction *> actions=this->ui->mainToolBar->actions();
        actions.at(0)->setEnabled(false);
        actions.at(0)->setChecked(false);
        actions.at(1)->setEnabled(false);
        actions.at(1)->setChecked(false);
        actions.at(2)->setEnabled(false);
        actions.at(2)->setChecked(false);
        this->ui->executionPagesWidget->setCurrentIndex(2);
        this->enableMarketsPageCheckableWidgets();
        this->enableTraderPageCheckableWidgets();
        this->enableExecutionPageCheckableWidgets();
        this->ui->homeButton->setDisabled(false);
        this->ui->checkupButton->setDisabled(false);
        this->ui->executeSessionButton->setDisabled(false);
        this->getTradingSessionState()->setState(TradingSessionState::State::COMPLETED);
        this->getTradingSessionState()->save();
        emit this->tradingSessionStopped();
    }
}


void MainWindow::enableMarketsPageCheckableWidgets()
{
    this->ui->selectAllUsdMarketsButton->setEnabled(true);
    this->ui->deselectAllUsdMarketsButton->setEnabled(true);
    for (int i=0;i<this->ui->usdMarketsTable->rowCount();i++)
    {
        QTableWidgetItem *check_item=this->ui->usdMarketsTable->item(i,0);
        check_item->setFlags(check_item->flags() | Qt::ItemIsEnabled);
    }

    this->ui->selectAllBtcMarketsButton->setEnabled(true);
    this->ui->deselectAllBtcMarketsButton->setEnabled(true);
    for (int i=0;i<this->ui->btcMarketsTable->rowCount();i++)
    {
        QTableWidgetItem *check_item=this->ui->btcMarketsTable->item(i,0);
        check_item->setFlags(check_item->flags() | Qt::ItemIsEnabled);
    }

    this->ui->selectAllEthMarketsButton->setEnabled(true);
    this->ui->deselectAllEthMarketsButton->setEnabled(true);
    for (int i=0;i<this->ui->ethMarketsTable->rowCount();i++)
    {
        QTableWidgetItem *check_item=this->ui->ethMarketsTable->item(i,0);
        check_item->setFlags(check_item->flags() | Qt::ItemIsEnabled);
    }

    this->ui->selectAllUsdtMarketsButton->setEnabled(true);
    this->ui->deselectAllUsdtMarketsButton->setEnabled(true);
    for (int i=0;i<this->ui->usdtMarketsTable->rowCount();i++)
    {
        QTableWidgetItem *check_item=this->ui->usdtMarketsTable->item(i,0);
        check_item->setFlags(check_item->flags() | Qt::ItemIsEnabled);
    }

    this->ui->selectAllEurMarketsButton->setEnabled(true);
    this->ui->deselectAllEurMarketsButton->setEnabled(true);
    for (int i=0;i<this->ui->eurMarketsTable->rowCount();i++)
    {
        QTableWidgetItem *check_item=this->ui->eurMarketsTable->item(i,0);
        check_item->setFlags(check_item->flags() | Qt::ItemIsEnabled);
    }
}

void MainWindow::enableTraderPageCheckableWidgets()
{
    QList<QTreeWidgetItem *> filterRuleItems;
    this->ui->strategiesTree->blockSignals(true);
    this->ui->risksTree->blockSignals(true);
    this->ui->strategiesTree->enableItems();
    this->ui->risksTree->enableItems();
    this->ui->strategiesTree->blockSignals(false);
    this->ui->risksTree->blockSignals(false);
}


void MainWindow::enableExecutionPageCheckableWidgets()
{
    this->ui->selectedUsdMarketsCapital->setEnabled(true);
    for (int i=0;i<this->ui->selectedUsdMarketsTable->rowCount();i++)
    {
        QWidget *percentage_capital_item=this->ui->selectedUsdMarketsTable->cellWidget(i,1);
        QWidget *trading_round_item=this->ui->selectedUsdMarketsTable->cellWidget(i,2);
        percentage_capital_item->setEnabled(true);
        trading_round_item->setEnabled(true);
    }

    this->ui->selectedBtcMarketsCapital->setEnabled(true);
    for (int i=0;i<this->ui->selectedBtcMarketsTable->rowCount();i++)
    {
        QWidget *percentage_capital_item=this->ui->selectedBtcMarketsTable->cellWidget(i,1);
        QWidget *trading_round_item=this->ui->selectedBtcMarketsTable->cellWidget(i,2);
        percentage_capital_item->setEnabled(true);
        trading_round_item->setEnabled(true);
    }

    this->ui->selectedEthMarketsCapital->setEnabled(true);
    for (int i=0;i<this->ui->selectedEthMarketsTable->rowCount();i++)
    {
        QWidget *percentage_capital_item=this->ui->selectedEthMarketsTable->cellWidget(i,1);
        QWidget *trading_round_item=this->ui->selectedEthMarketsTable->cellWidget(i,2);
        percentage_capital_item->setEnabled(true);
        trading_round_item->setEnabled(true);
    }

    this->ui->selectedUsdtMarketsCapital->setEnabled(true);
    for (int i=0;i<this->ui->selectedUsdtMarketsTable->rowCount();i++)
    {
        QWidget *percentage_capital_item=this->ui->selectedUsdtMarketsTable->cellWidget(i,1);
        QWidget *trading_round_item=this->ui->selectedUsdtMarketsTable->cellWidget(i,2);
        percentage_capital_item->setEnabled(true);
        trading_round_item->setEnabled(true);
    }

    this->ui->selectedEurMarketsCapital->setEnabled(true);
    for (int i=0;i<this->ui->selectedEurMarketsTable->rowCount();i++)
    {
        QWidget *percentage_capital_item=this->ui->selectedEurMarketsTable->cellWidget(i,1);
        QWidget *trading_round_item=this->ui->selectedEurMarketsTable->cellWidget(i,2);
        percentage_capital_item->setEnabled(true);
        trading_round_item->setEnabled(true);
    }
}


void MainWindow::disableMarketsPageCheckableWidgets()
{
    this->ui->selectAllUsdMarketsButton->setEnabled(false);
    this->ui->deselectAllUsdMarketsButton->setEnabled(false);
    for (int i=0;i<this->ui->usdMarketsTable->rowCount();i++)
    {
        QTableWidgetItem *check_item=this->ui->usdMarketsTable->item(i,0);
        check_item->setFlags(check_item->flags() & ~Qt::ItemIsEnabled);
    }

    this->ui->selectAllBtcMarketsButton->setEnabled(false);
    this->ui->deselectAllBtcMarketsButton->setEnabled(false);
    for (int i=0;i<this->ui->btcMarketsTable->rowCount();i++)
    {
        QTableWidgetItem *check_item=this->ui->btcMarketsTable->item(i,0);
        check_item->setFlags(check_item->flags() & ~Qt::ItemIsEnabled);
    }

    this->ui->selectAllEthMarketsButton->setEnabled(false);
    this->ui->deselectAllEthMarketsButton->setEnabled(false);
    for (int i=0;i<this->ui->ethMarketsTable->rowCount();i++)
    {
        QTableWidgetItem *check_item=this->ui->ethMarketsTable->item(i,0);
        check_item->setFlags(check_item->flags() & ~Qt::ItemIsEnabled);
    }

    this->ui->selectAllUsdtMarketsButton->setEnabled(false);
    this->ui->deselectAllUsdtMarketsButton->setEnabled(false);
    for (int i=0;i<this->ui->usdtMarketsTable->rowCount();i++)
    {
        QTableWidgetItem *check_item=this->ui->usdtMarketsTable->item(i,0);
        check_item->setFlags(check_item->flags() & ~Qt::ItemIsEnabled);
    }

    this->ui->selectAllEurMarketsButton->setEnabled(false);
    this->ui->deselectAllEurMarketsButton->setEnabled(false);
    for (int i=0;i<this->ui->eurMarketsTable->rowCount();i++)
    {
        QTableWidgetItem *check_item=this->ui->eurMarketsTable->item(i,0);
        check_item->setFlags(check_item->flags() & ~Qt::ItemIsEnabled);
    }
}

void MainWindow::disableTraderPageCheckableWidgets()
{
    this->ui->strategiesTree->disableItems();
    this->ui->risksTree->disableItems();
}


void MainWindow::disableExecutionPageCheckableWidgets()
{
    this->ui->selectedUsdMarketsCapital->setEnabled(false);
    for (int i=0;i<this->ui->selectedUsdMarketsTable->rowCount();i++)
    {
        QWidget *percentage_capital_item=this->ui->selectedUsdMarketsTable->cellWidget(i,1);
        QWidget *trading_round_item=this->ui->selectedUsdMarketsTable->cellWidget(i,2);
        percentage_capital_item->setEnabled(false);
        trading_round_item->setEnabled(false);
    }

    this->ui->selectedBtcMarketsCapital->setEnabled(false);
    for (int i=0;i<this->ui->selectedBtcMarketsTable->rowCount();i++)
    {
        QWidget *percentage_capital_item=this->ui->selectedBtcMarketsTable->cellWidget(i,1);
        QWidget *trading_round_item=this->ui->selectedBtcMarketsTable->cellWidget(i,2);
        percentage_capital_item->setEnabled(false);
        trading_round_item->setEnabled(false);
    }

    this->ui->selectedEthMarketsCapital->setEnabled(false);
    for (int i=0;i<this->ui->selectedEthMarketsTable->rowCount();i++)
    {
        QWidget *percentage_capital_item=this->ui->selectedEthMarketsTable->cellWidget(i,1);
        QWidget *trading_round_item=this->ui->selectedEthMarketsTable->cellWidget(i,2);
        percentage_capital_item->setEnabled(false);
        trading_round_item->setEnabled(false);
    }

    this->ui->selectedUsdtMarketsCapital->setEnabled(false);
    for (int i=0;i<this->ui->selectedUsdtMarketsTable->rowCount();i++)
    {
        QWidget *percentage_capital_item=this->ui->selectedUsdtMarketsTable->cellWidget(i,1);
        QWidget *trading_round_item=this->ui->selectedUsdtMarketsTable->cellWidget(i,2);
        percentage_capital_item->setEnabled(false);
        trading_round_item->setEnabled(false);
    }

    this->ui->selectedEurMarketsCapital->setEnabled(false);
    for (int i=0;i<this->ui->selectedEurMarketsTable->rowCount();i++)
    {
        QWidget *percentage_capital_item=this->ui->selectedEurMarketsTable->cellWidget(i,1);
        QWidget *trading_round_item=this->ui->selectedEurMarketsTable->cellWidget(i,2);
        percentage_capital_item->setEnabled(false);
        trading_round_item->setEnabled(false);
    }
}


void MainWindow::restoreMarketsPageCheckableWidgets()
{
    trader::AssetManager *asset_manager=this->getTrader()->getAssetManager();
    trader::AssetHash::iterator asset_iterator;

    for (asset_iterator=asset_manager->getAssets()->begin();asset_iterator!=asset_manager->getAssets()->end();asset_iterator++)
    {
        trader::Asset *current_asset=asset_iterator.value();
        QString market=current_asset->getMarket();

        bool usd_table_contains_market=this->ui->usdMarketsTable->getMap()->contains(market);
        bool btc_table_contains_market=this->ui->btcMarketsTable->getMap()->contains(market);
        bool eth_table_contains_market=this->ui->ethMarketsTable->getMap()->contains(market);
        bool usdt_table_contains_market=this->ui->usdtMarketsTable->getMap()->contains(market);
        bool eur_table_contains_market=this->ui->eurMarketsTable->getMap()->contains(market);

        if (usd_table_contains_market==true)
        {
            QList<QTableWidgetItem *> table_widget_list=this->ui->usdMarketsTable->getMap()->value(market);
            QTableWidgetItem *checked_item=table_widget_list.at(0);
            checked_item->setCheckState(Qt::CheckState::Checked);
        }

        if (btc_table_contains_market==true)
        {
            QList<QTableWidgetItem *> table_widget_list=this->ui->btcMarketsTable->getMap()->value(market);
            QTableWidgetItem *checked_item=table_widget_list.at(0);
            checked_item->setCheckState(Qt::CheckState::Checked);
        }

        if (eth_table_contains_market==true)
        {
            QList<QTableWidgetItem *> table_widget_list=this->ui->ethMarketsTable->getMap()->value(market);
            QTableWidgetItem *checked_item=table_widget_list.at(0);
            checked_item->setCheckState(Qt::CheckState::Checked);
        }

        if (usdt_table_contains_market==true)
        {
            QList<QTableWidgetItem *> table_widget_list=this->ui->usdtMarketsTable->getMap()->value(market);
            QTableWidgetItem *checked_item=table_widget_list.at(0);
            checked_item->setCheckState(Qt::CheckState::Checked);
        }

        if (eur_table_contains_market==true)
        {
            QList<QTableWidgetItem *> table_widget_list=this->ui->eurMarketsTable->getMap()->value(market);
            QTableWidgetItem *checked_item=table_widget_list.at(0);
            checked_item->setCheckState(Qt::CheckState::Checked);
        }
    }
}


void MainWindow::restoreTraderPageCheckableWidgets()
{
    trader::StrategyManager *strategy_manager=this->getTrader()->getStrategyManager();
    trader::StrategyHash::iterator strategy_iterator;

    for (strategy_iterator=strategy_manager->getStrategies()->begin();strategy_iterator!=strategy_manager->getStrategies()->end();strategy_iterator++)
    {
        trader::Strategy *current_strategy=strategy_iterator.value();
        QString name=current_strategy->getName();
        QList<QTreeWidgetItem *> tree_widget_list=this->ui->strategiesTree->findItems(tr(name.toLatin1()),Qt::MatchRecursive);

        if (tree_widget_list.count()>0)
        {
            for (int i=0;i<tree_widget_list.count();i++)
            {
                QTreeWidgetItem *checked_item=tree_widget_list.at(i);
                checked_item->setCheckState(0,Qt::CheckState::Checked);
            }
        }
    }

    trader::RiskManager *risk_manager=this->getTrader()->getRiskManager();
    trader::RiskHash::iterator risk_iterator;

    for (risk_iterator=risk_manager->getRisks()->begin();risk_iterator!=risk_manager->getRisks()->end();risk_iterator++)
    {
        trader::Risk *current_risk=risk_iterator.value();
        QString name=current_risk->getName();
        QList<QTreeWidgetItem *> tree_widget_list=this->ui->risksTree->findItems(tr(name.toLatin1()),Qt::MatchRecursive);

        if (tree_widget_list.count()>0)
        {
            for (int i=0;i<tree_widget_list.count();i++)
            {
                QTreeWidgetItem *checked_item=tree_widget_list.at(i);
                checked_item->setCheckState(0,Qt::CheckState::Checked);
            }
        }
    }
}


void MainWindow::restoreExecutionPageCheckableWidgets()
{
    trader::Portfolio *usd_portfolio=this->getTrader()->getPortfolioManager()->getPortfolio("USD-Portfolio");
    trader::Portfolio *btc_portfolio=this->getTrader()->getPortfolioManager()->getPortfolio("BTC-Portfolio");
    trader::Portfolio *eth_portfolio=this->getTrader()->getPortfolioManager()->getPortfolio("ETH-Portfolio");
    trader::Portfolio *usdt_portfolio=this->getTrader()->getPortfolioManager()->getPortfolio("USDT-Portfolio");
    trader::Portfolio *eur_portfolio=this->getTrader()->getPortfolioManager()->getPortfolio("EUR-Portfolio");

    double usd_portfolio_initial_capital=usd_portfolio->getInitialCapital();
    double btc_portfolio_initial_capital=btc_portfolio->getInitialCapital();
    double eth_portfolio_initial_capital=eth_portfolio->getInitialCapital();
    double usdt_portfolio_initial_capital=usdt_portfolio->getInitialCapital();
    double eur_portfolio_initial_capital=eur_portfolio->getInitialCapital();

    this->ui->selectedUsdMarketsCapital->setValue(usd_portfolio_initial_capital);
    this->ui->selectedBtcMarketsCapital->setValue(btc_portfolio_initial_capital);
    this->ui->selectedEthMarketsCapital->setValue(eth_portfolio_initial_capital);
    this->ui->selectedUsdtMarketsCapital->setValue(usdt_portfolio_initial_capital);
    this->ui->selectedEurMarketsCapital->setValue(eur_portfolio_initial_capital);

    trader::AssetManager *asset_manager=this->getTrader()->getAssetManager();
    trader::AssetHash::iterator asset_iterator;

    for (asset_iterator=asset_manager->getAssets()->begin();asset_iterator!=asset_manager->getAssets()->end();asset_iterator++)
    {
        trader::Asset *current_asset=asset_iterator.value();
        QString market=current_asset->getMarket();

        bool market_belongs_to_usd_table=(market.contains("-USD")==true && market.contains("-USDT")==false);
        bool market_belongs_to_btc_table=market.contains("-BTC");
        bool market_belongs_to_eth_table=market.contains("-ETH");
        bool market_belongs_to_usdt_table=market.contains("-USDT");
        bool market_belongs_to_eur_table=market.contains("-EUR");

        if (market_belongs_to_usd_table==true)
        {
            this->ui->selectedUsdMarketsTable->insertMarket(market);
            this->ui->selectedUsdMarketsTable->specifyCapitalPercentage(market,current_asset->getCapitalPercentage());
            this->ui->selectedUsdMarketsTable->specifyTradingRounds(market,current_asset->getTradingRounds());
        }

        if (market_belongs_to_btc_table==true)
        {
            this->ui->selectedBtcMarketsTable->insertMarket(market);
            this->ui->selectedBtcMarketsTable->specifyCapitalPercentage(market,current_asset->getCapitalPercentage());
            this->ui->selectedBtcMarketsTable->specifyTradingRounds(market,current_asset->getTradingRounds());
        }

        if (market_belongs_to_eth_table==true)
        {
            this->ui->selectedEthMarketsTable->insertMarket(market);
            this->ui->selectedEthMarketsTable->specifyCapitalPercentage(market,current_asset->getCapitalPercentage());
            this->ui->selectedEthMarketsTable->specifyTradingRounds(market,current_asset->getTradingRounds());
        }

        if (market_belongs_to_usdt_table==true)
        {
            this->ui->selectedUsdtMarketsTable->insertMarket(market);
            this->ui->selectedUsdtMarketsTable->specifyCapitalPercentage(market,current_asset->getCapitalPercentage());
            this->ui->selectedUsdtMarketsTable->specifyTradingRounds(market,current_asset->getTradingRounds());
        }

        if (market_belongs_to_eur_table==true)
        {
            this->ui->selectedEurMarketsTable->insertMarket(market);
            this->ui->selectedEurMarketsTable->specifyCapitalPercentage(market,current_asset->getCapitalPercentage());
            this->ui->selectedEurMarketsTable->specifyTradingRounds(market,current_asset->getTradingRounds());
        }
    }
}


void MainWindow::checkRestoration()
{
    bool check_state=this->getTradingSessionState()->check();
    if (check_state==false)
    {
        QString message=tr("BittrexBot terminated while trading session was still active!");
        QString informative_message=tr("Would you like to restore the trading session?");
        QString descriptive_message=tr("The BittrexBot program either crashed or was terminated by the user before the trading session had completed.");
        descriptive_message+=tr(" It is highly reccommended to restore your previous active trading session.");
        descriptive_message+=tr(" If you do not restore, you will be left with exposed positions.");
        bool result=this->getDialogManager()->showRestorationMessageBox(message,informative_message,descriptive_message);
        if (result==true)
        {
            this->getTradingSessionState()->restore();
            this->getTradingSessionState()->setState(TradingSessionState::State::RESTORED);
            QString public_key=this->getBittrexRestApiHandler()->getPublicKey();
            QString private_key=this->getBittrexRestApiHandler()->getPrivateKey();
            this->ui->bittrexPublicKeyInput->setText(public_key);
            this->ui->bittrexPrivateKeyInput->setText(private_key);
            this->restoreMarketsPageCheckableWidgets();
            this->restoreTraderPageCheckableWidgets();
            this->restoreExecutionPageCheckableWidgets();
            this->ui->stackedWidget->setCurrentIndex(1);
            this->ui->pagesWidget->setCurrentIndex(3);
            this->ui->executionPagesWidget->setCurrentIndex(1);
            this->startTradingSession();
        }
    }
}

void MainWindow::restoreSettings()
{
    this->getApplicationSettings()->restore();
    bool light_mode_status=this->getApplicationSettings()->getLightMode();
    bool dark_mode_status=this->getApplicationSettings()->getDarkMode();
    bool logger_status=this->getApplicationSettings()->getLogger();
    QString language_name=this->getApplicationSettings()->getLanguage();
    if (light_mode_status==true)
    {
        this->changeToLightMode();
        QList<QAction *> toolbar_actions=this->ui->mainToolBar->actions();
        QAction *light_mode_action=toolbar_actions.at(4);
        light_mode_action->toggle();
    }

    if (dark_mode_status==true)
    {
        this->changeToDarkMode();
        QList<QAction *> toolbar_actions=this->ui->mainToolBar->actions();
        QAction *dark_mode_action=toolbar_actions.at(5);
        dark_mode_action->toggle();
    }

    this->toggleLogger(logger_status);
    if (logger_status==true)
    {
        QList<QAction *> toolbar_actions=this->ui->mainToolBar->actions();
        QAction *logger_action=toolbar_actions.at(3);
        logger_action->toggle();
    }

    this->getLanguageSelectorDialog()->changeLanguage(language_name);
}


void MainWindow::marketSummaryReceived(const QString &market,const QJsonObject &result)
{
    bool doesAnalyserManagerContainMarket=this->getTrader()->getAnalyserManager()->getAnalysers()->contains(market);
    if (doesAnalyserManagerContainMarket==true)
    {
        BittrexAnalyser *bittrexAnalyser=static_cast<BittrexAnalyser *>(this->getTrader()->getAnalyserManager()->getAnalyser(market));
        bittrexAnalyser->processMarketSummary(market,result);
    }
}



void MainWindow::marketOrderBookReceived(const QString &market,const int &depth,const QJsonObject &result)
{
    Q_UNUSED(depth)
    bool doesAnalyserManagerContainMarket=this->getTrader()->getAnalyserManager()->getAnalysers()->contains(market);
    if (doesAnalyserManagerContainMarket==true)
    {
        BittrexAnalyser *bittrexAnalyser=static_cast<BittrexAnalyser *>(this->getTrader()->getAnalyserManager()->getAnalyser(market));
        bittrexAnalyser->processMarketOrderBook(market,result);
    }
}


void MainWindow::marketTradesReceived(const QString &market,const QJsonArray &result)
{
    bool doesAnalyserManagerContainMarket=this->getTrader()->getAnalyserManager()->getAnalysers()->contains(market);
    if (doesAnalyserManagerContainMarket==true)
    {
        BittrexAnalyser *bittrexAnalyser=static_cast<BittrexAnalyser *>(this->getTrader()->getAnalyserManager()->getAnalyser(market));
        bittrexAnalyser->processMarketTrades(market,result);
    }
}

void MainWindow::placeOrderReceived(const QString &orderId,const QJsonObject &result)
{
    trader::ExecutionManager *executionManager=this->getTrader()->getExecutionManager();
    BittrexExecution *bittrex_execution=static_cast<BittrexExecution *>(executionManager->getExecution(orderId));

    if (result.contains("code")==false)
    {
        QString uuid=result.value("id").toString();
        bittrex_execution->getOrder()->setExchangeOrderId(uuid);
        emit bittrex_execution->opened();
    }
    else
    {
        bittrex_execution->canceled();
        executionManager->removeExecution(orderId);
    }
}

void MainWindow::placeCancelOrderReceived(const QString &orderId,const QString &exchangeOrderid,const QJsonObject &result)
{
    Q_UNUSED(exchangeOrderid);
    trader::ExecutionManager *executionManager=this->getTrader()->getExecutionManager();
    BittrexExecution *bittrex_execution=static_cast<BittrexExecution *>(executionManager->getExecution(orderId));

    if (result.contains("code")==false)
    {
        emit bittrex_execution->canceled();
        bittrex_execution->stop();
        executionManager->removeExecution(orderId);
    }
}

void MainWindow::queryOrderReceived(const QString &orderId,const QString &exchangeOrderId,const QJsonObject &result)
{
    Q_UNUSED(exchangeOrderId);
    trader::ExecutionManager *executionManager=this->getTrader()->getExecutionManager();
    BittrexExecution *bittrex_execution=static_cast<BittrexExecution *>(executionManager->getExecution(orderId));
    if (bittrex_execution==nullptr) { return; }
    if (result.contains("code")==false) { bittrex_execution->recondition(orderId,result); }
}

void MainWindow::changeToLightMode()
{
    QFile file=QFile(":themes/light_mode/light_mode.css");
    file.open(QFile::ReadOnly);
    QString style_sheet=QLatin1String(file.readAll());
    this->setStyleSheet(QString());
    this->setStyleSheet(style_sheet);

    this->ui->usdPortfolioInitialCapital->setStyleSheet("color: blue");
    this->ui->usdPortfolioCurrentCapital->setStyleSheet("color: blue");
    this->ui->usdPortfolioEquity->setStyleSheet("color: blue");
    this->ui->usdPortfolioSessionSummaryInitialCapital->setStyleSheet("color: blue");
    this->ui->usdPortfolioSessionSummaryCurrentCapital->setStyleSheet("color: blue");
    this->ui->usdPortfolioSessionSummaryTotalEquity->setStyleSheet("color: blue");

    double usd_portfolio_realised_pnl=this->ui->usdPortfolioRealisedPnL->value();
    double usd_portfolio_unrealised_pnl=this->ui->usdPortfolioUnrealisedPnL->value();
    double usd_portfolio_return_on_investment=this->ui->usdPortfolioSessionSummaryReturnOnInvestment->value();

    if (usd_portfolio_realised_pnl>0.0)
    {
        this->ui->usdPortfolioRealisedPnL->setStyleSheet("color: green");
        this->ui->usdPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: green");
    }

    if (usd_portfolio_realised_pnl==0.0)
    {
        this->ui->usdPortfolioRealisedPnL->setStyleSheet("color: orange");
        this->ui->usdPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: orange");
    }
    if (usd_portfolio_realised_pnl<0.0)
    {
        this->ui->usdPortfolioRealisedPnL->setStyleSheet("color: red");
        this->ui->usdPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: red");
    }

    if (usd_portfolio_return_on_investment>0.0) { this->ui->usdPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: green"); }
    if (usd_portfolio_return_on_investment==0.0) { this->ui->usdPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: orange"); }
    if (usd_portfolio_return_on_investment<0.0) { this->ui->usdPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: red"); }

    if (usd_portfolio_unrealised_pnl>0.0) { this->ui->usdPortfolioUnrealisedPnL->setStyleSheet("color: green"); }
    if (usd_portfolio_unrealised_pnl==0.0) { this->ui->usdPortfolioUnrealisedPnL->setStyleSheet("color: orange"); }
    if (usd_portfolio_unrealised_pnl<0.0) { this->ui->usdPortfolioUnrealisedPnL->setStyleSheet("color: red"); }

    this->ui->btcPortfolioInitialCapital->setStyleSheet("color: blue");
    this->ui->btcPortfolioCurrentCapital->setStyleSheet("color: blue");
    this->ui->btcPortfolioEquity->setStyleSheet("color: blue");
    this->ui->btcPortfolioSessionSummaryInitialCapital->setStyleSheet("color: blue");
    this->ui->btcPortfolioSessionSummaryCurrentCapital->setStyleSheet("color: blue");
    this->ui->btcPortfolioSessionSummaryTotalEquity->setStyleSheet("color: blue");

    double bitcoin_portfolio_realised_pnl=this->ui->btcPortfolioRealisedPnL->value();
    double bitcoin_portfolio_unrealised_pnl=this->ui->btcPortfolioUnrealisedPnL->value();
    double bitcoin_portfolio_return_on_investment=this->ui->btcPortfolioSessionSummaryReturnOnInvestment->value();

    if (bitcoin_portfolio_realised_pnl>0.0)
    {
        this->ui->btcPortfolioRealisedPnL->setStyleSheet("color: green");
        this->ui->btcPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: green");
    }

    if (bitcoin_portfolio_realised_pnl==0.0)
    {
        this->ui->btcPortfolioRealisedPnL->setStyleSheet("color: orange");
        this->ui->btcPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: orange");
    }
    if (bitcoin_portfolio_realised_pnl<0.0)
    {
        this->ui->btcPortfolioRealisedPnL->setStyleSheet("color: red");
        this->ui->btcPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: red");
    }

    if (bitcoin_portfolio_return_on_investment>0.0) { this->ui->btcPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: green"); }
    if (bitcoin_portfolio_return_on_investment==0.0) { this->ui->btcPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: orange"); }
    if (bitcoin_portfolio_return_on_investment<0.0) { this->ui->btcPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: red"); }

    if (bitcoin_portfolio_unrealised_pnl>0.0) { this->ui->btcPortfolioUnrealisedPnL->setStyleSheet("color: green"); }
    if (bitcoin_portfolio_unrealised_pnl==0.0) { this->ui->btcPortfolioUnrealisedPnL->setStyleSheet("color: orange"); }
    if (bitcoin_portfolio_unrealised_pnl<0.0) { this->ui->btcPortfolioUnrealisedPnL->setStyleSheet("color: red"); }

    this->ui->ethPortfolioInitialCapital->setStyleSheet("color: blue");
    this->ui->ethPortfolioCurrentCapital->setStyleSheet("color: blue");
    this->ui->ethPortfolioEquity->setStyleSheet("color: blue");
    this->ui->ethPortfolioSessionSummaryInitialCapital->setStyleSheet("color: blue");
    this->ui->ethPortfolioSessionSummaryCurrentCapital->setStyleSheet("color: blue");
    this->ui->ethPortfolioSessionSummaryTotalEquity->setStyleSheet("color: blue");

    double ethereum_portfolio_realised_pnl=this->ui->ethPortfolioRealisedPnL->value();
    double ethereum_portfolio_unrealised_pnl=this->ui->ethPortfolioUnrealisedPnL->value();
    double ethereum_portfolio_return_on_investment=this->ui->ethPortfolioSessionSummaryReturnOnInvestment->value();

    if (ethereum_portfolio_realised_pnl>0.0)
    {
        this->ui->ethPortfolioRealisedPnL->setStyleSheet("color: green");
        this->ui->ethPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: green");
    }

    if (ethereum_portfolio_realised_pnl==0.0)
    {
        this->ui->ethPortfolioRealisedPnL->setStyleSheet("color: orange");
        this->ui->ethPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: orange");
    }

    if (ethereum_portfolio_realised_pnl<0.0)
    {
        this->ui->ethPortfolioRealisedPnL->setStyleSheet("color: red");
        this->ui->ethPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: red");
    }

    if (ethereum_portfolio_return_on_investment>0.0) { this->ui->ethPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: green"); }
    if (ethereum_portfolio_return_on_investment==0.0) { this->ui->ethPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: orange"); }
    if (ethereum_portfolio_return_on_investment<0.0) { this->ui->ethPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: red"); }

    if (ethereum_portfolio_unrealised_pnl>0.0) { this->ui->ethPortfolioUnrealisedPnL->setStyleSheet("color: green"); }
    if (ethereum_portfolio_unrealised_pnl==0.0) { this->ui->ethPortfolioUnrealisedPnL->setStyleSheet("color: orange"); }
    if (ethereum_portfolio_unrealised_pnl<0.0) { this->ui->ethPortfolioUnrealisedPnL->setStyleSheet("color: red"); }

    this->ui->usdtPortfolioInitialCapital->setStyleSheet("color: blue");
    this->ui->usdtPortfolioCurrentCapital->setStyleSheet("color: blue");
    this->ui->usdtPortfolioEquity->setStyleSheet("color: blue");
    this->ui->usdtPortfolioSessionSummaryInitialCapital->setStyleSheet("color: blue");
    this->ui->usdtPortfolioSessionSummaryCurrentCapital->setStyleSheet("color: blue");
    this->ui->usdtPortfolioSessionSummaryTotalEquity->setStyleSheet("color: blue");

    double usdt_portfolio_realised_pnl=this->ui->usdtPortfolioRealisedPnL->value();
    double usdt_portfolio_unrealised_pnl=this->ui->usdtPortfolioUnrealisedPnL->value();
    double usdt_portfolio_return_on_investment=this->ui->usdtPortfolioSessionSummaryReturnOnInvestment->value();

    if (usdt_portfolio_realised_pnl>0.0)
    {
        this->ui->usdtPortfolioRealisedPnL->setStyleSheet("color: green");
        this->ui->usdtPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: green");
    }

    if (usdt_portfolio_realised_pnl==0.0)
    {
        this->ui->usdtPortfolioRealisedPnL->setStyleSheet("color: orange");
        this->ui->usdtPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: orange");
    }

    if (usdt_portfolio_realised_pnl<0.0)
    {
        this->ui->usdtPortfolioRealisedPnL->setStyleSheet("color: red");
        this->ui->usdtPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: red");
    }

    if (usdt_portfolio_return_on_investment>0.0) { this->ui->usdtPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: green"); }
    if (usdt_portfolio_return_on_investment==0.0) { this->ui->usdtPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: orange"); }
    if (usdt_portfolio_return_on_investment<0.0) { this->ui->usdtPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: red"); }

    if (usdt_portfolio_unrealised_pnl>0.0) { this->ui->usdtPortfolioUnrealisedPnL->setStyleSheet("color: green"); }
    if (usdt_portfolio_unrealised_pnl==0.0) { this->ui->usdtPortfolioUnrealisedPnL->setStyleSheet("color: orange"); }
    if (usdt_portfolio_unrealised_pnl<0.0) { this->ui->usdtPortfolioUnrealisedPnL->setStyleSheet("color: red"); }

    this->ui->eurPortfolioInitialCapital->setStyleSheet("color: blue");
    this->ui->eurPortfolioCurrentCapital->setStyleSheet("color: blue");
    this->ui->eurPortfolioEquity->setStyleSheet("color: blue");
    this->ui->eurPortfolioSessionSummaryInitialCapital->setStyleSheet("color: blue");
    this->ui->eurPortfolioSessionSummaryCurrentCapital->setStyleSheet("color: blue");
    this->ui->eurPortfolioSessionSummaryTotalEquity->setStyleSheet("color: blue");

    double eur_portfolio_realised_pnl=this->ui->eurPortfolioRealisedPnL->value();
    double eur_portfolio_unrealised_pnl=this->ui->eurPortfolioUnrealisedPnL->value();
    double eur_portfolio_return_on_investment=this->ui->eurPortfolioSessionSummaryReturnOnInvestment->value();

    if (eur_portfolio_realised_pnl>0.0)
    {
        this->ui->eurPortfolioRealisedPnL->setStyleSheet("color: green");
        this->ui->eurPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: green");
    }

    if (eur_portfolio_realised_pnl==0.0)
    {
        this->ui->eurPortfolioRealisedPnL->setStyleSheet("color: orange");
        this->ui->eurPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: orange");
    }

    if (eur_portfolio_realised_pnl<0.0)
    {
        this->ui->eurPortfolioRealisedPnL->setStyleSheet("color: red");
        this->ui->eurPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: red");
    }

    if (eur_portfolio_return_on_investment>0.0) { this->ui->eurPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: green"); }
    if (eur_portfolio_return_on_investment==0.0) { this->ui->eurPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: orange"); }
    if (eur_portfolio_return_on_investment<0.0) { this->ui->eurPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: red"); }

    if (eur_portfolio_unrealised_pnl>0.0) { this->ui->eurPortfolioUnrealisedPnL->setStyleSheet("color: green"); }
    if (eur_portfolio_unrealised_pnl==0.0) { this->ui->eurPortfolioUnrealisedPnL->setStyleSheet("color: orange"); }
    if (eur_portfolio_unrealised_pnl<0.0) { this->ui->eurPortfolioUnrealisedPnL->setStyleSheet("color: red"); }

    this->ui->usdMarketsTable->changeToLightMode();
    this->ui->btcMarketsTable->changeToLightMode();
    this->ui->ethMarketsTable->changeToLightMode();
    this->ui->usdtMarketsTable->changeToLightMode();
    this->ui->eurMarketsTable->changeToLightMode();

    this->ui->openOrdersTable->changeToLightMode();
    this->ui->closedOrdersTable->changeToLightMode();

    this->ui->usdOpenPositionsTable->changeToLightMode();
    this->ui->usdClosedPositionsTable->changeToLightMode();

    this->ui->btcOpenPositionsTable->changeToLightMode();
    this->ui->btcClosedPositionsTable->changeToLightMode();

    this->ui->ethOpenPositionsTable->changeToLightMode();
    this->ui->ethClosedPositionsTable->changeToLightMode();

    this->ui->usdtOpenPositionsTable->changeToLightMode();
    this->ui->usdtClosedPositionsTable->changeToLightMode();

    this->ui->eurOpenPositionsTable->changeToLightMode();
    this->ui->eurClosedPositionsTable->changeToLightMode();

    this->ui->usdPortfolioSessionSummaryChartView->setStyleSheet("background-color: rgba(0, 170, 255, 50)");
    this->ui->btcPortfolioSessionSummaryChartView->setStyleSheet("background-color: rgba(0, 170, 255, 50)");
    this->ui->ethPortfolioSessionSummaryChartView->setStyleSheet("background-color: rgba(0, 170, 255, 50)");
    this->ui->usdtPortfolioSessionSummaryChartView->setStyleSheet("background-color: rgba(0, 170, 255, 50)");
    this->ui->eurPortfolioSessionSummaryChartView->setStyleSheet("background-color: rgba(0, 170, 255, 50)");

    this->setColorMode(MainWindow::ColorMode::LIGHT);
    this->changeToLightModeCharts();
    this->changeToLightModeIcons();
    this->getApplicationSettings()->setDarkMode(false);
    this->getApplicationSettings()->setLightMode(true);
}


void MainWindow::changeToDarkMode()
{
    QFile file=QFile(":themes/dark_mode/dark_mode.css");
    file.open(QFile::ReadOnly);
    QString style_sheet=QLatin1String(file.readAll());
    this->setStyleSheet(QString());
    this->setStyleSheet(style_sheet);

    this->ui->usdPortfolioInitialCapital->setStyleSheet("color: cyan");
    this->ui->usdPortfolioCurrentCapital->setStyleSheet("color: cyan");
    this->ui->usdPortfolioEquity->setStyleSheet("color: cyan");
    this->ui->usdPortfolioSessionSummaryInitialCapital->setStyleSheet("color: cyan");
    this->ui->usdPortfolioSessionSummaryCurrentCapital->setStyleSheet("color: cyan");
    this->ui->usdPortfolioSessionSummaryTotalEquity->setStyleSheet("color: cyan");

    double usd_portfolio_realised_pnl=this->ui->usdPortfolioRealisedPnL->value();
    double usd_portfolio_unrealised_pnl=this->ui->usdPortfolioUnrealisedPnL->value();
    double usd_portfolio_return_on_investment=this->ui->usdPortfolioSessionSummaryReturnOnInvestment->value();

    if (usd_portfolio_realised_pnl>0.0)
    {
        this->ui->usdPortfolioRealisedPnL->setStyleSheet("color: lime");
        this->ui->usdPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: lime");
    }

    if (usd_portfolio_realised_pnl==0.0)
    {
        this->ui->usdPortfolioRealisedPnL->setStyleSheet("color: yellow");
        this->ui->usdPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: yellow");
    }

    if (usd_portfolio_realised_pnl<0.0)
    {
        this->ui->usdPortfolioRealisedPnL->setStyleSheet("color: orange");
        this->ui->usdPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: orange");
    }

    if (usd_portfolio_return_on_investment>0.0) { this->ui->usdPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: lime"); }
    if (usd_portfolio_return_on_investment==0.0) { this->ui->usdPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: yellow"); }
    if (usd_portfolio_return_on_investment<0.0) { this->ui->usdPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: orange"); }

    if (usd_portfolio_unrealised_pnl>0.0) { this->ui->usdPortfolioUnrealisedPnL->setStyleSheet("color: lime"); }
    if (usd_portfolio_unrealised_pnl==0.0) { this->ui->usdPortfolioUnrealisedPnL->setStyleSheet("color: yellow"); }
    if (usd_portfolio_unrealised_pnl<0.0) { this->ui->usdPortfolioUnrealisedPnL->setStyleSheet("color: orange"); }

    this->ui->btcPortfolioInitialCapital->setStyleSheet("color: cyan");
    this->ui->btcPortfolioCurrentCapital->setStyleSheet("color: cyan");
    this->ui->btcPortfolioEquity->setStyleSheet("color: cyan");
    this->ui->btcPortfolioSessionSummaryInitialCapital->setStyleSheet("color: cyan");
    this->ui->btcPortfolioSessionSummaryCurrentCapital->setStyleSheet("color: cyan");
    this->ui->btcPortfolioSessionSummaryTotalEquity->setStyleSheet("color: cyan");

    double bitcoin_portfolio_realised_pnl=this->ui->btcPortfolioRealisedPnL->value();
    double bitcoin_portfolio_unrealised_pnl=this->ui->btcPortfolioUnrealisedPnL->value();
    double bitcoin_portfolio_return_on_investment=this->ui->btcPortfolioSessionSummaryReturnOnInvestment->value();

    if (bitcoin_portfolio_realised_pnl>0.0)
    {
        this->ui->btcPortfolioRealisedPnL->setStyleSheet("color: lime");
        this->ui->btcPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: lime");
    }

    if (bitcoin_portfolio_realised_pnl==0.0)
    {
        this->ui->btcPortfolioRealisedPnL->setStyleSheet("color: yellow");
        this->ui->btcPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: yellow");
    }

    if (bitcoin_portfolio_realised_pnl<0.0)
    {
        this->ui->btcPortfolioRealisedPnL->setStyleSheet("color: orange");
        this->ui->btcPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: orange");
    }

    if (bitcoin_portfolio_return_on_investment>0.0) { this->ui->btcPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: lime"); }
    if (bitcoin_portfolio_return_on_investment==0.0) { this->ui->btcPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: yellow"); }
    if (bitcoin_portfolio_return_on_investment<0.0) { this->ui->btcPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: orange"); }

    if (bitcoin_portfolio_unrealised_pnl>0.0) { this->ui->btcPortfolioUnrealisedPnL->setStyleSheet("color: lime"); }
    if (bitcoin_portfolio_unrealised_pnl==0.0) { this->ui->btcPortfolioUnrealisedPnL->setStyleSheet("color: yellow"); }
    if (bitcoin_portfolio_unrealised_pnl<0.0) { this->ui->btcPortfolioUnrealisedPnL->setStyleSheet("color: orange"); }

    this->ui->ethPortfolioInitialCapital->setStyleSheet("color: cyan");
    this->ui->ethPortfolioCurrentCapital->setStyleSheet("color: cyan");
    this->ui->ethPortfolioEquity->setStyleSheet("color: cyan");
    this->ui->ethPortfolioSessionSummaryInitialCapital->setStyleSheet("color: cyan");
    this->ui->ethPortfolioSessionSummaryCurrentCapital->setStyleSheet("color: cyan");
    this->ui->ethPortfolioSessionSummaryTotalEquity->setStyleSheet("color: cyan");

    double ethereum_portfolio_realised_pnl=this->ui->ethPortfolioRealisedPnL->value();
    double ethereum_portfolio_unrealised_pnl=this->ui->ethPortfolioUnrealisedPnL->value();
    double ethereum_portfolio_return_on_investment=this->ui->ethPortfolioSessionSummaryReturnOnInvestment->value();

    if (ethereum_portfolio_realised_pnl>0.0)
    {
        this->ui->ethPortfolioRealisedPnL->setStyleSheet("color: lime");
        this->ui->ethPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: lime");
    }

    if (ethereum_portfolio_realised_pnl==0.0)
    {
        this->ui->ethPortfolioRealisedPnL->setStyleSheet("color: yellow");
        this->ui->ethPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: yellow");
    }

    if (ethereum_portfolio_realised_pnl<0.0)
    {
        this->ui->ethPortfolioRealisedPnL->setStyleSheet("color: orange");
        this->ui->ethPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: orange");
    }

    if (ethereum_portfolio_return_on_investment>0.0) { this->ui->ethPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: lime"); }
    if (ethereum_portfolio_return_on_investment==0.0) { this->ui->ethPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: yellow"); }
    if (ethereum_portfolio_return_on_investment<0.0) { this->ui->ethPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: orange"); }

    if (ethereum_portfolio_unrealised_pnl>0.0) { this->ui->ethPortfolioUnrealisedPnL->setStyleSheet("color: lime"); }
    if (ethereum_portfolio_unrealised_pnl==0.0) { this->ui->ethPortfolioUnrealisedPnL->setStyleSheet("color: yellow"); }
    if (ethereum_portfolio_unrealised_pnl<0.0) { this->ui->ethPortfolioUnrealisedPnL->setStyleSheet("color: orange"); }

    this->ui->usdtPortfolioInitialCapital->setStyleSheet("color: cyan");
    this->ui->usdtPortfolioCurrentCapital->setStyleSheet("color: cyan");
    this->ui->usdtPortfolioEquity->setStyleSheet("color: cyan");
    this->ui->usdtPortfolioSessionSummaryInitialCapital->setStyleSheet("color: cyan");
    this->ui->usdtPortfolioSessionSummaryCurrentCapital->setStyleSheet("color: cyan");
    this->ui->usdtPortfolioSessionSummaryTotalEquity->setStyleSheet("color: cyan");

    double usdt_portfolio_realised_pnl=this->ui->usdtPortfolioRealisedPnL->value();
    double usdt_portfolio_unrealised_pnl=this->ui->usdtPortfolioUnrealisedPnL->value();
    double usdt_portfolio_return_on_investment=this->ui->usdtPortfolioSessionSummaryReturnOnInvestment->value();

    if (usdt_portfolio_realised_pnl>0.0)
    {
        this->ui->usdtPortfolioRealisedPnL->setStyleSheet("color: lime");
        this->ui->usdtPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: lime");
    }

    if (usdt_portfolio_realised_pnl==0.0)
    {
        this->ui->usdtPortfolioRealisedPnL->setStyleSheet("color: yellow");
        this->ui->usdtPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: yellow");
    }

    if (usdt_portfolio_realised_pnl<0.0)
    {
        this->ui->usdtPortfolioRealisedPnL->setStyleSheet("color: orange");
        this->ui->usdtPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: orange");
    }

    if (usdt_portfolio_return_on_investment>0.0) { this->ui->usdtPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: lime"); }
    if (usdt_portfolio_return_on_investment==0.0) { this->ui->usdtPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: yellow"); }
    if (usdt_portfolio_return_on_investment<0.0) { this->ui->usdtPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: orange"); }

    if (usdt_portfolio_unrealised_pnl>0.0) { this->ui->usdtPortfolioUnrealisedPnL->setStyleSheet("color: lime"); }
    if (usdt_portfolio_unrealised_pnl==0.0) { this->ui->usdtPortfolioUnrealisedPnL->setStyleSheet("color: yellow"); }
    if (usdt_portfolio_unrealised_pnl<0.0) { this->ui->usdtPortfolioUnrealisedPnL->setStyleSheet("color: orange"); }

    this->ui->eurPortfolioInitialCapital->setStyleSheet("color: cyan");
    this->ui->eurPortfolioCurrentCapital->setStyleSheet("color: cyan");
    this->ui->eurPortfolioEquity->setStyleSheet("color: cyan");
    this->ui->eurPortfolioSessionSummaryInitialCapital->setStyleSheet("color: cyan");
    this->ui->eurPortfolioSessionSummaryCurrentCapital->setStyleSheet("color: cyan");
    this->ui->eurPortfolioSessionSummaryTotalEquity->setStyleSheet("color: cyan");

    double eur_portfolio_realised_pnl=this->ui->eurPortfolioRealisedPnL->value();
    double eur_portfolio_unrealised_pnl=this->ui->eurPortfolioUnrealisedPnL->value();
    double eur_portfolio_return_on_investment=this->ui->eurPortfolioSessionSummaryReturnOnInvestment->value();

    if (eur_portfolio_realised_pnl>0.0)
    {
        this->ui->eurPortfolioRealisedPnL->setStyleSheet("color: lime");
        this->ui->eurPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: lime");
    }

    if (eur_portfolio_realised_pnl==0.0)
    {
        this->ui->eurPortfolioRealisedPnL->setStyleSheet("color: yellow");
        this->ui->eurPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: yellow");
    }

    if (eur_portfolio_realised_pnl<0.0)
    {
        this->ui->eurPortfolioRealisedPnL->setStyleSheet("color: orange");
        this->ui->eurPortfolioSessionSummaryRealisedPnL->setStyleSheet("color: orange");
    }

    if (eur_portfolio_return_on_investment>0.0) { this->ui->eurPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: lime"); }
    if (eur_portfolio_return_on_investment==0.0) { this->ui->eurPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: yellow"); }
    if (eur_portfolio_return_on_investment<0.0) { this->ui->eurPortfolioSessionSummaryReturnOnInvestment->setStyleSheet("color: orange"); }

    if (eur_portfolio_unrealised_pnl>0.0) { this->ui->eurPortfolioUnrealisedPnL->setStyleSheet("color: lime"); }
    if (eur_portfolio_unrealised_pnl==0.0) { this->ui->eurPortfolioUnrealisedPnL->setStyleSheet("color: yellow"); }
    if (eur_portfolio_unrealised_pnl<0.0) { this->ui->eurPortfolioUnrealisedPnL->setStyleSheet("color: orange"); }

    this->ui->usdMarketsTable->changeToDarkMode();
    this->ui->btcMarketsTable->changeToDarkMode();
    this->ui->ethMarketsTable->changeToDarkMode();
    this->ui->usdtMarketsTable->changeToDarkMode();
    this->ui->eurMarketsTable->changeToDarkMode();

    this->ui->openOrdersTable->changeToDarkMode();
    this->ui->closedOrdersTable->changeToDarkMode();

    this->ui->usdOpenPositionsTable->changeToDarkMode();
    this->ui->usdClosedPositionsTable->changeToDarkMode();

    this->ui->btcOpenPositionsTable->changeToDarkMode();
    this->ui->btcClosedPositionsTable->changeToDarkMode();

    this->ui->ethOpenPositionsTable->changeToDarkMode();
    this->ui->ethClosedPositionsTable->changeToDarkMode();

    this->ui->usdtOpenPositionsTable->changeToDarkMode();
    this->ui->usdtClosedPositionsTable->changeToDarkMode();

    this->ui->eurOpenPositionsTable->changeToDarkMode();
    this->ui->eurClosedPositionsTable->changeToDarkMode();

    this->setColorMode(MainWindow::ColorMode::DARK);
    this->changeToDarkModeCharts();
    this->changeToDarkModeIcons();

    this->getApplicationSettings()->setDarkMode(true);
    this->getApplicationSettings()->setLightMode(false);
}

void MainWindow::changeToLightModeIcons()
{
    QList<QAction *> actions=this->ui->mainToolBar->actions();
    actions.at(0)->setIcon(QIcon(":/icons/light_mode/resume_icon.png"));
    actions.at(1)->setIcon(QIcon(":/icons/light_mode/pause_icon.png"));
    actions.at(2)->setIcon(QIcon(":/icons/light_mode/stop_icon.png"));
    actions.at(3)->setIcon(QIcon(":/icons/light_mode/logger_icon.png"));
    actions.at(4)->setIcon(QIcon(":/icons/light_mode/light_mode_icon.png"));
    actions.at(5)->setIcon(QIcon(":/icons/light_mode/dark_mode_icon.png"));
    actions.at(6)->setIcon(QIcon(":/icons/light_mode/language_translator_icon.png"));
    actions.at(7)->setIcon(QIcon(":/icons/light_mode/tray_icon.png"));

    this->ui->analyticsButton->setIcon(QIcon(":/icons/light_mode/analytics_icon.png"));
    this->ui->marketsButton->setIcon(QIcon(":/icons/light_mode/markets_icon.png"));
    this->ui->traderButton->setIcon(QIcon(":/icons/light_mode/trader_icon.png"));
    this->ui->executionButton->setIcon(QIcon(":/icons/light_mode/execution_icon.png"));
    this->ui->portfoliosButton->setIcon(QIcon(":/icons/light_mode/portfolios_icon.png"));
    this->ui->settingsButton->setIcon(QIcon(":/icons/light_mode/settings_icon.png"));
    this->ui->homeButton->setIcon(QIcon(":/icons/light_mode/back_icon.png"));

    this->ui->analyticsTabWidget->setTabIcon(0,QIcon(":/icons/light_mode/summary_icon.png"));
    this->ui->analyticsTabWidget->setTabIcon(1,QIcon(":/icons/light_mode/technical_analysis_icon.png"));

    this->ui->strategiesTree->headerItem()->setIcon(0,QIcon(":/icons/light_mode/strategy_icon.png"));
    this->ui->strategiesTree->headerItem()->setIcon(1,QIcon(":/icons/light_mode/description_icon.png"));
    this->ui->strategiesTree->findItems(tr("Filter Rule"),Qt::MatchExactly).at(0)->setIcon(0,QIcon(":/icons/light_mode/filter_rule_icon.png"));

    this->ui->risksTree->headerItem()->setIcon(0,QIcon(":/icons/light_mode/risk_icon.png"));
    this->ui->risksTree->headerItem()->setIcon(1,QIcon(":/icons/light_mode/description_icon.png"));
    this->ui->risksTree->headerItem()->setIcon(2,QIcon(":/icons/light_mode/risk_indicator_icon.png"));
    this->ui->risksTree->findItems(tr("Stop Loss"),Qt::MatchExactly).at(0)->setIcon(0,QIcon(":/icons/light_mode/stop_loss_icon.png"));
    this->ui->risksTree->findItems(tr("Take Profit"),Qt::MatchExactly).at(0)->setIcon(0,QIcon(":/icons/light_mode/take_profit_icon.png"));

    QList<QTreeWidgetItem *> lowRiskItems=this->ui->risksTree->findItems(tr("Low Risk"),Qt::MatchExactly | Qt::MatchRecursive,2);
    for (int i=0;i<lowRiskItems.count();i++) { lowRiskItems.at(i)->setIcon(2,QIcon(":/icons/light_mode/low_risk_icon.png")); }

    QList<QTreeWidgetItem *> mediumRiskItems=this->ui->risksTree->findItems(tr("Medium Risk"),Qt::MatchExactly | Qt::MatchRecursive,2);
    for (int i=0;i<mediumRiskItems.count();i++) { mediumRiskItems.at(i)->setIcon(2,QIcon(":/icons/light_mode/medium_risk_icon.png")); }

    QList<QTreeWidgetItem *> highRiskItems=this->ui->risksTree->findItems(tr("High Risk"),Qt::MatchExactly | Qt::MatchRecursive,2);
    for (int i=0;i<highRiskItems.count();i++) { highRiskItems.at(i)->setIcon(2,QIcon(":/icons/light_mode/high_risk_icon.png")); }
}


void MainWindow::changeToDarkModeIcons()
{
    QList<QAction *> actions=this->ui->mainToolBar->actions();
    actions.at(0)->setIcon(QIcon(":/icons/dark_mode/resume_icon.png"));
    actions.at(1)->setIcon(QIcon(":/icons/dark_mode/pause_icon.png"));
    actions.at(2)->setIcon(QIcon(":/icons/dark_mode/stop_icon.png"));
    actions.at(3)->setIcon(QIcon(":/icons/dark_mode/logger_icon.png"));
    actions.at(4)->setIcon(QIcon(":/icons/dark_mode/light_mode_icon.png"));
    actions.at(5)->setIcon(QIcon(":/icons/dark_mode/dark_mode_icon.png"));
    actions.at(6)->setIcon(QIcon(":/icons/dark_mode/language_translator_icon.png"));
    actions.at(7)->setIcon(QIcon(":/icons/dark_mode/tray_icon.png"));

    this->ui->analyticsButton->setIcon(QIcon(":/icons/dark_mode/analytics_icon.png"));
    this->ui->marketsButton->setIcon(QIcon(":/icons/dark_mode/markets_icon.png"));
    this->ui->traderButton->setIcon(QIcon(":/icons/dark_mode/trader_icon.png"));
    this->ui->executionButton->setIcon(QIcon(":/icons/dark_mode/execution_icon.png"));
    this->ui->portfoliosButton->setIcon(QIcon(":/icons/dark_mode/portfolios_icon.png"));
    this->ui->settingsButton->setIcon(QIcon(":/icons/dark_mode/settings_icon.png"));
    this->ui->homeButton->setIcon(QIcon(":/icons/dark_mode/back_icon.png"));

    this->ui->analyticsTabWidget->setTabIcon(0,QIcon(":/icons/dark_mode/summary_icon.png"));
    this->ui->analyticsTabWidget->setTabIcon(1,QIcon(":/icons/dark_mode/technical_analysis_icon.png"));

    this->ui->strategiesTree->headerItem()->setIcon(0,QIcon(":/icons/dark_mode/strategy_icon.png"));
    this->ui->strategiesTree->headerItem()->setIcon(1,QIcon(":/icons/dark_mode/description_icon.png"));
    this->ui->strategiesTree->findItems(tr("Filter Rule"),Qt::MatchExactly).at(0)->setIcon(0,QIcon(":/icons/dark_mode/filter_rule_icon.png"));

    this->ui->risksTree->headerItem()->setIcon(0,QIcon(":/icons/dark_mode/risk_icon.png"));
    this->ui->risksTree->headerItem()->setIcon(1,QIcon(":/icons/dark_mode/description_icon.png"));
    this->ui->risksTree->headerItem()->setIcon(2,QIcon(":/icons/dark_mode/risk_indicator_icon.png"));
    this->ui->risksTree->findItems(tr("Stop Loss"),Qt::MatchExactly).at(0)->setIcon(0,QIcon(":/icons/dark_mode/stop_loss_icon.png"));
    this->ui->risksTree->findItems(tr("Take Profit"),Qt::MatchExactly).at(0)->setIcon(0,QIcon(":/icons/dark_mode/take_profit_icon.png"));

    QList<QTreeWidgetItem *> lowRiskItems=this->ui->risksTree->findItems(tr("Low Risk"),Qt::MatchExactly | Qt::MatchRecursive,2);
    for (int i=0;i<lowRiskItems.count();i++) { lowRiskItems.at(i)->setIcon(2,QIcon(":/icons/dark_mode/low_risk_icon.png")); }

    QList<QTreeWidgetItem *> mediumRiskItems=this->ui->risksTree->findItems(tr("Medium Risk"),Qt::MatchExactly | Qt::MatchRecursive,2);
    for (int i=0;i<mediumRiskItems.count();i++) { mediumRiskItems.at(i)->setIcon(2,QIcon(":/icons/dark_mode/medium_risk_icon.png")); }

    QList<QTreeWidgetItem *> highRiskItems=this->ui->risksTree->findItems(tr("High Risk"),Qt::MatchExactly | Qt::MatchRecursive,2);
    for (int i=0;i<highRiskItems.count();i++) { highRiskItems.at(i)->setIcon(2,QIcon(":/icons/dark_mode/high_risk_icon.png")); }
}


void MainWindow::changeToLightModeCharts()
{
    this->ui->marketChart->setBackground(QColor("white"));
    this->ui->usdPortfolioSessionSummaryChartView->changeToLightMode();
    this->ui->btcPortfolioSessionSummaryChartView->changeToLightMode();
    this->ui->ethPortfolioSessionSummaryChartView->changeToLightMode();
    this->ui->usdtPortfolioSessionSummaryChartView->changeToLightMode();
    this->ui->eurPortfolioSessionSummaryChartView->changeToLightMode();
}


void MainWindow::changeToDarkModeCharts()
{
    this->ui->marketChart->setBackground(QColor("#3d79cc"));
    this->ui->usdPortfolioSessionSummaryChartView->changeToDarkMode();
    this->ui->btcPortfolioSessionSummaryChartView->changeToDarkMode();
    this->ui->ethPortfolioSessionSummaryChartView->changeToDarkMode();
    this->ui->usdtPortfolioSessionSummaryChartView->changeToDarkMode();
    this->ui->eurPortfolioSessionSummaryChartView->changeToDarkMode();
}

void MainWindow::toggleLogger(bool toggled)
{
    if (toggled==false)
    {
        this->disconnect(this->bittrexRestApiHandler,nullptr,this->ui->logger,nullptr);
        this->ui->logger->hide();
        this->getApplicationSettings()->setLogger(false);
    }

    if (toggled==true)
    {
        this->disconnect(this->bittrexRestApiHandler,nullptr,this->ui->logger,nullptr);
        this->initializeLogger();
        this->ui->logger->show();
        this->getApplicationSettings()->setLogger(true);
    }
}

void MainWindow::showGeneralInformation(const QString &information)
{
    this->ui->statusBar->showMessage(information);
}

void MainWindow::showLanguageSelector()
{
    this->getLanguageSelectorDialog()->show();
}

void MainWindow::initializeQDoubleSpinBoxs()
{
    this->ui->selectedUsdMarketsCapital->setMaximum(1000000000.000000);
    this->ui->selectedUsdMarketsCapital->setValue(0.0);
    this->ui->selectedUsdMarketsCapital->setSingleStep(1.0);

    this->ui->selectedBtcMarketsCapital->setMaximum(1000000000.00000);
    this->ui->selectedBtcMarketsCapital->setValue(0.0);
    this->ui->selectedBtcMarketsCapital->setSingleStep(1.0);

    this->ui->selectedEthMarketsCapital->setMaximum(1000000000.00000);
    this->ui->selectedEthMarketsCapital->setValue(0.0);
    this->ui->selectedEthMarketsCapital->setSingleStep(1.0);

    this->ui->selectedUsdtMarketsCapital->setMaximum(1000000000.00000);
    this->ui->selectedUsdtMarketsCapital->setValue(0.0);
    this->ui->selectedUsdtMarketsCapital->setSingleStep(1.0);

    this->ui->selectedEurMarketsCapital->setMaximum(1000000000.00000);
    this->ui->selectedEurMarketsCapital->setValue(0.0);
    this->ui->selectedEurMarketsCapital->setSingleStep(1.0);
}



void MainWindow::initializeQToolBars()
{
    this->addToolBar(Qt::RightToolBarArea,this->ui->mainToolBar);
}

void MainWindow::initializeQActions()
{
    QActionGroup *session_management_action_group=new QActionGroup(this);
    QActionGroup *theme_mode_action_group=new QActionGroup(this);
    QList<QAction *> actions=this->ui->mainToolBar->actions();

    actions.at(0)->setEnabled(false);
    actions.at(1)->setEnabled(false);
    actions.at(2)->setEnabled(false);
    session_management_action_group->addAction(actions.at(0));
    session_management_action_group->addAction(actions.at(1));
    session_management_action_group->addAction(actions.at(2));
    session_management_action_group->setExclusive(true);

    theme_mode_action_group->addAction(actions.at(4));
    theme_mode_action_group->addAction(actions.at(5));
    theme_mode_action_group->setExclusive(true);
}


void MainWindow::initializeQListWidgets()
{
    this->ui->marketsList->setColorMode(MarketList::ColorMode::LIGHT);
}

void MainWindow::initializeQTabWidgets()
{

}

void MainWindow::initializeQTableWidgets()
{
    this->ui->btcMarketsTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    this->ui->usdMarketsTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    this->ui->usdtMarketsTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    this->ui->ethMarketsTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    this->ui->eurMarketsTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    this->ui->usdOpenPositionsTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    this->ui->usdClosedPositionsTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    this->ui->btcOpenPositionsTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    this->ui->btcClosedPositionsTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    this->ui->ethOpenPositionsTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    this->ui->ethClosedPositionsTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    this->ui->usdtOpenPositionsTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    this->ui->usdtClosedPositionsTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    this->ui->eurOpenPositionsTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    this->ui->eurClosedPositionsTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    this->ui->selectedUsdMarketsTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    this->ui->selectedBtcMarketsTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    this->ui->selectedEthMarketsTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    this->ui->selectedUsdtMarketsTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    this->ui->selectedEurMarketsTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    this->ui->openOrdersTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    this->ui->closedOrdersTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    this->ui->usdOpenPositionsTable->setBaseCurrency(OpenPositionTable::BaseCurrency::USD_CURRENCY);
    this->ui->ethOpenPositionsTable->setBaseCurrency(OpenPositionTable::BaseCurrency::ETH_CURRENCY);
    this->ui->btcOpenPositionsTable->setBaseCurrency(OpenPositionTable::BaseCurrency::BTC_CURRENCY);
    this->ui->usdtOpenPositionsTable->setBaseCurrency(OpenPositionTable::BaseCurrency::USDT_CURRENCY);
    this->ui->eurOpenPositionsTable->setBaseCurrency(OpenPositionTable::BaseCurrency::EUR_CURRENCY);
}


void MainWindow::initializeQTreeWidgets()
{
    this->ui->strategiesTree->header()->setSectionResizeMode(0,QHeaderView::Stretch);
    this->ui->strategiesTree->header()->setSectionResizeMode(1,QHeaderView::Stretch);

    this->ui->risksTree->header()->setSectionResizeMode(0,QHeaderView::Stretch);
    this->ui->risksTree->header()->setSectionResizeMode(1,QHeaderView::Stretch);
    this->ui->risksTree->header()->setSectionResizeMode(2,QHeaderView::Stretch);
}


void MainWindow::initializeQChartViews()
{
    this->ui->usdPortfolioSessionSummaryChartView->setRenderHint(QPainter::Antialiasing);
    this->ui->btcPortfolioSessionSummaryChartView->setRenderHint(QPainter::Antialiasing);
    this->ui->ethPortfolioSessionSummaryChartView->setRenderHint(QPainter::Antialiasing);
    this->ui->usdtPortfolioSessionSummaryChartView->setRenderHint(QPainter::Antialiasing);
    this->ui->eurPortfolioSessionSummaryChartView->setRenderHint(QPainter::Antialiasing);
}


void MainWindow::on_verificationButton_clicked()
{
    this->setTradingMode(MainWindow::TradingMode::LIVE_TRADING);
    this->showGeneralInformation("bittrexbot version: v2.0, bittrex API v3.0, trading mode: live trading");
    QString publicKey=this->ui->bittrexPublicKeyInput->text();
    QString privateKey=this->ui->bittrexPrivateKeyInput->text();
    this->getBittrexRestApiHandler()->setPublicKey(publicKey);
    this->getBittrexRestApiHandler()->setPrivateKey(privateKey);
    this->getBittrexRestApiHandler()->queryBalances();
    this->ui->stackedWidget->setCurrentIndex(1);
}

void MainWindow::on_noVerificationButton_clicked()
{
    this->setTradingMode(MainWindow::TradingMode::PAPER_TRADING);
    this->showGeneralInformation("bittrexbot version: v2.0, bittrex API v3.0, trading mode: paper trading");
    this->getBittrexRestApiHandler()->setPublicKey(QString());
    this->getBittrexRestApiHandler()->setPrivateKey(QString());
    this->initializeQDoubleSpinBoxs();
    this->ui->stackedWidget->setCurrentIndex(1);
}

void MainWindow::on_homeButton_clicked()
{
    this->setTradingMode(MainWindow::TradingMode::PAPER_TRADING);
    this->ui->stackedWidget->setCurrentIndex(0);
    this->ui->statusBar->showMessage(QString());
}


void MainWindow::on_marketsList_itemClicked(QListWidgetItem *item)
{
    // TODO: implement later.
    Q_UNUSED(item)
}



void MainWindow::marketSummariesReceived(const QJsonArray &result)
{
    foreach (const QJsonValue &value, result)
    {
        QJsonObject current_object=value.toObject();
        QString market_name=current_object.value("symbol").toString();
        bool contains_market_name=this->ui->marketsList->getMap()->contains(market_name);
        if (contains_market_name==false) { continue; }
        QListWidgetItem *current_list_widget_item=this->ui->marketsList->getMap()->value(market_name);
        MarketListItem *current_market_list_item=qobject_cast<MarketListItem *>(this->ui->marketsList->itemWidget(current_list_widget_item));
        current_market_list_item->setMarketSummaryData(current_object);
        this->ui->marketsList->modifyListItem(market_name,current_object);
        QString quote_currency_symbol=current_market_list_item->getMarketData().value("quoteCurrencySymbol").toString();
        if (quote_currency_symbol==QString("BTC")) { this->ui->btcMarketsTable->modifyTableItems(market_name,current_object); }
        if (quote_currency_symbol==QString("USD")) { this->ui->usdMarketsTable->modifyTableItems(market_name,current_object); }
        if (quote_currency_symbol==QString("ETH")) { this->ui->ethMarketsTable->modifyTableItems(market_name,current_object); }
        if (quote_currency_symbol==QString("USDT")) { this->ui->usdtMarketsTable->modifyTableItems(market_name,current_object); }
        if (quote_currency_symbol==QString("EUR")) { this->ui->eurMarketsTable->modifyTableItems(market_name,current_object); }
    }
}

void MainWindow::marketTickersReceived(const QJsonArray &result)
{
    foreach (const QJsonValue &value, result)
    {
        QJsonObject current_object=value.toObject();
        QString market_name=current_object.value("symbol").toString();
        bool contains_market_name=this->ui->marketsList->getMap()->contains(market_name);
        if (contains_market_name==false) { continue; }
        QListWidgetItem *current_list_widget_item=this->ui->marketsList->getMap()->value(market_name);
        MarketListItem *current_market_list_item=qobject_cast<MarketListItem *>(this->ui->marketsList->itemWidget(current_list_widget_item));
        current_market_list_item->setMarketTickerData(current_object);
        this->ui->marketsList->modifyListItem(market_name,current_object);
        QString quote_currency_symbol=current_market_list_item->getMarketData().value("quoteCurrencySymbol").toString();
        if (quote_currency_symbol==QString("BTC")) { this->ui->btcMarketsTable->modifyTableItems(market_name,current_object); }
        if (quote_currency_symbol==QString("USD")) { this->ui->usdMarketsTable->modifyTableItems(market_name,current_object); }
        if (quote_currency_symbol==QString("ETH")) { this->ui->ethMarketsTable->modifyTableItems(market_name,current_object); }
        if (quote_currency_symbol==QString("USDT")) { this->ui->usdtMarketsTable->modifyTableItems(market_name,current_object); }
        if (quote_currency_symbol==QString("EUR")) { this->ui->eurMarketsTable->modifyTableItems(market_name,current_object); }
    }
}

void MainWindow::marketReceived(const QString &market,const QJsonObject &data)
{
    bool contains_asset=this->getTrader()->getAssetManager()->getAssets()->contains(market);
    if (contains_asset==true)
    {
        BittrexAsset *bittrex_asset=static_cast<BittrexAsset *>(this->getTrader()->getAssetManager()->getAsset(market));
        bittrex_asset->setPrecision(data.value("precision").toInt());
    }
}

void MainWindow::marketsReceived(const QJsonArray &result)
{
    foreach (const QJsonValue &value, result)
    {
        QJsonObject current_object=value.toObject();
        QListWidgetComparableItem *item=new QListWidgetComparableItem(this->ui->marketsList);
        item->setSizeHint(QSize(10,25));
        MarketListItem *marketListItem=new MarketListItem(this);
        marketListItem->setMarketName(current_object.value("symbol").toString());
        marketListItem->setPercentChange(QString::number(0)+"%");
        marketListItem->setMarketData(current_object);
        this->ui->marketsList->addItem(item);
        this->ui->marketsList->setItemWidget(item,marketListItem);
        QString marketName=current_object.value("symbol").toString();
        this->ui->marketsList->getMap()->insert(marketName,item);
        QString quote_currency_symbol=current_object.value("quoteCurrencySymbol").toString();
        if (quote_currency_symbol==QString("BTC")) { this->ui->btcMarketsTable->insertTableItems(marketName,current_object); }
        if (quote_currency_symbol==QString("USD")) { this->ui->usdMarketsTable->insertTableItems(marketName,current_object); }
        if (quote_currency_symbol==QString("ETH")) { this->ui->ethMarketsTable->insertTableItems(marketName,current_object); }
        if (quote_currency_symbol==QString("USDT")) { this->ui->usdtMarketsTable->insertTableItems(marketName,current_object); }
        if (quote_currency_symbol==QString("EUR")) { this->ui->eurMarketsTable->insertTableItems(marketName,current_object); }
    }
}

void MainWindow::balancesReceived(const QJsonArray &result)
{
    foreach (const QJsonValue &value, result)
    {
        double incremental_step=0.0;
        QJsonObject current_object=value.toObject();
        QString currency=current_object.value("currencySymbol").toString();
        double availableBalance=current_object.value("available").toString().toDouble();

        if (currency==QString("USD"))
        {
            this->ui->selectedUsdMarketsCapital->setMaximum(availableBalance);
            this->ui->selectedUsdMarketsCapital->setValue(availableBalance);
            incremental_step=availableBalance/100000.0;
            this->ui->selectedUsdMarketsCapital->setSingleStep(incremental_step);
        }

        if (currency==QString("BTC"))
        {
            this->ui->selectedBtcMarketsCapital->setMaximum(availableBalance);
            this->ui->selectedBtcMarketsCapital->setValue(availableBalance);
            incremental_step=availableBalance/100000.0;
            this->ui->selectedBtcMarketsCapital->setSingleStep(incremental_step);
        }

        if (currency==QString("ETH"))
        {
            this->ui->selectedEthMarketsCapital->setMaximum(availableBalance);
            this->ui->selectedEthMarketsCapital->setValue(availableBalance);
            incremental_step=availableBalance/100000.0;
            this->ui->selectedEthMarketsCapital->setSingleStep(incremental_step);
        }

        if (currency==QString("USDT"))
        {
            this->ui->selectedUsdtMarketsCapital->setMaximum(availableBalance);
            this->ui->selectedUsdtMarketsCapital->setValue(availableBalance);
            incremental_step=availableBalance/100000.0;
            this->ui->selectedUsdtMarketsCapital->setSingleStep(incremental_step);
        }

        if (currency==QString("EUR"))
        {
            this->ui->selectedEurMarketsCapital->setMaximum(availableBalance);
            this->ui->selectedEurMarketsCapital->setValue(availableBalance);
            incremental_step=availableBalance/100000.0;
            this->ui->selectedEurMarketsCapital->setSingleStep(incremental_step);
        }
    }
}

void MainWindow::on_selectedUsdMarketsCapital_valueChanged(double arg1)
{
    trader::Portfolio *usd_portfolio=this->getTrader()->getPortfolioManager()->getPortfolio("USD-Portfolio");
    usd_portfolio->setInitialCapital(arg1);
}

void MainWindow::on_selectedBtcMarketsCapital_valueChanged(double arg1)
{
    trader::Portfolio *btc_portfolio=this->getTrader()->getPortfolioManager()->getPortfolio("BTC-Portfolio");
    btc_portfolio->setInitialCapital(arg1);
}

void MainWindow::on_selectedEthMarketsCapital_valueChanged(double arg1)
{
    trader::Portfolio *eth_portfolio=this->getTrader()->getPortfolioManager()->getPortfolio("ETH-Portfolio");
    eth_portfolio->setInitialCapital(arg1);
}

void MainWindow::on_selectedUsdtMarketsCapital_valueChanged(double arg1)
{
    trader::Portfolio *usdt_portfolio=this->getTrader()->getPortfolioManager()->getPortfolio("USDT-Portfolio");
    usdt_portfolio->setInitialCapital(arg1);
}

void MainWindow::on_selectedEurMarketsCapital_valueChanged(double arg1)
{
    trader::Portfolio *eur_portfolio=this->getTrader()->getPortfolioManager()->getPortfolio("EUR-Portfolio");
    eur_portfolio->setInitialCapital(arg1);
}

void MainWindow::on_marketsComboBox_activated(const QString &arg1)
{
    QString base_currency=arg1.split(" ")[0];
    if (!base_currency.contains("All"))
    {
        for (int i=0;i<this->ui->marketsList->count();i++)
        {
            QListWidgetItem *item=this->ui->marketsList->item(i);
            MarketListItem *current_market_list_item=static_cast<MarketListItem *>(this->ui->marketsList->itemWidget(item));
            QString quote_currency_symbol=current_market_list_item->getMarketData().value("quoteCurrencySymbol").toString();
            if (quote_currency_symbol!=base_currency) { this->ui->marketsList->item(i)->setHidden(true); }
            else { this->ui->marketsList->item(i)->setHidden(false); }
        }
    }
    else
    {
        for (int i=0;i<this->ui->marketsList->count();i++)
        {
            this->ui->marketsList->item(i)->setHidden(false);
        }
    }
}

void MainWindow::on_marketsFilterComboBox_currentIndexChanged(int index)
{
    if (index==0)
    {
        this->ui->marketsList->setSortBy(MarketList::SortBy::VOLUME);
        this->ui->marketsList->sortItems(Qt::SortOrder::DescendingOrder);
    }
    else if (index==1)
    {
        this->ui->marketsList->setSortBy(MarketList::SortBy::VOLUME);
        this->ui->marketsList->sortItems(Qt::SortOrder::AscendingOrder);
    }
    else if (index==2)
    {
        this->ui->marketsList->setSortBy(MarketList::SortBy::CHANGE);
        this->ui->marketsList->sortItems(Qt::SortOrder::DescendingOrder);
    }
    else if (index==3)
    {
        this->ui->marketsList->setSortBy(MarketList::SortBy::CHANGE);
        this->ui->marketsList->sortItems(Qt::SortOrder::AscendingOrder);
    }
    else if (index==4)
    {
        this->ui->marketsList->setSortBy(MarketList::SortBy::LAST_PRICE);
        this->ui->marketsList->sortItems(Qt::SortOrder::DescendingOrder);
    }
    else if (index==5)
    {
        this->ui->marketsList->setSortBy(MarketList::SortBy::LAST_PRICE);
        this->ui->marketsList->sortItems(Qt::SortOrder::AscendingOrder);
    }
}

void MainWindow::on_searchMarketsLineEdit_textChanged(const QString &arg1)
{
    if (arg1.isEmpty()==false)
    {
        for (int i=0;i<this->ui->marketsList->count();i++)
        {
            QListWidgetItem *item=this->ui->marketsList->item(i);
            MarketListItem *current_market_list_item=static_cast<MarketListItem *>(this->ui->marketsList->itemWidget(item));
            bool containsSearchArgument=current_market_list_item->getMarketName().contains(arg1,Qt::CaseInsensitive);
            if (containsSearchArgument==true) { this->ui->marketsList->item(i)->setHidden(false); }
            else { this->ui->marketsList->item(i)->setHidden(true); }
        }
    }
    else
    {
        for (int i=0;i<this->ui->marketsList->count();i++)
        {
            this->ui->marketsList->item(i)->setHidden(false);
        }
    }
}

void MainWindow::on_analyticsButton_clicked() { this->ui->pagesWidget->setCurrentIndex(0); }

void MainWindow::on_marketsButton_clicked() { this->ui->pagesWidget->setCurrentIndex(1); }

void MainWindow::on_traderButton_clicked() { this->ui->pagesWidget->setCurrentIndex(2); }

void MainWindow::on_executionButton_clicked() { this->ui->pagesWidget->setCurrentIndex(3);  }

void MainWindow::on_portfoliosButton_clicked() { this->ui->pagesWidget->setCurrentIndex(4); }

void MainWindow::on_selectAllUsdMarketsButton_clicked()
{
    for (int i=0;i<this->ui->usdMarketsTable->rowCount();i++)
    {
        QTableWidgetItem *checkedItem=this->ui->usdMarketsTable->item(i,0);
        QTableWidgetItem *marketItem=this->ui->usdMarketsTable->item(i,1);
        if (checkedItem->checkState()==Qt::Checked) { continue; }
        checkedItem->setCheckState(Qt::Checked);
        emit this->ui->usdMarketsTable->marketSelected(marketItem->text());
    }
}

void MainWindow::on_deselectAllUsdMarketsButton_clicked()
{
    for (int i=0;i<this->ui->usdMarketsTable->rowCount();i++)
    {
        QTableWidgetItem *checkedItem=this->ui->usdMarketsTable->item(i,0);
        QTableWidgetItem *marketItem=this->ui->usdMarketsTable->item(i,1);
        if (checkedItem->checkState()==Qt::Unchecked) { continue; }
        checkedItem->setCheckState(Qt::Unchecked);
        emit this->ui->usdMarketsTable->marketDeselected(marketItem->text());
    }
}

void MainWindow::on_selectAllBtcMarketsButton_clicked()
{
    for (int i=0;i<this->ui->btcMarketsTable->rowCount();i++)
    {
        QTableWidgetItem *checkedItem=this->ui->btcMarketsTable->item(i,0);
        QTableWidgetItem *marketItem=this->ui->btcMarketsTable->item(i,1);
        if (checkedItem->checkState()==Qt::Checked) { continue; }
        checkedItem->setCheckState(Qt::Checked);
        emit this->ui->btcMarketsTable->marketSelected(marketItem->text());
    }
}

void MainWindow::on_deselectAllBtcMarketsButton_clicked()
{
    for (int i=0;i<this->ui->btcMarketsTable->rowCount();i++)
    {
        QTableWidgetItem *checkedItem=this->ui->btcMarketsTable->item(i,0);
        QTableWidgetItem *marketItem=this->ui->btcMarketsTable->item(i,1);
        if (checkedItem->checkState()==Qt::Unchecked) { continue; }
        checkedItem->setCheckState(Qt::Unchecked);
        emit this->ui->btcMarketsTable->marketDeselected(marketItem->text());
    }
}

void MainWindow::on_selectAllEthMarketsButton_clicked()
{
    for (int i=0;i<this->ui->ethMarketsTable->rowCount();i++)
    {
        QTableWidgetItem *checkedItem=this->ui->ethMarketsTable->item(i,0);
        QTableWidgetItem *marketItem=this->ui->ethMarketsTable->item(i,1);
        if (checkedItem->checkState()==Qt::Checked) { continue; }
        checkedItem->setCheckState(Qt::Checked);
        emit this->ui->ethMarketsTable->marketSelected(marketItem->text());
    }
}

void MainWindow::on_deselectAllEthMarketsButton_clicked()
{
    for (int i=0;i<this->ui->ethMarketsTable->rowCount();i++)
    {
        QTableWidgetItem *checkedItem=this->ui->ethMarketsTable->item(i,0);
        QTableWidgetItem *marketItem=this->ui->ethMarketsTable->item(i,1);
        if (checkedItem->checkState()==Qt::Unchecked) { continue; }
        checkedItem->setCheckState(Qt::Unchecked);
        emit this->ui->ethMarketsTable->marketDeselected(marketItem->text());
    }
}

void MainWindow::on_selectAllUsdtMarketsButton_clicked()
{
    for (int i=0;i<this->ui->usdtMarketsTable->rowCount();i++)
    {
        QTableWidgetItem *checkedItem=this->ui->usdtMarketsTable->item(i,0);
        QTableWidgetItem *marketItem=this->ui->usdtMarketsTable->item(i,1);
        if (checkedItem->checkState()==Qt::Checked) { continue; }
        checkedItem->setCheckState(Qt::Checked);
        emit this->ui->usdtMarketsTable->marketSelected(marketItem->text());
    }
}

void MainWindow::on_deselectAllUsdtMarketsButton_clicked()
{
    for (int i=0;i<this->ui->usdtMarketsTable->rowCount();i++)
    {
        QTableWidgetItem *checkedItem=this->ui->usdtMarketsTable->item(i,0);
        QTableWidgetItem *marketItem=this->ui->usdtMarketsTable->item(i,1);
        if (checkedItem->checkState()==Qt::Unchecked) { continue; }
        checkedItem->setCheckState(Qt::Unchecked);
        emit this->ui->usdtMarketsTable->marketDeselected(marketItem->text());
    }
}

void MainWindow::on_selectAllEurMarketsButton_clicked()
{
    for (int i=0;i<this->ui->eurMarketsTable->rowCount();i++)
    {
        QTableWidgetItem *checked_item=this->ui->eurMarketsTable->item(i,0);
        QTableWidgetItem *market_item=this->ui->eurMarketsTable->item(i,1);
        if (checked_item->checkState()==Qt::Checked) { continue; }
        checked_item->setCheckState(Qt::Checked);
        emit this->ui->eurMarketsTable->marketSelected(market_item->text());
    }
}

void MainWindow::on_deselectAllEurMarketsButton_clicked()
{
    for (int i=0;i<this->ui->eurMarketsTable->rowCount();i++)
    {
        QTableWidgetItem *checked_item=this->ui->eurMarketsTable->item(i,0);
        QTableWidgetItem *market_item=this->ui->eurMarketsTable->item(i,1);
        if (checked_item->checkState()==Qt::Unchecked) { continue; }
        checked_item->setCheckState(Qt::Unchecked);
        emit this->ui->eurMarketsTable->marketDeselected(market_item->text());
    }
}


void MainWindow::on_searchUsdMarketsLineEdit_textChanged(const QString &arg1)
{
    if (arg1.isEmpty()==false)
    {
        for (int i=0;i<this->ui->usdMarketsTable->rowCount();i++)
        {
            QTableWidgetItem *item=this->ui->usdMarketsTable->item(i,1);
            bool containsSearchArgument=item->text().contains(arg1,Qt::CaseInsensitive);
            if (containsSearchArgument==true) { this->ui->usdMarketsTable->setRowHidden(i,false); }
            else { this->ui->usdMarketsTable->setRowHidden(i,true); }
        }
    }
    else
    {
        for (int i=0;i<this->ui->usdMarketsTable->rowCount();i++)
        {
            this->ui->usdMarketsTable->setRowHidden(i,false);
        }
    }
}

void MainWindow::on_searchBtcMarketsLineEdit_textChanged(const QString &arg1)
{
    if (arg1.isEmpty()==false)
    {
        for (int i=0;i<this->ui->btcMarketsTable->rowCount();i++)
        {
            QTableWidgetItem *item=this->ui->btcMarketsTable->item(i,1);
            bool containsSearchArguments=item->text().contains(arg1,Qt::CaseInsensitive);
            if (containsSearchArguments==true) { this->ui->btcMarketsTable->setRowHidden(i,false); }
            else { this->ui->btcMarketsTable->setRowHidden(i,true); }
        }
    }
    else
    {
        for (int i=0;i<this->ui->btcMarketsTable->rowCount();i++)
        {
            this->ui->btcMarketsTable->setRowHidden(i,false);
        }
    }
}

void MainWindow::on_searchEthMarketsLineEdit_textChanged(const QString &arg1)
{
    if (arg1.isEmpty()==false)
    {
        for (int i=0;i<this->ui->ethMarketsTable->rowCount();i++)
        {
            QTableWidgetItem *item=this->ui->ethMarketsTable->item(i,1);
            bool containsSearchArguments=item->text().contains(arg1,Qt::CaseInsensitive);
            if (containsSearchArguments==true) { this->ui->ethMarketsTable->setRowHidden(i,false); }
            else { this->ui->ethMarketsTable->setRowHidden(i,true); }
        }
    }
    else
    {
        for (int i=0;i<this->ui->ethMarketsTable->rowCount();i++)
        {
            this->ui->ethMarketsTable->setRowHidden(i,false);
        }
    }
}


void MainWindow::on_searchUsdtMarketsLineEdit_textChanged(const QString &arg1)
{
    if (arg1.isEmpty()==false)
    {
        for (int i=0;i<this->ui->usdtMarketsTable->rowCount();i++)
        {
            QTableWidgetItem *item=this->ui->usdtMarketsTable->item(i,1);
            bool containsSearchArguments=item->text().contains(arg1,Qt::CaseInsensitive);
            if (containsSearchArguments==true) { this->ui->usdtMarketsTable->setRowHidden(i,false); }
            else { this->ui->usdtMarketsTable->setRowHidden(i,true); }
        }
    }
    else
    {
        for (int i=0;i<this->ui->usdtMarketsTable->rowCount();i++)
        {
            this->ui->usdtMarketsTable->setRowHidden(i,false);
        }
    }
}

void MainWindow::on_searchEurMarketsLineEdit_textChanged(const QString &arg1)
{
    if (arg1.isEmpty()==false)
    {
        for (int i=0;i<this->ui->eurMarketsTable->rowCount();i++)
        {
            QTableWidgetItem *item=this->ui->eurMarketsTable->item(i,1);
            bool contains_search_arguments=item->text().contains(arg1,Qt::CaseInsensitive);
            if (contains_search_arguments==true) { this->ui->eurMarketsTable->setRowHidden(i,false); }
            else { this->ui->eurMarketsTable->setRowHidden(i,true); }
        }
    }
    else
    {
        for (int i=0;i<this->ui->eurMarketsTable->rowCount();i++)
        {
            this->ui->eurMarketsTable->setRowHidden(i,false);
        }
    }
}


void MainWindow::on_checkupButton_clicked()
{
    trader::StrategyManager *strategy_manager=this->getTrader()->getStrategyManager();
    int strategies_count=strategy_manager->getStrategies()->count();

    if (strategies_count==0)
    {
        QString message=tr("An error was discovered regarding strategy selection!");
        QString informative_message=tr("No strategy has been selected!");
        QString descriptive_message=tr("You must select at least one trading strategy, ");
        descriptive_message+=tr("In order to do so navigate to the trader page and select a strategy from the strategies list.");
        this->getDialogManager()->showErrorMessageBox(message,informative_message,descriptive_message);
        return;
    }

    trader::RiskManager *risk_manager=this->getTrader()->getRiskManager();
    int risks_count=risk_manager->getRisks()->count();

    if (risks_count<2)
    {
        QString message=tr("An error was discovered regarding risk selection!");
        QString informative_message=tr("No stop-loss or take-profit risk was selected!");
        QString descriptive_message=tr("You must select at least one take-profit risk ");
        descriptive_message+=tr("and at least one stop-loss risk, in order to do so ");
        descriptive_message+=tr("navigate to the trader page and select the risks from the risks list.");
        this->getDialogManager()->showErrorMessageBox(message,informative_message,descriptive_message);
        return;
    }

    trader::AssetHash::iterator asset_iterator;
    trader::AssetManager *asset_manager=this->getTrader()->getAssetManager();
    double usd_portfolio_initial_capital=this->getTrader()->getPortfolioManager()->getPortfolio("USD-Portfolio")->getInitialCapital();
    double btc_portfolio_initial_capital=this->getTrader()->getPortfolioManager()->getPortfolio("BTC-Portfolio")->getInitialCapital();
    double eth_portfolio_initial_capital=this->getTrader()->getPortfolioManager()->getPortfolio("ETH-Portfolio")->getInitialCapital();
    double usdt_portfolio_initial_capital=this->getTrader()->getPortfolioManager()->getPortfolio("USDT-Portfolio")->getInitialCapital();
    double eur_portfolio_initial_capital=this->getTrader()->getPortfolioManager()->getPortfolio("EUR-Portfolio")->getInitialCapital();

    double usd_total_percentage_allocated=0.0;
    double btc_total_percentage_allocated=0.0;
    double eth_total_percentage_allocated=0.0;
    double usdt_total_percentage_allocated=0.0;
    double eur_total_percentage_allocated=0.0;

    for (asset_iterator=asset_manager->getAssets()->begin();asset_iterator!=asset_manager->getAssets()->end();asset_iterator++)
    {
        QString market=asset_iterator.value()->getMarket();
        QListWidgetComparableItem *item=static_cast<QListWidgetComparableItem *>(this->ui->marketsList->getMap()->value(market));
        MarketListItem *marketListItem=static_cast<MarketListItem *>(this->ui->marketsList->itemWidget(item));
        double minimumTradesize=marketListItem->getMarketData().value("minTradeSize").toDouble();
        double last_price=marketListItem->getMarketTickerData().value("lastTradeRate").toDouble();
        BittrexAsset *bittrex_asset=dynamic_cast<BittrexAsset *>(asset_iterator.value());
        bittrex_asset->setMinimumTradeSize(minimumTradesize);

        if (market.contains("-USD")==true && market.contains("-USDT")==false)
        {
            double capital_percentage=asset_iterator.value()->getCapitalPercentage();
            double allocated_capital=usd_portfolio_initial_capital*(capital_percentage/100.0);
            asset_iterator.value()->setAllocatedCapital(allocated_capital);
            double calculated_quantity=allocated_capital/last_price;
            usd_total_percentage_allocated+=capital_percentage;

            if (calculated_quantity<=minimumTradesize)
            {
                QString message=tr("An inaccuracy was discovered regarding capital allocation!");
                QString informative_message=tr("There was a capital allocation error for market ")+market;
                QString descriptive_message=tr("The allocated quantity for market ")+market+tr(" ");
                descriptive_message+=tr("was ")+QString::number(calculated_quantity)+tr(" while ");
                descriptive_message+=tr("the minimum trading size for the market is ")+QString::number(minimumTradesize);
                descriptive_message+=tr(", in order to deal with this issue either increase the allocated capital percentage or ");
                descriptive_message+=tr("if there is insufficient capital navigate back to the markets page and deselect market ")+market;
                descriptive_message+=tr(" from the USD markets table.");
                this->getDialogManager()->showErrorMessageBox(message,informative_message,descriptive_message);
                return;
            }
        }
        else if (market.contains("-BTC")==true)
        {
            double capital_percentage=asset_iterator.value()->getCapitalPercentage();
            double allocated_capital=btc_portfolio_initial_capital*(capital_percentage/100.0);
            asset_iterator.value()->setAllocatedCapital(allocated_capital);
            double calculated_quantity=allocated_capital/last_price;
            btc_total_percentage_allocated+=capital_percentage;

            if (calculated_quantity<=minimumTradesize)
            {
                QString message=tr("An inaccuracy was discovered regarding capital allocation!");
                QString informative_message=tr("There was a capital allocation error for market ")+market;
                QString descriptive_message=tr("The allocated quantity for market ")+market+tr(" ");
                descriptive_message+=tr("was ")+QString::number(calculated_quantity)+tr(" while ");
                descriptive_message+=tr("the minimum trading size for the market is ")+QString::number(minimumTradesize);
                descriptive_message+=tr(", in order to deal with this issue either increase the allocated capital percentage or ");
                descriptive_message+=tr("if there is insufficient capital navigate back to the markets page and deselect market ")+market;
                descriptive_message+=tr(" from the BTC markets table.");
                this->getDialogManager()->showErrorMessageBox(message,informative_message,descriptive_message);
                return;
            }
        }
        else if (market.contains("-ETH")==true)
        {
            double capital_percentage=asset_iterator.value()->getCapitalPercentage();
            double allocated_capital=eth_portfolio_initial_capital*(capital_percentage/100.0);
            asset_iterator.value()->setAllocatedCapital(allocated_capital);
            double calculated_quantity=allocated_capital/last_price;
            eth_total_percentage_allocated+=capital_percentage;

            if (calculated_quantity<=minimumTradesize)
            {
                QString message=tr("An inaccuracy was discovered regarding capital allocation!");
                QString informative_message=tr("There was a capital allocation error for market ")+market;
                QString descriptive_message=tr("The allocated quantity for market ")+market+tr(" ");
                descriptive_message+=tr("was ")+QString::number(calculated_quantity)+tr(" while ");
                descriptive_message+=tr("the minimum trading size for the market is ")+QString::number(minimumTradesize);
                descriptive_message+=tr(", in order to deal with this issue either increase the allocated capital percentage or ");
                descriptive_message+=tr("if there is insufficient capital navigate back to the markets page and deselect market ")+market;
                descriptive_message+=tr(" from the ETH markets table.");
                this->getDialogManager()->showErrorMessageBox(message,informative_message,descriptive_message);
                return;
            }
        }
        else if (market.contains("-USDT")==true)
        {
            double capital_percentage=asset_iterator.value()->getCapitalPercentage();
            double allocated_capital=usdt_portfolio_initial_capital*(capital_percentage/100.0);
            asset_iterator.value()->setAllocatedCapital(allocated_capital);
            double calculated_quantity=allocated_capital/last_price;
            usdt_total_percentage_allocated+=capital_percentage;

            if (calculated_quantity<=minimumTradesize)
            {
                QString message=tr("An inaccuracy was discovered regarding capital allocation!");
                QString informative_message=tr("There was a capital allocation error for market ")+market;
                QString descriptive_message=tr("The allocated quantity for market ")+market+tr(" ");
                descriptive_message+=tr("was ")+QString::number(calculated_quantity)+tr(" while ");
                descriptive_message+=tr("the minimum trading size for the market is ")+QString::number(minimumTradesize);
                descriptive_message+=tr(", in order to deal with this issue either increase the allocated capital percentage or ");
                descriptive_message+=tr("if there is insufficient capital navigate back to the markets page and deselect market ")+market;
                descriptive_message+=tr(" from the USDT markets table.");
                this->getDialogManager()->showErrorMessageBox(message,informative_message,descriptive_message);
                return;
            }
        }
        else if (market.contains("-EUR")==true)
        {
            double capital_percentage=asset_iterator.value()->getCapitalPercentage();
            double allocated_capital=eur_portfolio_initial_capital*(capital_percentage/100.0);
            asset_iterator.value()->setAllocatedCapital(allocated_capital);
            double calculated_quantity=allocated_capital/last_price;
            eur_total_percentage_allocated+=capital_percentage;

            if (calculated_quantity<=minimumTradesize)
            {
                QString message=tr("An inaccuracy was discovered regarding capital allocation!");
                QString informative_message=tr("There was a capital allocation error for market ")+market;
                QString descriptive_message=tr("The allocated quantity for market ")+market+tr(" ");
                descriptive_message+=tr("was ")+QString::number(calculated_quantity)+tr(" while ");
                descriptive_message+=tr("the minimum trading size for the market is ")+QString::number(minimumTradesize);
                descriptive_message+=tr(", in order to deal with this issue either increase the allocated capital percentage or ");
                descriptive_message+=tr("if there is insufficient capital navigate back to the markets page and deselect market ")+market;
                descriptive_message+=tr(" from the EUR markets table.");
                this->getDialogManager()->showErrorMessageBox(message,informative_message,descriptive_message);
                return;
            }
        }
    }

    if (usd_total_percentage_allocated>100.0)
    {
        QString message=tr("An inaccuracy was discovered regarding capital allocation!");
        QString informative_message=tr("There was a capital allocation error for USD markets!");
        QString descriptive_message=tr("The sum of the allocated percentages exceeds the 100% upper bound limit, ");
        descriptive_message+=tr("in order to deal with this issue either decrease the allocated percentages or ");
        descriptive_message+=tr(" navigate back to the markets page and deselect a market from the USD markets table.");
        this->getDialogManager()->showErrorMessageBox(message,informative_message,descriptive_message);
        return;
    }

    if (btc_total_percentage_allocated>100.0)
    {
        QString message=tr("An inaccuracy was discovered regarding capital allocation!");
        QString informative_message=tr("There was a capital allocation error for BTC markets!");
        QString descriptive_message=tr("The sum of the allocated percentages exceeds the 100% upper bound limit, ");
        descriptive_message+=tr("in order to deal with this issue either decrease the allocated percentages or ");
        descriptive_message+=tr(" navigate back to the markets page and deselect a market from the BTC markets table.");
        this->getDialogManager()->showErrorMessageBox(message,informative_message,descriptive_message);
        return;
    }

    if (eth_total_percentage_allocated>100.0)
    {
        QString message=tr("An inaccuracy was discovered regarding capital allocation!");
        QString informative_message=tr("There was a capital allocation error for ETH markets!");
        QString descriptive_message=tr("The sum of the allocated percentages exceeds the 100% upper bound limit, ");
        descriptive_message+=tr("in order to deal with this issue either decrease the allocated percentages or ");
        descriptive_message+=tr(" navigate back to the markets page and deselect a market from the ETH markets table.");
        this->getDialogManager()->showErrorMessageBox(message,informative_message,descriptive_message);
        return;
    }

    if (usdt_total_percentage_allocated>100.0)
    {
        QString message=tr("An inaccuracy was discovered regarding capital allocation!");
        QString informative_message=tr("There was a capital allocation error for USDT markets!");
        QString descriptive_message=tr("The sum of the allocated percentages exceeds the 100% upper bound limit, ");
        descriptive_message+=tr("in order to deal with this issue either decrease the allocated percentages or ");
        descriptive_message+=tr(" navigate back to the markets page and deselect a market from the USDT markets table.");
        this->getDialogManager()->showErrorMessageBox(message,informative_message,descriptive_message);
        return;
    }

    if (eur_total_percentage_allocated>100.0)
    {
        QString message=tr("An inaccuracy was discovered regarding capital allocation!");
        QString informative_message=tr("There was a capital allocation error for EUR markets!");
        QString descriptive_message=tr("The sum of the allocated percentages exceeds the 100% upper bound limit, ");
        descriptive_message+=tr("in order to deal with this issue either decrease the allocated percentages or ");
        descriptive_message+=tr(" navigate back to the markets page and deselect a market from the EUR markets table.");
        this->getDialogManager()->showErrorMessageBox(message,informative_message,descriptive_message);
        return;
    }

    QString message=tr("Sanity check completed, everything looks good!");
    QString informative_message=tr("Click execute button to start the session!");
    QString descriptive_message=tr("BittrexBot performed a thorough sanity check ");
    descriptive_message+=tr("on the trading configurations and did not encounter any errors.");
    this->getDialogManager()->showConfirmationMessageBox(message,informative_message,descriptive_message);
}



void MainWindow::on_executeSessionButton_clicked()
{
    trader::Portfolio *usd_portfolio=this->getTrader()->getPortfolioManager()->getPortfolio("USD-Portfolio");
    trader::Portfolio *btc_portfolio=this->getTrader()->getPortfolioManager()->getPortfolio("BTC-Portfolio");
    trader::Portfolio *eth_portfolio=this->getTrader()->getPortfolioManager()->getPortfolio("ETH-Portfolio");
    trader::Portfolio *usdt_portfolio=this->getTrader()->getPortfolioManager()->getPortfolio("USDT-Portfolio");
    trader::Portfolio *eur_portfolio=this->getTrader()->getPortfolioManager()->getPortfolio("EUR-Portfolio");

    usd_portfolio->clearOpenPositions();
    usd_portfolio->clearClosedPositions();
    btc_portfolio->clearOpenPositions();
    btc_portfolio->clearClosedPositions();
    eth_portfolio->clearOpenPositions();
    eth_portfolio->clearClosedPositions();
    usdt_portfolio->clearOpenPositions();
    usdt_portfolio->clearClosedPositions();
    eur_portfolio->clearOpenPositions();
    eur_portfolio->clearClosedPositions();

    this->getTrader()->getExecutionManager()->clear();
    this->getTrader()->getOrderManager()->clear();

    this->ui->openOrdersTable->clearContents(); this->ui->openOrdersTable->setRowCount(0);
    this->ui->closedOrdersTable->clearContents(); this->ui->closedOrdersTable->setRowCount(0);

    this->ui->usdOpenPositionsTable->clearContents(); this->ui->usdOpenPositionsTable->setRowCount(0);
    this->ui->usdClosedPositionsTable->clearContents(); this->ui->usdClosedPositionsTable->setRowCount(0);
    this->ui->btcOpenPositionsTable->clearContents(); this->ui->btcOpenPositionsTable->setRowCount(0);
    this->ui->btcClosedPositionsTable->clearContents(); this->ui->btcClosedPositionsTable->setRowCount(0);
    this->ui->ethOpenPositionsTable->clearContents(); this->ui->ethOpenPositionsTable->setRowCount(0);
    this->ui->ethClosedPositionsTable->clearContents(); this->ui->ethClosedPositionsTable->setRowCount(0);
    this->ui->usdtOpenPositionsTable->clearContents(); this->ui->usdtOpenPositionsTable->setRowCount(0);
    this->ui->usdtClosedPositionsTable->clearContents(); this->ui->usdtClosedPositionsTable->setRowCount(0);
    this->ui->eurOpenPositionsTable->clearContents(); this->ui->eurOpenPositionsTable->setRowCount(0);
    this->ui->eurClosedPositionsTable->clearContents(); this->ui->eurClosedPositionsTable->setRowCount(0);

    usd_portfolio->setCurrentCapital(usd_portfolio->getInitialCapital());
    usd_portfolio->setEquity(usd_portfolio->getInitialCapital());
    usd_portfolio->setUnrealisedPnL(0.0);
    usd_portfolio->setRealisedPnL(0.0);
    usd_portfolio->setReturnOnInvestment(0.0);

    btc_portfolio->setCurrentCapital(btc_portfolio->getInitialCapital());
    btc_portfolio->setEquity(btc_portfolio->getInitialCapital());
    btc_portfolio->setUnrealisedPnL(0.0);
    btc_portfolio->setRealisedPnL(0.0);
    btc_portfolio->setReturnOnInvestment(0.0);

    eth_portfolio->setCurrentCapital(eth_portfolio->getInitialCapital());
    eth_portfolio->setEquity(eth_portfolio->getInitialCapital());
    eth_portfolio->setUnrealisedPnL(0.0);
    eth_portfolio->setRealisedPnL(0.0);
    eth_portfolio->setReturnOnInvestment(0.0);

    usdt_portfolio->setCurrentCapital(usdt_portfolio->getInitialCapital());
    usdt_portfolio->setEquity(usdt_portfolio->getInitialCapital());
    usdt_portfolio->setUnrealisedPnL(0.0);
    usdt_portfolio->setRealisedPnL(0.0);
    usdt_portfolio->setReturnOnInvestment(0.0);

    eur_portfolio->setCurrentCapital(eur_portfolio->getInitialCapital());
    eur_portfolio->setEquity(eur_portfolio->getInitialCapital());
    eur_portfolio->setUnrealisedPnL(0.0);
    eur_portfolio->setRealisedPnL(0.0);
    eur_portfolio->setReturnOnInvestment(0.0);

    this->startTradingSession();
}


void MainWindow::on_searchOpenOrdersLineEdit_textChanged(const QString &arg1)
{
    if (arg1.isEmpty()==false)
    {
        for (int i=0;i<this->ui->openOrdersTable->rowCount();i++)
        {
            QTableWidgetItem *item=this->ui->openOrdersTable->item(i,2);
            bool containsSearchArgument=item->text().contains(arg1,Qt::CaseInsensitive);
            if (containsSearchArgument==true) { this->ui->openOrdersTable->setRowHidden(i,false); }
            else { this->ui->openOrdersTable->setRowHidden(i,true); }
        }
    }
    else
    {
        for (int i=0;i<this->ui->openOrdersTable->rowCount();i++)
        {
            this->ui->openOrdersTable->setRowHidden(i,false);
        }
    }
}

void MainWindow::on_searchClosedOrdersLineEdit_textChanged(const QString &arg1)
{
    if (arg1.isEmpty()==false)
    {
        for (int i=0;i<this->ui->closedOrdersTable->rowCount();i++)
        {
            QTableWidgetItem *item=this->ui->closedOrdersTable->item(i,2);
            bool containsSearchArgument=item->text().contains(arg1,Qt::CaseInsensitive);
            if (containsSearchArgument==true) { this->ui->closedOrdersTable->setRowHidden(i,false); }
            else { this->ui->closedOrdersTable->setRowHidden(i,true); }
        }
    }
    else
    {
        for (int i=0;i<this->ui->closedOrdersTable->rowCount();i++)
        {
            this->ui->closedOrdersTable->setRowHidden(i,false);
        }
    }
}

void MainWindow::on_searchUsdOpenPositionsLineEdit_textChanged(const QString &arg1)
{
    if (arg1.isEmpty()==false)
    {
        for (int i=0;i<this->ui->usdOpenPositionsTable->rowCount();i++)
        {
            QTableWidgetItem *item=this->ui->usdOpenPositionsTable->item(i,2);
            bool containsSearchArgument=item->text().contains(arg1,Qt::CaseInsensitive);
            if (containsSearchArgument==true) { this->ui->openOrdersTable->setRowHidden(i,false); }
            else { this->ui->usdOpenPositionsTable->setRowHidden(i,true); }
        }
    }
    else
    {
        for (int i=0;i<this->ui->usdOpenPositionsTable->rowCount();i++)
        {
            this->ui->usdOpenPositionsTable->setRowHidden(i,false);
        }
    }
}

void MainWindow::on_searchUsdClosedPositionsLineEdit_textChanged(const QString &arg1)
{
    if (arg1.isEmpty()==false)
    {
        for (int i=0;i<this->ui->usdClosedPositionsTable->rowCount();i++)
        {
            QTableWidgetItem *item=this->ui->usdClosedPositionsTable->item(i,1);
            bool containsSearchArgument=item->text().contains(arg1,Qt::CaseInsensitive);
            if (containsSearchArgument==true) { this->ui->usdClosedPositionsTable->setRowHidden(i,false); }
            else { this->ui->usdClosedPositionsTable->setRowHidden(i,true); }
        }
    }
    else
    {
        for (int i=0;i<this->ui->usdClosedPositionsTable->rowCount();i++)
        {
            this->ui->usdClosedPositionsTable->setRowHidden(i,false);
        }
    }
}

void MainWindow::on_searchBtcOpenPositionsLineEdit_textChanged(const QString &arg1)
{
    if (arg1.isEmpty()==false)
    {
        for (int i=0;i<this->ui->btcOpenPositionsTable->rowCount();i++)
        {
            QTableWidgetItem *item=this->ui->btcOpenPositionsTable->item(i,2);
            bool containsSearchArgument=item->text().contains(arg1,Qt::CaseInsensitive);
            if (containsSearchArgument==true) { this->ui->btcOpenPositionsTable->setRowHidden(i,false); }
            else { this->ui->btcOpenPositionsTable->setRowHidden(i,true); }
        }
    }
    else
    {
        for (int i=0;i<this->ui->btcOpenPositionsTable->rowCount();i++)
        {
            this->ui->btcOpenPositionsTable->setRowHidden(i,false);
        }
    }
}

void MainWindow::on_searchBtcClosedPositionsLineEdit_textChanged(const QString &arg1)
{
    if (arg1.isEmpty()==false)
    {
        for (int i=0;i<this->ui->btcClosedPositionsTable->rowCount();i++)
        {
            QTableWidgetItem *item=this->ui->btcClosedPositionsTable->item(i,1);
            bool containsSearchArgument=item->text().contains(arg1,Qt::CaseInsensitive);
            if (containsSearchArgument==true) { this->ui->btcClosedPositionsTable->setRowHidden(i,false); }
            else { this->ui->btcClosedPositionsTable->setRowHidden(i,true); }
        }
    }
    else
    {
        for (int i=0;i<this->ui->btcClosedPositionsTable->rowCount();i++)
        {
            this->ui->btcClosedPositionsTable->setRowHidden(i,false);
        }
    }
}

void MainWindow::on_searchEthOpenPositionsLineEdit_textChanged(const QString &arg1)
{
    if (arg1.isEmpty()==false)
    {
        for (int i=0;i<this->ui->ethOpenPositionsTable->rowCount();i++)
        {
            QTableWidgetItem *item=this->ui->ethOpenPositionsTable->item(i,2);
            bool containsSearchArgument=item->text().contains(arg1,Qt::CaseInsensitive);
            if (containsSearchArgument==true) { this->ui->ethOpenPositionsTable->setRowHidden(i,false); }
            else { this->ui->ethOpenPositionsTable->setRowHidden(i,true); }
        }
    }
    else
    {
        for (int i=0;i<this->ui->ethOpenPositionsTable->rowCount();i++)
        {
            this->ui->ethOpenPositionsTable->setRowHidden(i,false);
        }
    }
}

void MainWindow::on_searchEthClosedPositionsLineEdit_textChanged(const QString &arg1)
{
    if (arg1.isEmpty()==false)
    {
        for (int i=0;i<this->ui->ethClosedPositionsTable->rowCount();i++)
        {
            QTableWidgetItem *item=this->ui->ethClosedPositionsTable->item(i,1);
            bool containsSearchArgument=item->text().contains(arg1,Qt::CaseInsensitive);
            if (containsSearchArgument==true) { this->ui->ethClosedPositionsTable->setRowHidden(i,false); }
            else { this->ui->ethClosedPositionsTable->setRowHidden(i,true); }
        }
    }
    else
    {
        for (int i=0;i<this->ui->ethClosedPositionsTable->rowCount();i++)
        {
            this->ui->ethClosedPositionsTable->setRowHidden(i,false);
        }
    }
}


void MainWindow::on_searchUsdtOpenPositionsLineEdit_textChanged(const QString &arg1)
{
    if (arg1.isEmpty()==false)
    {
        for (int i=0;i<this->ui->usdtOpenPositionsTable->rowCount();i++)
        {
            QTableWidgetItem *item=this->ui->usdtOpenPositionsTable->item(i,2);
            bool containsSearchArgument=item->text().contains(arg1,Qt::CaseInsensitive);
            if (containsSearchArgument==true) { this->ui->usdtOpenPositionsTable->setRowHidden(i,false); }
            else { this->ui->usdtOpenPositionsTable->setRowHidden(i,true); }
        }
    }
    else
    {
        for (int i=0;i<this->ui->usdtOpenPositionsTable->rowCount();i++)
        {
            this->ui->usdtOpenPositionsTable->setRowHidden(i,false);
        }
    }
}

void MainWindow::on_searchUsdtClosedPositionsLineEdit_textChanged(const QString &arg1)
{
    if (arg1.isEmpty()==false)
    {
        for (int i=0;i<this->ui->usdtClosedPositionsTable->rowCount();i++)
        {
            QTableWidgetItem *item=this->ui->usdtClosedPositionsTable->item(i,1);
            bool containsSearchArgument=item->text().contains(arg1,Qt::CaseInsensitive);
            if (containsSearchArgument==true) { this->ui->usdtClosedPositionsTable->setRowHeight(i,false); }
            else { this->ui->usdtClosedPositionsTable->setRowHidden(i,true); }
        }
    }
    else
    {
        for (int i=0;i<this->ui->usdtClosedPositionsTable->rowCount();i++)
        {
            this->ui->usdtClosedPositionsTable->setRowHidden(i,false);
        }
    }
}

void MainWindow::on_searchEurOpenPositionsLineEdit_textChanged(const QString &arg1)
{
    if (arg1.isEmpty()==false)
    {
        for (int i=0;i<this->ui->eurOpenPositionsTable->rowCount();i++)
        {
            QTableWidgetItem *item=this->ui->eurOpenPositionsTable->item(i,2);
            bool contains_search_argument=item->text().contains(arg1,Qt::CaseInsensitive);
            if (contains_search_argument==true) { this->ui->eurOpenPositionsTable->setRowHidden(i,false); }
            else { this->ui->eurOpenPositionsTable->setRowHidden(i,true); }
        }
    }
    else
    {
        for (int i=0;i<this->ui->eurOpenPositionsTable->rowCount();i++)
        {
            this->ui->eurOpenPositionsTable->setRowHidden(i,false);
        }
    }
}

void MainWindow::on_searchEurClosedPositionsLineEdit_textChanged(const QString &arg1)
{
    if (arg1.isEmpty()==false)
    {
        for (int i=0;i<this->ui->eurClosedPositionsTable->rowCount();i++)
        {
            QTableWidgetItem *item=this->ui->eurClosedPositionsTable->item(i,2);
            bool contains_search_argument=item->text().contains(arg1,Qt::CaseInsensitive);
            if (contains_search_argument==true) { this->ui->eurClosedPositionsTable->setRowHidden(i,false); }
            else { this->ui->eurClosedPositionsTable->setRowHidden(i,true); }
        }
    }
    else
    {
        for (int i=0;i<this->ui->eurClosedPositionsTable->rowCount();i++)
        {
            this->ui->eurClosedPositionsTable->setRowHidden(i,false);
        }
    }
}

void MainWindow::on_usdPortfolioLiquifyAllButton_clicked()
{
    for (int i=0;i<this->ui->usdOpenPositionsTable->rowCount();i++)
    {
        QTimer *timer=new QTimer(this);
        QToolButton *liquify_button=static_cast<QToolButton *>(this->ui->usdOpenPositionsTable->cellWidget(i,0));
        timer->singleShot(i,[=]() { liquify_button->click(); timer->deleteLater(); });
    }
}

void MainWindow::on_btcPortfolioLiquifyAllButton_clicked()
{
    for (int i=0;i<this->ui->btcOpenPositionsTable->rowCount();i++)
    {
        QTimer *timer=new QTimer(this);
        QToolButton *liquify_button=static_cast<QToolButton *>(this->ui->btcOpenPositionsTable->cellWidget(i,0));
        timer->singleShot(i,[=]() { liquify_button->click(); timer->deleteLater(); });
    }
}

void MainWindow::on_ethPortfolioLiquifyAllButton_clicked()
{
    for (int i=0;i<this->ui->ethOpenPositionsTable->rowCount();i++)
    {
        QTimer *timer=new QTimer(this);
        QToolButton *liquify_button=static_cast<QToolButton *>(this->ui->ethOpenPositionsTable->cellWidget(i,0));
        timer->singleShot(1,[=]() { liquify_button->click(); timer->deleteLater(); });
    }
}

void MainWindow::on_usdtPortfolioLiquifyAllButton_clicked()
{
    for (int i=0;i<this->ui->usdtOpenPositionsTable->rowCount();i++)
    {
        QTimer *timer=new QTimer(this);
        QToolButton *liquify_button=static_cast<QToolButton *>(this->ui->usdtOpenPositionsTable->cellWidget(i,0));
        timer->singleShot(1,[=]() { liquify_button->click(); timer->deleteLater(); });
    }
}

void MainWindow::on_eurPortfolioLiquifyAllButton_clicked()
{
    for (int i=0;i<this->ui->eurOpenPositionsTable->rowCount();i++)
    {
        QTimer *timer=new QTimer(this);
        QToolButton *liquify_button=static_cast<QToolButton *>(this->ui->eurOpenPositionsTable->cellWidget(i,0));
        timer->singleShot(1,[=]() { liquify_button->click(); timer->deleteLater(); });
    }
}

void MainWindow::on_cancelAllOrdersButton_clicked()
{
    for (int i=0;i<this->ui->openOrdersTable->rowCount();i++)
    {
        QTimer *timer=new QTimer(this);
        QToolButton *cancel_button=static_cast<QToolButton *>(this->ui->openOrdersTable->cellWidget(i,0));
        timer->singleShot(1,[=]() { cancel_button->click(); timer->deleteLater(); });
    }
}

void MainWindow::on_forwardToOrdersPageButton_clicked() { this->ui->executionPagesWidget->setCurrentIndex(1); }

void MainWindow::on_backToConfigurationPageButton_clicked()
{
    this->ui->executionPagesWidget->setCurrentIndex(0);
    if (this->getSessionStatus()==MainWindow::SessionStatus::STOPPED)
    {
        this->getBittrexRestApiHandler()->queryBalances();
    }
}

void MainWindow::on_forwardToSessionSummaryPageButton_clicked() { this->ui->executionPagesWidget->setCurrentIndex(2); }

void MainWindow::on_backToOrdersPageButton_clicked() { this->ui->executionPagesWidget->setCurrentIndex(1); }



void MainWindow::populateSessionSummaryChartViews()
{
    QPieSeries *usd_series=new QPieSeries(this); QPieSeries *usd_series_copy=new QPieSeries(this);
    QChart *usd_chart=new QChart(); QChart *usd_chart_copy=new QChart();

    QPieSeries *btc_series=new QPieSeries(this); QPieSeries *btc_series_copy=new QPieSeries(this);
    QChart *btc_chart=new QChart(); QChart *btc_chart_copy=new QChart();

    QPieSeries *eth_series=new QPieSeries(this); QPieSeries *eth_series_copy=new QPieSeries(this);
    QChart *eth_chart=new QChart(); QChart *eth_chart_copy=new QChart();

    QPieSeries *usdt_series=new QPieSeries(this); QPieSeries *usdt_series_copy=new QPieSeries(this);
    QChart *usdt_chart=new QChart(); QChart *usdt_chart_copy=new QChart();

    QPieSeries *eur_series=new QPieSeries(this); QPieSeries *eur_series_copy=new QPieSeries(this);
    QChart *eur_chart=new QChart(); QChart *eur_chart_copy=new QChart();

    trader::AssetHash::iterator asset_iterator;
    trader::AssetManager *assetManager=this->getTrader()->getAssetManager();

    double usd_initial_capital_allocated=this->getTrader()->getPortfolioManager()->getPortfolio("USD-Portfolio")->getInitialCapital();
    double btc_initial_capital_allocated=this->getTrader()->getPortfolioManager()->getPortfolio("BTC-Portfolio")->getInitialCapital();
    double eth_initial_capital_allocated=this->getTrader()->getPortfolioManager()->getPortfolio("ETH-Portfolio")->getInitialCapital();
    double usdt_initial_capital_allocated=this->getTrader()->getPortfolioManager()->getPortfolio("USDT-Portfolio")->getInitialCapital();
    double eur_initial_capital_allocated=this->getTrader()->getPortfolioManager()->getPortfolio("EUR-Portfolio")->getInitialCapital();

    double usd_total_percentage_allocated=0.0; double usd_total_percentage_unallocated=0.0; double usd_total_capital_allocated=0.0; double usd_total_capital_unallocated=0.0;
    double btc_total_percentage_allocated=0.0; double btc_total_percentage_unallocated=0.0;double btc_total_capital_allocated=0.0; double btc_total_capital_unallocated=0.0;
    double eth_total_percentage_allocated=0.0; double eth_total_percentage_unallocated=0.0; double eth_total_capital_allocated=0.0; double eth_total_capital_unallocated=0.0;
    double usdt_total_percentage_allocated=0.0; double usdt_total_percentage_unallocated=0.0; double usdt_total_capital_allocated=0.0; double usdt_total_capital_unallocated=0.0;
    double eur_total_percentage_allocated=0.0; double eur_total_percentage_unallocated=0.0; double eur_total_capital_allocated=0.0; double eur_total_capital_unallocated=0.0;

    for (asset_iterator=assetManager->getAssets()->begin();asset_iterator!=assetManager->getAssets()->end();asset_iterator++)
    {
        trader::Asset *current_asset=asset_iterator.value();
        QString asset_market=current_asset->getMarket();
        double percentage_allocated=current_asset->getCapitalPercentage();
        double allocated_capital=current_asset->getAllocatedCapital();

        if (asset_market.contains("-USD")==true && asset_market.contains("-USDT")==false)
        {
            QString short_label=asset_market+QString(" - ")+QString::number(percentage_allocated,'g',3)+QString("%");
            usd_series->append(short_label,percentage_allocated);
            QString long_label=short_label+QString(" - ")+QString("$")+QString::number(allocated_capital,'f',6);
            usd_series_copy->append(long_label,percentage_allocated);
            usd_total_percentage_allocated+=percentage_allocated;
            usd_total_capital_allocated+=allocated_capital;
        }

        if (asset_market.contains("-BTC")==true)
        {
            QString short_label=asset_market+QString(" - ")+QString::number(percentage_allocated,'g',3)+QString("%");
            btc_series->append(short_label,percentage_allocated);
            QString long_label=short_label+QString(" - ")+QString("\u20BF")+QString::number(allocated_capital,'f',6);
            btc_series_copy->append(long_label,percentage_allocated);
            btc_total_percentage_allocated+=percentage_allocated;
            btc_total_capital_allocated+=allocated_capital;
        }

        if (asset_market.contains("-ETH")==true)
        {
            QString short_label=asset_market+QString(" - ")+QString::number(percentage_allocated,'g',3)+QString("%");
            eth_series->append(short_label,percentage_allocated);
            QString long_label=short_label+QString(" - ")+QString("\u039e")+QString::number(allocated_capital,'f',6);
            eth_series_copy->append(long_label,percentage_allocated);
            eth_total_percentage_allocated+=percentage_allocated;
            eth_total_capital_allocated+=allocated_capital;
        }

        if (asset_market.contains("-USDT")==true)
        {
            QString short_label=asset_market+QString(" - ")+QString::number(percentage_allocated,'g',3)+QString("%");
            usdt_series->append(short_label,percentage_allocated);
            QString long_label=short_label+QString(" - ")+QString("\u20ae")+QString::number(allocated_capital,'f',6);
            usdt_series_copy->append(long_label,percentage_allocated);
            usdt_total_percentage_allocated+=percentage_allocated;
            usdt_total_capital_allocated+=allocated_capital;
        }

        if (asset_market.contains("-EUR")==true)
        {
            QString short_label=asset_market+QString(" - ")+QString::number(percentage_allocated,'g',3)+QString("%");
            eur_series->append(short_label,percentage_allocated);
            QString long_label=short_label+QString(" - ")+QString("\u20ac")+QString::number(allocated_capital,'f',6);
            eur_series_copy->append(long_label,percentage_allocated);
            eur_total_percentage_allocated+=percentage_allocated;
            eur_total_capital_allocated+=allocated_capital;
        }
    }

    usd_total_percentage_unallocated=100.0-usd_total_percentage_allocated;
    btc_total_percentage_unallocated=100.0-btc_total_percentage_allocated;
    eth_total_percentage_unallocated=100.0-eth_total_percentage_allocated;
    usdt_total_percentage_unallocated=100.0-usdt_total_percentage_allocated;
    eur_total_percentage_unallocated=100.0-eur_total_percentage_allocated;

    usd_total_capital_unallocated=usd_initial_capital_allocated-usd_total_capital_allocated;
    btc_total_capital_unallocated=btc_initial_capital_allocated-btc_total_capital_allocated;
    eth_total_capital_unallocated=eth_initial_capital_allocated-eth_total_capital_allocated;
    usdt_total_capital_unallocated=usdt_initial_capital_allocated-usdt_total_capital_allocated;
    eur_total_capital_unallocated=eur_initial_capital_allocated-eur_total_capital_allocated;

    QString usd_short_label=tr("reserved")+QString(" - ")+QString::number(usd_total_percentage_unallocated,'g',3)+QString("%");
    usd_series->append(usd_short_label,usd_total_percentage_unallocated);
    QString usd_long_label=usd_short_label+QString(" - ")+QString("$")+QString::number(usd_total_capital_unallocated,'f',6);
    usd_series_copy->append(usd_long_label,usd_total_percentage_unallocated);

    QString btc_short_label=tr("reserved")+QString(" - ")+QString::number(btc_total_percentage_unallocated,'g',3)+QString("%");
    btc_series->append(btc_short_label,btc_total_percentage_unallocated);
    QString btc_long_label=btc_short_label+QString(" - ")+QString("\u20BF")+QString::number(btc_total_capital_unallocated,'f',6);
    btc_series_copy->append(btc_long_label,btc_total_percentage_unallocated);

    QString eth_short_label=tr("reserved")+QString(" - ")+QString::number(eth_total_percentage_unallocated,'g',3)+QString("%");
    eth_series->append(eth_short_label,eth_total_percentage_unallocated);
    QString eth_long_label=eth_short_label+QString(" - ")+QString("\u039e")+QString::number(eth_total_capital_unallocated,'f',6);
    eth_series_copy->append(eth_long_label,eth_total_percentage_unallocated);

    QString usdt_short_label=tr("reserved")+QString(" - ")+QString::number(usdt_total_percentage_unallocated,'g',3)+QString("%");
    usdt_series->append(usdt_short_label,usdt_total_percentage_unallocated);
    QString usdt_long_label=usdt_short_label+QString(" - ")+QString("\u20ae")+QString::number(usdt_total_capital_unallocated,'f',6);
    usdt_series_copy->append(usdt_long_label,usdt_total_percentage_unallocated);

    QString eur_short_label=tr("reserved")+QString(" - ")+QString::number(eur_total_percentage_unallocated,'g',3)+QString("%");
    eur_series->append(eur_short_label,eur_total_percentage_unallocated);
    QString eur_long_label=eur_short_label+QString(" - ")+QString("\u20ac")+QString::number(eur_total_capital_unallocated,'f',6);
    eur_series_copy->append(eur_long_label,eur_total_percentage_unallocated);

    this->connect(usd_series,&QPieSeries::hovered,this,[=](QPieSlice *slice,bool state) { slice->setLabelVisible(state); slice->setExploded(state); });
    this->connect(btc_series,&QPieSeries::hovered,this,[=](QPieSlice *slice,bool state) { slice->setLabelVisible(state); slice->setExploded(state); });
    this->connect(eth_series,&QPieSeries::hovered,this,[=](QPieSlice *slice,bool state) { slice->setLabelVisible(state); slice->setExploded(state); });
    this->connect(usdt_series,&QPieSeries::hovered,this,[=](QPieSlice *slice,bool state) { slice->setLabelVisible(state); slice->setExploded(state); });
    this->connect(eur_series,&QPieSeries::hovered,this,[=](QPieSlice *slice,bool state) { slice->setLabelVisible(state); slice->setExploded(state); });

    this->connect(usd_series_copy,&QPieSeries::hovered,this,[=](QPieSlice *slice,bool state) { slice->setLabelVisible(state); slice->setExploded(state); });
    this->connect(btc_series_copy,&QPieSeries::hovered,this,[=](QPieSlice *slice,bool state) { slice->setLabelVisible(state); slice->setExploded(state); });
    this->connect(eth_series_copy,&QPieSeries::hovered,this,[=](QPieSlice *slice,bool state) { slice->setLabelVisible(state); slice->setExploded(state); });
    this->connect(usdt_series_copy,&QPieSeries::hovered,this,[=](QPieSlice *slice,bool state) { slice->setLabelVisible(state); slice->setExploded(state); });
    this->connect(eur_series_copy,&QPieSeries::hovered,this,[=](QPieSlice *slice,bool state) { slice->setLabelVisible(state); slice->setExploded(state); });

    usd_chart->setTitle(tr("Allocated capital for USD-Portfolio"));
    usd_chart->setAnimationOptions(usd_chart->animationOptions() | QChart::AllAnimations);
    usd_chart->legend()->setAlignment(Qt::AlignLeft);
    usd_chart->legend()->setFont(QFont(usd_chart->legend()->font().family(),7));
    usd_chart->addSeries(usd_series);

    usd_chart_copy->setTitle(tr("Allocated capital for USD-Portfolio"));
    usd_chart_copy->setAnimationOptions(usd_chart_copy->animationOptions() | QChart::AllAnimations);
    usd_chart_copy->addSeries(usd_series_copy);
    usd_chart_copy->legend()->setAlignment(Qt::AlignLeft);

    btc_chart->setTitle(tr("Allocated capital for BTC-Portfolio"));
    btc_chart->setAnimationOptions(btc_chart->animationOptions() | QChart::AllAnimations);
    btc_chart->legend()->setAlignment(Qt::AlignLeft);
    btc_chart->legend()->setFont(QFont(usd_chart->legend()->font().family(),7));
    btc_chart->addSeries(btc_series);

    btc_chart_copy->setTitle(tr("Allocated capital for BTC-Portfolio"));
    btc_chart_copy->setAnimationOptions(btc_chart_copy->animationOptions() | QChart::AllAnimations);
    btc_chart_copy->legend()->setAlignment(Qt::AlignLeft);
    btc_chart_copy->addSeries(btc_series_copy);

    eth_chart->setTitle(tr("Allocated capital for ETH-Portfolio"));
    eth_chart->setAnimationOptions(eth_chart->animationOptions() | QChart::AllAnimations);
    eth_chart->legend()->setAlignment(Qt::AlignLeft);
    eth_chart->legend()->setFont(QFont(usd_chart->legend()->font().family(),7));
    eth_chart->addSeries(eth_series);

    eth_chart_copy->setTitle(tr("Allocated capital for ETH-Portfolio"));
    eth_chart_copy->setAnimationOptions(eth_chart_copy->animationOptions() | QChart::AllAnimations);
    eth_chart_copy->legend()->setAlignment(Qt::AlignLeft);
    eth_chart_copy->addSeries(eth_series_copy);

    usdt_chart->setTitle(tr("Allocated capital for USDT-Portfolio"));
    usdt_chart->setAnimationOptions(usdt_chart->animationOptions() | QChart::AllAnimations);
    usdt_chart->legend()->setAlignment(Qt::AlignLeft);
    usdt_chart->legend()->setFont(QFont(usd_chart->legend()->font().family(),7));
    usdt_chart->addSeries(usdt_series);

    usdt_chart_copy->setTitle(tr("Allocated capital for USDT-Portfolio"));
    usdt_chart_copy->setAnimationOptions(usdt_chart_copy->animationOptions() | QChart::AllAnimations);
    usdt_chart_copy->legend()->setAlignment(Qt::AlignLeft);
    usdt_chart_copy->addSeries(usdt_series_copy);

    eur_chart->setTitle(tr("Allocated capital for EUR-Portfolio"));
    eur_chart->setAnimationOptions(eur_chart->animationOptions() | QChart::AllAnimations);
    eur_chart->legend()->setAlignment(Qt::AlignLeft);
    eur_chart->legend()->setFont(QFont(eur_chart->legend()->font().family(),7));
    eur_chart->addSeries(eur_series);

    eur_chart_copy->setTitle(tr("Allocated capital for EUR-Portfolio"));
    eur_chart_copy->setAnimationOptions(eur_chart_copy->animationOptions() | QChart::AllAnimations);
    eur_chart_copy->legend()->setAlignment(Qt::AlignLeft);
    eur_chart_copy->legend()->setFont(QFont(eur_chart_copy->legend()->font().family(),7));
    eur_chart_copy->addSeries(eur_series_copy);

    this->ui->usdPortfolioSessionSummaryChartView->setChart(usd_chart);
    this->ui->btcPortfolioSessionSummaryChartView->setChart(btc_chart);
    this->ui->ethPortfolioSessionSummaryChartView->setChart(eth_chart);
    this->ui->usdtPortfolioSessionSummaryChartView->setChart(usdt_chart);
    this->ui->eurPortfolioSessionSummaryChartView->setChart(eur_chart);

    this->ui->usdPortfolioSessionSummaryChartView->getFullScreenChartView().setChart(usd_chart_copy);
    this->ui->btcPortfolioSessionSummaryChartView->getFullScreenChartView().setChart(btc_chart_copy);
    this->ui->ethPortfolioSessionSummaryChartView->getFullScreenChartView().setChart(eth_chart_copy);
    this->ui->usdtPortfolioSessionSummaryChartView->getFullScreenChartView().setChart(usdt_chart_copy);
    this->ui->eurPortfolioSessionSummaryChartView->getFullScreenChartView().setChart(eur_chart_copy);

    if (this->getColorMode()==MainWindow::ColorMode::LIGHT) { this->changeToLightModeCharts(); }
    if (this->getColorMode()==MainWindow::ColorMode::DARK) { this->changeToDarkModeCharts(); }
}



void MainWindow::changeEvent(QEvent *event)
{
    if (event->type()==QEvent::LanguageChange)
    {
        this->getUi()->retranslateUi(this);
        QList<QAction *> tray_actions=this->getSystemTray()->getTrayIcon()->contextMenu()->actions();
        for (int i=0;i<tray_actions.count();i++)
        {
            QAction *current_tray_action=tray_actions.at(i);
            QString current_tray_action_text=current_tray_action->data().toString();
            QString translated_text=QCoreApplication::translate("SystemTray",current_tray_action_text.toLatin1());
            current_tray_action->setText(translated_text);
        }
    }

    QMainWindow::changeEvent(event);
}


void MainWindow::showEvent(QShowEvent *event)
{
    QWidget::update();
    QMainWindow::showEvent(event);
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    QMainWindow::closeEvent(event);
}
