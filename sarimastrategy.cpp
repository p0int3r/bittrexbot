#include "sarimastrategy.hpp"

SARIMAStrategy::SARIMAStrategy(): SARIMAStrategy(QString(),QString(),trader::Strategy::FrequencyLevel::NONE) {}


SARIMAStrategy::SARIMAStrategy(QString name,QString description,trader::Strategy::FrequencyLevel frequencyLevel)
{
    this->setName(name);
    this->setDescription(description);
    this->setFrequencyLevel(frequencyLevel);
}


SARIMAStrategy::~SARIMAStrategy() {}


void SARIMAStrategy::preprocess(const QString &market,const QObject *object)
{
    Q_UNUSED(market)
    Q_UNUSED(object)

    // NOTE: emit signal at the end.
    emit this->preprocessed(market,object);
}

void SARIMAStrategy::strategise(const QString &market,const QObject *object)
{
    Q_UNUSED(market)
    Q_UNUSED(object)

    // NOTE: emit signal at the end.
    emit this->strategised(market,object);
}
