#include "strategytree.hpp"
#include <QDebug>


StrategyTree::StrategyTree(QWidget *parent): QTreeWidget(parent) { this->initSignalsAndSlots(); }

StrategyTree::~StrategyTree() {}

void StrategyTree::initSignalsAndSlots()
{
    this->connect(this,&StrategyTree::itemChanged,this,&StrategyTree::receiveItemChanged);
    this->connect(this,&StrategyTree::singleSelectionsChanged,this,&StrategyTree::filterSingleSelections);
}


void StrategyTree::receiveItemChanged(QTreeWidgetItem *item,int column)
{
    if (column!=0) { return; }
    if (item->parent()==nullptr) { return; }
    QString strategy=item->text(column);
    QString strategyCategoryStr=item->parent()->text(column);
    StrategyTree::StrategyCategory strategyCategory=StrategyTree::StrategyCategory::NONE;
    if (strategyCategoryStr==tr("Time Series")) { strategyCategory=StrategyTree::StrategyCategory::TIME_SERIES; }
    if (strategyCategoryStr==tr("Neural Networks")) { strategyCategory=StrategyTree::StrategyCategory::NEURAL_NETWORKS; }
    if (strategyCategoryStr==tr("Order Book Properties")) { strategyCategory=StrategyTree::StrategyCategory::ORDER_BOOK_PROPERTIES; }
    if (strategyCategoryStr==tr("Markov Models")) { strategyCategory=StrategyTree::StrategyCategory::MARKOV_MODELS; }
    if (strategyCategoryStr==tr("Filter Rule")) { strategyCategory=StrategyTree::StrategyCategory::FILTER_RULE; }
    if (item->checkState(column)==Qt::Unchecked) { emit this->strategyDeselected(strategyCategory,strategy); }
    if (item->checkState(column)==Qt::Checked)
    {
        emit this->strategySelected(strategyCategory,strategy);
        emit this->singleSelectionsChanged(item,column);
    }
}

void StrategyTree::filterSingleSelections(QTreeWidgetItem *item,int column)
{
    if (column!=0) { return; }
    if (item->parent()==nullptr) { return; }
    QString parent_name=item->parent()->text(column);
    if (parent_name!=tr("Filter Rule")) { return; }

    for (int i=0;i<item->parent()->childCount();i++)
    {
        QTreeWidgetItem *child_item=item->parent()->child(i);
        if (child_item==item) { continue; }
        bool isChecked=(child_item->checkState(column)==Qt::Checked);
        if (isChecked==true) { child_item->setCheckState(column,Qt::Unchecked); }
    }
}

void StrategyTree::enableItems()
{
    for (int i=0;i<this->topLevelItemCount();i++)
    {
        QTreeWidgetItem *current_top_item=this->topLevelItem(i);
        current_top_item->setFlags(current_top_item->flags() | Qt::ItemIsEnabled);
    }
}

void StrategyTree::disableItems()
{
    for (int i=0;i<this->topLevelItemCount();i++)
    {
        QTreeWidgetItem *current_top_item=this->topLevelItem(i);
        current_top_item->setFlags(current_top_item->flags() & ~Qt::ItemIsEnabled);
    }
}
