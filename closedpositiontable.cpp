#include "closedpositiontable.hpp"
#include "qtablenumericitem.hpp"


ClosedPositionTable::ClosedPositionTable(QWidget *parent): QTableWidget(parent)
{
    this->setMap(new QMap<QString,QList<QTableWidgetItem *>>());
    this->initializeSignalsAndSlots();
}

ClosedPositionTable::~ClosedPositionTable() { delete this->map; }

void ClosedPositionTable::initializeSignalsAndSlots()
{
    // TODO: implement.
}

QMap<QString, QList<QTableWidgetItem *> > *ClosedPositionTable::getMap() const { return this->map; }

void ClosedPositionTable::setMap(QMap<QString, QList<QTableWidgetItem *> > *value) { this->map=value; }

ClosedPositionTable::ColorMode ClosedPositionTable::getColorMode() const { return this->colorMode; }

void ClosedPositionTable::setColorMode(const ClosedPositionTable::ColorMode &value) { this->colorMode=value; }

void ClosedPositionTable::changeToLightMode()
{
    this->setColorMode(ClosedPositionTable::ColorMode::LIGHT);
    QMap<QString,QList<QTableWidgetItem *>>::iterator iterator;
    for (iterator=this->getMap()->begin();iterator!=this->getMap()->end();iterator++)
    {
        QString foregroundColor=QString();
        QList<QTableWidgetItem *> tableWidgetList=iterator.value();
        QTableWidgetItem *realisedPnLItem=tableWidgetList.at(5);
        double realisedPnL=realisedPnLItem->text().toDouble();
        if (realisedPnL>0.0) { foregroundColor=QString("green"); }
        if (realisedPnL==0.0) { foregroundColor=QString("orange"); }
        if (realisedPnL<0.0) { foregroundColor=QString("red"); }
        realisedPnLItem->setForeground(QColor(foregroundColor));
        QTableWidgetItem *unrealisedPnLItem=tableWidgetList.at(6);
        double unrealisedPnL=unrealisedPnLItem->text().toDouble();
        if (unrealisedPnL>0.0) { foregroundColor=QString("green"); }
        if (unrealisedPnL==0.0) { foregroundColor=QString("orange"); }
        if (unrealisedPnL<0.0) { foregroundColor=QString("red"); }
        unrealisedPnLItem->setForeground(QColor(foregroundColor));
        QTableWidgetItem *quantityItem=tableWidgetList.at(7);
        quantityItem->setForeground(QColor(0,85,127));
        QTableWidgetItem *priceItem=tableWidgetList.at(8);
        priceItem->setForeground(QColor("purple"));
    }
}

void ClosedPositionTable::changeToDarkMode()
{
    this->setColorMode(ClosedPositionTable::ColorMode::DARK);
    QMap<QString,QList<QTableWidgetItem *>>::iterator iterator;
    for (iterator=this->getMap()->begin();iterator!=this->getMap()->end();iterator++)
    {
        QString foregroundColor=QString();
        QList<QTableWidgetItem *> tableWidgetList=iterator.value();
        QTableWidgetItem *realisedPnLItem=tableWidgetList.at(5);
        double realisedPnL=realisedPnLItem->text().toDouble();
        if (realisedPnL>0.0) { foregroundColor=QString("lime"); }
        if (realisedPnL==0.0) { foregroundColor=QString("yellow"); }
        if (realisedPnL<0.0) { foregroundColor=QString("orange"); }
        realisedPnLItem->setForeground(QColor(foregroundColor));
        QTableWidgetItem *unrealisedPnLItem=tableWidgetList.at(6);
        double unrealisedPnL=unrealisedPnLItem->text().toDouble();
        if (unrealisedPnL>0.0) { foregroundColor=QString("lime"); }
        if (unrealisedPnL==0.0) { foregroundColor=QString("yellow"); }
        if (unrealisedPnL<0.0) { foregroundColor=QString("orange"); }
        unrealisedPnLItem->setForeground(QColor(foregroundColor));
        QTableWidgetItem *quantityItem=tableWidgetList.at(7);
        quantityItem->setForeground(QColor("cyan"));
        QTableWidgetItem *priceItem=tableWidgetList.at(8);
        priceItem->setForeground(QColor("#ffddf4"));
    }
}


void ClosedPositionTable::insertTableItems(const QString &market,const trader::Position *position)
{
    QString foregroundColor=QString();
    int currentRow=this->rowCount();
    this->insertRow(currentRow);
    QList<QTableWidgetItem *> tableWidgetList=QList<QTableWidgetItem *>();

    QTableWidgetItem *actionItem=new QTableWidgetItem();
    actionItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    actionItem->setText(QString("SOLD"));
    actionItem->setToolTip(QString("SOLD"));
    this->setItem(currentRow,0,actionItem);

    QTableWidgetItem *marketItem=new QTableWidgetItem();
    marketItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    QString marketStr=position->getMarket();
    marketItem->setText(marketStr);
    marketItem->setToolTip(marketStr);
    this->setItem(currentRow,1,marketItem);

    QTableNumericItem *averagePriceItem=new QTableNumericItem();
    averagePriceItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double averagePrice=position->getAveragePrice();
    averagePriceItem->setText(QString::number(averagePrice,'g'));
    averagePriceItem->setToolTip(QString::number(averagePrice,'f',8));
    this->setItem(currentRow,2,averagePriceItem);

    QTableNumericItem *marketValueItem=new QTableNumericItem();
    marketValueItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double marketValue=position->getMarketValue();
    marketValueItem->setText(QString::number(marketValue,'g'));
    marketValueItem->setToolTip(QString::number(marketValue,'f',8));
    this->setItem(currentRow,3,marketValueItem);

    QTableNumericItem *costBasisItem=new QTableNumericItem();
    costBasisItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double costBasis=position->getCostBasis();
    costBasisItem->setText(QString::number(costBasis,'g'));
    costBasisItem->setToolTip(QString::number(costBasis,'f',8));
    this->setItem(currentRow,4,costBasisItem);

    QTableNumericItem *realisedPnLItem=new QTableNumericItem();
    realisedPnLItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double realisedPnL=position->getRealisedPnL();
    if (this->getColorMode()==ClosedPositionTable::ColorMode::LIGHT)
    {
        if (realisedPnL>0.0) { foregroundColor=QString("green"); }
        if (realisedPnL==0.0) { foregroundColor=QString("orange"); }
        if (realisedPnL<0.0) { foregroundColor=QString("red"); }
    }
    if (this->getColorMode()==ClosedPositionTable::ColorMode::DARK)
    {
        if (realisedPnL>0.0) { foregroundColor=QString("lime"); }
        if (realisedPnL==0.0) { foregroundColor=QString("yellow"); }
        if (realisedPnL<0.0) { foregroundColor=QString("orange"); }
    }
    realisedPnLItem->setForeground(QColor(foregroundColor));
    realisedPnLItem->setText(QString::number(realisedPnL,'g'));
    realisedPnLItem->setToolTip(QString::number(realisedPnL,'f',8));
    this->setItem(currentRow,5,realisedPnLItem);

    QTableNumericItem *unrealisedPnLItem=new QTableNumericItem();
    unrealisedPnLItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double unrealisedPnL=position->getUnrealisedPnL();
    if (this->getColorMode()==ClosedPositionTable::ColorMode::LIGHT)
    {
        if (unrealisedPnL>0.0) { foregroundColor=QString("green"); }
        if (unrealisedPnL==0.0) { foregroundColor=QString("orange"); }
        if (unrealisedPnL<0.0) { foregroundColor=QString("red"); }
    }
    if (this->getColorMode()==ClosedPositionTable::ColorMode::DARK)
    {
        if (unrealisedPnL>0.0) { foregroundColor=QString("lime"); }
        if (unrealisedPnL==0.0) { foregroundColor=QString("yellow"); }
        if (unrealisedPnL<0.0) { foregroundColor=QString("orange"); }
    }
    unrealisedPnLItem->setForeground(QColor(foregroundColor));
    unrealisedPnLItem->setText(QString::number(unrealisedPnL,'g'));
    unrealisedPnLItem->setToolTip(QString::number(unrealisedPnL,'f',8));
    this->setItem(currentRow,6,unrealisedPnLItem);

    QTableNumericItem *quantityItem=new QTableNumericItem();
    quantityItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double quantity=position->getQuantity();
    if (this->getColorMode()==ClosedPositionTable::ColorMode::LIGHT) { quantityItem->setForeground(QColor(0,85,127)); }
    if (this->getColorMode()==ClosedPositionTable::ColorMode::DARK) { quantityItem->setForeground(QColor("cyan")); }
    quantityItem->setText(QString::number(quantity,'g'));
    quantityItem->setToolTip(QString::number(quantity,'f',8));
    this->setItem(currentRow,7,quantityItem);

    QTableNumericItem *priceItem=new QTableNumericItem();
    priceItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double price=position->getPrice();
    if (this->getColorMode()==ClosedPositionTable::ColorMode::LIGHT) { priceItem->setForeground(QColor("purple")); }
    if (this->getColorMode()==ClosedPositionTable::ColorMode::DARK) { priceItem->setForeground(QColor("#ffddf4")); }
    priceItem->setText(QString::number(price,'g'));
    priceItem->setToolTip(QString::number(price,'f',8));
    this->setItem(currentRow,8,priceItem);


    QTableNumericItem *buysItem=new QTableNumericItem();
    buysItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double buys=position->getBuys();
    buysItem->setText(QString::number(buys,'g'));
    buysItem->setToolTip(QString::number(buys,'f',8));
    this->setItem(currentRow,9,buysItem);

    QTableNumericItem *sellsItem=new QTableNumericItem();
    sellsItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double sells=position->getSells();
    sellsItem->setText(QString::number(sells,'g'));
    sellsItem->setToolTip(QString::number(sells,'f',8));
    this->setItem(currentRow,10,sellsItem);

    QTableNumericItem *netItem=new QTableNumericItem();
    netItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double net=position->getNet();
    netItem->setText(QString::number(net,'g'));
    netItem->setToolTip(QString::number(net,'f',8));
    this->setItem(currentRow,11,netItem);

    QTableNumericItem *netTotalItem=new QTableNumericItem();
    netTotalItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double netTotal=position->getNetTotal();
    netTotalItem->setText(QString::number(netTotal,'g'));
    netTotalItem->setToolTip(QString::number(netTotal,'f',8));
    this->setItem(currentRow,12,netTotalItem);

    QTableNumericItem *netInclusiveCommissionItem=new QTableNumericItem();
    netInclusiveCommissionItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double netInclusiveCommission=position->getNetInclusiveCommission();
    netInclusiveCommissionItem->setText(QString::number(netInclusiveCommission,'g'));
    netInclusiveCommissionItem->setToolTip(QString::number(netInclusiveCommission,'f',8));
    this->setItem(currentRow,13,netInclusiveCommissionItem);

    tableWidgetList.insert(0,actionItem);
    tableWidgetList.insert(1,marketItem);
    tableWidgetList.insert(2,averagePriceItem);
    tableWidgetList.insert(3,marketValueItem);
    tableWidgetList.insert(4,costBasisItem);
    tableWidgetList.insert(5,realisedPnLItem);
    tableWidgetList.insert(6,unrealisedPnLItem);
    tableWidgetList.insert(7,quantityItem);
    tableWidgetList.insert(8,priceItem);
    tableWidgetList.insert(9,buysItem);
    tableWidgetList.insert(10,sellsItem);
    tableWidgetList.insert(11,netItem);
    tableWidgetList.insert(12,netTotalItem);
    tableWidgetList.insert(13,netInclusiveCommissionItem);
    this->getMap()->insert(market,tableWidgetList);

    emit this->tableItemsInserted(market,position);}


void ClosedPositionTable::cancelTableItems(const QString &market,const trader::Position *position)
{
    QList<QTableWidgetItem *> tableWidgetList=this->getMap()->value(market);
    if (tableWidgetList.length()==0) { return; }
    int current_row=tableWidgetList.at(0)->row();
    this->removeRow(current_row);
    this->getMap()->remove(market);
    emit this->tableItemsCanceled(market,position);
}

void ClosedPositionTable::removeTableItems(const QString &market,const trader::Position *position)
{
    QList<QTableWidgetItem *> tableWidgetList=this->getMap()->value(market);
    if (tableWidgetList.length()==0) { return; }
    int current_row=tableWidgetList.at(0)->row();
    this->removeRow(current_row);
    this->getMap()->remove(market);
    emit this->tableItemsRemoved(market,position);
}

void ClosedPositionTable::modifyTableItems(const QString &market,const trader::Position *position)
{
    QList<QTableWidgetItem *> tableWidgetList=this->getMap()->value(market);
    if (tableWidgetList.length()==0) { return; }
    QString actionStr=QString();
    QString foregroundColor=QString();
    QTableWidgetItem *actionItem=tableWidgetList.at(0);
    trader::Position::Action action=position->getAction();
    if (action==trader::Position::Action::SOLD) { actionStr=QString("SOLD"); }
    if (action==trader::Position::Action::BOUGHT) { actionStr=QString("BOUGHT"); }
    actionItem->setText(actionStr);

    QTableWidgetItem *averagePriceItem=tableWidgetList.at(2);
    double averagePrice=position->getAveragePrice();
    averagePriceItem->setText(QString::number(averagePrice));

    QTableWidgetItem *marketValueItem=tableWidgetList.at(3);
    double marketValue=position->getMarketValue();
    marketValueItem->setText(QString::number(marketValue));

    QTableWidgetItem *costBasisItem=tableWidgetList.at(4);
    costBasisItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double costBasis=position->getCostBasis();
    costBasisItem->setText(QString::number(costBasis));

    QTableWidgetItem *realisedPnLItem=tableWidgetList.at(5);
    double realisedPnL=position->getRealisedPnL();
    if (this->getColorMode()==ClosedPositionTable::ColorMode::LIGHT)
    {
        if (realisedPnL>0.0) { foregroundColor=QString("green"); }
        if (realisedPnL==0.0) { foregroundColor=QString("orange"); }
        if (realisedPnL<0.0) { foregroundColor=QString("red"); }
    }
    if (this->getColorMode()==ClosedPositionTable::ColorMode::DARK)
    {
        if (realisedPnL>0.0) { foregroundColor=QString("lime"); }
        if (realisedPnL==0.0) { foregroundColor=QString("yellow"); }
        if (realisedPnL<0.0) { foregroundColor=QString("orange"); }
    }
    realisedPnLItem->setForeground(QColor(foregroundColor));
    realisedPnLItem->setText(QString::number(realisedPnL));

    QTableWidgetItem *unrealisedPnLItem=tableWidgetList.at(6);
    double unrealisedPnL=position->getUnrealisedPnL();
    if (this->getColorMode()==ClosedPositionTable::ColorMode::LIGHT)
    {
        if (unrealisedPnL>0.0) { foregroundColor=QString("green"); }
        if (unrealisedPnL==0.0) { foregroundColor=QString("orange"); }
        if (unrealisedPnL<0.0) { foregroundColor=QString("red"); }
    }
    if (this->getColorMode()==ClosedPositionTable::ColorMode::DARK)
    {
        if (unrealisedPnL>0.0) { foregroundColor=QString("lime"); }
        if (unrealisedPnL==0.0) { foregroundColor=QString("yellow"); }
        if (unrealisedPnL<0.0) { foregroundColor=QString("orange"); }
    }
    unrealisedPnLItem->setForeground(QColor(foregroundColor));
    unrealisedPnLItem->setText(QString::number(unrealisedPnL));

    QTableWidgetItem *quantityItem=tableWidgetList.at(7);
    double quantity=position->getQuantity();
    if (this->getColorMode()==ClosedPositionTable::ColorMode::LIGHT) { quantityItem->setForeground(QColor(0,85,127)); }
    if (this->getColorMode()==ClosedPositionTable::ColorMode::DARK) { quantityItem->setForeground(QColor("cyan")); }
    quantityItem->setText(QString::number(quantity));

    QTableWidgetItem *priceItem=tableWidgetList.at(8);
    double price=position->getPrice();
    if (this->getColorMode()==ClosedPositionTable::ColorMode::LIGHT) { priceItem->setForeground(QColor("purple")); }
    if (this->getColorMode()==ClosedPositionTable::ColorMode::DARK) { priceItem->setForeground(QColor("#ffddf4")); }
    priceItem->setText(QString::number(price));

    QTableWidgetItem *buysItem=tableWidgetList.at(9);
    double buys=position->getBuys();
    buysItem->setText(QString::number(buys));

    QTableWidgetItem *sellsItem=tableWidgetList.at(10);
    double sells=position->getSells();
    sellsItem->setText(QString::number(sells));

    QTableWidgetItem *netItem=tableWidgetList.at(11);
    netItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double net=position->getNet();
    netItem->setText(QString::number(net));

    QTableWidgetItem *netTotalItem=tableWidgetList.at(12);
    netTotalItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double netTotal=position->getNetTotal();
    netTotalItem->setText(QString::number(netTotal));

    QTableWidgetItem *netInclusiveCommissionItem=tableWidgetList.at(13);
    netInclusiveCommissionItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double netInclusiveCommission=position->getNetInclusiveCommission();
    netInclusiveCommissionItem->setText(QString::number(netInclusiveCommission));

    emit this->tableItemsModified(market,position);
}
