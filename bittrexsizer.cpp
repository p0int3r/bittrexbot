#include <cmath>
#include "bittrexsizer.hpp"
#include "bittrexasset.hpp"
#include "bittrexanalyser.hpp"


BittrexSizer::BittrexSizer(): BittrexSizer(nullptr) {}

BittrexSizer::BittrexSizer(trader::Trader *trader) { this->setTrader(trader); }

BittrexSizer::~BittrexSizer() { this->setTrader(nullptr); }

trader::Trader *BittrexSizer::getTrader() const { return this->trader; }

void BittrexSizer::setTrader(trader::Trader *value) { this->trader=value; }

inline double modify_precision(double x, int n)
{
    int d=0;
    if ((x*pow(10,n+1))-(floor(x*pow(10,n)))>4) { d=1; }
    x =(floor(x*pow(10,n))+d)/pow(10,n);
    return x;
}

void BittrexSizer::size(const QString &market,const QObject *object)
{
    trader::Order *order=const_cast<trader::Order *>(static_cast<const trader::Order *>(object));
    trader::Order::Type order_type=order->getType();
    trader::Trader *trader=this->getTrader();
    BittrexAsset *bittrex_asset=static_cast<BittrexAsset *>(trader->getAssetManager()->getAsset(market));

    if (order_type==trader::Order::Type::LIMIT_BUY)
    {
        double allocated_capital=bittrex_asset->getAllocatedCapital();
        int price_precision=bittrex_asset->getPrecision();
        double middle_price=order->getPrice();
        middle_price=modify_precision(middle_price,price_precision);
        order->setPrice(middle_price);
        double calculated_quantity=allocated_capital/middle_price;
        calculated_quantity=modify_precision(calculated_quantity,8);
        order->setQuantity(calculated_quantity);
        emit this->resized(order->getOrderId(),order);
    }

    if (order_type==trader::Order::Type::LIMIT_SELL)
    {
        int precision=bittrex_asset->getPrecision();
        double middle_price=order->getPrice();
        middle_price=modify_precision(middle_price,precision);
        order->setPrice(middle_price);
        emit this->resized(order->getOrderId(),order);
    }
}
