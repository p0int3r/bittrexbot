#include "orderbookpropertystrategyfactory.hpp"

OrderBookPropertyStrategyFactory::OrderBookPropertyStrategyFactory(QObject *parent): QObject(parent) {}

OrderBookPropertyStrategyFactory::~OrderBookPropertyStrategyFactory() {}

trader::Strategy *OrderBookPropertyStrategyFactory::createOrderBookPropertyStrategy(QString name)
{
    Q_UNUSED(name)
    trader::Strategy *orderBookPropertyStrategy=nullptr;
    return orderBookPropertyStrategy;
}

trader::Strategy *OrderBookPropertyStrategyFactory::createOrderBookPropertyStrategy(OrderBookPropertyStrategyFactory::OrderBookProperty orderBookProperty)
{
    Q_UNUSED(orderBookProperty)
    trader::Strategy *orderBookPropertyStrategy=nullptr;
    return orderBookPropertyStrategy;
}
