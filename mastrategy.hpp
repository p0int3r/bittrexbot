#ifndef MASTRATEGY_HPP
#define MASTRATEGY_HPP

#include <QObject>
#include <QString>
#include <strategy.hpp>

// TODO: Implement the moving average time series model.

class MAStrategy: public trader::Strategy
{
    Q_OBJECT

    public:
        MAStrategy();
        MAStrategy(QString name,QString description,
            trader::Strategy::FrequencyLevel frequencyLevel);
        ~MAStrategy();


    public slots:
        void preprocess(const QString &market,const QObject *object) override;
        void strategise(const QString &market,const QObject *object) override;
};

#endif // MASTRATEGY_HPP
