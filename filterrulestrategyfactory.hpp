#ifndef FILTERRULESTRATEGYFACTORY_HPP
#define FILTERRULESTRATEGYFACTORY_HPP

#include <QObject>
#include <QString>
#include "strategy.hpp"

class FilterRuleStrategyFactory: public QObject
{
    Q_OBJECT

    public:
        explicit FilterRuleStrategyFactory(QObject *parent=nullptr);
        ~FilterRuleStrategyFactory();

    signals:

    public slots:
        trader::Strategy *createFilterRuleStrategy(double filterRule,int backwardSteps);
        trader::Strategy *createFilterRuleStrategy(QString name,int backwardSteps);
        QString translateFilterRuleStrategyToSourceLanguageceLanguage(QString name);
};

#endif // FILTERRULESTRATEGYFACTORY_HPP
