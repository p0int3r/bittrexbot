#ifndef MARKETTABLE_HPP
#define MARKETTABLE_HPP

#include <QWidget>
#include <QMap>
#include <QJsonObject>
#include <QTableWidget>



class MarketTable: public QTableWidget
{
    Q_OBJECT

    public:
        enum class ColorMode
        {
            LIGHT,
            DARK
        }; Q_ENUM(ColorMode)

    private:
        MarketTable::ColorMode colorMode;
        QMap<QString,QList<QTableWidgetItem *>> *map=nullptr;

    public:
        explicit MarketTable(QWidget *parent=nullptr);
        ~MarketTable();
        MarketTable::ColorMode getColorMode() const;
        void setColorMode(const MarketTable::ColorMode &value);
        QMap<QString,QList<QTableWidgetItem *>> *getMap() const;
        void setMap(QMap<QString,QList<QTableWidgetItem *>> *value);
        void initializeSignalsAndSlots();

    signals:
        void tableItemsModified(const QString &market,const QJsonObject &data);
        void tableItemsInserted(const QString &market,const QJsonObject &data);
        void tableItemsRemoved(const QString &market,const QJsonObject &data);
        void marketSelected(const QString &market);
        void marketDeselected(const QString &market);

    public slots:
        void changeToLightMode();
        void changeToDarkMode();
        void insertTableItems(const QString &market,const QJsonObject &data);
        void removeTableItems(const QString &market,const QJsonObject &data);
        void modifyTableItems(const QString &market,const QJsonObject &data);
};

#endif // MARKETTABLE_HPP
