#include "takeprofitrisk.hpp"
#include "position.hpp"
#include "order.hpp"

TakeProfitRisk::TakeProfitRisk(): TakeProfitRisk(QString(),QString(),trader::Risk::RiskIndicator::NONE) {}

TakeProfitRisk::TakeProfitRisk(QString name,QString description,trader::Risk::RiskIndicator riskIndicator)
{
    this->setName(name);
    this->setDescription(description);
    this->setRiskIndicator(riskIndicator);
}

TakeProfitRisk::~TakeProfitRisk() {}

double TakeProfitRisk::getTakeProfit() const { return this->takeProfit; }

void TakeProfitRisk::setTakeProfit(double value) { this->takeProfit=value; }


void TakeProfitRisk::preprocess(const QString &market,const QObject *object)
{
    trader::Position *position=const_cast<trader::Position *>(static_cast<const trader::Position *>(object));
    double unrealised_pnl=position->getUnrealisedPnL();
    double cost_basis=position->getCostBasis();
    double middle_price=position->getMiddlePrice();
    double quantity=position->getBuys();
    double commission=quantity*middle_price*0.0020;
    double take_profit_percentage=(unrealised_pnl-commission)/cost_basis;
    bool detected_take_profit=(take_profit_percentage>=this->getTakeProfit());
    if (detected_take_profit==true) { emit this->preprocessed(market,object); }
}


void TakeProfitRisk::assess(const QString &market,const QObject *object)
{
    const trader::Position *position=static_cast<const trader::Position *>(object);
    double middle_price=position->getMiddlePrice();
    double volume=position->getQuantity();
    trader::Order *order=new trader::Order();
    order->setMarket(market);
    order->setQuantity(volume);
    order->setPrice(middle_price);
    order->setType(trader::Order::Type::LIMIT_SELL);
    order->setExchange(trader::Order::Exchange::BITTREX);
    order->setStatus(trader::Order::Status::SUGGESTED);
    emit this->assessed(market,order);
}
