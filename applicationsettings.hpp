#ifndef APPLICATIONSETTINGS_HPP
#define APPLICATIONSETTINGS_HPP

#include <QObject>
#include <QString>

class ApplicationSettings: public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool lightMode READ getLightMode WRITE setLightMode NOTIFY lightModeChanged)
    Q_PROPERTY(bool darkMode READ getDarkMode WRITE setDarkMode NOTIFY darkModeChanged)
    Q_PROPERTY(bool logger READ getLogger WRITE setLogger NOTIFY loggerChanged)
    Q_PROPERTY(QString language READ getLanguage WRITE setLanguage NOTIFY languageChanged)

    private:
        bool lightMode;
        bool darkMode;
        bool logger;
        QString language;
        QString applicationSettingsFilePath;

        bool existsApplicationSettingsFile();
        bool createApplicationSettingsFile();
        bool loadApplicationSettingsFile();

    public:
        explicit ApplicationSettings(QObject *parent=nullptr);
        bool getLightMode() const;
        void setLightMode(bool value);
        bool getDarkMode() const;
        void setDarkMode(bool value);
        bool getLogger() const;
        void setLogger(bool value);
        QString getLanguage() const;
        void setLanguage(const QString &value);
        QString getApplicationSettingsFilePath() const;
        void setApplicationSettingsFilePath(const QString &value);
        void initializeSignalsAndSlots();

    signals:
        void lightModeChanged(const bool &lightMode);
        void darkModeChanged(const bool &darkMode);
        void loggerChanged(const bool &logger);
        void languageChanged(const QString &language);

    public slots:
        void defaults();
        void save();
        void restore();
};

#endif // APPLICATIONSETTINGS_HPP
