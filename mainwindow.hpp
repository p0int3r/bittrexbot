#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>
#include <QListWidgetItem>
#include <QTimer>
#include <QString>
#include <QCloseEvent>
#include <QEvent>
#include <QShowEvent>
#include "v3_bittrexrestapihandler.hpp"
#include "qcustomplot.hpp"
#include "trader.hpp"
#include "strategytree.hpp"
#include "risktree.hpp"
#include "timeseriesstrategyfactory.hpp"
#include "orderbookpropertystrategyfactory.hpp"
#include "filterrulestrategyfactory.hpp"
#include "takeprofitriskfactory.hpp"
#include "stoplossriskfactory.hpp"
#include "tradingsessionstate.hpp"
#include "dialogmanager.hpp"
#include "systemtray.hpp"
#include "languageselectordialog.hpp"
#include "applicationsettings.hpp"


namespace Ui { class MainWindow; }

class MainWindow: public QMainWindow
{
    Q_OBJECT

    public:

        enum class TradingMode
        {
            PAPER_TRADING,
            LIVE_TRADING
        }; Q_ENUM(TradingMode)

        enum class ColorMode
        {
            DARK,
            LIGHT
        }; Q_ENUM(ColorMode)

        enum class SessionStatus
        {
            EXECUTING,
            PAUSED,
            STOPPED
        }; Q_ENUM(SessionStatus)

    private:
        ApplicationSettings *applicationSettings=nullptr;
        MainWindow::SessionStatus sessionStatus;
        MainWindow::ColorMode colorMode;
        MainWindow::TradingMode tradingMode;
        DialogManager *dialogManager=nullptr;
        SystemTray *systemTray=nullptr;
        LanguageSelectorDialog *languageSelectorDialog=nullptr;
        TradingSessionState *tradingSessionState=nullptr;
        Ui::MainWindow *ui=nullptr;
        trader::Trader *trader=nullptr;
        QTimer *globalTimer=nullptr;
        bittrexapi::v3::BittrexRestApiHandler *bittrexRestApiHandler=nullptr;
        TimeSeriesStrategyFactory *timeSeriesStrategyFactory=nullptr;
        OrderBookPropertyStrategyFactory *orderBookPropertyStrategyFactory=nullptr;
        FilterRuleStrategyFactory *filterRuleStrategyFactory=nullptr;
        TakeProfitRiskFactory *takeProfitRiskFactory=nullptr;
        StopLossRiskFactory *stopLossRiskFactory=nullptr;

        void initializeQDoubleSpinBoxs();
        void initializeQActions();
        void initializeQToolBars();
        void initializeQListWidgets();
        void initializeQTableWidgets();
        void initializeQTreeWidgets();
        void initializeQTabWidgets();
        void initializeQChartViews();
        void initializeSignalsAndSlots();
        void initializeLogger();
        void initializePortfolios();
        void initializeFilterers();
        void initializeSizers();

        void addMarketSelection(const QString &market);
        void removeMarketSelection(const QString &market);

        void addStrategySelection(const StrategyTree::StrategyCategory &strategyCategory,const QString &strategy);
        void removeStrategySelection(const StrategyTree::StrategyCategory &strategyCategory,const QString &strategy);

        void addRiskSelection(const RiskTree::RiskCategory &riskCategory,const QString &risk);
        void removeRiskSelection(const RiskTree::RiskCategory &riskCategory,const QString &risk);

        void applyCapitalPercentage(const QString &market,const double &capitalPercentage);
        void applyTradingRounds(const QString &market,const int &tradingRounds);

        void executeBittrexExecution(const QString &orderId,const trader::Order *order);
        void removeBittrexExecution(const QString &orderId,const trader::Order *order);

        void modifyUsdPortfolioUnrealisedPnL(const double &unrealisedPnL);
        void modifyUsdPortfolioRealisedPnL(const double &realisedPnL);
        void modifyUsdPortfolioInitialCapital(const double &initialCapital);
        void modifyUsdPortfolioCurrentCapital(const double &currentCapital);
        void modifyUsdPortfolioEquity(const double &equity);
        void modifyUsdPortfolioSessionSummaryReturnOnInvestment(const double &returnOnInvestment);

        void modifyBtcPortfolioUnrealisedPnL(const double &unrealisedPnL);
        void modifyBtcPortfolioRealisedPnL(const double &realisedPnL);
        void modifyBtcPortfolioInitialCapital(const double &initialCapital);
        void modifyBtcPortfolioCurrentCapital(const double &currentCapital);
        void modifyBtcPortfolioEquity(const double &equity);
        void modifyBtcPortfolioSessionSummaryReturnOnInvestment(const double &returnOnInvestment);

        void modifyEthPortfolioUnrealisedPnL(const double &unrealisedPnL);
        void modifyEthPortfolioRealisedPnL(const double &realisedPnL);
        void modifyEthPortfolioInitialCapital(const double &initialCapital);
        void modifyEthPortfolioCurrentCapital(const double &currentCapital);
        void modifyEthPortfolioEquity(const double &equity);
        void modifyEthPortfolioSessionSummaryReturnOnInvestment(const double &returnOnInvestment);

        void modifyUsdtPortfolioUnrealisedPnL(const double &unrealisedPnL);
        void modifyUsdtPortfolioRealisedPnL(const double &realisedPnL);
        void modifyUsdtPortfolioInitialCapital(const double &initialCapital);
        void modifyUsdtPortfolioCurrentCapital(const double &currentCapital);
        void modifyUsdtPortfolioEquity(const double &equity);
        void modifyUsdtPortfolioSessionSummaryReturnOnInvestment(const double &returnOnInvestment);

        void modifyEurPortfolioUnrealisedPnL(const double &unrealisedPnL);
        void modifyEurPortfolioRealisedPnL(const double &realisedPnL);
        void modifyEurPortfolioInitialCapital(const double &initialCapital);
        void modifyEurPortfolioCurrentCapital(const double &currentCapital);
        void modifyEurPortfolioEquity(const double &equity);
        void modifyEurPortfolioSessionSummaryReturnOnInvestment(const double &returnOnInvestment);

        void modifyPortfolio(const QString &orderId,const trader::Order *order);

        void modifyTradingRound(const QString &market,const trader::Position *position);
        void checkAllTradingRounds();

        void populateSessionSummaryChartViews();

        void enableMarketsPageCheckableWidgets();
        void enableTraderPageCheckableWidgets();
        void enableExecutionPageCheckableWidgets();

        void disableMarketsPageCheckableWidgets();
        void disableTraderPageCheckableWidgets();
        void disableExecutionPageCheckableWidgets();

        void restoreMarketsPageCheckableWidgets();
        void restoreTraderPageCheckableWidgets();
        void restoreExecutionPageCheckableWidgets();

        void checkRestoration();
        void restoreSettings();

    protected:
        void changeEvent(QEvent *event) override;
        void showEvent(QShowEvent *event) override;
        void closeEvent(QCloseEvent *event) override;

    public:
        explicit MainWindow(QWidget *parent=nullptr);
        ~MainWindow() override;
        QTimer *getGlobalTimer() const;
        void setGlobalTimer(QTimer *value);
        trader::Trader *getTrader() const;
        void setTrader(trader::Trader *value);
        bittrexapi::v3::BittrexRestApiHandler *getBittrexRestApiHandler() const;
        void setBittrexRestApiHandler(bittrexapi::v3::BittrexRestApiHandler *value);
        TimeSeriesStrategyFactory *getTimeSeriesStrategyFactory() const;
        void setTimeSeriesStrategyFactory(TimeSeriesStrategyFactory *value);
        OrderBookPropertyStrategyFactory *getOrderBookPropertyStrategyFactory() const;
        void setOrderBookPropertyStrategyFactory(OrderBookPropertyStrategyFactory *value);
        FilterRuleStrategyFactory *getFilterRuleStrategyFactory() const;
        void setFilterRuleStrategyFactory(FilterRuleStrategyFactory *value);
        TakeProfitRiskFactory *getTakeProfitRiskFactory() const;
        void setTakeProfitRiskFactory(TakeProfitRiskFactory *value);
        StopLossRiskFactory *getStopLossRiskFactory() const;
        void setStopLossRiskFactory(StopLossRiskFactory *value);
        MainWindow::ColorMode getColorMode() const;
        void setColorMode(const MainWindow::ColorMode &value);
        MainWindow::SessionStatus getSessionStatus() const;
        void setSessionStatus(const MainWindow::SessionStatus &value);
        TradingSessionState *getTradingSessionState() const;
        void setTradingSessionState(TradingSessionState *value);
        MainWindow::TradingMode getTradingMode() const;
        void setTradingMode(const MainWindow::TradingMode &value);
        DialogManager *getDialogManager() const;
        void setDialogManager(DialogManager *value);
        SystemTray *getSystemTray() const;
        void setSystemTray(SystemTray *value);
        LanguageSelectorDialog *getLanguageSelectorDialog() const;
        void setLanguageSelectorDialog(LanguageSelectorDialog *value);
        Ui::MainWindow *getUi() const;
        void setUi(Ui::MainWindow *value);
        ApplicationSettings *getApplicationSettings() const;
        void setApplicationSettings(ApplicationSettings *value);

    private slots:
        void marketsReceived(const QJsonArray &result);
        void marketTickersReceived(const QJsonArray &result);
        void marketSummariesReceived(const QJsonArray &result);
        void balancesReceived(const QJsonArray &result);
        void marketOrderBookReceived(const QString &market,const int &depth,const QJsonObject &result);
        void marketTradesReceived(const QString &market,const QJsonArray &result);
        void marketSummaryReceived(const QString &market,const QJsonObject &result);
        void marketTickerReceived(const QString &market,const QJsonObject &result);

        void placeOrderReceived(const QString &orderId,const QJsonObject &result);
        void placeCancelOrderReceived(const QString &orderId,const QString &exchangeOrderId,const QJsonObject &result);
        void queryOrderReceived(const QString &orderId,const QString &exchangeOrderId,const QJsonObject &result);

        void marketReceived(const QString &market,const QJsonObject &data);

        void startTradingSession();
        void resumeTradingSession();
        void pauseTradingSession();
        void stopTradingSession();
        void stopTradingSessionConfirmation();

        void changeToLightMode();
        void changeToDarkMode();

        void changeToLightModeIcons();
        void changeToDarkModeIcons();

        void changeToLightModeCharts();
        void changeToDarkModeCharts();

        void toggleLogger(bool toggled);

        void showLanguageSelector();

        void on_verificationButton_clicked();
        void on_noVerificationButton_clicked();
        void on_homeButton_clicked();
        void on_marketsList_itemClicked(QListWidgetItem *item);
        void on_marketsComboBox_activated(const QString &arg1);
        void on_searchMarketsLineEdit_textChanged(const QString &arg1);
        void on_analyticsButton_clicked();
        void on_marketsButton_clicked();
        void on_portfoliosButton_clicked();
        void on_traderButton_clicked();
        void on_executionButton_clicked();

        void on_selectAllUsdMarketsButton_clicked();
        void on_deselectAllUsdMarketsButton_clicked();
        void on_selectAllBtcMarketsButton_clicked();
        void on_deselectAllBtcMarketsButton_clicked();
        void on_selectAllEthMarketsButton_clicked();
        void on_deselectAllEthMarketsButton_clicked();
        void on_selectAllUsdtMarketsButton_clicked();
        void on_deselectAllUsdtMarketsButton_clicked();
        void on_selectAllEurMarketsButton_clicked();
        void on_deselectAllEurMarketsButton_clicked();

        void on_searchUsdMarketsLineEdit_textChanged(const QString &arg1);
        void on_searchBtcMarketsLineEdit_textChanged(const QString &arg1);
        void on_searchEthMarketsLineEdit_textChanged(const QString &arg1);
        void on_searchUsdtMarketsLineEdit_textChanged(const QString &arg1);
        void on_searchEurMarketsLineEdit_textChanged(const QString &arg1);

        void on_selectedUsdMarketsCapital_valueChanged(double arg1);
        void on_selectedBtcMarketsCapital_valueChanged(double arg1);
        void on_selectedEthMarketsCapital_valueChanged(double arg1);
        void on_selectedUsdtMarketsCapital_valueChanged(double arg1);
        void on_selectedEurMarketsCapital_valueChanged(double arg1);

        void on_marketsFilterComboBox_currentIndexChanged(int index);
        void on_executeSessionButton_clicked();
        void on_checkupButton_clicked();

        void on_searchOpenOrdersLineEdit_textChanged(const QString &arg1);
        void on_searchClosedOrdersLineEdit_textChanged(const QString &arg1);
        void on_searchUsdOpenPositionsLineEdit_textChanged(const QString &arg1);
        void on_searchUsdClosedPositionsLineEdit_textChanged(const QString &arg1);
        void on_searchBtcOpenPositionsLineEdit_textChanged(const QString &arg1);
        void on_searchBtcClosedPositionsLineEdit_textChanged(const QString &arg1);
        void on_searchEthOpenPositionsLineEdit_textChanged(const QString &arg1);
        void on_searchEthClosedPositionsLineEdit_textChanged(const QString &arg1);
        void on_searchUsdtOpenPositionsLineEdit_textChanged(const QString &arg1);
        void on_searchUsdtClosedPositionsLineEdit_textChanged(const QString &arg1);
        void on_searchEurOpenPositionsLineEdit_textChanged(const QString &arg1);
        void on_searchEurClosedPositionsLineEdit_textChanged(const QString &arg1);

        void on_usdPortfolioLiquifyAllButton_clicked();
        void on_btcPortfolioLiquifyAllButton_clicked();
        void on_ethPortfolioLiquifyAllButton_clicked();
        void on_usdtPortfolioLiquifyAllButton_clicked();
        void on_eurPortfolioLiquifyAllButton_clicked();

        void on_cancelAllOrdersButton_clicked();
        void on_forwardToOrdersPageButton_clicked();
        void on_backToConfigurationPageButton_clicked();
        void on_forwardToSessionSummaryPageButton_clicked();
        void on_backToOrdersPageButton_clicked();

    public slots:
        void showGeneralInformation(const QString &information);

    signals:
        void tradingSessionStarted();
        void tradingSessionPaused();
        void tradingSessionResumed();
        void tradingSessionStopped();
};

#endif // MAINWINDOW_HPP
