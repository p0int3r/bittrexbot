#include "takeprofitriskfactory.hpp"
#include "takeprofitrisk.hpp"

TakeProfitRiskFactory::TakeProfitRiskFactory(QObject *parent): QObject(parent) {}

TakeProfitRiskFactory::~TakeProfitRiskFactory() {}

trader::Risk *TakeProfitRiskFactory::createTakeProfitRisk(QString name)
{
    TakeProfitRisk *takeProfitRisk=nullptr;

    if (name==QString("Take-profit at 0.03%"))
    {
        double takeProfit=0.03;
        QString description=QString("Trigger a take-profit order at 0.03% above the price at which you bought the asset");
        trader::Risk::RiskIndicator riskIndicator=trader::Risk::RiskIndicator::LOW;
        takeProfitRisk=new TakeProfitRisk(QString("Take-profit at 0.03%"),description,riskIndicator);
        takeProfitRisk->setTakeProfit(takeProfit);
    }
    else if (name==QString("Take-profit at 0.04%"))
    {
        double takeProfit=0.04;
        QString description=QString("Trigger a take-profit order at 0.04% above the price at which you bought the asset");
        trader::Risk::RiskIndicator riskIndicator=trader::Risk::RiskIndicator::LOW;
        takeProfitRisk=new TakeProfitRisk(QString("Take-profit at 0.04%"),description,riskIndicator);
        takeProfitRisk->setTakeProfit(takeProfit);
    }
    else if (name==QString("Take-profit at 0.05%"))
    {
        double takeProfit=0.05;
        QString description=QString("Trigger a take-profit order at 0.05% above the price at which you bought the asset");
        trader::Risk::RiskIndicator riskIndicator=trader::Risk::RiskIndicator::LOW;
        takeProfitRisk=new TakeProfitRisk(QString("Take-profit at 0.05%"),description,riskIndicator);
        takeProfitRisk->setTakeProfit(takeProfit);
    }
    else if (name==QString("Take-profit at 0.06%"))
    {
        double takeProfit=0.06;
        QString description=QString("Trigger a take-profit order at 0.06% above the price at which you bought the asset");
        trader::Risk::RiskIndicator riskIndicator=trader::Risk::RiskIndicator::LOW;
        takeProfitRisk=new TakeProfitRisk(QString("Take-profit at 0.06%"),description,riskIndicator);
        takeProfitRisk->setTakeProfit(takeProfit);
    }
    else if (name==QString("Take-profit at 0.07%"))
    {
        double takeProfit=0.08;
        QString description=QString("Trigger a take-profit order at 0.07% above the price at which you bought the asset");
        trader::Risk::RiskIndicator riskIndicator=trader::Risk::RiskIndicator::LOW;
        takeProfitRisk=new TakeProfitRisk(QString("Take-profit at 0.07%"),description,riskIndicator);
        takeProfitRisk->setTakeProfit(takeProfit);
    }
    else if (name==QString("Take-profit at 0.08%"))
    {
        double takeProfit=0.08;
        QString description=QString("Trigger a take-profit order at 0.08% above the price at which you bought the asset");
        trader::Risk::RiskIndicator riskIndicator=trader::Risk::RiskIndicator::LOW;
        takeProfitRisk=new TakeProfitRisk(QString("Take-profit at 0.08%"),description,riskIndicator);
        takeProfitRisk->setTakeProfit(takeProfit);
    }
    else if (name==QString("Take-profit at 0.09%"))
    {
        double takeProfit=0.09;
        QString description=QString("Trigger a take-profit order at 0.09% above the price at which you bought the asset");
        trader::Risk::RiskIndicator riskIndicator=trader::Risk::RiskIndicator::LOW;
        takeProfitRisk=new TakeProfitRisk(QString("Take-profit at 0.09%"),description,riskIndicator);
        takeProfitRisk->setTakeProfit(takeProfit);
    }
    else if (name==QString("Take-profit at 0.1%"))
    {
        double takeProfit=0.1;
        QString description=QString("Trigger a take-profit order at 0.1% above the price at which you bought the asset");
        trader::Risk::RiskIndicator riskIndicator=trader::Risk::RiskIndicator::LOW;
        takeProfitRisk=new TakeProfitRisk(QString("Take-profit at 0.1%"),description,riskIndicator);
        takeProfitRisk->setTakeProfit(takeProfit);
    }
    else if (name==QString("Take-profit at 0.2%"))
    {
        double takeProfit=0.2;
        QString description=QString("Trigger a take-profit order at 0.2% above the price at which you bought the asset");
        trader::Risk::RiskIndicator riskIndicator=trader::Risk::RiskIndicator::MEDIUM;
        takeProfitRisk=new TakeProfitRisk(QString("Take-profit at 0.2%"),description,riskIndicator);
        takeProfitRisk->setTakeProfit(takeProfit);
    }
    else if (name==QString("Take-profit at 0.3%"))
    {
        double takeProfit=0.3;
        QString description=QString("Trigger a take-profit order at 0.3% above the price at which you bought the asset");
        trader::Risk::RiskIndicator riskIndicator=trader::Risk::RiskIndicator::MEDIUM;
        takeProfitRisk=new TakeProfitRisk(QString("Take-profit at 0.3%"),description,riskIndicator);
        takeProfitRisk->setTakeProfit(takeProfit);
    }
    else if (name==QString("Take-profit at 0.4%"))
    {
        double takeProfit=0.4;
        QString description=QString("Trigger a take-profit order at 0.4% above the price at which you bought the asset");
        trader::Risk::RiskIndicator riskIndicator=trader::Risk::RiskIndicator::MEDIUM;
        takeProfitRisk=new TakeProfitRisk(QString("Take-profit at 0.4%"),description,riskIndicator);
        takeProfitRisk->setTakeProfit(takeProfit);
    }
    else if (name==QString("Take-profit at 0.5%"))
    {
        double takeProfit=0.5;
        QString description=QString("Trigger a take-profit order at 0.5% above the price at which you bought the asset");
        trader::Risk::RiskIndicator riskIndicator=trader::Risk::RiskIndicator::MEDIUM;
        takeProfitRisk=new TakeProfitRisk(QString("Take-profit at 0.5%"),description,riskIndicator);
        takeProfitRisk->setTakeProfit(takeProfit);
    }
    else if (name==QString("Take-profit at 0.6%"))
    {
        double takeProfit=0.6;
        QString description=QString("Trigger a take-profit order at 0.6% above the price at which you bought the asset");
        trader::Risk::RiskIndicator riskIndicator=trader::Risk::RiskIndicator::HIGH;
        takeProfitRisk=new TakeProfitRisk(QString("Take-profit at 0.6%"),description,riskIndicator);
        takeProfitRisk->setTakeProfit(takeProfit);
    }
    else if (name==QString("Take-profit at 0.7%"))
    {
        double takeProfit=0.7;
        QString description=QString("Trigger a take-profit order at 0.7% above the price at which you bought the asset");
        trader::Risk::RiskIndicator riskIndicator=trader::Risk::RiskIndicator::HIGH;
        takeProfitRisk=new TakeProfitRisk(QString("Take-profit at 0.7%"),description,riskIndicator);
        takeProfitRisk->setTakeProfit(takeProfit);
    }
    else if (name==QString("Take-profit at 0.8%"))
    {
        double takeProfit=0.8;
        QString description=QString("Trigger a take-profit order at 0.8% above the price at which you bought the asset");
        trader::Risk::RiskIndicator riskIndicator=trader::Risk::RiskIndicator::HIGH;
        takeProfitRisk=new TakeProfitRisk(QString("Take-profit at 0.8%"),description,riskIndicator);
        takeProfitRisk->setTakeProfit(takeProfit);
    }
    else if (name==QString("Take-profit at 0.9%"))
    {
        double takeProfit=0.9;
        QString description=QString("Trigger a take-profit order at 0.9% above the price at which you bought the asset");
        trader::Risk::RiskIndicator riskIndicator=trader::Risk::RiskIndicator::HIGH;
        takeProfitRisk=new TakeProfitRisk(QString("Take-profit at 0.9%"),description,riskIndicator);
        takeProfitRisk->setTakeProfit(takeProfit);
    }
    else if (name==QString("Take-profit at 1.0%"))
    {
        double takeProfit=1.0;
        QString description=QString("Trigger a take-profit order at 1.0% above the price at which you bought the asset");
        trader::Risk::RiskIndicator riskIndicator=trader::Risk::RiskIndicator::HIGH;
        takeProfitRisk=new TakeProfitRisk(QString("Take-profit at 1.0%"),description,riskIndicator);
        takeProfitRisk->setTakeProfit(takeProfit);
    }

    return takeProfitRisk;
}


trader::Risk *TakeProfitRiskFactory::createTakeProfitRisk(double takeProfit)
{
    TakeProfitRisk *takeProfitRisk=nullptr;
    QString name=QString("Take-profit at ")+QString::number(takeProfit)+QString("%");
    QString description=QString("Trigger a take-profit order at ")+QString::number(takeProfit)+QString("% above the price at which you bought the asset");
    trader::Risk::RiskIndicator riskIndicator=trader::Risk::RiskIndicator::NONE;
    if (takeProfit<=1.0) { riskIndicator=trader::Risk::RiskIndicator::LOW; }
    if (takeProfit>1.0 && takeProfit<=4.0) { riskIndicator=trader::Risk::RiskIndicator::MEDIUM; }
    if (takeProfit>4.0) { riskIndicator=trader::Risk::RiskIndicator::HIGH; }
    takeProfitRisk=new TakeProfitRisk(name,description,riskIndicator);
    takeProfitRisk->setTakeProfit(takeProfit);
    return takeProfitRisk;
}


QString TakeProfitRiskFactory::translateTakeProfitRiskToSourceLanguage(QString name)
{
    QString translated_risk_source_language=QString();

    if (name==tr("Take-profit at 0.03%"))
    {
        translated_risk_source_language=QString("Take-profit at 0.03%");
    }
    else if (name==tr("Take-profit at 0.04%"))
    {
        translated_risk_source_language=QString("Take-profit at 0.04%");
    }
    else if (name==tr("Take-profit at 0.05%"))
    {
        translated_risk_source_language=QString("Take-profit at 0.05%");
    }
    else if (name==tr("Take-profit at 0.06%"))
    {
        translated_risk_source_language=QString("Take-profit at 0.06%");
    }
    else if (name==tr("Take-profit at 0.07%"))
    {
        translated_risk_source_language=QString("Take-profit at 0.07%");
    }
    else if (name==tr("Take-profit at 0.08%"))
    {
        translated_risk_source_language=QString("Take-profit at 0.08%");
    }
    else if (name==tr("Take-profit at 0.09%"))
    {
        translated_risk_source_language=QString("Take-profit at 0.09%");
    }
    else if (name==tr("Take-profit at 0.1%"))
    {
        translated_risk_source_language=QString("Take-profit at 0.1%");
    }
    else if (name==tr("Take-profit at 0.2%"))
    {
        translated_risk_source_language=QString("Take-profit at 0.2%");
    }
    else if (name==tr("Take-profit at 0.3%"))
    {
        translated_risk_source_language=QString("Take-profit at 0.3%");
    }
    else if (name==tr("Take-profit at 0.4%"))
    {
        translated_risk_source_language=QString("Take-profit at 0.4%");
    }
    else if (name==tr("Take-profit at 0.5%"))
    {
        translated_risk_source_language=QString("Take-profit at 0.5%");
    }
    else if (name==tr("Take-profit at 0.6%"))
    {
        translated_risk_source_language=QString("Take-profit at 0.6%");
    }
    else if (name==tr("Take-profit at 0.7%"))
    {
        translated_risk_source_language=QString("Take-profit at 0.7%");
    }
    else if (name==tr("Take-profit at 0.8%"))
    {
        translated_risk_source_language=QString("Take-profit at 0.8%");
    }
    else if (name==tr("Take-profit at 0.9%"))
    {
        translated_risk_source_language=QString("Take-profit at 0.9%");
    }

    return translated_risk_source_language;
}
