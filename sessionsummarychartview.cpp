#include "sessionsummarychartview.hpp"
#include <QLinearGradient>
#include <QBrush>
#include <QColor>

FullScreenChartView::FullScreenChartView(QWidget *parent): QChartView(parent) {}

void FullScreenChartView::mouseDoubleClickEvent(QMouseEvent *event)
{
    Q_UNUSED(event)
    this->hide();
}

void FullScreenChartView::changeToLightMode()
{
    this->setStyleSheet("background-color: #9CD8F2;");
    QChart *chart=this->chart();
    if (chart!=nullptr)
    {
        QBrush brush=QBrush(QColor("white"));
        chart->setBackgroundBrush(brush);
        chart->legend()->setLabelBrush(QColor("#00557f"));
        chart->legend()->setFont(QFont(chart->legend()->font().family(),chart->legend()->font().pointSize(),QFont::Bold));
        chart->setTitleBrush(QBrush(QColor("#00557f")));
        chart->setTitleFont(QFont(chart->titleFont().family(),chart->titleFont().pointSize(),QFont::Bold));

        for (int i=0;i<chart->series().count();i++)
        {
            QPieSeries *current_pieseries=static_cast<QPieSeries *>(chart->series().at(i));
            if (current_pieseries!=nullptr)
            {
                for (int j=0;j<current_pieseries->slices().count();j++)
                {
                    QPieSlice *current_pieslice=current_pieseries->slices().at(j);
                    if (current_pieslice!=nullptr)
                    {
                        current_pieslice->setLabelColor("#00557f");
                        current_pieslice->setLabelFont(QFont(current_pieslice->labelFont().family(),current_pieslice->labelFont().pointSize(),QFont::Bold));
                    }
                }
            }
        }
    }
}


void FullScreenChartView::changeToDarkMode()
{
    this->setStyleSheet("background-color: black;");
    QChart *chart=this->chart();
    if (chart!=nullptr)
    {
        QLinearGradient gradient=QLinearGradient(this->width(),this->height()*0.48,0,this->height()*0.42);
        gradient.setColorAt(0,QColor(1,89,222,255));
        gradient.setColorAt(1,QColor(1,24,65,255));
        QBrush brush=QBrush(gradient);
        chart->setBackgroundBrush(brush);
        chart->legend()->setLabelColor(QColor("CornflowerBlue"));
        chart->legend()->setFont(QFont(chart->legend()->font().family(),chart->legend()->font().pointSize(),QFont::Bold));
        chart->setTitleBrush(QBrush(QColor("CornflowerBlue")));
        chart->setTitleFont(QFont(chart->titleFont().family(),chart->titleFont().pointSize(),QFont::Bold));

        for (int i=0;i<chart->series().count();i++)
        {
            QPieSeries *current_pieseries=static_cast<QPieSeries *>(chart->series().at(i));
            if (current_pieseries!=nullptr)
            {
                for (int j=0;j<current_pieseries->slices().count();j++)
                {
                    QPieSlice *current_pieslice=current_pieseries->slices().at(j);
                    if (current_pieslice!=nullptr)
                    {
                        current_pieslice->setLabelColor("CornflowerBlue");
                        current_pieslice->setLabelFont(QFont(current_pieslice->labelFont().family(),current_pieslice->labelFont().pointSize(),QFont::Bold));
                    }
                }
            }
        }
    }
}


FullScreenChartView &SessionSummaryChartView::getFullScreenChartView() { return this->fullScreenChartView; }

SessionSummaryChartView::SessionSummaryChartView(QWidget *parent): QChartView(parent)
{
    this->fullScreenChartView.setRenderHint(QPainter::Antialiasing);
}

void SessionSummaryChartView::mouseDoubleClickEvent(QMouseEvent *event)
{
    Q_UNUSED(event)
    this->fullScreenChartView.showFullScreen();
}


void SessionSummaryChartView::changeToLightMode()
{
    this->setStyleSheet("background-color: #9CD8F2;");
    QChart *chart=this->chart();
    if (chart!=nullptr)
    {
        QBrush brush=QBrush(QColor("white"));
        chart->setBackgroundBrush(brush);
        chart->legend()->setLabelBrush(QColor("#00557f"));
        chart->legend()->setFont(QFont(chart->legend()->font().family(),chart->legend()->font().pointSize(),QFont::Bold));
        chart->setTitleBrush(QBrush(QColor("#00557f")));
        chart->setTitleFont(QFont(chart->titleFont().family(),chart->titleFont().pointSize(),QFont::Bold));

        for (int i=0;i<chart->series().count();i++)
        {
            QPieSeries *current_pieseries=static_cast<QPieSeries *>(chart->series().at(i));
            if (current_pieseries!=nullptr)
            {
                for (int j=0;j<current_pieseries->slices().count();j++)
                {
                    QPieSlice *current_pieslice=current_pieseries->slices().at(j);
                    if (current_pieslice!=nullptr)
                    {
                        current_pieslice->setLabelColor("#00557f");
                        current_pieslice->setLabelFont(QFont(current_pieslice->labelFont().family(),current_pieslice->labelFont().pointSize(),QFont::Bold));
                    }
                }
            }
        }
    }

    this->fullScreenChartView.changeToLightMode();
}

void SessionSummaryChartView::changeToDarkMode()
{
    this->setStyleSheet("background-color: rgba(1,89,222,150);");
    QChart *chart=this->chart();
    if (chart!=nullptr)
    {
        QLinearGradient gradient=QLinearGradient(this->width(),this->height()*0.48,0,this->height()*0.42);
        gradient.setColorAt(0,QColor(1,89,222,255));
        gradient.setColorAt(1,QColor(1,24,65,255));
        QBrush brush=QBrush(gradient);
        chart->setBackgroundBrush(brush);
        chart->legend()->setLabelColor(QColor("CornflowerBlue"));
        chart->legend()->setFont(QFont(chart->legend()->font().family(),chart->legend()->font().pointSize(),QFont::Bold));
        chart->setTitleBrush(QBrush(QColor("CornflowerBlue")));
        chart->setTitleFont(QFont(chart->titleFont().family(),chart->titleFont().pointSize(),QFont::Bold));

        for (int i=0;i<chart->series().count();i++)
        {
            QPieSeries *current_pieseries=static_cast<QPieSeries *>(chart->series().at(i));
            if (current_pieseries!=nullptr)
            {
                for (int j=0;j<current_pieseries->slices().count();j++)
                {
                    QPieSlice *current_pieslice=current_pieseries->slices().at(j);
                    if (current_pieslice!=nullptr)
                    {
                        current_pieslice->setLabelColor("CornflowerBlue");
                        current_pieslice->setLabelFont(QFont(current_pieslice->labelFont().family(),current_pieslice->labelFont().pointSize(),QFont::Bold));
                    }
                }
            }
        }
    }

    this->fullScreenChartView.changeToDarkMode();
}
