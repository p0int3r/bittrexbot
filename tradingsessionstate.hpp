#ifndef TRADINGSESSIONSTATE_HPP
#define TRADINGSESSIONSTATE_HPP

#include <QObject>
#include <QString>
#include <QJsonObject>
#include "trader.hpp"
#include "v3_bittrexrestapihandler.hpp"

class TradingSessionState: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QJsonObject stateObject READ getStateObject WRITE setStateObject NOTIFY stateObjectChanged)
    Q_PROPERTY(QString restorationFilePath READ getRestorationFilePath WRITE setRestorationFilePath NOTIFY restorationFilePathChanged)
    Q_PROPERTY(TradingSessionState::State state READ getState WRITE setState NOTIFY stateChanged)
    Q_PROPERTY(trader::Trader *trader READ getTrader WRITE setTrader NOTIFY traderChanged)
    Q_PROPERTY(bittrexapi::v3::BittrexRestApiHandler *bittrexRestApiHandler READ getBittrexRestApiHandler WRITE setBittrexRestApiHandler NOTIFY bittrexRestApiHandlerChanged)

    public:

        enum class State
        {
            CRASHED,
            RESTORED,
            COMPLETED
        }; Q_ENUM(State)

    private:
        QJsonObject stateObject;
        QString restorationFilePath;
        TradingSessionState::State state;
        trader::Trader *trader=nullptr;
        bittrexapi::v3::BittrexRestApiHandler *bittrexRestApiHandler=nullptr;

        bool existsRestorationFile();
        bool createRestorationFile();
        bool loadRestorationFile();

    public:
        explicit TradingSessionState(QObject *parent=nullptr);
        trader::Trader *getTrader() const;
        void setTrader(trader::Trader *value);
        QJsonObject getStateObject() const;
        void setStateObject(const QJsonObject &value);
        QString getRestorationFilePath() const;
        void setRestorationFilePath(const QString &value);
        TradingSessionState::State getState() const;
        void setState(const TradingSessionState::State &value);
        bittrexapi::v3::BittrexRestApiHandler *getBittrexRestApiHandler() const;
        void setBittrexRestApiHandler(bittrexapi::v3::BittrexRestApiHandler *value);

    signals:
        void stateObjectChanged(const QJsonObject &stateObject);
        void restorationFilePathChanged(const QString &restorationFilePath);
        void stateChanged(const TradingSessionState::State &state);
        void traderChanged(const trader::Trader *trader);
        void bittrexRestApiHandlerChanged(const bittrexapi::v3::BittrexRestApiHandler *bittrexRestApiHandler);

    public slots:
        bool check();
        void save();
        void restore();
};

#endif // TRADINGSESSIONSTATE_HPP
