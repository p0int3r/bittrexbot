#ifndef TAKEPROFITRISK_HPP
#define TAKEPROFITRISK_HPP

#include <QObject>
#include <QString>
#include "risk.hpp"


class TakeProfitRisk: public trader::Risk
{
    Q_OBJECT
    Q_PROPERTY(double takeProfit READ getTakeProfit WRITE setTakeProfit NOTIFY takeProfitChanged)

    private:
        double takeProfit;

    public:
        TakeProfitRisk();
        TakeProfitRisk(QString name,QString description,
            trader::Risk::RiskIndicator riskIndicator);
        ~TakeProfitRisk();
        double getTakeProfit() const;
        void setTakeProfit(double value);

    signals:
        void takeProfitChanged(const double &takeProfit);

    public slots:
        void preprocess(const QString &market,const QObject *object) override;
        void assess(const QString &market,const QObject *object) override;
};

#endif // TAKEPROFITRISK_HPP
