#ifndef BITTREXASSET_HPP
#define BITTREXASSET_HPP

#include <QObject>
#include "asset.hpp"
#include "v1_bittrexrestapihandler.hpp"
#include "v3_bittrexrestapihandler.hpp"


class BittrexAsset: public trader::Asset
{
    Q_OBJECT
    Q_PROPERTY(int precision READ getPrecision WRITE setPrecision NOTIFY precisionChanged)
    Q_PROPERTY(bittrexapi::v3::BittrexRestApiHandler *bittrexRestApiHandler READ getBittrexRestApiHandler WRITE setBittrexRestApiHandler NOTIFY bittrexRestApiHandlerChanged)
    Q_PROPERTY(double minimumTradeSize READ getMinimumTradeSize WRITE setMinimumTradeSize NOTIFY minimumTradeSizeChanged)

    private:
        int precision;
        double minimumTradeSize;
        bittrexapi::v3::BittrexRestApiHandler *bittrexRestApiHandler=nullptr;

    public:
        BittrexAsset(bittrexapi::v3::BittrexRestApiHandler *bittrexRestApiHandler=nullptr);
        bittrexapi::v3::BittrexRestApiHandler *getBittrexRestApiHandler() const;
        void setBittrexRestApiHandler(bittrexapi::v3::BittrexRestApiHandler *value);
        int getPrecision() const;
        void setPrecision(int value);
        double getMinimumTradeSize() const;
        void setMinimumTradeSize(double value);

    signals:
        void precisionChanged(const int &precision);
        void minimumTradeSizeChanged(const double &minimumTradeSize);
        void bittrexRestApiHandlerChanged(const bittrexapi::v3::BittrexRestApiHandler *bittrexRestApiHandler);

    public slots:
        void start() override;
        void update() override;
        void stop() override;
};

#endif // BITTREXASSET_HPP
