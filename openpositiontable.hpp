#ifndef OPENPOSITIONTABLE_HPP
#define OPENPOSITIONTABLE_HPP

#include <QObject>
#include <QWidget>
#include <QTableWidgetItem>
#include <QMap>
#include "position.hpp"
#include "order.hpp"

class OpenPositionTable: public QTableWidget
{
    Q_OBJECT

    public:

        enum class ColorMode
        {
            LIGHT,
            DARK
        }; Q_ENUM(ColorMode)

        enum class BaseCurrency
        {
            USD_CURRENCY,
            BTC_CURRENCY,
            ETH_CURRENCY,
            USDT_CURRENCY,
            EUR_CURRENCY
        }; Q_ENUM(BaseCurrency)

    private:
        OpenPositionTable::ColorMode colorMode;
        OpenPositionTable::BaseCurrency baseCurrency;
        QMap<QString,QList<QTableWidgetItem *>> *map=nullptr;

    public:
        explicit OpenPositionTable(QWidget *parent=nullptr);
        ~OpenPositionTable();
        OpenPositionTable::ColorMode getColorMode() const;
        void setColorMode(const OpenPositionTable::ColorMode &value);
        QMap<QString, QList<QTableWidgetItem *> > *getMap() const;
        void setMap(QMap<QString, QList<QTableWidgetItem *> > *value);
        OpenPositionTable::BaseCurrency getBaseCurrency() const;
        void setBaseCurrency(const OpenPositionTable::BaseCurrency &value);
        void initializeSignalsAndSlots();

    signals:
        void tableItemsModified(const QString &market,const trader::Position *position);
        void tableItemsInserted(const QString &market,const trader::Position *position);
        void tableItemsCanceled(const QString &market,const trader::Position *position);
        void tableItemsRemoved(const QString &market,const trader::Position *position);
        void positionLiquified(const QString &market,const trader::Order *order);

    public slots:
        void changeToLightMode();
        void changeToDarkMode();
        void insertTableItems(const QString &market,const trader::Position *position);
        void cancelTableItems(const QString &market,const trader::Position *position);
        void removeTableItems(const QString &market,const trader::Position *position);
        void modifyTableItems(const QString &market,const trader::Position *position);
        void liquifyPosition(const QString &market,const trader::Position *position);
};

#endif // OPENPOSITIONTABLE_HPP
