#include "selectedmarkettable.hpp"
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QDebug>


SelectedMarketTable::SelectedMarketTable(QWidget *parent): QTableWidget(parent)
{
    this->setWidgetMap(new QMap<QString,QList<QTableWidgetItem *>>());
    this->setRowMap(new QMap<QString,int>());
}

SelectedMarketTable::~SelectedMarketTable()
{
    delete this->widgetMap;
    delete this->rowMap;
}

QMap<QString, QList<QTableWidgetItem *> > *SelectedMarketTable::getWidgetMap() const { return this->widgetMap; }

void SelectedMarketTable::setWidgetMap(QMap<QString,QList<QTableWidgetItem *> > *value) { this->widgetMap=value; }

QMap<QString,int> *SelectedMarketTable::getRowMap() const { return this->rowMap; }

void SelectedMarketTable::setRowMap(QMap<QString,int> *value) { this->rowMap=value; }



void SelectedMarketTable::insertMarket(const QString &market)
{
    bool containsMarket=this->getWidgetMap()->contains(market);
    if (containsMarket==true) { return; }
    QList<QTableWidgetItem *> tableWidgetList;
    QTableWidgetItem *selectedMarketItem=new QTableWidgetItem();
    selectedMarketItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    selectedMarketItem->setFlags(selectedMarketItem->flags() & ~Qt::ItemIsEditable);
    selectedMarketItem->setText(market);
    QTableWidgetItem *capitalPercentageItem=new QTableWidgetItem();
    QDoubleSpinBox *capitalPercentageSpinBox=new QDoubleSpinBox();
    capitalPercentageSpinBox->setStyleSheet("background-color:#aaaaff");
    capitalPercentageSpinBox->setRange(0,100);
    capitalPercentageSpinBox->setValue(1.0);
    this->connect(capitalPercentageSpinBox,QOverload<double>::of(&QDoubleSpinBox::valueChanged),
        this,[=](int value) { emit this->capitalPercentageSpecified(market,value); });
    QTableWidgetItem *tradingRoundsItem=new QTableWidgetItem();
    QSpinBox *tradingRoundsSpinBox=new QSpinBox(); tradingRoundsSpinBox->setRange(1,50);
    tradingRoundsSpinBox->setStyleSheet("background-color:#40E0D0");
    this->connect(tradingRoundsSpinBox,QOverload<int>::of(&QSpinBox::valueChanged),
        this,[=](int value){ emit this->tradingRoundsSpecified(market,value); });
    int currentRow=this->rowCount(); this->insertRow(currentRow);
    this->setItem(currentRow,0,selectedMarketItem);
    this->setItem(currentRow,1,capitalPercentageItem);
    this->setItem(currentRow,2,tradingRoundsItem);
    this->setCellWidget(currentRow,1,capitalPercentageSpinBox);
    this->setCellWidget(currentRow,2,tradingRoundsSpinBox);
    tableWidgetList.append(selectedMarketItem);
    tableWidgetList.append(capitalPercentageItem);
    tableWidgetList.append(tradingRoundsItem);
    this->getWidgetMap()->insert(market,tableWidgetList);
    this->getRowMap()->insert(market,currentRow);
    emit this->marketInserted(market);
}


void SelectedMarketTable::removeMarket(const QString &market)
{
    bool containsMarket=this->getWidgetMap()->contains(market);
    if (containsMarket==false) { return; }
    int row=this->getRowMap()->value(market);
    this->removeRow(row);
    this->getWidgetMap()->remove(market);
    this->getRowMap()->remove(market);
    QMap<QString,int>::iterator iter;

    for (iter=this->getRowMap()->begin();iter!=this->getRowMap()->end();iter++)
    {
        QString key=iter.key();
        int rowNumber=iter.value();
        if (rowNumber>row) { this->getRowMap()->insert(key,--rowNumber); }
    }

    emit this->marketRemoved(market);
}


void SelectedMarketTable::specifyTradingRounds(const QString &market,const int &tradingRounds)
{
    bool contains_market=this->getWidgetMap()->contains(market);
    if (contains_market==false) { return; }
    QList<QTableWidgetItem *> table_widget_list=this->getWidgetMap()->value(market);
    QTableWidgetItem *table_widget_item=table_widget_list.at(2);
    int cell_row=table_widget_item->row();
    int cell_column=table_widget_item->column();
    QSpinBox *trading_rounds_item=static_cast<QSpinBox *>(this->cellWidget(cell_row,cell_column));
    trading_rounds_item->setValue(tradingRounds);
    emit this->tradingRoundsSpecified(market,tradingRounds);
}

void SelectedMarketTable::specifyCapitalPercentage(const QString &market,const double &capitalPercentage)
{
    bool contains_market=this->getWidgetMap()->contains(market);
    if (contains_market==false) { return; }
    QList<QTableWidgetItem *> table_widget_list=this->getWidgetMap()->value(market);
    QTableWidgetItem *table_widget_item=table_widget_list.at(1);
    int cell_row=table_widget_item->row();
    int cell_column=table_widget_item->column();
    QDoubleSpinBox *capital_percentage_item=static_cast<QDoubleSpinBox *>(this->cellWidget(cell_row,cell_column));
    capital_percentage_item->setValue(capitalPercentage);
    emit this->capitalPercentageSpecified(market,capitalPercentage);
}
