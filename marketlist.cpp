#include "marketlist.hpp"
#include "marketlistitem.hpp"


MarketList::MarketList(QWidget *parent): QListWidget(parent)
{
    this->setSortBy(MarketList::SortBy::VOLUME);
    this->setMap(new QMap<QString,QListWidgetItem *>());
}

MarketList::~MarketList() { delete this->map; }

QMap<QString,QListWidgetItem *> *MarketList::getMap() const { return this->map; }

void MarketList::setMap(QMap<QString,QListWidgetItem *> *value) { this->map=value; }

MarketList::SortBy MarketList::getSortBy() const { return this->sortBy; }

void MarketList::setSortBy(const MarketList::SortBy &value) { this->sortBy=value; }

MarketList::ColorMode MarketList::getColorMode() const { return this->colorMode; }

void MarketList::setColorMode(const MarketList::ColorMode &value) { colorMode = value; }

void MarketList::changeToLightMode()
{
    this->setColorMode(MarketList::ColorMode::LIGHT);
    QMap<QString,QListWidgetItem *>::iterator iterator;
    for (iterator=this->getMap()->begin();iterator!=this->getMap()->end();iterator++)
    {
        QListWidgetItem *item=iterator.value();
        MarketListItem *marketListItem=static_cast<MarketListItem *>(this->itemWidget(item));
        if (marketListItem->getPercentChange().toDouble()<0.0) { marketListItem->changeColor("red"); }
        if (marketListItem->getPercentChange().toDouble()>0.0) { marketListItem->changeColor("green"); }
        if (qFuzzyIsNull(marketListItem->getPercentChange().toDouble())==true) { marketListItem->changeColor("orange"); }
    }
}

void MarketList::changeToDarkMode()
{
    this->setColorMode(MarketList::ColorMode::DARK);
    QMap<QString,QListWidgetItem *>::iterator iterator;
    for (iterator=this->getMap()->begin();iterator!=this->getMap()->end();iterator++)
    {
        QListWidgetItem *item=iterator.value();
        MarketListItem *marketListItem=static_cast<MarketListItem *>(this->itemWidget(item));
        if (marketListItem->getPercentChange().toDouble()<0.0) { marketListItem->changeColor("orange"); }
        if (marketListItem->getPercentChange().toDouble()>0.0) { marketListItem->changeColor("lime"); }
        if (qFuzzyIsNull(marketListItem->getPercentChange().toDouble())==true) { marketListItem->changeColor("Yellow"); }
    }
}

void MarketList::modifyListItem(const QString &market,const QJsonObject &data)
{
    QListWidgetItem *item=this->getMap()->value(market);
    MarketListItem *market_list_item=qobject_cast<MarketListItem *>(this->itemWidget(item));

    if (data.contains("percentChange")==true)
    {
        double percent_change=data.value("percentChange").toString().toDouble();

        if (this->getColorMode()==MarketList::ColorMode::DARK)
        {
            if (percent_change<0.0) { market_list_item->changeColor("orange"); }
            if (percent_change>0.0) { market_list_item->changeColor("lime"); }
            if (qFuzzyIsNull(percent_change)==true) { market_list_item->changeColor("Yellow"); }
        }

        if (this->getColorMode()==MarketList::ColorMode::LIGHT)
        {
            if (percent_change<0.0) { market_list_item->changeColor("red"); }
            if (percent_change>0.0) { market_list_item->changeColor("green"); }
            if (qFuzzyIsNull(percent_change)==true) { market_list_item->changeColor("orange"); }
        }

        market_list_item->setPercentChange(QString::number(percent_change,'f',3)+QString("%"));
    }

    if (data.contains("quoteVolume")==true)
    {
        double quote_volume=data.value("quoteVolume").toString().toDouble();
        market_list_item->setQuoteVolume(QString::number(quote_volume,'f'));
    }

    if (data.contains("lastTradeRate")==true)
    {
        double last_price=data.value("lastTradeRate").toString().toDouble();
        market_list_item->setLastPrice(QString::number(last_price,'g'));
    }

    emit this->listItemModified();
}
