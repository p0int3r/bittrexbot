#include "varmaxstrategy.hpp"

VARMAXStrategy::VARMAXStrategy(): VARMAXStrategy(QString(),QString(),trader::Strategy::FrequencyLevel::NONE) {}


VARMAXStrategy::VARMAXStrategy(QString name,QString description,trader::Strategy::FrequencyLevel frequencyLevel)
{
    this->setName(name);
    this->setDescription(description);
    this->setFrequencyLevel(frequencyLevel);
}


VARMAXStrategy::~VARMAXStrategy() {}

void VARMAXStrategy::preprocess(const QString &market,const QObject *object)
{
    Q_UNUSED(market)
    Q_UNUSED(object)

    // NOTE: emit signal at the end.
    emit this->preprocessed(market,object);
}

void VARMAXStrategy::strategise(const QString &market,const QObject *object)
{
    Q_UNUSED(market)
    Q_UNUSED(object)


    // NOTE: emit signal at the end.
    emit this->strategised(market,object);
}
