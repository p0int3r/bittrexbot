#include "dialogmanager.hpp"
#include <QAbstractButton>




DialogManager::DialogManager(QWidget *parent): QWidget(parent)
{
    this->setRestorationMessageBox(new QMessageBox(parent));
    this->setErrorMessageBox(new QMessageBox(parent));
    this->setConfirmationMessageBox(new QMessageBox(parent));
    this->setRatificationMessageBox(new QMessageBox(parent));
    this->initializeMessageBoxes();
}

DialogManager::~DialogManager()
{
    delete this->restorationMessageBox;
    delete this->errorMessageBox;
    delete this->confirmationMessageBox;
    delete this->ratificationMessageBox;
}

QMessageBox *DialogManager::getRestorationMessageBox() const { return this->restorationMessageBox; }

void DialogManager::setRestorationMessageBox(QMessageBox *value) { this->restorationMessageBox=value; }

QMessageBox *DialogManager::getErrorMessageBox() const { return this->errorMessageBox; }

void DialogManager::setErrorMessageBox(QMessageBox *value) { this->errorMessageBox=value; }

QMessageBox *DialogManager::getConfirmationMessageBox() const { return this->confirmationMessageBox; }

void DialogManager::setConfirmationMessageBox(QMessageBox *value) { this->confirmationMessageBox=value; }

QMessageBox *DialogManager::getRatificationMessageBox() const { return this->ratificationMessageBox; }

void DialogManager::setRatificationMessageBox(QMessageBox *value) { this->ratificationMessageBox=value; }


void DialogManager::initializeMessageBoxes()
{
    this->getRestorationMessageBox()->setWindowIcon(QIcon(QString(":/icons/light_mode/bittrexbot_logo.png")));
    this->getRestorationMessageBox()->setWindowTitle(tr("Restoration Dialog"));
    this->getErrorMessageBox()->setWindowIcon(QIcon(QString(":/icons/light_mode/bittrexbot_logo.png")));
    this->getErrorMessageBox()->setWindowTitle(tr("Error Dialog"));
    this->getConfirmationMessageBox()->setWindowTitle(tr("Confirmation Dialog"));
    this->getConfirmationMessageBox()->setWindowIcon(QIcon(QString(":/icons/light_mode/bittrexbot_logo.png")));
}



bool DialogManager::showRestorationMessageBox(const QString &message,const QString &informativeMessage,const QString &descriptiveMessage)
{
    bool restore_state=false;
    this->getRestorationMessageBox()->setIcon(QMessageBox::Critical);
    this->getRestorationMessageBox()->setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    this->getRestorationMessageBox()->setText(message);
    this->getRestorationMessageBox()->setInformativeText(informativeMessage);
    this->getRestorationMessageBox()->setDetailedText(descriptiveMessage);
    int result=this->getRestorationMessageBox()->exec();
    if (result==QMessageBox::Yes) { restore_state=true; }
    else if (result==QMessageBox::No) { restore_state=false; }
    return restore_state;
}


bool DialogManager::showErrorMessageBox(const QString &message,const QString &informativeMessage,const QString &descriptiveMessage)
{
    this->getErrorMessageBox()->setIcon(QMessageBox::Warning);
    this->getErrorMessageBox()->setStandardButtons(QMessageBox::Ok);
    this->getErrorMessageBox()->setText(message);
    this->getErrorMessageBox()->setInformativeText(informativeMessage);
    this->getErrorMessageBox()->setDetailedText(descriptiveMessage);
    this->getErrorMessageBox()->exec();
    return true;
}

bool DialogManager::showConfirmationMessageBox(const QString &message,const QString &informativeMessage,const QString &descriptiveMessage)
{
    this->getConfirmationMessageBox()->setIcon(QMessageBox::Icon::Information);
    this->getConfirmationMessageBox()->setStandardButtons(QMessageBox::Ok);
    this->getConfirmationMessageBox()->setText(message);
    this->getConfirmationMessageBox()->setInformativeText(informativeMessage);
    this->getConfirmationMessageBox()->setDetailedText(descriptiveMessage);
    this->getConfirmationMessageBox()->exec();
    return true;
}


bool DialogManager::showRatificationMessageBox(const QString &message,const QString &informativeMessage,const QString &descriptiveMessage)
{
    bool ratification_state=false;
    this->getRatificationMessageBox()->setIcon(QMessageBox::Warning);
    this->getRatificationMessageBox()->setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    this->getRatificationMessageBox()->setText(message);
    this->getRatificationMessageBox()->setInformativeText(informativeMessage);
    this->getRatificationMessageBox()->setDetailedText(descriptiveMessage);
    int result=this->getRatificationMessageBox()->exec();
    if (result==QMessageBox::Yes) { ratification_state=true; }
    else if (result==QMessageBox::No) { ratification_state=false; }
    return ratification_state;
}
