#ifndef SARIMASTRATEGY_HPP
#define SARIMASTRATEGY_HPP

#include <QObject>
#include <QString>
#include "strategy.hpp"

// TODO: Implement the Seasonal Autoregressive Integrated Moving-Average.


class SARIMAStrategy: public trader::Strategy
{
    Q_OBJECT

    public:
        SARIMAStrategy();
        SARIMAStrategy(QString name,QString description,
            trader::Strategy::FrequencyLevel frequencyLevel);
        ~SARIMAStrategy();

    public slots:
        void preprocess(const QString &market,const QObject *object);
        void strategise(const QString &market,const QObject *object);
};

#endif // SARIMASTRATEGY_HPP
