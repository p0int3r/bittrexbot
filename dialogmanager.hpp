#ifndef DIALOGMANAGER_HPP
#define DIALOGMANAGER_HPP

#include <QObject>
#include <QMessageBox>
#include <QInputDialog>


class DialogManager: public QWidget
{
    Q_OBJECT

    private:
        QMessageBox *restorationMessageBox=nullptr;
        QMessageBox *errorMessageBox=nullptr;
        QMessageBox *confirmationMessageBox=nullptr;
        QMessageBox *ratificationMessageBox=nullptr;

    public:
        explicit DialogManager(QWidget *parent=nullptr);
        ~DialogManager();
        QMessageBox *getRestorationMessageBox() const;
        void setRestorationMessageBox(QMessageBox *value);
        void initializeMessageBoxes();
        QMessageBox *getErrorMessageBox() const;
        void setErrorMessageBox(QMessageBox *value);
        QMessageBox *getConfirmationMessageBox() const;
        void setConfirmationMessageBox(QMessageBox *value);
        QMessageBox *getRatificationMessageBox() const;
        void setRatificationMessageBox(QMessageBox *value);

signals:

public slots:
        bool showRestorationMessageBox(const QString &message,const QString &informativeMessage,const QString &descriptiveMessage);
        bool showErrorMessageBox(const QString &message,const QString &informativeMessage,const QString &descriptiveMessage);
        bool showConfirmationMessageBox(const QString &message,const QString &informativeMessage,const QString &descriptiveMessage);
        bool showRatificationMessageBox(const QString &message,const QString &informativeMessage,const QString &descriptiveMessage);

};

#endif // DIALOGMANAGER_HPP
