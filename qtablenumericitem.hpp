#ifndef QTABLENUMERICITEM_HPP
#define QTABLENUMERICITEM_HPP

#include <QTableWidgetItem>

class QTableNumericItem: public QTableWidgetItem
{
    public:

        bool operator <(const QTableWidgetItem &rhs) const
        {
            const QTableNumericItem *item=dynamic_cast<const QTableNumericItem *>(&rhs);
            return this->text().toDouble()<item->text().toDouble();
        }
};

#endif // QTABLENUMERICITEM_HPP
