#include "languageselectordialog.hpp"
#include "ui_languageselectordialog.h"
#include <QCoreApplication>


LanguageSelectorDialog::LanguageSelectorDialog(QWidget *parent): QDialog(parent), ui(new Ui::LanguageSelectorDialog()) { this->ui->setupUi(this); }

LanguageSelectorDialog::~LanguageSelectorDialog() { delete this->ui; }

QTranslator *LanguageSelectorDialog::getActiveTranslator() const { return this->activeTranslator; }

void LanguageSelectorDialog::setActiveTranslator(QTranslator *value) { this->activeTranslator=value; }


void LanguageSelectorDialog::changeLanguage(const QString &language)
{
    if (language==QString("English")) { this->on_englishCheckBox_clicked(); this->ui->englishCheckBox->setChecked(true); }
    if (language==QString("Chinese")) { this->on_chineseCheckBox_clicked(); this->ui->chineseCheckBox->setChecked(true); }
    if (language==QString("Hindi")) { this->on_hindiCheckBox_clicked(); this->ui->hindiCheckBox->setChecked(true); }
    if (language==QString("Spanish")) { this->on_spanishCheckBox_clicked(); this->ui->spanishCheckBox->setChecked(true); }
    if (language==QString("Arabic")) { this->on_arabicCheckBox_clicked(); this->ui->arabicCheckBox->setChecked(true); }
    if (language==QString("Portuguese")) { this->on_portugueseCheckBox_clicked(); this->ui->portugueseCheckBox->setChecked(true); }
    if (language==QString("Russian")) { this->on_russianCheckBox_clicked(); this->ui->russianCheckBox->setChecked(true); }
    if (language==QString("Japanese")) { this->on_japaneseCheckBox_clicked(); this->ui->japaneseCheckBox->setChecked(true); }
    if (language==QString("Malay")) { this->on_malayCheckBox_clicked(); this->ui->malayCheckBox->setChecked(true); }
    if (language==QString("German")) { this->on_germanCheckBox_clicked(); this->ui->germanCheckBox->setChecked(true); }
    if (language==QString("Javanese")) { this->on_javaneseCheckBox_clicked(); this->ui->javaneseCheckBox->setChecked(true); }
    if (language==QString("Korean")) { this->on_koreanCheckBox_clicked(); this->ui->koreanCheckBox->setChecked(true); }
    if (language==QString("Italian")) { this->on_italianCheckBox_clicked(); this->ui->italianCheckBox->setChecked(true); }
}

void LanguageSelectorDialog::on_englishCheckBox_clicked()
{
    QHash<QString,QTranslator *>::iterator translator_iterator;
    QString qm_file=QString(":/languages/translations/bittrexbot_english.qm");
    for (translator_iterator=this->translators.begin();translator_iterator!=this->translators.end();translator_iterator++)
    {
        QTranslator *current_translator=translator_iterator.value();
        QCoreApplication::removeTranslator(current_translator);
    }

    QTranslator *translator=new QTranslator(this->parent());
    translator->load(qm_file);
    this->translators.insert(qm_file,translator);
    this->activeTranslator=translator;
    QCoreApplication::installTranslator(translator);
    emit this->languageChanged(QString("English"));
}

void LanguageSelectorDialog::on_chineseCheckBox_clicked()
{
    QHash<QString,QTranslator *>::iterator translator_iterator;
    QString qm_file=QString(":/languages/translations/bittrexbot_chinese.qm");
    for (translator_iterator=this->translators.begin();translator_iterator!=this->translators.end();translator_iterator++)
    {
        QTranslator *current_translator=translator_iterator.value();
        QCoreApplication::removeTranslator(current_translator);
    }

    QTranslator *translator=new QTranslator(this->parent());
    translator->load(qm_file);
    this->translators.insert(qm_file,translator);
    this->activeTranslator=translator;
    QCoreApplication::installTranslator(translator);
    emit this->languageChanged(QString("Chinese"));
}

void LanguageSelectorDialog::on_hindiCheckBox_clicked()
{
    QHash<QString,QTranslator *>::iterator translator_iterator;
    QString qm_file=QString(":/languages/translations/bittrexbot_hindi.qm");
    for (translator_iterator=this->translators.begin();translator_iterator!=this->translators.end();translator_iterator++)
    {
        QTranslator *current_translator=translator_iterator.value();
        QCoreApplication::removeTranslator(current_translator);
    }

    QTranslator *translator=new QTranslator(this->parent());
    translator->load(qm_file);
    this->translators.insert(qm_file,translator);
    this->activeTranslator=translator;
    QCoreApplication::installTranslator(translator);
    emit this->languageChanged(QString("Hindi"));
}

void LanguageSelectorDialog::on_spanishCheckBox_clicked()
{
    QHash<QString,QTranslator *>::iterator translator_iterator;
    QString qm_file=QString(":/languages/translations/bittrexbot_spanish.qm");
    for (translator_iterator=this->translators.begin();translator_iterator!=this->translators.end();translator_iterator++)
    {
        QTranslator *current_translator=translator_iterator.value();
        QCoreApplication::removeTranslator(current_translator);
    }

    QTranslator *translator=new QTranslator(this->parent());
    translator->load(qm_file);
    this->translators.insert(qm_file,translator);
    this->activeTranslator=translator;
    QCoreApplication::installTranslator(translator);
    emit this->languageChanged(QString("Spanish"));
}

void LanguageSelectorDialog::on_arabicCheckBox_clicked()
{
    QHash<QString,QTranslator *>::iterator translator_iterator;
    QString qm_file=QString(":/languages/translations/bittrexbot_arabic.qm");
    for (translator_iterator=this->translators.begin();translator_iterator!=this->translators.end();translator_iterator++)
    {
        QTranslator *current_translator=translator_iterator.value();
        QCoreApplication::removeTranslator(current_translator);
    }

    QTranslator *translator=new QTranslator(this->parent());
    translator->load(qm_file);
    this->translators.insert(qm_file,translator);
    this->activeTranslator=translator;
    QCoreApplication::installTranslator(translator);
    emit this->languageChanged(QString("Arabic"));
}

void LanguageSelectorDialog::on_portugueseCheckBox_clicked()
{
    QHash<QString,QTranslator *>::iterator translator_iterator;
    QString qm_file=QString(":/languages/translations/bittrexbot_portuguese.qm");
    for (translator_iterator=this->translators.begin();translator_iterator!=this->translators.end();translator_iterator++)
    {
        QTranslator *current_translator=translator_iterator.value();
        QCoreApplication::removeTranslator(current_translator);
    }

    QTranslator *translator=new QTranslator(this->parent());
    translator->load(qm_file);
    this->translators.insert(qm_file,translator);
    this->activeTranslator=translator;
    QCoreApplication::installTranslator(translator);
    emit this->languageChanged(QString("Portuguese"));
}

void LanguageSelectorDialog::on_russianCheckBox_clicked()
{
    QHash<QString,QTranslator *>::iterator translator_iterator;
    QString qm_file=QString(":/languages/translations/bittrexbot_russian.qm");
    for (translator_iterator=this->translators.begin();translator_iterator!=this->translators.end();translator_iterator++)
    {
        QTranslator *current_translator=translator_iterator.value();
        QCoreApplication::removeTranslator(current_translator);
    }

    QTranslator *translator=new QTranslator(this->parent());
    translator->load(qm_file);
    this->translators.insert(qm_file,translator);
    this->activeTranslator=translator;
    QCoreApplication::installTranslator(translator);
    emit this->languageChanged(QString("Russian"));
}

void LanguageSelectorDialog::on_japaneseCheckBox_clicked()
{
    QHash<QString,QTranslator *>::iterator translator_iterator;
    QString qm_file=QString(":/languages/translations/bittrexbot_japanese.qm");
    for (translator_iterator=this->translators.begin();translator_iterator!=this->translators.end();translator_iterator++)
    {
        QTranslator *current_translator=translator_iterator.value();
        QCoreApplication::removeTranslator(current_translator);
    }

    QTranslator *translator=new QTranslator(this->parent());
    translator->load(qm_file);
    this->translators.insert(qm_file,translator);
    this->activeTranslator=translator;
    QCoreApplication::installTranslator(translator);
    emit this->languageChanged(QString("Japanese"));
}

void LanguageSelectorDialog::on_malayCheckBox_clicked()
{
    QHash<QString,QTranslator *>::iterator translator_iterator;
    QString qm_file=QString(":/languages/translations/bittrexbot_malay.qm");
    for (translator_iterator=this->translators.begin();translator_iterator!=this->translators.end();translator_iterator++)
    {
        QTranslator *current_translator=translator_iterator.value();
        QCoreApplication::removeTranslator(current_translator);
    }

    QTranslator *translator=new QTranslator(this->parent());
    translator->load(qm_file);
    this->translators.insert(qm_file,translator);
    this->activeTranslator=translator;
    QCoreApplication::installTranslator(translator);
    emit this->languageChanged(QString("Malay"));
}

void LanguageSelectorDialog::on_germanCheckBox_clicked()
{
    QHash<QString,QTranslator *>::iterator translator_iterator;
    QString qm_file=QString(":/languages/translations/bittrexbot_german.qm");
    for (translator_iterator=this->translators.begin();translator_iterator!=this->translators.end();translator_iterator++)
    {
        QTranslator *current_translator=translator_iterator.value();
        QCoreApplication::removeTranslator(current_translator);
    }

    QTranslator *translator=new QTranslator(this->parent());
    translator->load(qm_file);
    this->translators.insert(qm_file,translator);
    this->activeTranslator=translator;
    QCoreApplication::installTranslator(translator);
    emit this->languageChanged(QString("German"));
}

void LanguageSelectorDialog::on_javaneseCheckBox_clicked()
{
    QHash<QString,QTranslator *>::iterator translator_iterator;
    QString qm_file=QString(":/languages/translations/bittrexbot_javanese.qm");
    for (translator_iterator=this->translators.begin();translator_iterator!=this->translators.end();translator_iterator++)
    {
        QTranslator *current_translator=translator_iterator.value();
        QCoreApplication::removeTranslator(current_translator);
    }

    QTranslator *translator=new QTranslator(this->parent());
    translator->load(qm_file);
    this->translators.insert(qm_file,translator);
    this->activeTranslator=translator;
    QCoreApplication::installTranslator(translator);
    emit this->languageChanged(QString("Javanese"));
}

void LanguageSelectorDialog::on_koreanCheckBox_clicked()
{
    QHash<QString,QTranslator *>::iterator translator_iterator;
    QString qm_file=QString(":/languages/translations/bittrexbot_korean.qm");
    for (translator_iterator=this->translators.begin();translator_iterator!=this->translators.end();translator_iterator++)
    {
        QTranslator *current_translator=translator_iterator.value();
        QCoreApplication::removeTranslator(current_translator);
    }

    QTranslator *translator=new QTranslator(this->parent());
    translator->load(qm_file);
    this->translators.insert(qm_file,translator);
    this->activeTranslator=translator;
    QCoreApplication::installTranslator(translator);
    emit this->languageChanged(QString("Korean"));
}

void LanguageSelectorDialog::on_italianCheckBox_clicked()
{
    QHash<QString,QTranslator *>::iterator translator_iterator;
    QString qm_file=QString(":/languages/translations/bittrexbot_italian.qm");
    for (translator_iterator=this->translators.begin();translator_iterator!=this->translators.end();translator_iterator++)
    {
        QTranslator *current_translator=translator_iterator.value();
        QCoreApplication::removeTranslator(current_translator);
    }

    QTranslator *translator=new QTranslator(this->parent());
    translator->load(qm_file);
    this->translators.insert(qm_file,translator);
    this->activeTranslator=translator;
    QCoreApplication::installTranslator(translator);
    emit this->languageChanged(QString("Italian"));
}

void LanguageSelectorDialog::on_closeButton_clicked() { this->close(); }
