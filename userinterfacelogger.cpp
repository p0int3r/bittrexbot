#include <QDateTime>
#include <QJsonDocument>
#include <QJsonArray>
#include "userinterfacelogger.hpp"

UserInterfaceLogger::UserInterfaceLogger(QWidget *parent): QTextEdit(parent) {}

UserInterfaceLogger::~UserInterfaceLogger() {}


void UserInterfaceLogger::logQueryMarketsFinishedMessage(const QJsonArray &result)
{
    QJsonDocument document;
    QDateTime current_date=QDateTime::currentDateTime();
    QString log_message=QString("[ timestamp = ")+current_date.toString("hh:mm:ss.zzz")+QString(" ] ");
    log_message+=QString("[ message = received all markets from the bittrex exchange ")+QString(" ] ");
    document.setArray(result);
    log_message+=QString(document.toJson(QJsonDocument::Compact))+QString("\n");
    this->insertPlainText(log_message);
    this->moveCursor(QTextCursor::End);
}

void UserInterfaceLogger::logQueryCurrenciesFinishedMessage(const QJsonArray &result)
{
    QJsonDocument document;
    QDateTime current_date=QDateTime::currentDateTime();
    QString log_message=QString("[ timestamp = ")+current_date.toString("hh:mm:ss.zzz")+QString(" ] ");
    log_message+=QString("[ message = received all currencies from the bittrex exchange ")+QString(" ] ");
    document.setArray(result);
    log_message+=QString(document.toJson(QJsonDocument::Compact))+QString("\n");
    this->insertPlainText(log_message);
    this->moveCursor(QTextCursor::End);
}

void UserInterfaceLogger::logQueryMarketTickerFinishedMessage(const QString &market,const QJsonObject &result)
{
    QJsonDocument document;
    QDateTime current_date=QDateTime::currentDateTime();
    QString log_message=QString("[ timestamp = ")+current_date.toString("hh:mm:ss.zzz")+QString(" ] ");
    log_message+=QString("[ message = received ticker for ")+market+QString(" from the bittrex exchange ")+QString(" ] ");
    document.setObject(result);
    log_message+=QString(document.toJson(QJsonDocument::Compact))+QString("\n");
    this->insertPlainText(log_message);
    this->moveCursor(QTextCursor::End);
}


void UserInterfaceLogger::logQueryMarketSummariesFinishedMessage(const QJsonArray &result)
{
    QJsonDocument document;
    QDateTime current_date=QDateTime::currentDateTime();
    QString log_message=QString("[ timestamp = ")+current_date.toString("hh:mm:ss.zzz")+QString(" ] ");
    log_message+=QString("[ message = received all market summaries from the bittrex exchange ")+QString(" ] ");
    document.setArray(result);
    log_message+=QString(document.toJson(QJsonDocument::Compact))+QString("\n");
    this->insertPlainText(log_message);
    this->moveCursor(QTextCursor::End);
}


void UserInterfaceLogger::logQueryMarketSummaryFinishedMessage(const QString &market,const QJsonObject &result)
{
    QJsonDocument document;
    QDateTime current_date=QDateTime::currentDateTime();
    QString log_message=QString("[ timestamp = ")+current_date.toString("hh:mm:ss.zzz")+QString(" ] ");
    log_message+=QString("[ message = received market summary for ")+market+QString(" from the bittrex exchange ")+QString(" ] ");
    document.setObject(result);
    log_message+=QString(document.toJson(QJsonDocument::Compact))+QString("\n");
    this->insertPlainText(log_message);
    this->moveCursor(QTextCursor::End);
}

void UserInterfaceLogger::logQueryMarketOrderBookFinishedMessage(const QString &market,const int &depth,const QJsonObject &result)
{
    Q_UNUSED(depth)
    QJsonDocument document;
    QDateTime current_date=QDateTime::currentDateTime();
    QString log_message=QString("[ timestamp = ")+current_date.toString("hh:mm:ss.zzz")+QString(" ] ");
    log_message+=QString("[ message = received ")+QString(" order book for ")+market+QString(" from the bittrex exchange ")+QString(" ] ");
    document.setObject(result);
    log_message+=QString(document.toJson(QJsonDocument::Compact))+QString("\n");
    this->insertPlainText(log_message);
    this->moveCursor(QTextCursor::End);
}


void UserInterfaceLogger::logQueryMarketTradesFinishedMessage(const QString &market,const QJsonArray &result)
{
    QJsonDocument document;
    QDateTime current_date=QDateTime::currentDateTime();
    QString log_message=QString("[ timestamp = ")+current_date.toString("hh:mm:ss.zzz")+QString(" ] ");
    log_message+=QString("[ message = received market trades for ")+market+QString(" from the bittrex exchange ")+QString(" ] ");
    document.setArray(result);
    log_message+=QString(document.toJson(QJsonDocument::Compact))+QString("\n");
    this->insertPlainText(log_message);
    this->moveCursor(QTextCursor::End);
}


void UserInterfaceLogger::logPlaceOrderFinishedMessage(const QString &orderId,const QJsonObject &result)
{
    Q_UNUSED(orderId)
    QJsonDocument document;
    QString direction=result.value("direction").toString();
    QString type=result.value("type").toString();
    QString market=result.value("symbol").toString();
    double quantity=result.value("quantity").toString().toDouble();
    double rate=result.value("rate").toString().toDouble();
    QDateTime current_date=QDateTime::currentDateTime();
    QString log_message=QString("[ timestamp = ")+current_date.toString("hh:mm:ss.zzz")+QString(" ] ");
    log_message+=QString("[ message = placed a ")+type+QString(" ")+direction+QString(" for ")+market+QString(" with quantity ")+
       QString::number(quantity)+QString(" and rate ")+QString::number(rate)+QString(" at the bittrex exchange ")+QString(" ] ");
    document.setObject(result);
    log_message+=QString(document.toJson(QJsonDocument::Compact))+QString("\n");
    this->insertPlainText(log_message);
    this->moveCursor(QTextCursor::End);
}

void UserInterfaceLogger::logCancelOrderFinishedMessage(const QString &orderId,const QString &exchangeOrderId,const QJsonObject &result)
{
    Q_UNUSED(orderId)
    QJsonDocument document;
    QDateTime current_date=QDateTime::currentDateTime();
    QString log_message=QString("[ timestamp = ")+current_date.toString("hh:mm:ss.zzz")+QString(" ] ");
    log_message+=QString("[ message = placed a cancel request for order with id ")+exchangeOrderId+QString(" at the bittrex exchange ")+QString(" ] ");
    document.setObject(result);
    log_message+=QString(document.toJson(QJsonDocument::Compact))+QString("\n");
    this->insertPlainText(log_message);
    this->moveCursor(QTextCursor::End);
}


void UserInterfaceLogger::logQueryBalancesFinishedMessage(const QJsonArray &result)
{
    QJsonDocument document;
    QDateTime current_date=QDateTime::currentDateTime();
    QString log_message=QString("[ timestamp = ")+current_date.toString("hh:mm:ss.zzz")+QString(" ] ");
    log_message+=QString("[ message = received balances from the bittrex exchange ")+QString(" ] ");
    document.setArray(result);
    log_message+=QString(document.toJson(QJsonDocument::Compact))+QString("\n");
    this->insertPlainText(log_message);
    this->moveCursor(QTextCursor::End);
}


void UserInterfaceLogger::logQueryOrderFinishedMessage(const QString &orderId,const QString &exchangeOrderId,const QJsonObject &result)
{
    Q_UNUSED(orderId)
    QJsonDocument document;
    QDateTime current_date=QDateTime::currentDateTime();
    QString log_message=QString("[ timestamp = ")+current_date.toString("hh:mm:ss.zzz")+QString(" ] ");
    log_message+=QString("[ message = received status for order ")+exchangeOrderId+QString(" from the bittrex exchange ")+QString(" ] ");
    document.setObject(result);
    log_message+=QString(document.toJson(QJsonDocument::Compact))+QString("\n");
    this->insertPlainText(log_message);
    this->moveCursor(QTextCursor::End);
}
