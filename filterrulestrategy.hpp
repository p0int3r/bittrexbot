#ifndef FILTERRULESTRATEGY_HPP
#define FILTERRULESTRATEGY_HPP

#include <QObject>
#include <QString>
#include "strategy.hpp"

class FilterRuleStrategy: public trader::Strategy
{
    Q_OBJECT
    Q_PROPERTY(double filterRule READ getFilterRule WRITE setFilterRule NOTIFY filterRuleChanged)
    Q_PROPERTY(int backwardSteps READ getBackwardSteps WRITE setBackwardSteps NOTIFY backwardStepsChanged)

    private:
        double filterRule;
        int backwardSteps;

    public:
        FilterRuleStrategy();
        FilterRuleStrategy(QString name,QString description,
            trader::Strategy::FrequencyLevel frequencyLevel);
        ~FilterRuleStrategy() override;
        double getFilterRule() const;
        void setFilterRule(double value);
        int getBackwardSteps() const;
        void setBackwardSteps(int value);

    signals:
        void filterRuleChanged(const double &filterRule);
        void backwardStepsChanged(const int &backwardSteps);

    public slots:
        void preprocess(const QString &market, const QObject *object) override;
        void strategise(const QString &market, const QObject *object) override;
};

#endif // FILTERRULESTRATEGY_HPP
