#ifndef USERINTERFACELOGGER_HPP
#define USERINTERFACELOGGER_HPP

#include <QObject>
#include <QPlainTextEdit>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>


class UserInterfaceLogger: public QTextEdit
{
    Q_OBJECT

    public:
        explicit UserInterfaceLogger(QWidget *parent=nullptr);
        ~UserInterfaceLogger();

    public slots:
        void logQueryMarketsFinishedMessage(const QJsonArray &result);
        void logQueryCurrenciesFinishedMessage(const QJsonArray &result);
        void logQueryMarketTickerFinishedMessage(const QString &market,const QJsonObject &result);
        void logQueryMarketSummariesFinishedMessage(const QJsonArray &result);
        void logQueryMarketSummaryFinishedMessage(const QString &market,const QJsonObject &result);
        void logQueryMarketOrderBookFinishedMessage(const QString &market,const int &depth,const QJsonObject &result);
        void logQueryMarketTradesFinishedMessage(const QString &market,const QJsonArray &result);
        void logPlaceOrderFinishedMessage(const QString &orderId,const QJsonObject &result);
        void logCancelOrderFinishedMessage(const QString &orderId,const QString &exchangeOrderId,const QJsonObject &result);
        void logQueryBalancesFinishedMessage(const QJsonArray &result);
        void logQueryOrderFinishedMessage(const QString &orderId,const QString &exchangeOrderId,const QJsonObject &result);
};

#endif // USERINTERFACELOGGER_HPP
