#include "stoplossriskfactory.hpp"
#include "stoplossrisk.hpp"

StopLossRiskFactory::StopLossRiskFactory(QObject *parent): QObject(parent) {}

StopLossRiskFactory::~StopLossRiskFactory() {}

trader::Risk *StopLossRiskFactory::createStopLossRisk(QString name)
{
    StopLossRisk *stopLossRisk=nullptr;

    if (name==QString("Stop-loss at 4.0%"))
    {
        double stopLoss=4.0;
        QString description=QString("Trigger a stop-loss order at 4.0% below the price at which you bought the asset");
        trader::Risk::RiskIndicator riskIndicator=trader::Risk::RiskIndicator::MEDIUM;
        stopLossRisk=new StopLossRisk(QString("Stop-loss at 4.0%"),description,riskIndicator);
        stopLossRisk->setStopLoss(stopLoss);
    }
    else if (name==QString("Stop-loss at 5.0%"))
    {
        double stopLoss=5.0;
        QString description=QString("Trigger a stop-loss order at 5.0% below the price at which you bought the asset");
        trader::Risk::RiskIndicator riskIndicator=trader::Risk::RiskIndicator::HIGH;
        stopLossRisk=new StopLossRisk(QString("Stop-loss at 5.0%"),description,riskIndicator);
        stopLossRisk->setStopLoss(stopLoss);
    }
    else if (name==QString("Stop-loss at 6.0%"))
    {
        double stopLoss=6.0;
        QString description=QString("Trigger a stop-loss order at 6.0% below the price at which you bought the asset");
        trader::Risk::RiskIndicator riskIndicator=trader::Risk::RiskIndicator::HIGH;
        stopLossRisk=new StopLossRisk(QString("Stop-loss at 6.0%"),description,riskIndicator);
        stopLossRisk->setStopLoss(stopLoss);
    }
    else if (name==QString("Stop-loss at 7.0%"))
    {
        double stopLoss=7.0;
        QString description=QString("Trigger a stop-loss order at 7.0% below the price at which you bought the asset");
        trader::Risk::RiskIndicator riskIndicator=trader::Risk::RiskIndicator::HIGH;
        stopLossRisk=new StopLossRisk(QString("Stop-loss at 7.0%"),description,riskIndicator);
        stopLossRisk->setStopLoss(stopLoss);
    }
    else if (name==QString("Stop-loss at 8.0%"))
    {
        double stopLoss=8.0;
        QString description=QString("Trigger a stop-loss order at 8.0% below the price at which you bought the asset");
        trader::Risk::RiskIndicator riskIndicator=trader::Risk::RiskIndicator::HIGH;
        stopLossRisk=new StopLossRisk(QString("Stop-loss at 8.0%"),description,riskIndicator);
        stopLossRisk->setStopLoss(stopLoss);
    }
    else if (name==QString("Stop-loss at 9.0%"))
    {
        double stopLoss=9.0;
        QString description=QString("Trigger a stop-loss order at 9.0% below the price at which you bought the asset");
        trader::Risk::RiskIndicator riskIndicator=trader::Risk::RiskIndicator::HIGH;
        stopLossRisk=new StopLossRisk(QString("Stop-loss at 9.0%"),description,riskIndicator);
        stopLossRisk->setStopLoss(stopLoss);
    }

    return stopLossRisk;
}



trader::Risk *StopLossRiskFactory::createStopLossRisk(double stopLoss)
{
    StopLossRisk *stopLossRisk=nullptr;
    QString name=QString("Stop-loss at ")+QString::number(stopLoss)+QString("%");
    QString description=QString("Trigger a stop-loss order at ")+QString::number(stopLoss)+QString("% below the price at which you bought the asset");
    trader::Risk::RiskIndicator riskIndicator=trader::Risk::RiskIndicator::NONE;
    if (stopLoss<=1.0) { riskIndicator=trader::Risk::RiskIndicator::LOW; }
    if (stopLoss>1.0 && stopLoss<=4.0) { riskIndicator=trader::Risk::RiskIndicator::MEDIUM; }
    if (stopLoss>4.0) { riskIndicator=trader::Risk::RiskIndicator::HIGH; }
    stopLossRisk=new StopLossRisk(name,description,riskIndicator);
    stopLossRisk->setStopLoss(stopLoss);
    return stopLossRisk;
}


QString StopLossRiskFactory::translateStopLossRiskToSourceLanguage(QString name)
{
    QString translated_risk_source_language=QString();

    if (name==tr("Stop-loss at 4.0%"))
    {
        translated_risk_source_language=QString("Stop-loss at 4.0%");
    }
    else if (name==tr("Stop-loss at 5.0%"))
    {
        translated_risk_source_language=QString("Stop-loss at 5.0%");
    }
    else if (name==tr("Stop-loss at 6.0%"))
    {
        translated_risk_source_language=QString("Stop-loss at 6.0%");
    }
    else if (name==tr("Stop-loss at 7.0%"))
    {
        translated_risk_source_language=QString("Stop-loss at 7.0%");
    }
    else if (name==tr("Stop-loss at 8.0%"))
    {
        translated_risk_source_language=QString("Stop-loss at 8.0%");
    }
    else if (name==tr("Stop-loss at 9.0%"))
    {
        translated_risk_source_language=QString("Stop-loss at 9.0%");
    }

    return translated_risk_source_language;
}
