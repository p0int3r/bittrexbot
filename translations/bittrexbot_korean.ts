<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ko_KR" sourcelanguage="en_US">
<context>
    <name>DialogManager</name>
    <message>
        <location filename="../dialogmanager.cpp" line="44"/>
        <source>Restoration Dialog</source>
        <translation>복원 대화 상자</translation>
    </message>
    <message>
        <location filename="../dialogmanager.cpp" line="46"/>
        <source>Error Dialog</source>
        <translation>오류 대화 상자</translation>
    </message>
    <message>
        <location filename="../dialogmanager.cpp" line="47"/>
        <source>Confirmation Dialog</source>
        <translation>확인 대화상자</translation>
    </message>
</context>
<context>
    <name>FilterRuleStrategyFactory</name>
    <message>
        <location filename="../filterrulestrategyfactory.cpp" line="200"/>
        <source>Trigger-buy at 0.01%</source>
        <translation>0.01% 에서 트리거 구매</translation>
    </message>
    <message>
        <location filename="../filterrulestrategyfactory.cpp" line="204"/>
        <source>Trigger-buy at 0.02%</source>
        <translation>0.02% 에서 트리거 구매</translation>
    </message>
    <message>
        <location filename="../filterrulestrategyfactory.cpp" line="208"/>
        <source>Trigger-buy at 0.03%</source>
        <translation>0.03% 에서 트리거 구매</translation>
    </message>
    <message>
        <location filename="../filterrulestrategyfactory.cpp" line="212"/>
        <source>Trigger-buy at 0.04%</source>
        <translation>0.04% 에서 트리거 구매</translation>
    </message>
    <message>
        <location filename="../filterrulestrategyfactory.cpp" line="216"/>
        <source>Trigger-buy at 0.05%</source>
        <translation>0.05% 에서 트리거 구매</translation>
    </message>
    <message>
        <location filename="../filterrulestrategyfactory.cpp" line="220"/>
        <source>Trigger-buy at 0.06%</source>
        <translation>0.06% 에서 트리거 구매</translation>
    </message>
    <message>
        <location filename="../filterrulestrategyfactory.cpp" line="224"/>
        <source>Trigger-buy at 0.07%</source>
        <translation>0.07% 에서 트리거 구매</translation>
    </message>
    <message>
        <location filename="../filterrulestrategyfactory.cpp" line="228"/>
        <source>Trigger-buy at 0.08%</source>
        <translation>0.08% 에서 트리거 구매</translation>
    </message>
    <message>
        <location filename="../filterrulestrategyfactory.cpp" line="232"/>
        <source>Trigger-buy at 0.09%</source>
        <translation>0.09% 에서 트리거 구매</translation>
    </message>
    <message>
        <location filename="../filterrulestrategyfactory.cpp" line="236"/>
        <source>Trigger-buy at 0.1%</source>
        <translation>0.1% 에서 트리거 구매</translation>
    </message>
    <message>
        <location filename="../filterrulestrategyfactory.cpp" line="240"/>
        <source>Trigger-buy at 0.2%</source>
        <translation>0.2% 에서 트리거 구매</translation>
    </message>
    <message>
        <location filename="../filterrulestrategyfactory.cpp" line="244"/>
        <source>Trigger-buy at 0.3%</source>
        <translation>0.3% 에서 트리거 구매</translation>
    </message>
    <message>
        <location filename="../filterrulestrategyfactory.cpp" line="248"/>
        <source>Trigger-buy at 0.4%</source>
        <translation>0.4% 에서 트리거 구매</translation>
    </message>
    <message>
        <location filename="../filterrulestrategyfactory.cpp" line="252"/>
        <source>Trigger-buy at 0.5%</source>
        <translation>0.5% 에서 트리거 구매</translation>
    </message>
    <message>
        <location filename="../filterrulestrategyfactory.cpp" line="256"/>
        <source>Trigger-buy at 0.6%</source>
        <translation>0.6% 에서 트리거 구매</translation>
    </message>
    <message>
        <location filename="../filterrulestrategyfactory.cpp" line="260"/>
        <source>Trigger-buy at 0.7%</source>
        <translation>0.7% 에서 트리거 구매</translation>
    </message>
    <message>
        <location filename="../filterrulestrategyfactory.cpp" line="264"/>
        <source>Trigger-buy at 0.8%</source>
        <translation>0.8% 에서 트리거 구매</translation>
    </message>
    <message>
        <location filename="../filterrulestrategyfactory.cpp" line="268"/>
        <source>Trigger-buy at 0.9%</source>
        <translation>0.9% 에서 트리거 구매</translation>
    </message>
</context>
<context>
    <name>LanguageSelectorDialog</name>
    <message>
        <location filename="../languageselectordialog.ui" line="17"/>
        <source>Language Selector</source>
        <translation>언어 선택기</translation>
    </message>
    <message>
        <location filename="../languageselectordialog.ui" line="27"/>
        <source>Languages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../languageselectordialog.ui" line="33"/>
        <source>Português</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../languageselectordialog.ui" line="43"/>
        <source>普通话</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../languageselectordialog.ui" line="53"/>
        <source>हिंदी</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../languageselectordialog.ui" line="63"/>
        <source>Deutsche</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../languageselectordialog.ui" line="73"/>
        <source>عربى</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../languageselectordialog.ui" line="83"/>
        <source>한국어</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../languageselectordialog.ui" line="93"/>
        <source>русский</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../languageselectordialog.ui" line="103"/>
        <source>Basa jawa</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../languageselectordialog.ui" line="113"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../languageselectordialog.ui" line="191"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../languageselectordialog.ui" line="126"/>
        <source>Español</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../languageselectordialog.ui" line="156"/>
        <source>日本語</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../languageselectordialog.ui" line="136"/>
        <source>Italiano</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../languageselectordialog.ui" line="146"/>
        <source>Melayu</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="29"/>
        <source>BittrexBot</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="214"/>
        <source>Enter public key here...</source>
        <translation>공개 키를 입력합니다...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="230"/>
        <source>Enter  private key here...</source>
        <translation>여기에 개인 키를 입력하십시오...</translation>
    </message>
    <message>
        <source>Verify and continue ( LIVE TRADING )</source>
        <translation type="vanished">확인 및 계속(실시간 거래 )</translation>
    </message>
    <message>
        <source>Continue without Verification</source>
        <translation type="vanished">검증 없이 계속 진행</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:36pt; font-style:italic;&quot;&gt;BITTREXBOT&lt;/span&gt;&lt;br/&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;Bleeding edge, fully automated, &lt;br/&gt;A.I. powered trading bot for the Bittrex Exchange.&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Terms and Conditions&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Tutorials and Usage&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Universal Trading Inc.&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Follow as on Facebook&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Follow as on Twitter&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:36pt; font-style:italic;&quot;&gt;BITTREXBOT&lt;/span&gt;&lt;br/&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;완전히 자동화된 출혈 가장자리,&lt;br/&gt;A.I.bittrex 교환을 위한 강화된 무역 로봇.&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;이용 약관&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;자습서 및 사용법&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;주식회사 유니버설 트레이딩&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Facebook 에 따라 가기&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;트위터에 따라&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:36pt; font-style:italic;&quot;&gt;BITTREXBOT&lt;/span&gt;&lt;br/&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;Bleeding edge, fully automated, &lt;br/&gt;A.I. powered trading bot for the Bittrex Exchange.&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Terms and Conditions&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Tutorials and Usage&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Positronic Technologies&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Follow as on Facebook&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Follow as on Twitter&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:36pt; font-style:italic;&quot;&gt;BITTREXBOT&lt;/span&gt;&lt;br/&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;완전히 자동화된 출혈 가장자리,&lt;br/&gt;A.I.bittrex 교환을 위한 강화된 무역 로봇.&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;이용 약관&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;자습서 및 사용법&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Positronic Technologies&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Facebook 에 따라 가기&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;트위터에 따라&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.bittrexbot.io/&quot;&gt;&lt;img src=&quot;:/icons/light_mode/bittrexbot_logo.png&quot; width=&quot;200&quot; height=&quot;200&quot;/&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:36pt; font-weight:600; font-style:italic;&quot;&gt;BittrexBot&lt;/span&gt;&lt;br/&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;Bleeding edge, fully automated, &lt;br/&gt;A.I. powered trading bot for the Bittrex Exchange.&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.bittrexbot.io/terms-and-conditions&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Terms and Conditions&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.bittrexbot.io/usage-and-tutorials/&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Usage and Tutorials&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.facebook.com/bittrexbot/&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Follow as on Facebook&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://twitter.com/BotBittrex&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Follow as on Twitter&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.reddit.com/user/bittrexbot_software&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Join us on Reddit&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.youtube.com/channel/UClCDp4wfPRGRjmUBRXFAGcg?view_as=public&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Subscribe to Youtube&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.bittrexbot.io/blog/&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;News and Blogs&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.bittrexbot.io/&quot;&gt;&lt;img src=&quot;:/icons/light_mode/bittrexbot_logo.png&quot; width=&quot;200&quot; height=&quot;200&quot;/&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:36pt; font-weight:600; font-style:italic;&quot;&gt;BittrexBot&lt;/span&gt;&lt;br/&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;완전 자동화 된 최첨단 기술&lt;br/&gt;인공 지능에 의해 구동 bittrex 교환 무역 봇&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.bittrexbot.io/terms-and-conditions&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;이용 약관&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.bittrexbot.io/usage-and-tutorials/&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;사용 및 자습서&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.facebook.com/bittrexbot/&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Facebook 에 따라 가기&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://twitter.com/BotBittrex&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;트위터에 따라&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.reddit.com/user/bittrexbot_software&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;레딧에 우리를 가입&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.youtube.com/channel/UClCDp4wfPRGRjmUBRXFAGcg?view_as=public&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;유튜브 구독&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.bittrexbot.io/blog/&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;뉴스 및 블로그&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="349"/>
        <source>Analytics</source>
        <translation>분석</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="378"/>
        <source>Markets</source>
        <translation>시장</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="407"/>
        <source>Trader</source>
        <translation>뜻상인,상인</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="436"/>
        <source>Execution</source>
        <translation>실행</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="465"/>
        <source>Portfolios</source>
        <translation>포트폴리오</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="507"/>
        <source>Settings</source>
        <translation>설정</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="536"/>
        <source>Back </source>
        <translation>뜻다시,다시</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="626"/>
        <location filename="../mainwindow.ui" line="1249"/>
        <location filename="../mainwindow.ui" line="1494"/>
        <location filename="../mainwindow.ui" line="1739"/>
        <location filename="../mainwindow.ui" line="1984"/>
        <location filename="../mainwindow.ui" line="2235"/>
        <location filename="../mainwindow.ui" line="3915"/>
        <location filename="../mainwindow.ui" line="4123"/>
        <location filename="../mainwindow.ui" line="5900"/>
        <location filename="../mainwindow.ui" line="6183"/>
        <location filename="../mainwindow.ui" line="6757"/>
        <location filename="../mainwindow.ui" line="7040"/>
        <location filename="../mainwindow.ui" line="7584"/>
        <location filename="../mainwindow.ui" line="7867"/>
        <location filename="../mainwindow.ui" line="8417"/>
        <location filename="../mainwindow.ui" line="8700"/>
        <location filename="../mainwindow.ui" line="9244"/>
        <location filename="../mainwindow.ui" line="9527"/>
        <source>Find...</source>
        <translation>찾기...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="658"/>
        <source>All Markets</source>
        <translation>모든 시장</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="663"/>
        <location filename="../mainwindow.ui" line="1300"/>
        <source>USD Markets</source>
        <translation>USD 시장</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="668"/>
        <location filename="../mainwindow.ui" line="1545"/>
        <source>BTC Markets</source>
        <translation>BTC 시장</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="673"/>
        <location filename="../mainwindow.ui" line="1790"/>
        <source>ETH Markets</source>
        <translation>ETH 시장</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="678"/>
        <location filename="../mainwindow.ui" line="2035"/>
        <source>USDT Markets</source>
        <translation>USDT 시장</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="716"/>
        <source>↑ Volume</source>
        <translation>↑량</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="721"/>
        <source>↓ Volume</source>
        <translation>↓양</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="726"/>
        <source>↑ Change</source>
        <translation>↑변경</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="731"/>
        <source>↓ Change</source>
        <translation>↓변경</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="736"/>
        <source>↑ Last Price</source>
        <translation>↑마지막 가격</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="741"/>
        <source>↓ Last Price</source>
        <translation>↓마지막 가격</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="799"/>
        <source>Summary</source>
        <translation>요약</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1005"/>
        <location filename="../mainwindow.ui" line="1392"/>
        <location filename="../mainwindow.ui" line="1637"/>
        <location filename="../mainwindow.ui" line="1882"/>
        <location filename="../mainwindow.ui" line="2133"/>
        <location filename="../mainwindow.ui" line="2378"/>
        <source>Last Price</source>
        <translation>마지막 가격</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1052"/>
        <source>Bid Price</source>
        <translation>입찰 가격</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1093"/>
        <source>Ask Price</source>
        <translation>가격을 물으십시오</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1134"/>
        <source>Mid Price</source>
        <translation>중간 가격</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="826"/>
        <source>Base Volume</source>
        <translation>기본 양</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="176"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.bittrexbot.io/&quot;&gt;&lt;img src=&quot;:/icons/light_mode/bittrexbot_logo.png&quot; width=&quot;200&quot; height=&quot;200&quot;/&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:36pt; font-weight:600; font-style:italic;&quot;&gt;BittrexBot&lt;/span&gt;&lt;br/&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;Bleeding edge, fully automated, &lt;br/&gt;A.I. powered trading bot for the Bittrex Exchange.&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.bittrexbot.io/terms-and-conditions&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Terms and Conditions&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.bittrexbot.io/usage-and-tutorials/&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Usage and Tutorials&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.facebook.com/bittrexbot/&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Follow as on Facebook&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://twitter.com/BotBittrex&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Follow as on Twitter&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.reddit.com/user/bittrexbot_software&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Join us on Reddit&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.youtube.com/channel/UCLwcXW6CLUpGFcj1UnBGHMg?view_as=subscriber&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Subscribe to Youtube&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://t.me/BittrexBotCommunityChat&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Join us on Telegram&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.bittrexbot.io/blog/&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;News and Blogs&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.bittrexbot.io/&quot;&gt;&lt;img src=&quot;:/icons/light_mode/bittrexbot_logo.png&quot; width=&quot;200&quot; height=&quot;200&quot;/&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:36pt; font-weight:600; font-style:italic;&quot;&gt;BittrexBot&lt;/span&gt;&lt;br/&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;완전히 자동화되는 출혈 가장자리,&lt;br/&gt;A.I.bittrex 교환을 위한 강화된 무역 로봇.&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.bittrexbot.io/terms-and-conditions&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;이용 약관&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.bittrexbot.io/usage-and-tutorials/&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;사용 및 자습서&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.facebook.com/bittrexbot/&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Facebook 에 따라 가기&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://twitter.com/BotBittrex&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;트위터에 따라&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.reddit.com/user/bittrexbot_software&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;레딧에 우리를 가입&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.youtube.com/channel/UCLwcXW6CLUpGFcj1UnBGHMg?view_as=subscriber&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;유튜브 구독&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://t.me/BittrexBotCommunityChat&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;전보에 우리와 함께&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.bittrexbot.io/blog/&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;뉴스 및 블로그&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="249"/>
        <source>Live Trading</source>
        <translation>라이브 트레이딩</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="268"/>
        <source>Paper Trading</source>
        <translation>종이 거래</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="281"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;https://bittrex.com/Account/Register?referralCode=JRM-DEC-0IH&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#007af4;&quot;&gt;Create Bittrex Account&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;https://bittrex.com/Account/Register?referralCode=JRM-DEC-0IH&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#007af4;&quot;&gt;Bittrex 계정 만들기&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="300"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;https://bittrex.zendesk.com/hc/en-us/articles/360031921872-How-to-create-an-API-key-&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#007af4;&quot;&gt;How to create an API key?&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;https://bittrex.zendesk.com/hc/en-us/articles/360031921872-How-to-create-an-API-key-&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#007af4;&quot;&gt;API 키를 만드는 방법?&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="683"/>
        <location filename="../mainwindow.ui" line="2286"/>
        <source>EUR Markets</source>
        <translation>EUR 시장</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="867"/>
        <location filename="../mainwindow.ui" line="1370"/>
        <location filename="../mainwindow.ui" line="1615"/>
        <location filename="../mainwindow.ui" line="1860"/>
        <location filename="../mainwindow.ui" line="2111"/>
        <location filename="../mainwindow.ui" line="2356"/>
        <source>Volume</source>
        <translation>양</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="908"/>
        <source>24H High</source>
        <translation>높은 24 시간</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="949"/>
        <source>24H Low</source>
        <translation>낮은 24 시간</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1220"/>
        <location filename="../mainwindow.ui" line="1465"/>
        <location filename="../mainwindow.ui" line="1710"/>
        <location filename="../mainwindow.ui" line="1955"/>
        <location filename="../mainwindow.ui" line="2206"/>
        <source>Select All</source>
        <translation>모두 선택</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1233"/>
        <location filename="../mainwindow.ui" line="1478"/>
        <location filename="../mainwindow.ui" line="1723"/>
        <location filename="../mainwindow.ui" line="1968"/>
        <location filename="../mainwindow.ui" line="2219"/>
        <source>Deselect All</source>
        <translation>모두 선택 해제</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1337"/>
        <location filename="../mainwindow.ui" line="1582"/>
        <location filename="../mainwindow.ui" line="1827"/>
        <location filename="../mainwindow.ui" line="2078"/>
        <location filename="../mainwindow.ui" line="2323"/>
        <source>Select</source>
        <translation>선택</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1348"/>
        <location filename="../mainwindow.ui" line="1593"/>
        <location filename="../mainwindow.ui" line="1838"/>
        <location filename="../mainwindow.ui" line="2089"/>
        <location filename="../mainwindow.ui" line="2334"/>
        <location filename="../mainwindow.ui" line="4025"/>
        <location filename="../mainwindow.ui" line="4233"/>
        <location filename="../mainwindow.ui" line="6016"/>
        <location filename="../mainwindow.ui" line="6288"/>
        <location filename="../mainwindow.ui" line="6873"/>
        <location filename="../mainwindow.ui" line="7145"/>
        <location filename="../mainwindow.ui" line="7700"/>
        <location filename="../mainwindow.ui" line="7972"/>
        <location filename="../mainwindow.ui" line="8533"/>
        <location filename="../mainwindow.ui" line="8805"/>
        <location filename="../mainwindow.ui" line="9360"/>
        <location filename="../mainwindow.ui" line="9632"/>
        <source>Market</source>
        <translation>시장</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1359"/>
        <location filename="../mainwindow.ui" line="1604"/>
        <location filename="../mainwindow.ui" line="1849"/>
        <location filename="../mainwindow.ui" line="2100"/>
        <location filename="../mainwindow.ui" line="2345"/>
        <source>Currency</source>
        <translation>통화</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1381"/>
        <location filename="../mainwindow.ui" line="1626"/>
        <location filename="../mainwindow.ui" line="1871"/>
        <location filename="../mainwindow.ui" line="2122"/>
        <location filename="../mainwindow.ui" line="2367"/>
        <source>Change %</source>
        <translation>변경 %</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1403"/>
        <location filename="../mainwindow.ui" line="1648"/>
        <location filename="../mainwindow.ui" line="1893"/>
        <location filename="../mainwindow.ui" line="2144"/>
        <location filename="../mainwindow.ui" line="2389"/>
        <source>24HR High</source>
        <translation>높은 24 시간</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1414"/>
        <location filename="../mainwindow.ui" line="1659"/>
        <location filename="../mainwindow.ui" line="1904"/>
        <location filename="../mainwindow.ui" line="2155"/>
        <location filename="../mainwindow.ui" line="2400"/>
        <source>24HR Low</source>
        <translation>낮은 24 시간</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1425"/>
        <location filename="../mainwindow.ui" line="1670"/>
        <location filename="../mainwindow.ui" line="1915"/>
        <location filename="../mainwindow.ui" line="2166"/>
        <location filename="../mainwindow.ui" line="2411"/>
        <source>Spread %</source>
        <translation>퍼짐 %</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1436"/>
        <location filename="../mainwindow.ui" line="1681"/>
        <location filename="../mainwindow.ui" line="1926"/>
        <location filename="../mainwindow.ui" line="2177"/>
        <location filename="../mainwindow.ui" line="2422"/>
        <source>Added</source>
        <translation>추가됨</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2471"/>
        <source>Strategy</source>
        <translation>전략</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2487"/>
        <location filename="../mainwindow.ui" line="2744"/>
        <source>Description</source>
        <translation>설명</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2503"/>
        <location filename="../mainwindow.cpp" line="2507"/>
        <location filename="../mainwindow.cpp" line="2551"/>
        <source>Filter Rule</source>
        <translation>필터 규칙</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2510"/>
        <source>A filter rule is a trading strategy based on pre-determined price changes, typically quantified as a percentage</source>
        <translation>필터 규칙은 일반적으로 백분율로 정량화 소정의 가격 변화를 기반으로 거래 전략이다</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2514"/>
        <source>Trigger-buy at 0.01%</source>
        <translation>0.01% 에서 트리거 구매</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2520"/>
        <source>Triggers a limit buy order at 0.01% momentum percentage increase</source>
        <translation>0.01%모멘텀 비율 증가에 따른 한도 구매 주문을 트리거합니다</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2525"/>
        <source>Trigger-buy at 0.02%</source>
        <translation>0.02% 에서 트리거 구매</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2531"/>
        <source>Triggers a limit buy order at 0.02% momentum percentage increase</source>
        <translation>0.02%모멘텀 비율 증가에 제한 구매 주문을 트리거합니다</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2536"/>
        <source>Trigger-buy at 0.03%</source>
        <translation>0.03% 에서 트리거 구매</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2542"/>
        <source>Triggers a limit buy order at 0.03% momentum percentage increase</source>
        <translation>구매 주문을 0.03%의 모멘텀 비율 증가에 트리거합니다</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2547"/>
        <source>Trigger-buy at 0.04%</source>
        <translation>0.04% 에서 트리거 구매</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2553"/>
        <source>Triggers a limit buy order at 0.04% momentum percentage increase</source>
        <translation>제한 구매 주문을 0.04%의 모멘텀 비율 증가로 트리거합니다</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2558"/>
        <source>Trigger-buy at 0.05%</source>
        <translation>0.05% 에서 트리거 구매</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2564"/>
        <source>Triggers a limit buy order at 0.05% momentum percentage increase</source>
        <translation>0.05%모멘텀 비율 증가에 제한 구매 주문을 트리거합니다.</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2569"/>
        <source>Trigger-buy at 0.06%</source>
        <translation>0.06% 에서 트리거 구매</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2575"/>
        <source>Triggers a limit buy order at 0.06% momentum percentage increase</source>
        <translation>제한 구매 주문을 0.06%의 모멘텀 비율 증가로 트리거합니다</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2580"/>
        <source>Trigger-buy at 0.07%</source>
        <translation>0.07% 에서 트리거 구매</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2586"/>
        <source>Triggers a limit buy order at 0.07% momentum percentage increase</source>
        <translation>구매 주문을 0.07%의 모멘텀 비율 증가로 트리거합니다</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2591"/>
        <source>Trigger-buy at 0.08%</source>
        <translation>0.08% 에서 트리거 구매</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2597"/>
        <source>Triggers a limit buy order at 0.08% momentum percentage increase</source>
        <translation>구매 주문을 0.08%의 모멘텀 비율 증가에 트리거합니다</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2602"/>
        <source>Trigger-buy at 0.09%</source>
        <translation>0.09% 에서 트리거 구매</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2608"/>
        <source>Triggers a limit buy order at 0.09% momentum percentage increase</source>
        <translation>구매 주문을 0.09%의 모멘텀 비율 증가에 트리거합니다</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2613"/>
        <source>Trigger-buy at 0.1%</source>
        <translation>0.1% 에서 트리거 구매</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2619"/>
        <source>Triggers a limit buy order at 0.1% momentum percentage increase</source>
        <translation>0.1%의 모멘텀 비율 증가에 제한 구매 주문을 트리거합니다.</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2624"/>
        <source>Trigger-buy at 0.2%</source>
        <translation>0.2% 에서 트리거 구매</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2630"/>
        <source>Triggers a limit buy order at 0.2% momentum percentage increase</source>
        <translation>0.2%모멘텀 비율 증가에 제한 구매 주문을 트리거합니다</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2635"/>
        <source>Trigger-buy at 0.3%</source>
        <translation>0.3% 에서 트리거 구매</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2641"/>
        <source>Triggers a limit buy order at 0.3% momentum percentage increase</source>
        <translation>0.3%의 모멘텀 비율 증가에 제한 구매 주문을 트리거합니다</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2646"/>
        <source>Trigger-buy at 0.4%</source>
        <translation>0.4% 에서 트리거 구매</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2652"/>
        <source>Triggers a limit buy order at 0.4% momentum percentage increase</source>
        <translation>0.4%의 모멘텀 비율 증가에 제한 구매 주문을 트리거합니다</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2657"/>
        <source>Trigger-buy at 0.5%</source>
        <translation>0.5% 에서 트리거 구매</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2663"/>
        <source>Triggers a limit buy order at 0.5% momentum percentage increase</source>
        <translation>0.5%의 모멘텀 비율 증가에 제한 구매 주문을 트리거합니다.</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2668"/>
        <source>Trigger-buy at 0.6%</source>
        <translation>0.6% 에서 트리거 구매</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2674"/>
        <source>Triggers a limit buy order at 0.6% momentum percentage increase</source>
        <translation>0.6%의 모멘텀 비율 증가에 제한 구매 주문을 트리거합니다.</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2679"/>
        <source>Trigger-buy at 0.7%</source>
        <translation>0.7% 에서 트리거 구매</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2685"/>
        <source>Triggers a  limit buy order at 0.7% momentum percentage increase</source>
        <translation>0.7%의 모멘텀 비율 증가에 제한 구매 주문을 트리거합니다</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2690"/>
        <source>Trigger-buy at 0.8%</source>
        <translation>0.8% 에서 트리거 구매</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2696"/>
        <source>Triggers a  limit buy order at 0.8% momentum percentage increase</source>
        <translation>0.8%모멘텀 비율 증가에 제한 구매 주문을 트리거합니다.</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2701"/>
        <source>Trigger-buy at 0.9%</source>
        <translation>0.9% 에서 트리거 구매</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2707"/>
        <source>Triggers a  limit buy order at 0.9% momentum percentage increase</source>
        <translation>0.9%모멘텀 비율 증가에 제한 구매 주문을 트리거합니다</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2729"/>
        <source>Risk</source>
        <translation>위험</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2759"/>
        <source>Risk Indicator</source>
        <translation>위험 표시기</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2774"/>
        <location filename="../mainwindow.cpp" line="2512"/>
        <location filename="../mainwindow.cpp" line="2556"/>
        <source>Stop Loss</source>
        <translation>정지 손실</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2781"/>
        <source>A stop-loss is designed to limit an investor&apos;s loss on an asset position</source>
        <translation>정지 손실은 자산 위치에 투자자의 손실을 제한 할 수 있도록 설계되었습니다</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2784"/>
        <location filename="../mainwindow.ui" line="2907"/>
        <source>-</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2788"/>
        <source>Stop-loss at 4.0%</source>
        <translation>4.0% 에서 정지 손실</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2794"/>
        <source>Trigger a stop-loss order at 4.0% below the price at which you bought the asset</source>
        <translation>당신이 자산을 구입 한 가격 아래의 4.0%에서 정지 손실 순서를 트리거합니다</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2797"/>
        <location filename="../mainwindow.ui" line="2815"/>
        <location filename="../mainwindow.ui" line="2920"/>
        <location filename="../mainwindow.ui" line="2938"/>
        <location filename="../mainwindow.ui" line="2956"/>
        <location filename="../mainwindow.ui" line="2974"/>
        <location filename="../mainwindow.ui" line="2992"/>
        <location filename="../mainwindow.ui" line="3010"/>
        <location filename="../mainwindow.ui" line="3028"/>
        <location filename="../mainwindow.ui" line="3046"/>
        <location filename="../mainwindow.cpp" line="2515"/>
        <location filename="../mainwindow.cpp" line="2559"/>
        <source>Low Risk</source>
        <translation>낮은 위험</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2806"/>
        <source>Stop-loss at 5.0%</source>
        <translation>5.0% 에서 정지 손실</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2812"/>
        <source>Trigger a stop-loss order at 5.0% below the price at which you bought the asset</source>
        <translation>당신이 자산을 구입 한 가격 아래의 5.0%에서 정지 손실 순서를 트리거합니다</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2824"/>
        <source>Stop-loss at 6.0%</source>
        <translation>6.0% 에서 정지 손실</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2830"/>
        <source>Trigger a stop-loss order at 6.0% below the price at which you bought the asset</source>
        <translation>당신이 자산을 구입 한 가격 아래의 6.0%에서 정지 손실 순서를 트리거합니다</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2833"/>
        <location filename="../mainwindow.ui" line="2851"/>
        <location filename="../mainwindow.ui" line="3064"/>
        <location filename="../mainwindow.ui" line="3082"/>
        <location filename="../mainwindow.ui" line="3100"/>
        <location filename="../mainwindow.ui" line="3118"/>
        <location filename="../mainwindow.cpp" line="2518"/>
        <location filename="../mainwindow.cpp" line="2562"/>
        <source>Medium Risk</source>
        <translation>중간 위험</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2842"/>
        <source>Stop-loss at 7.0%</source>
        <translation>7.0% 에서 정지 손실</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2848"/>
        <source>Trigger a stop-loss order at 7.0% below the price at which you bought the asset</source>
        <translation>당신이 자산을 구입 한 가격 아래의 7.0%에서 정지 손실 순서를 트리거합니다</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2860"/>
        <source>Stop-loss at 8.0%</source>
        <translation>8.0% 에서 정지 손실</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2866"/>
        <source>Trigger a stop-loss order at 8.0% below the price at which you bought the asset</source>
        <translation>당신이 자산을 구입 한 가격 아래의 8.0%에서 정지 손실 순서를 트리거합니다</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2869"/>
        <location filename="../mainwindow.ui" line="2887"/>
        <location filename="../mainwindow.ui" line="3136"/>
        <location filename="../mainwindow.ui" line="3154"/>
        <location filename="../mainwindow.ui" line="3172"/>
        <location filename="../mainwindow.ui" line="3190"/>
        <location filename="../mainwindow.cpp" line="2521"/>
        <location filename="../mainwindow.cpp" line="2565"/>
        <source>High Risk</source>
        <translation>높은 위험</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2878"/>
        <source>Stop-loss at 9.0%</source>
        <translation>9.0% 에서 정지 손실</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2884"/>
        <source>Trigger a stop-loss order at 9.0% below the price at which you bought the asset</source>
        <translation>당신이 자산을 구입 한 가격 아래의 9.0%에서 정지 손실 순서를 트리거합니다</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2897"/>
        <location filename="../mainwindow.cpp" line="2513"/>
        <location filename="../mainwindow.cpp" line="2557"/>
        <source>Take Profit</source>
        <translation>이익을</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2904"/>
        <source>A take-profit is designed to limit an investor&apos;s profit on an asset position</source>
        <translation>테이크 이익은 자산 위치에 투자자의 이익을 제한 할 수 있도록 설계되었습니다</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2911"/>
        <source>Take-profit at 0.03%</source>
        <translation>0.03% 에서 테이크 이익</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2917"/>
        <source>Trigger a take-profit order at 0.03% above the price at which you bought the asset</source>
        <translation>당신이 자산을 구입 한 가격 이상 0.03%에서 테이크 이익 순서를 트리거합니다</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2929"/>
        <source>Take-profit at 0.04%</source>
        <translation>0.04% 에서 테이크 이익</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2935"/>
        <source>Trigger a take-profit order at 0.04% above the price at which you bought the asset</source>
        <translation>당신이 자산을 구입 한 가격 이상 0.04%에서 테이크 이익 순서를 트리거합니다</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2947"/>
        <source>Take-profit at 0.05%</source>
        <translation>0.05% 에서 테이크 이익</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2953"/>
        <source>Trigger a take-profit order at 0.05% above the price at which you bought the asset</source>
        <translation>당신이 자산을 구입 한 가격 위의 0.05%에서 테이크 이익 순서를 트리거합니다</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2965"/>
        <source>Take-profit at 0.06%</source>
        <translation>0.06% 에서 테이크 이익</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2971"/>
        <source>Trigger a take-profit order at 0.06% above the price at which you bought the asset</source>
        <translation>당신이 자산을 구입 한 가격 이상 0.06%에서 테이크 이익 순서를 트리거합니다</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2983"/>
        <source>Take-profit at 0.07%</source>
        <translation>0.07% 에서 테이크 이익</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2989"/>
        <source>Trigger a take-profit order at 0.07% above the price at which you bought the asset</source>
        <translation>당신이 자산을 구입 한 가격 이상 0.07%에서 테이크 이익 순서를 트리거합니다</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3001"/>
        <source>Take-profit at 0.08%</source>
        <translation>0.08% 에서 테이크 이익</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3007"/>
        <source>Trigger a take-profit order at 0.08% above the price at which you bought the asset</source>
        <translation>당신이 자산을 구입 한 가격 위의 0.08%에서 테이크 이익 순서를 트리거합니다</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3019"/>
        <source>Take-profit at 0.09%</source>
        <translation>0.09% 에서 테이크 이익</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3025"/>
        <source>Trigger a take-profit order at 0.09% above the price at which you bought the asset</source>
        <translation>당신이 자산을 구입 한 가격 이상 0.09%에서 테이크 이익 순서를 트리거합니다</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3037"/>
        <source>Take-profit at 0.1%</source>
        <translation>0.1% 에서 테이크 이익</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3043"/>
        <source>Trigger a take-profit order at 0.1% above the price at which you bought the asset</source>
        <translation>당신이 자산을 구입 한 가격 위의 0.1%에서 테이크 이익 순서를 트리거합니다</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3055"/>
        <source>Take-profit at 0.2%</source>
        <translation>0.2% 에서 테이크 이익</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3061"/>
        <source>Trigger a take-profit order at 0.2% above the price at which you bought the asset</source>
        <translation>에서 테이크 이익 순서를 트리거 0.2%당신이 자산을 구입 한 가격 위의</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3073"/>
        <source>Take-profit at 0.3%</source>
        <translation>0.3% 에서 테이크 이익</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3079"/>
        <source>Trigger a take-profit order at 0.3% above the price at which you bought the asset</source>
        <translation>당신이 자산을 구입 한 가격 이상 0.3%에서 테이크 이익 순서를 트리거합니다</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3091"/>
        <source>Take-profit at 0.4%</source>
        <translation>0.4% 에서 테이크 이익</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3097"/>
        <source>Trigger a take-profit order at 0.4% above the price at which you bought the asset</source>
        <translation>당신이 자산을 구입 한 가격 이상 0.4%에서 테이크 이익 순서를 트리거합니다</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3109"/>
        <source>Take-profit at 0.5%</source>
        <translation>0.5% 에서 테이크 이익</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3115"/>
        <source>Trigger a take-profit order at 0.5% above the price at which you bought the asset</source>
        <translation>당신이 자산을 구입 한 가격 위의 0.5%에서 테이크 이익 순서를 트리거합니다</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3127"/>
        <source>Take-profit at 0.6%</source>
        <translation>0.6% 에서 테이크 이익</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3133"/>
        <source>Trigger a take-profit order at 0.6% above the price at which you bought the asset</source>
        <translation>당신이 자산을 구입 한 가격 이상 0.6%에서 테이크 이익 순서를 트리거합니다</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3145"/>
        <source>Take-profit at 0.7%</source>
        <translation>0.7% 에서 테이크 이익</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3151"/>
        <source>Trigger a take-profit order at 0.7% above the price at which you bought the asset</source>
        <translation>당신이 자산을 구입 한 가격 이상 0.7%에서 테이크 이익 순서를 트리거합니다</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3163"/>
        <source>Take-profit at 0.8%</source>
        <translation>0.8% 에서 테이크 이익</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3169"/>
        <source>Trigger a take-profit order at 0.8% above the price at which you bought the asset</source>
        <translation>당신이 자산을 구입 한 가격 위의 0.8%에서 테이크 이익 순서를 트리거합니다</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3181"/>
        <source>Take-profit at 0.9%</source>
        <translation>0.9% 에서 테이크 이익</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3187"/>
        <source>Trigger a take-profit order at 0.9% above the price at which you bought the asset</source>
        <translation>당신이 자산을 구입 한 가격 위의 0.9%에서 테이크 이익 순서를 트리거합니다</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3225"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600; text-decoration: underline;&quot;&gt;What is BittrexBot?&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;&lt;br /&gt;BittrexBot is a bleeding edge, fully automated, A.I. powered trading bot for the Bittrex exchange. It is a desktop application that enables users to automate trades at the Bittrex exchange in real time. &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Built on top of a signal-driven, asynchronous, multithreaded trading engine, it strives to maximize profit and mitigate risk by providing efficient mechanisms for detecting market trends, triggering orders and managining positions.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;BittrexBot is powered by a form of artificial intelligence known as stacked generalization, a meta-learning technique that combines different predictive modelling algorithms to achieve greater degree of accuracy in regards to price prediction.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;BittrexBot comes with a powerful recovery mechanism which enables users to restore trading sessions that might have been cut abruptly due to a power failure, crash or an accidental quit by the user.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Designed with security in mind, the application runs locally on your laptop or desktop computer, removing the need to store your API keys into the cloud or to external third party entities.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;To properly use the application, a user must have a valid bittrex account and must generate a public and a private token that are going to be used by the bot in order to communicate with the exchange and execute orders.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600; text-decoration: underline;&quot;&gt;BittrexBot 은 무엇인가?&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;&lt;br /&gt;BittrexBot 은 피 흘리는 가장자리입니다,완전 자동화,Bittrex 교환 A.I. 전원 거래 봇. 이 사용자가 실시간으로 Bittrex 교환에서 거래를 자동화 할 수있는 데스크톱 응용 프로그램입니다. &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;상단에 내장 신호의 중심,비동기,멀티 스레드 무역 엔진하기 위해 노력하고 있 이익을 극대화하고 위험을 완화하기 위해 제공하여 효율적인 메커니즘을 검출하는 시장 트렌드,트리거 주문 및 managining 위치.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;BittrexBot 에 의해 구동적으로 알려진 스택 일반화,메타-학습 기법을 결합하는 다른 예측적 모델링 알고리즘을 강화하고 달성하기 위해 수준의 정확도에 관해서 가격 예측.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;BittrexBot 와 함께 제공하는 강력한 복구 메커니즘을 활용하여 복원 거래되는 세션 수 있습을 잘라왔기 때문이전시,충돌하거나 실수로 종료에 의해 사용됩니다.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;보안을 염두에 두고 설계되었습,응용 프로그램을 실행에 로컬로 또는 노트북,데스크톱 컴퓨터 제거를 저장하는 데 필요한 API 으로 키 클라우드 또는 외부 세 번째 파티를 요한다.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;을 제대로 사용하여 응용 프로그램,사용자가 유효한 bittrex 에 계정 생성하는 공공 및 개인 토큰을 사용하는 것에 의해 로봇과 의사 소통을하기 위해 교환 및 실행 순서.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3268"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; font-weight:600; text-decoration: underline;&quot;&gt;Trading Configuration Guideline:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;1. Navigate to the markets page to select or deselect markets.&lt;br/&gt;2. Navigate to the trader page to select or deselect strategies.&lt;br/&gt;3. Navigate to the trader page to select or deselect risks.&lt;br/&gt;4. You must select at least one strategy.&lt;br/&gt;5. You must select at least one take-profit risk.&lt;br/&gt;6. You must select at least one stop-loss risk.&lt;br/&gt;7. You cannot change configuration during trading session.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; font-weight:600; text-decoration: underline;&quot;&gt;Capital Allocation Guideline:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;1. Carefully allocate initial capital (if any) for each market.&lt;br/&gt;2. For each asset in a market provide the capital percentage.&lt;br/&gt;3. For each asset in a market provide the trading rounds.&lt;br/&gt;4. Allocate capital based on technical analysis.&lt;br/&gt;5. Thoroughly check your capital allocations.&lt;br/&gt;6. Click the check-up button for sanity check.&lt;br/&gt;7. You cannot change capital allocation during trading session.&lt;br/&gt;8. When ready, click the execute button to start the trading session.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;&amp;quot;Aut non rem temptes aut perfice&amp;quot; - Ovid, 43 BC-17 AD, Roman poet&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; font-weight:600; text-decoration: underline;&quot;&gt;거래 구성 지침:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;1. 시장 페이지로 이동하여 시장 선택 또는 선택을 취소합니다.&lt;br/&gt;2. 전략을 선택하거나 선택을 취소 할 상인 페이지로 이동합니다.&lt;br/&gt;3. 거래자 페이지로 이동하여 위험을 선택 또는 선택 취소.&lt;br/&gt;4. 당신은 적어도 하나의 전략을 선택해야합니다.&lt;br/&gt;5. 당신은 적어도 하나의 테이크 이익 위험을 선택해야합니다.&lt;br/&gt;6. 당신은 적어도 하나의 정지 손실 위험을 선택해야합니다.&lt;br/&gt;7. 거래 세션 중에 구성을 변경할 수 없습니다.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; font-weight:600; text-decoration: underline;&quot;&gt;자본 배분 지침:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;1. 조심스럽게 각 시장에 대한 초기 자본(있는 경우)를 할당합니다.&lt;br/&gt;2. 시장의 각 자산에 대해 자본 비율을 제공합니다.&lt;br/&gt;3. 시장의 각 자산에 대해 거래 라운드를 제공합니다.&lt;br/&gt;4. 기술적 분석을 기반으로 자본을 할당하십시오.&lt;br/&gt;5. 철저하게 자본 할당을 확인합니다.&lt;br/&gt;6. 정신 검사에 대한 체크 업 버튼을 클릭합니다.&lt;br/&gt;7. 거래 세션 중에 자본 배분을 변경할 수 없습니다.&lt;br/&gt;8. 준비가되면,거래 세션을 시작 실행 버튼을 클릭합니다.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;&amp;quot;Aut non rem temptes aut perfice&amp;quot; - Ovid, 43 BC-17 AD, Roman poet&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3303"/>
        <source>CHECK-UP</source>
        <translation>체크인</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3329"/>
        <source>EXECUTE</source>
        <translation>실행</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3355"/>
        <source>USD Capital ($)</source>
        <translation>USD 자본 ($)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3410"/>
        <source>Selected USD Markets</source>
        <translation>USD 시장 선정</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3421"/>
        <location filename="../mainwindow.ui" line="3524"/>
        <location filename="../mainwindow.ui" line="3628"/>
        <location filename="../mainwindow.ui" line="3731"/>
        <source>Capital Percentage %</source>
        <translation>자본 비율 %</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3432"/>
        <location filename="../mainwindow.ui" line="3535"/>
        <location filename="../mainwindow.ui" line="3639"/>
        <location filename="../mainwindow.ui" line="3742"/>
        <location filename="../mainwindow.ui" line="3835"/>
        <source>Trading Rounds</source>
        <translation>거래 라운드</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3458"/>
        <source>BTC Capital (₿)</source>
        <translation>BTC 자본 (₿)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3513"/>
        <source>Selected BTC Markets</source>
        <translation>선정된 BTC 시장</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3562"/>
        <source>ETH Capital (Ξ)</source>
        <translation>ETH 자본 (Ξ)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3617"/>
        <source>Selected ETH Markets</source>
        <translation>ETH 시장 선정</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3665"/>
        <source>USDT Capital (₮)</source>
        <translation>USDT 자본 (₮)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3720"/>
        <source>Selected USDT Markets</source>
        <translation>USDT 시장 선정</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3771"/>
        <source>EUR Capital (€)</source>
        <translation>EUR 자본 (€)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3813"/>
        <source>Selected EUR Markets</source>
        <translation>EUR 시장 선정</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3824"/>
        <source>Capital Percentage (%)</source>
        <translation>자본 비율 (%)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3902"/>
        <source>Cancel All</source>
        <translation>모두 취소</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3966"/>
        <source>Open Orders</source>
        <translation>열기 주문</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4003"/>
        <source>Cancel</source>
        <translation>취소</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4014"/>
        <source>Opened</source>
        <translation>열림</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4036"/>
        <location filename="../mainwindow.ui" line="4244"/>
        <source>Side</source>
        <translation>측</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4047"/>
        <location filename="../mainwindow.ui" line="4255"/>
        <source>Type</source>
        <translation>유형</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4058"/>
        <location filename="../mainwindow.ui" line="4266"/>
        <location filename="../mainwindow.ui" line="6093"/>
        <location filename="../mainwindow.ui" line="6365"/>
        <location filename="../mainwindow.ui" line="6950"/>
        <location filename="../mainwindow.ui" line="7222"/>
        <location filename="../mainwindow.ui" line="7777"/>
        <location filename="../mainwindow.ui" line="8049"/>
        <location filename="../mainwindow.ui" line="8610"/>
        <location filename="../mainwindow.ui" line="8882"/>
        <location filename="../mainwindow.ui" line="9437"/>
        <location filename="../mainwindow.ui" line="9709"/>
        <source>Price</source>
        <translation>가격</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4069"/>
        <location filename="../mainwindow.ui" line="4277"/>
        <location filename="../mainwindow.ui" line="6082"/>
        <location filename="../mainwindow.ui" line="6354"/>
        <location filename="../mainwindow.ui" line="6939"/>
        <location filename="../mainwindow.ui" line="7211"/>
        <location filename="../mainwindow.ui" line="7766"/>
        <location filename="../mainwindow.ui" line="8038"/>
        <location filename="../mainwindow.ui" line="8599"/>
        <location filename="../mainwindow.ui" line="8871"/>
        <location filename="../mainwindow.ui" line="9426"/>
        <location filename="../mainwindow.ui" line="9698"/>
        <source>Quantity</source>
        <translation>양</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4080"/>
        <location filename="../mainwindow.ui" line="4288"/>
        <source>Filled %</source>
        <translation>채워진 %</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4091"/>
        <source>Est. Total</source>
        <translation>추정된 합계</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4174"/>
        <source>Closed Orders</source>
        <translation>닫힌 주문</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4211"/>
        <source>Closed Date</source>
        <translation>닫은 날짜</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4222"/>
        <source>Opened Date</source>
        <translation>열린 날짜</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4299"/>
        <source>Total</source>
        <translation>총</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4427"/>
        <location filename="../mainwindow.ui" line="5735"/>
        <source>Initial Capital ($)</source>
        <translation>초기 자본 ($)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4464"/>
        <location filename="../mainwindow.ui" line="5782"/>
        <source>Current Capital ($)</source>
        <translation>현재 자본 ($)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4501"/>
        <source>Total Equity ($)</source>
        <translation>총 주식 ($)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4538"/>
        <location filename="../mainwindow.ui" line="5625"/>
        <source>Realised PnL ($)</source>
        <translation>실현된 이익/손실 ($)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4572"/>
        <location filename="../mainwindow.ui" line="4795"/>
        <location filename="../mainwindow.ui" line="5018"/>
        <location filename="../mainwindow.ui" line="5256"/>
        <location filename="../mainwindow.ui" line="5472"/>
        <source>ROI (%)</source>
        <translation>투자 수익 (%)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4625"/>
        <source>USD Portfolio Session Summary</source>
        <translation>USD 포트폴리오 세션 요약</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4653"/>
        <location filename="../mainwindow.ui" line="6592"/>
        <source>Initial Capital (₿)</source>
        <translation>초기 자본금 (₿)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4690"/>
        <location filename="../mainwindow.ui" line="6639"/>
        <source>Current Capital (₿)</source>
        <translation>현재 자본 (₿)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4727"/>
        <source>Total Equity (₿)</source>
        <translation>총 주식 (₿)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4761"/>
        <location filename="../mainwindow.ui" line="6482"/>
        <source>Realised PnL (₿)</source>
        <translation>수익/손실 실현 (₿)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4848"/>
        <source>BTC Portfolio Session Summary</source>
        <translation>BTC 포트폴리오 세션 요약</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="5114"/>
        <location filename="../mainwindow.ui" line="8264"/>
        <source>Initial Capital (₮)</source>
        <translation>초기 자본금 (₮)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="5151"/>
        <location filename="../mainwindow.ui" line="8311"/>
        <source>Current Capital (₮)</source>
        <translation>현재 자본 (₮)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="5188"/>
        <source>Total Equity (₮)</source>
        <translation>총 주식 (₮)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="5222"/>
        <location filename="../mainwindow.ui" line="8166"/>
        <source>Realised PnL (₮)</source>
        <translation>수익/손실 실현 (₮)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="5299"/>
        <source>USDT Portfolio Session Summary</source>
        <translation>USDT 포트폴리오 세션 요약</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4876"/>
        <location filename="../mainwindow.ui" line="7437"/>
        <source>Initial Capital (Ξ)</source>
        <translation>초기 자본금 (Ξ)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4913"/>
        <location filename="../mainwindow.ui" line="7478"/>
        <source>Current Capital (Ξ)</source>
        <translation>현재 자본 (Ξ)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4950"/>
        <source>Total Equity (Ξ)</source>
        <translation>총 주식 (Ξ)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4984"/>
        <location filename="../mainwindow.ui" line="7339"/>
        <source>Realised PnL (Ξ)</source>
        <translation>수익/손실 실현(Ξ)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="5076"/>
        <source>ETH Portfolio Session Summary</source>
        <translation>ETH 포트폴리오 세션 요약</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="5327"/>
        <location filename="../mainwindow.ui" line="9091"/>
        <source>Initial Capital (€)</source>
        <translation>초기 자본금 (€)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="5364"/>
        <location filename="../mainwindow.ui" line="9138"/>
        <source>Current Capital (€)</source>
        <translation>현재 자본 (€)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="5401"/>
        <source>Total Equity (€)</source>
        <translation>총 주식 (€)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="5438"/>
        <location filename="../mainwindow.ui" line="8993"/>
        <source>Realised PnL (€)</source>
        <translation>수익/손실 실현 (€)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="5525"/>
        <source>EUR Portfolio Session Summary</source>
        <translation>EUR 포트폴리오 세션 요약</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="5594"/>
        <source>USD Portfolio</source>
        <translation>USD 포트폴리오</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="5678"/>
        <source>Unrealised PnL ($)</source>
        <translation>미실현 이익/손실 ($)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="5829"/>
        <source>Equity ($)</source>
        <translation>주식 ($)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="5881"/>
        <location filename="../mainwindow.ui" line="6738"/>
        <location filename="../mainwindow.ui" line="7565"/>
        <location filename="../mainwindow.ui" line="8398"/>
        <location filename="../mainwindow.ui" line="9225"/>
        <source>Liquify All</source>
        <translation>모두 픽셀 유동화</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="5951"/>
        <location filename="../mainwindow.ui" line="6808"/>
        <location filename="../mainwindow.ui" line="7635"/>
        <location filename="../mainwindow.ui" line="8468"/>
        <location filename="../mainwindow.ui" line="9295"/>
        <source>Open Positions</source>
        <translation>열린 위치</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="5994"/>
        <location filename="../mainwindow.ui" line="6851"/>
        <location filename="../mainwindow.ui" line="7678"/>
        <location filename="../mainwindow.ui" line="8511"/>
        <location filename="../mainwindow.ui" line="9338"/>
        <source>Liquify</source>
        <translation>픽셀 유동화</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="6005"/>
        <location filename="../mainwindow.ui" line="6277"/>
        <location filename="../mainwindow.ui" line="6862"/>
        <location filename="../mainwindow.ui" line="7134"/>
        <location filename="../mainwindow.ui" line="7689"/>
        <location filename="../mainwindow.ui" line="7961"/>
        <location filename="../mainwindow.ui" line="8522"/>
        <location filename="../mainwindow.ui" line="8794"/>
        <location filename="../mainwindow.ui" line="9349"/>
        <location filename="../mainwindow.ui" line="9621"/>
        <source>Action</source>
        <translation>동작</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="6027"/>
        <location filename="../mainwindow.ui" line="6299"/>
        <location filename="../mainwindow.ui" line="6884"/>
        <location filename="../mainwindow.ui" line="7156"/>
        <location filename="../mainwindow.ui" line="7711"/>
        <location filename="../mainwindow.ui" line="7983"/>
        <location filename="../mainwindow.ui" line="8544"/>
        <location filename="../mainwindow.ui" line="8816"/>
        <location filename="../mainwindow.ui" line="9371"/>
        <location filename="../mainwindow.ui" line="9643"/>
        <source>Average Price</source>
        <translation>평균 가격</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="6038"/>
        <location filename="../mainwindow.ui" line="6310"/>
        <location filename="../mainwindow.ui" line="6895"/>
        <location filename="../mainwindow.ui" line="7167"/>
        <location filename="../mainwindow.ui" line="7722"/>
        <location filename="../mainwindow.ui" line="7994"/>
        <location filename="../mainwindow.ui" line="8555"/>
        <location filename="../mainwindow.ui" line="8827"/>
        <location filename="../mainwindow.ui" line="9382"/>
        <location filename="../mainwindow.ui" line="9654"/>
        <source>Market Value</source>
        <translation>시장 가치</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="6049"/>
        <location filename="../mainwindow.ui" line="6321"/>
        <location filename="../mainwindow.ui" line="6906"/>
        <location filename="../mainwindow.ui" line="7178"/>
        <location filename="../mainwindow.ui" line="7733"/>
        <location filename="../mainwindow.ui" line="8005"/>
        <location filename="../mainwindow.ui" line="8566"/>
        <location filename="../mainwindow.ui" line="8838"/>
        <location filename="../mainwindow.ui" line="9393"/>
        <location filename="../mainwindow.ui" line="9665"/>
        <source>Cost Basis</source>
        <translation>비용 기준</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="6060"/>
        <location filename="../mainwindow.ui" line="6332"/>
        <location filename="../mainwindow.ui" line="6917"/>
        <location filename="../mainwindow.ui" line="7189"/>
        <location filename="../mainwindow.ui" line="7744"/>
        <location filename="../mainwindow.ui" line="8016"/>
        <location filename="../mainwindow.ui" line="8577"/>
        <location filename="../mainwindow.ui" line="8849"/>
        <location filename="../mainwindow.ui" line="9404"/>
        <location filename="../mainwindow.ui" line="9676"/>
        <source>Realised PnL</source>
        <translation>실현된 이익/손실</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="6071"/>
        <location filename="../mainwindow.ui" line="6343"/>
        <location filename="../mainwindow.ui" line="6928"/>
        <location filename="../mainwindow.ui" line="7200"/>
        <location filename="../mainwindow.ui" line="7755"/>
        <location filename="../mainwindow.ui" line="8027"/>
        <location filename="../mainwindow.ui" line="8588"/>
        <location filename="../mainwindow.ui" line="8860"/>
        <location filename="../mainwindow.ui" line="9415"/>
        <location filename="../mainwindow.ui" line="9687"/>
        <source>Unrealised PnL</source>
        <translation>미실현 이익/손실</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="6104"/>
        <location filename="../mainwindow.ui" line="6376"/>
        <location filename="../mainwindow.ui" line="6961"/>
        <location filename="../mainwindow.ui" line="7233"/>
        <location filename="../mainwindow.ui" line="7788"/>
        <location filename="../mainwindow.ui" line="8060"/>
        <location filename="../mainwindow.ui" line="8621"/>
        <location filename="../mainwindow.ui" line="8893"/>
        <location filename="../mainwindow.ui" line="9448"/>
        <location filename="../mainwindow.ui" line="9720"/>
        <source>Buys</source>
        <translation>구매</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="6115"/>
        <location filename="../mainwindow.ui" line="6387"/>
        <location filename="../mainwindow.ui" line="6972"/>
        <location filename="../mainwindow.ui" line="7244"/>
        <location filename="../mainwindow.ui" line="7799"/>
        <location filename="../mainwindow.ui" line="8071"/>
        <location filename="../mainwindow.ui" line="8632"/>
        <location filename="../mainwindow.ui" line="8904"/>
        <location filename="../mainwindow.ui" line="9459"/>
        <location filename="../mainwindow.ui" line="9731"/>
        <source>Sells</source>
        <translation>판매</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="6126"/>
        <location filename="../mainwindow.ui" line="6398"/>
        <location filename="../mainwindow.ui" line="6983"/>
        <location filename="../mainwindow.ui" line="7255"/>
        <location filename="../mainwindow.ui" line="7810"/>
        <location filename="../mainwindow.ui" line="8082"/>
        <location filename="../mainwindow.ui" line="8643"/>
        <location filename="../mainwindow.ui" line="8915"/>
        <location filename="../mainwindow.ui" line="9470"/>
        <location filename="../mainwindow.ui" line="9742"/>
        <source>Net</source>
        <translation>그물</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="6137"/>
        <location filename="../mainwindow.ui" line="6409"/>
        <location filename="../mainwindow.ui" line="6994"/>
        <location filename="../mainwindow.ui" line="7266"/>
        <location filename="../mainwindow.ui" line="7821"/>
        <location filename="../mainwindow.ui" line="8093"/>
        <location filename="../mainwindow.ui" line="8654"/>
        <location filename="../mainwindow.ui" line="8926"/>
        <location filename="../mainwindow.ui" line="9481"/>
        <location filename="../mainwindow.ui" line="9753"/>
        <source>Net Total</source>
        <translation>순 총</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="6148"/>
        <location filename="../mainwindow.ui" line="6420"/>
        <location filename="../mainwindow.ui" line="7005"/>
        <location filename="../mainwindow.ui" line="7277"/>
        <location filename="../mainwindow.ui" line="7832"/>
        <location filename="../mainwindow.ui" line="8104"/>
        <location filename="../mainwindow.ui" line="8665"/>
        <location filename="../mainwindow.ui" line="8937"/>
        <location filename="../mainwindow.ui" line="9492"/>
        <location filename="../mainwindow.ui" line="9764"/>
        <source>N.I.C</source>
        <translation>넷 포함 수수료</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="6234"/>
        <location filename="../mainwindow.ui" line="7091"/>
        <location filename="../mainwindow.ui" line="7918"/>
        <location filename="../mainwindow.ui" line="8751"/>
        <location filename="../mainwindow.ui" line="9578"/>
        <source>Closed Positions</source>
        <translation>닫힌 위치</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="6451"/>
        <source>BTC Portfolio</source>
        <translation>BTC 포트폴리오</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="6535"/>
        <source>Unrealised PnL (₿)</source>
        <translation>미실현 이익/손실 (₿)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="6686"/>
        <source>Equity (₿)</source>
        <translation>주식 (₿)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="7308"/>
        <source>ETH Portfolio</source>
        <translation>ETH 포트폴리오</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="7386"/>
        <source>Unrealised PnL (Ξ)</source>
        <translation>미실현 이익/손실(Ξ)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="7519"/>
        <source>Equity (Ξ)</source>
        <translation>주식 (Ξ)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="8135"/>
        <source>USDT Portfolio</source>
        <translation>USDT 포트폴리오</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="8213"/>
        <source>Unrealised PnL (₮)</source>
        <translation>미실현 이익/손실(₮)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="8352"/>
        <source>Equity (₮)</source>
        <translation>주식 (₮)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="8962"/>
        <source>EUR Portfolio</source>
        <translation>EUR 포트폴리오</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="9040"/>
        <source>Unrealised PnL (€)</source>
        <translation>미실현 이익/손실 (€)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="9179"/>
        <source>Equity (€)</source>
        <translation>주식 (€)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="9857"/>
        <source>Light Mode</source>
        <translation>가벼운 형태</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="9872"/>
        <source>Dark Mode</source>
        <translation>어두운 형태</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="9887"/>
        <source>Logger</source>
        <translation>로거</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="9902"/>
        <location filename="../mainwindow.ui" line="9905"/>
        <source>Resume Execution</source>
        <translation>실행 재개</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="9920"/>
        <source>Pause Execution</source>
        <translation>실행 일시 정지</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="9935"/>
        <source>Stop Execution</source>
        <translation>실행 중지</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="9948"/>
        <source>System Tray</source>
        <translation>시스템 트레이</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="9951"/>
        <source>Minimize to system tray</source>
        <translation>시스템 트레이로 최소화</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="9960"/>
        <source>Language</source>
        <translation>언어</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="9963"/>
        <source>Language Translator</source>
        <translation>언어 번역기</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1407"/>
        <source>The current trading session is still running!</source>
        <translation>거래 세션이 아직 진행 중입니다!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1408"/>
        <source>Are you sure you want to stop execution?</source>
        <translation>실행을 중단 하시겠습니까?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1409"/>
        <source>Stopping the current session while executing </source>
        <translation>실행 중 현재 세션 중지</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1410"/>
        <source>can leave you exposed with open positions. It is highly </source>
        <translation>열린 위치에 노출 될 수 있습니다. 그래서</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1411"/>
        <source> advised to wait until the session completes.</source>
        <translation>세션이 완료 될 때까지 기다리십시오.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1893"/>
        <source>BittrexBot terminated while trading session was still active!</source>
        <translation>거래 세션이 활성 상태인 동안 BittrexBot 이 종료되었습니다!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1894"/>
        <source>Would you like to restore the trading session?</source>
        <translation>당신은 거래 세션을 복원 하시겠습니까?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1895"/>
        <source>The BittrexBot program either crashed or was terminated by the user before the trading session had completed.</source>
        <translation>Bittrexbot 프로그램이 충돌하거나 거래 세션이 완료되기 전에 사용자에 의해 종료되었습니다.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1896"/>
        <source> It is highly reccommended to restore your previous active trading session.</source>
        <translation> 그것은 매우 이전 활성 거래 세션을 복원하는 것이 좋습니다.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1897"/>
        <source> If you do not restore, you will be left with exposed positions.</source>
        <translation>복원하지 않는 경우,당신은 노출 된 위치로 남아있을 것입니다.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3255"/>
        <source>An error was discovered regarding strategy selection!</source>
        <translation>전략 선택에 관한 오류가 발견되었습니다!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3256"/>
        <source>No strategy has been selected!</source>
        <translation>어떤 전략이 선택되지 않았습니다!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3257"/>
        <source>You must select at least one trading strategy, </source>
        <translation>당신은 적어도 하나 개의 거래 전략을 선택해야합니다,</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3258"/>
        <source>In order to do so navigate to the trader page and select a strategy from the strategies list.</source>
        <translation>그래서 상인 페이지로 이동하여 전략 목록에서 전략을 선택 할 수 있습니다.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3268"/>
        <source>An error was discovered regarding risk selection!</source>
        <translation>위험 선택에 관한 오류가 발견되었습니다!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3269"/>
        <source>No stop-loss or take-profit risk was selected!</source>
        <translation>정지 손실 또는 테이크 이익 위험이 선택되지 않았습니다!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3270"/>
        <source>You must select at least one take-profit risk </source>
        <translation>당신은 적어도 하나의 테이크 이익 위험을 선택해야합니다</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3271"/>
        <source>and at least one stop-loss risk, in order to do so </source>
        <translation>그리고 적어도 하나의 정지 손실 위험,그렇게하기 위해</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3272"/>
        <source>navigate to the trader page and select the risks from the risks list.</source>
        <translation>상인 페이지로 이동 및 위험 목록에서 위험을 선택합니다.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3311"/>
        <location filename="../mainwindow.cpp" line="3333"/>
        <location filename="../mainwindow.cpp" line="3355"/>
        <location filename="../mainwindow.cpp" line="3377"/>
        <location filename="../mainwindow.cpp" line="3399"/>
        <location filename="../mainwindow.cpp" line="3415"/>
        <location filename="../mainwindow.cpp" line="3426"/>
        <location filename="../mainwindow.cpp" line="3437"/>
        <location filename="../mainwindow.cpp" line="3448"/>
        <location filename="../mainwindow.cpp" line="3459"/>
        <source>An inaccuracy was discovered regarding capital allocation!</source>
        <translation>부정확은 자본 배분에 대하여 발견되었다!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3312"/>
        <location filename="../mainwindow.cpp" line="3334"/>
        <location filename="../mainwindow.cpp" line="3356"/>
        <location filename="../mainwindow.cpp" line="3378"/>
        <location filename="../mainwindow.cpp" line="3400"/>
        <source>There was a capital allocation error for market </source>
        <translation>시장에 대한 자본 배분 오류가 발생했습니다</translation>
    </message>
    <message>
        <source>The allocated capital for market </source>
        <translation type="vanished">시장에 할당 된 자본</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3313"/>
        <location filename="../mainwindow.cpp" line="3335"/>
        <location filename="../mainwindow.cpp" line="3357"/>
        <location filename="../mainwindow.cpp" line="3379"/>
        <location filename="../mainwindow.cpp" line="3401"/>
        <source> </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3313"/>
        <location filename="../mainwindow.cpp" line="3335"/>
        <location filename="../mainwindow.cpp" line="3357"/>
        <location filename="../mainwindow.cpp" line="3379"/>
        <location filename="../mainwindow.cpp" line="3401"/>
        <source>The allocated quantity for market </source>
        <translation>시장을 위한 할당된 양</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3314"/>
        <location filename="../mainwindow.cpp" line="3336"/>
        <location filename="../mainwindow.cpp" line="3358"/>
        <location filename="../mainwindow.cpp" line="3380"/>
        <location filename="../mainwindow.cpp" line="3402"/>
        <source>was </source>
        <translation>했다</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3314"/>
        <location filename="../mainwindow.cpp" line="3336"/>
        <location filename="../mainwindow.cpp" line="3358"/>
        <location filename="../mainwindow.cpp" line="3380"/>
        <location filename="../mainwindow.cpp" line="3402"/>
        <source> while </source>
        <translation>동안</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3315"/>
        <location filename="../mainwindow.cpp" line="3337"/>
        <location filename="../mainwindow.cpp" line="3359"/>
        <location filename="../mainwindow.cpp" line="3381"/>
        <location filename="../mainwindow.cpp" line="3403"/>
        <source>the minimum trading size for the market is </source>
        <translation>시장에 대한 최소 거래 크기는</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3316"/>
        <location filename="../mainwindow.cpp" line="3338"/>
        <location filename="../mainwindow.cpp" line="3360"/>
        <location filename="../mainwindow.cpp" line="3382"/>
        <location filename="../mainwindow.cpp" line="3404"/>
        <source>, in order to deal with this issue either increase the allocated capital percentage or </source>
        <translation>,이 문제를 처리하기 위해 할당 된 자본 비율을 증가 시키거나</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3317"/>
        <location filename="../mainwindow.cpp" line="3339"/>
        <location filename="../mainwindow.cpp" line="3361"/>
        <location filename="../mainwindow.cpp" line="3383"/>
        <location filename="../mainwindow.cpp" line="3405"/>
        <source>if there is insufficient capital navigate back to the markets page and deselect market </source>
        <translation>불충분 한 자본이있는 경우 시장 페이지로 돌아가서 시장 선택을 취소하십시오</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3318"/>
        <source> from the USD markets table.</source>
        <translation>USD 시장 테이블에서.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3340"/>
        <source> from the BTC markets table.</source>
        <translation>BTC 시장 테이블에서.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3362"/>
        <source> from the ETH markets table.</source>
        <translation>ETH 시장 테이블에서.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3384"/>
        <source> from the USDT markets table.</source>
        <translation>USDT 시장 테이블에서.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3406"/>
        <source> from the EUR markets table.</source>
        <translation>EUR 시장 테이블에서.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3416"/>
        <source>There was a capital allocation error for USD markets!</source>
        <translation>USD 시장에 대한 자본 배분 오류가 있었다!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3417"/>
        <location filename="../mainwindow.cpp" line="3428"/>
        <location filename="../mainwindow.cpp" line="3439"/>
        <location filename="../mainwindow.cpp" line="3450"/>
        <location filename="../mainwindow.cpp" line="3461"/>
        <source>The sum of the allocated percentages exceeds the 100% upper bound limit, </source>
        <translation>할당 된 백분율의 합이 100%상한 제한을 초과합니다,</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3418"/>
        <location filename="../mainwindow.cpp" line="3429"/>
        <location filename="../mainwindow.cpp" line="3440"/>
        <location filename="../mainwindow.cpp" line="3451"/>
        <location filename="../mainwindow.cpp" line="3462"/>
        <source>in order to deal with this issue either decrease the allocated percentages or </source>
        <translation>이 문제를 처리하기 위해 할당 된 비율을 줄이거 나</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3419"/>
        <source> navigate back to the markets page and deselect a market from the USD markets table.</source>
        <translation>시장 페이지로 돌아가서 USD 시장 테이블에서 시장을 선택 취소하십시오.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3427"/>
        <source>There was a capital allocation error for BTC markets!</source>
        <translation>BTC 시장에 대한 자본 배분 오류가 있었다!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3430"/>
        <source> navigate back to the markets page and deselect a market from the BTC markets table.</source>
        <translation>시장 페이지로 돌아가서 BTC 시장 테이블에서 시장을 선택 취소하십시오.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3438"/>
        <source>There was a capital allocation error for ETH markets!</source>
        <translation>ETH 시장에 대한 자본 배분 오류가 있었다!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3441"/>
        <source> navigate back to the markets page and deselect a market from the ETH markets table.</source>
        <translation>시장 페이지로 돌아가서 ETH 시장 테이블에서 시장을 선택 취소하십시오.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3449"/>
        <source>There was a capital allocation error for USDT markets!</source>
        <translation>USDT 시장에 대한 자본 배분 오류가 있었다!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3452"/>
        <source> navigate back to the markets page and deselect a market from the USDT markets table.</source>
        <translation>시장 페이지로 돌아가서 USDT 시장 테이블에서 시장을 선택 취소합니다.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3460"/>
        <source>There was a capital allocation error for EUR markets!</source>
        <translation>EUR 시장에 대한 자본 배분 오류가 있었다!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3463"/>
        <source> navigate back to the markets page and deselect a market from the EUR markets table.</source>
        <translation>시장 페이지로 돌아가서 EUR 시장 테이블에서 시장을 선택 취소하십시오.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3468"/>
        <source>Sanity check completed, everything looks good!</source>
        <translation>정신 검사 완료,모든 것이 좋아 보인다!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3469"/>
        <source>Click execute button to start the session!</source>
        <translation>세션을 시작하려면 실행 버튼을 클릭!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="4060"/>
        <location filename="../mainwindow.cpp" line="4066"/>
        <source>Allocated capital for EUR-Portfolio</source>
        <translation>EUR 포트폴리오 할당 자본</translation>
    </message>
    <message>
        <source>Everything looks good!</source>
        <translation type="vanished">모든 것이 좋아 보인다!</translation>
    </message>
    <message>
        <source>Click execute button!</source>
        <translation type="vanished">실행 버튼을 클릭하십시오!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3470"/>
        <source>BittrexBot performed a thorough sanity check </source>
        <translation>BittrexBot 은 철저한 정신 검사를 수행했습니다</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3471"/>
        <source>on the trading configurations and did not encounter any errors.</source>
        <translation>는 구성 거래 및 오류가 발생하지 않았다.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3979"/>
        <location filename="../mainwindow.cpp" line="3984"/>
        <location filename="../mainwindow.cpp" line="3989"/>
        <location filename="../mainwindow.cpp" line="3994"/>
        <location filename="../mainwindow.cpp" line="3999"/>
        <source>reserved</source>
        <translation>예약됨</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="4016"/>
        <location filename="../mainwindow.cpp" line="4022"/>
        <source>Allocated capital for USD-Portfolio</source>
        <translation>USD 포트폴리오 할당 자본</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="4027"/>
        <location filename="../mainwindow.cpp" line="4033"/>
        <source>Allocated capital for BTC-Portfolio</source>
        <translation>BTC 포트폴리오 할당 자본</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="4038"/>
        <location filename="../mainwindow.cpp" line="4044"/>
        <source>Allocated capital for ETH-Portfolio</source>
        <translation>ETH 포트폴리오 할당 자본</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="4049"/>
        <location filename="../mainwindow.cpp" line="4055"/>
        <source>Allocated capital for USDT-Portfolio</source>
        <translation>USDT 포트폴리오 할당 자본</translation>
    </message>
</context>
<context>
    <name>MarketListItem</name>
    <message>
        <location filename="../marketlistitem.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>RiskTree</name>
    <message>
        <location filename="../risktree.cpp" line="23"/>
        <location filename="../risktree.cpp" line="38"/>
        <source>Stop Loss</source>
        <translation>정지 손실</translation>
    </message>
    <message>
        <location filename="../risktree.cpp" line="24"/>
        <location filename="../risktree.cpp" line="38"/>
        <source>Take Profit</source>
        <translation>이익을</translation>
    </message>
</context>
<context>
    <name>StopLossRiskFactory</name>
    <message>
        <location filename="../stoplossriskfactory.cpp" line="85"/>
        <source>Stop-loss at 4.0%</source>
        <translation>4.0% 에서 정지 손실</translation>
    </message>
    <message>
        <location filename="../stoplossriskfactory.cpp" line="89"/>
        <source>Stop-loss at 5.0%</source>
        <translation>5.0% 에서 정지 손실</translation>
    </message>
    <message>
        <location filename="../stoplossriskfactory.cpp" line="93"/>
        <source>Stop-loss at 6.0%</source>
        <translation>6.0% 에서 정지 손실</translation>
    </message>
    <message>
        <location filename="../stoplossriskfactory.cpp" line="97"/>
        <source>Stop-loss at 7.0%</source>
        <translation>7.0% 에서 정지 손실</translation>
    </message>
    <message>
        <location filename="../stoplossriskfactory.cpp" line="101"/>
        <source>Stop-loss at 8.0%</source>
        <translation>8.0% 에서 정지 손실</translation>
    </message>
    <message>
        <location filename="../stoplossriskfactory.cpp" line="105"/>
        <source>Stop-loss at 9.0%</source>
        <translation>9.0% 에서 정지 손실</translation>
    </message>
</context>
<context>
    <name>StrategyTree</name>
    <message>
        <location filename="../strategytree.cpp" line="23"/>
        <source>Time Series</source>
        <translation>시계 시리즈</translation>
    </message>
    <message>
        <location filename="../strategytree.cpp" line="24"/>
        <source>Neural Networks</source>
        <translation>신경 네트워크</translation>
    </message>
    <message>
        <location filename="../strategytree.cpp" line="25"/>
        <source>Order Book Properties</source>
        <translation>예약 속성 주문</translation>
    </message>
    <message>
        <location filename="../strategytree.cpp" line="26"/>
        <source>Markov Models</source>
        <translation>마르코프 모델</translation>
    </message>
    <message>
        <location filename="../strategytree.cpp" line="27"/>
        <location filename="../strategytree.cpp" line="41"/>
        <source>Filter Rule</source>
        <translation>필터 규칙</translation>
    </message>
</context>
<context>
    <name>SystemTray</name>
    <message>
        <location filename="../systemtray.cpp" line="10"/>
        <source>Open</source>
        <translation>열기</translation>
    </message>
    <message>
        <location filename="../systemtray.cpp" line="12"/>
        <source>Quit</source>
        <translation>종료</translation>
    </message>
</context>
<context>
    <name>TakeProfitRiskFactory</name>
    <message>
        <location filename="../takeprofitriskfactory.cpp" line="172"/>
        <source>Take-profit at 0.03%</source>
        <translation>0.03% 에서 테이크 이익</translation>
    </message>
    <message>
        <location filename="../takeprofitriskfactory.cpp" line="176"/>
        <source>Take-profit at 0.04%</source>
        <translation>0.04% 에서 테이크 이익</translation>
    </message>
    <message>
        <location filename="../takeprofitriskfactory.cpp" line="180"/>
        <source>Take-profit at 0.05%</source>
        <translation>0.05% 에서 테이크 이익</translation>
    </message>
    <message>
        <location filename="../takeprofitriskfactory.cpp" line="184"/>
        <source>Take-profit at 0.06%</source>
        <translation>0.06% 에서 테이크 이익</translation>
    </message>
    <message>
        <location filename="../takeprofitriskfactory.cpp" line="188"/>
        <source>Take-profit at 0.07%</source>
        <translation>0.07% 에서 테이크 이익</translation>
    </message>
    <message>
        <location filename="../takeprofitriskfactory.cpp" line="192"/>
        <source>Take-profit at 0.08%</source>
        <translation>0.08% 에서 테이크 이익</translation>
    </message>
    <message>
        <location filename="../takeprofitriskfactory.cpp" line="196"/>
        <source>Take-profit at 0.09%</source>
        <translation>0.09% 에서 테이크 이익</translation>
    </message>
    <message>
        <location filename="../takeprofitriskfactory.cpp" line="200"/>
        <source>Take-profit at 0.1%</source>
        <translation>0.1% 에서 테이크 이익</translation>
    </message>
    <message>
        <location filename="../takeprofitriskfactory.cpp" line="204"/>
        <source>Take-profit at 0.2%</source>
        <translation>0.2% 에서 테이크 이익</translation>
    </message>
    <message>
        <location filename="../takeprofitriskfactory.cpp" line="208"/>
        <source>Take-profit at 0.3%</source>
        <translation>0.3% 에서 테이크 이익</translation>
    </message>
    <message>
        <location filename="../takeprofitriskfactory.cpp" line="212"/>
        <source>Take-profit at 0.4%</source>
        <translation>0.4% 에서 테이크 이익</translation>
    </message>
    <message>
        <location filename="../takeprofitriskfactory.cpp" line="216"/>
        <source>Take-profit at 0.5%</source>
        <translation>0.5% 에서 테이크 이익</translation>
    </message>
    <message>
        <location filename="../takeprofitriskfactory.cpp" line="220"/>
        <source>Take-profit at 0.6%</source>
        <translation>0.6% 에서 테이크 이익</translation>
    </message>
    <message>
        <location filename="../takeprofitriskfactory.cpp" line="224"/>
        <source>Take-profit at 0.7%</source>
        <translation>0.7% 에서 테이크 이익</translation>
    </message>
    <message>
        <location filename="../takeprofitriskfactory.cpp" line="228"/>
        <source>Take-profit at 0.8%</source>
        <translation>0.8% 에서 테이크 이익</translation>
    </message>
    <message>
        <location filename="../takeprofitriskfactory.cpp" line="232"/>
        <source>Take-profit at 0.9%</source>
        <translation>0.9% 에서 테이크 이익</translation>
    </message>
    <message>
        <source>Take-profit at 1.0%</source>
        <translation type="vanished">0.1% 에서 테이크 이익</translation>
    </message>
</context>
</TS>
