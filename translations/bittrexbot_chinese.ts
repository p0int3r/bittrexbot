<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN" sourcelanguage="en_US">
<context>
    <name>DialogManager</name>
    <message>
        <location filename="../dialogmanager.cpp" line="44"/>
        <source>Restoration Dialog</source>
        <translation>恢复对话框</translation>
    </message>
    <message>
        <location filename="../dialogmanager.cpp" line="46"/>
        <source>Error Dialog</source>
        <translation>错误对话框</translation>
    </message>
    <message>
        <location filename="../dialogmanager.cpp" line="47"/>
        <source>Confirmation Dialog</source>
        <translation>确认对话框</translation>
    </message>
</context>
<context>
    <name>FilterRuleStrategyFactory</name>
    <message>
        <location filename="../filterrulestrategyfactory.cpp" line="200"/>
        <source>Trigger-buy at 0.01%</source>
        <translation>触发买入价为0.01％</translation>
    </message>
    <message>
        <source>Triggers a limit buy order at 0.01% momentum percentage increase</source>
        <translation type="vanished">在动量百分比增加0.01％时触发限价买单</translation>
    </message>
    <message>
        <location filename="../filterrulestrategyfactory.cpp" line="204"/>
        <source>Trigger-buy at 0.02%</source>
        <translation>触发买入0.02％</translation>
    </message>
    <message>
        <source>Triggers a limit buy order at 0.02% momentum percentage increase</source>
        <translation type="vanished">在动量百分比增加0.02％时触发限价买单</translation>
    </message>
    <message>
        <location filename="../filterrulestrategyfactory.cpp" line="208"/>
        <source>Trigger-buy at 0.03%</source>
        <translation>触发买入价为0.03％</translation>
    </message>
    <message>
        <source>Triggers a limit buy order at 0.03% momentum percentage increase</source>
        <translation type="vanished">在动量百分比增加0.03％时触发限价买单</translation>
    </message>
    <message>
        <location filename="../filterrulestrategyfactory.cpp" line="212"/>
        <source>Trigger-buy at 0.04%</source>
        <translation>触发买入价为0.04％</translation>
    </message>
    <message>
        <source>Triggers a limit buy order at 0.04% momentum percentage increase</source>
        <translation type="vanished">在动量百分比增加0.04％时触发限价买单</translation>
    </message>
    <message>
        <location filename="../filterrulestrategyfactory.cpp" line="216"/>
        <source>Trigger-buy at 0.05%</source>
        <translation>触发买入0.05％</translation>
    </message>
    <message>
        <source>Triggers a limit buy order at 0.05% momentum percentage increase</source>
        <translation type="vanished">在动量百分比增加0.05％时触发限价买单</translation>
    </message>
    <message>
        <location filename="../filterrulestrategyfactory.cpp" line="220"/>
        <source>Trigger-buy at 0.06%</source>
        <translation>触发买入价为0.06％</translation>
    </message>
    <message>
        <source>Triggers a limit buy order at 0.06% momentum percentage increase</source>
        <translation type="vanished">以0.06％的动量百分比增加触发限价买单</translation>
    </message>
    <message>
        <location filename="../filterrulestrategyfactory.cpp" line="224"/>
        <source>Trigger-buy at 0.07%</source>
        <translation>触发买入价为0.07％</translation>
    </message>
    <message>
        <source>Triggers a limit buy order at 0.07% momentum percentage increase</source>
        <translation type="vanished">在动量百分比增加0.07％时触发限价买单</translation>
    </message>
    <message>
        <location filename="../filterrulestrategyfactory.cpp" line="228"/>
        <source>Trigger-buy at 0.08%</source>
        <translation>触发买入价为0.08％</translation>
    </message>
    <message>
        <source>Triggers a limit buy order at 0.08% momentum percentage increase</source>
        <translation type="vanished">在动量百分比增加0.08％时触发限价买单</translation>
    </message>
    <message>
        <location filename="../filterrulestrategyfactory.cpp" line="232"/>
        <source>Trigger-buy at 0.09%</source>
        <translation>触发买入价为0.09％</translation>
    </message>
    <message>
        <location filename="../filterrulestrategyfactory.cpp" line="236"/>
        <source>Trigger-buy at 0.1%</source>
        <translation>触发购买0.1％</translation>
    </message>
    <message>
        <source>Triggers a limit buy order at 0.1% momentum percentage increase</source>
        <translation type="vanished">在动量百分比增加0.1％时触发限价买单</translation>
    </message>
    <message>
        <location filename="../filterrulestrategyfactory.cpp" line="240"/>
        <source>Trigger-buy at 0.2%</source>
        <translation>触发买入价为0.2％</translation>
    </message>
    <message>
        <source>Triggers a limit buy order at 0.2% momentum percentage increase</source>
        <translation type="vanished">在动量百分比增加0.2％时触发限价买单</translation>
    </message>
    <message>
        <location filename="../filterrulestrategyfactory.cpp" line="244"/>
        <source>Trigger-buy at 0.3%</source>
        <translation>触发购买0.3％</translation>
    </message>
    <message>
        <source>Triggers a limit buy order at 0.3% momentum percentage increase</source>
        <translation type="vanished">在动量百分比增加0.3％时触发限价买单</translation>
    </message>
    <message>
        <location filename="../filterrulestrategyfactory.cpp" line="248"/>
        <source>Trigger-buy at 0.4%</source>
        <translation>触发买入0.4％</translation>
    </message>
    <message>
        <source>Triggers a limit buy order at 0.4% momentum percentage increase</source>
        <translation type="vanished">在动量百分比增加0.4％时触发限价买单</translation>
    </message>
    <message>
        <location filename="../filterrulestrategyfactory.cpp" line="252"/>
        <source>Trigger-buy at 0.5%</source>
        <translation>触发买入价为0.5％</translation>
    </message>
    <message>
        <source>Triggers a limit buy order at 0.5% momentum percentage increase</source>
        <translation type="vanished">在动量百分比增加0.5％时触发限价买单</translation>
    </message>
    <message>
        <location filename="../filterrulestrategyfactory.cpp" line="256"/>
        <source>Trigger-buy at 0.6%</source>
        <translation>以0.6％的价格买进</translation>
    </message>
    <message>
        <source>Triggers a limit buy order at 0.6% momentum percentage increase</source>
        <translation type="vanished">在动量百分比增加0.6％时触发限价买单</translation>
    </message>
    <message>
        <location filename="../filterrulestrategyfactory.cpp" line="260"/>
        <source>Trigger-buy at 0.7%</source>
        <translation>触发购买0.7％</translation>
    </message>
    <message>
        <source>Triggers a limit buy order at 0.7% momentum percentage increase</source>
        <translation type="vanished">在动量百分比增加0.6％时触发限价买单 {0.7%?}</translation>
    </message>
    <message>
        <location filename="../filterrulestrategyfactory.cpp" line="264"/>
        <source>Trigger-buy at 0.8%</source>
        <translation>触发买入0.8％</translation>
    </message>
    <message>
        <source>Triggers a limit buy order at 0.8% momentum percentage increase</source>
        <translation type="vanished">在动量百分比增加0.6％时触发限价买单 {0.8%?}</translation>
    </message>
    <message>
        <location filename="../filterrulestrategyfactory.cpp" line="268"/>
        <source>Trigger-buy at 0.9%</source>
        <translation>触发买入0.9％</translation>
    </message>
    <message>
        <source>Triggers a limit buy order at 0.9% momentum percentage increase</source>
        <translation type="vanished">在动量百分比增加0.6％时触发限价买单 {0.9%?}</translation>
    </message>
</context>
<context>
    <name>LanguageSelectorDialog</name>
    <message>
        <location filename="../languageselectordialog.ui" line="17"/>
        <source>Language Selector</source>
        <translation>语言选择器</translation>
    </message>
    <message>
        <location filename="../languageselectordialog.ui" line="27"/>
        <source>Languages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../languageselectordialog.ui" line="126"/>
        <source>Español</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../languageselectordialog.ui" line="191"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../languageselectordialog.ui" line="113"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../languageselectordialog.ui" line="63"/>
        <source>Deutsche</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../languageselectordialog.ui" line="156"/>
        <source>日本語</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../languageselectordialog.ui" line="103"/>
        <source>Basa jawa</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../languageselectordialog.ui" line="83"/>
        <source>한국어</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../languageselectordialog.ui" line="93"/>
        <source>русский</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../languageselectordialog.ui" line="43"/>
        <source>普通话</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../languageselectordialog.ui" line="53"/>
        <source>हिंदी</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../languageselectordialog.ui" line="136"/>
        <source>Italiano</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../languageselectordialog.ui" line="33"/>
        <source>Português</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../languageselectordialog.ui" line="73"/>
        <source>عربى</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../languageselectordialog.ui" line="146"/>
        <source>Melayu</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="29"/>
        <source>BittrexBot</source>
        <translation></translation>
    </message>
    <message>
        <source>Verify and continue ( LIVE TRADING )</source>
        <translation type="vanished">验证并继续（实时交易）</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:36pt; font-style:italic;&quot;&gt;BITTREXBOT&lt;/span&gt;&lt;br/&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;Bleeding edge, fully automated, &lt;br/&gt;A.I. powered trading bot for the Bittrex Exchange.&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Terms and Conditions&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Tutorials and Usage&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Universal Trading Inc.&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Follow as on Facebook&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Follow as on Twitter&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:36pt; font-style:italic;&quot;&gt;BITTREXBOT&lt;/span&gt;&lt;br/&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;出血边缘，全自动，
 &lt;br/&gt;用于Bittrex交易所的人工智能交易机器人。&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;条款和条件&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;教程和用法&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Positronic Technologies
&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;在Facebook上关注&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;在Facebook上关注&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;在Twitter上关注&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="349"/>
        <source>Analytics</source>
        <translation>分析工具</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="378"/>
        <source>Markets</source>
        <translation>市场</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="407"/>
        <source>Trader</source>
        <translation>商人</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="436"/>
        <source>Execution</source>
        <translation>执行</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="465"/>
        <source>Portfolios</source>
        <translation>作品集</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="507"/>
        <source>Settings</source>
        <translation>设定值</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="214"/>
        <source>Enter public key here...</source>
        <translation>在此处输入公钥...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="230"/>
        <source>Enter  private key here...</source>
        <translation>在此处输入私钥...</translation>
    </message>
    <message>
        <source>Continue without Verification</source>
        <translation type="vanished">继续而不验证</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:36pt; font-style:italic;&quot;&gt;BITTREXBOT&lt;/span&gt;&lt;br/&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;Bleeding edge, fully automated, &lt;br/&gt;A.I. powered trading bot for the Bittrex Exchange.&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Terms and Conditions&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Tutorials and Usage&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Positronic Technologies&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Follow as on Facebook&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Follow as on Twitter&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:36pt; font-style:italic;&quot;&gt;BITTREXBOT&lt;/span&gt;&lt;br/&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;出血边缘，全自动，
 &lt;br/&gt;用于Bittrex交易所的人工智能交易机器人。&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;条款和条件&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;教程和用法&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Positronic Technologies&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;在Facebook上关注&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;www.universaltrading.ai&quot;在Facebook上关注&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;在Twitter上关注&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.bittrexbot.io/&quot;&gt;&lt;img src=&quot;:/icons/light_mode/bittrexbot_logo.png&quot; width=&quot;200&quot; height=&quot;200&quot;/&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:36pt; font-weight:600; font-style:italic;&quot;&gt;BittrexBot&lt;/span&gt;&lt;br/&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;Bleeding edge, fully automated, &lt;br/&gt;A.I. powered trading bot for the Bittrex Exchange.&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.bittrexbot.io/terms-and-conditions&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Terms and Conditions&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.bittrexbot.io/usage-and-tutorials/&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Usage and Tutorials&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.facebook.com/bittrexbot/&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Follow as on Facebook&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://twitter.com/BotBittrex&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Follow as on Twitter&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.reddit.com/user/bittrexbot_software&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Join us on Reddit&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.youtube.com/channel/UClCDp4wfPRGRjmUBRXFAGcg?view_as=public&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Subscribe to Youtube&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.bittrexbot.io/blog/&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;News and Blogs&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.bittrexbot.io/&quot;&gt;&lt;img src=&quot;:/icons/light_mode/bittrexbot_logo.png&quot; width=&quot;200&quot; height=&quot;200&quot;/&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:36pt; font-weight:600; font-style:italic;&quot;&gt;BittrexBot&lt;/span&gt;&lt;br/&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;出血缘、完全自动化的,&lt;br/&gt;人工智能交易机器人用于Bittrex交易所。&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.bittrexbot.io/terms-and-conditions&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;条款及细则&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.bittrexbot.io/usage-and-tutorials/&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;用法和教程&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.facebook.com/bittrexbot/&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;在facebook上关注&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://twitter.com/BotBittrex&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;在Twitter上关注&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.reddit.com/user/bittrexbot_software&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;加入我们的Reddit&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.youtube.com/channel/UClCDp4wfPRGRjmUBRXFAGcg?view_as=public&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;订阅Youtube&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.bittrexbot.io/blog/&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;新闻和博客&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="536"/>
        <source>Back </source>
        <translation>背部</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="626"/>
        <location filename="../mainwindow.ui" line="1249"/>
        <location filename="../mainwindow.ui" line="1494"/>
        <location filename="../mainwindow.ui" line="1739"/>
        <location filename="../mainwindow.ui" line="1984"/>
        <location filename="../mainwindow.ui" line="2235"/>
        <location filename="../mainwindow.ui" line="3915"/>
        <location filename="../mainwindow.ui" line="4123"/>
        <location filename="../mainwindow.ui" line="5900"/>
        <location filename="../mainwindow.ui" line="6183"/>
        <location filename="../mainwindow.ui" line="6757"/>
        <location filename="../mainwindow.ui" line="7040"/>
        <location filename="../mainwindow.ui" line="7584"/>
        <location filename="../mainwindow.ui" line="7867"/>
        <location filename="../mainwindow.ui" line="8417"/>
        <location filename="../mainwindow.ui" line="8700"/>
        <location filename="../mainwindow.ui" line="9244"/>
        <location filename="../mainwindow.ui" line="9527"/>
        <source>Find...</source>
        <translation>找</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="658"/>
        <source>All Markets</source>
        <translation>所有市场</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="663"/>
        <location filename="../mainwindow.ui" line="1300"/>
        <source>USD Markets</source>
        <translation>USD市场</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="668"/>
        <location filename="../mainwindow.ui" line="1545"/>
        <source>BTC Markets</source>
        <translation>BTC市场</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="673"/>
        <location filename="../mainwindow.ui" line="1790"/>
        <source>ETH Markets</source>
        <translation>ETH市场</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="678"/>
        <location filename="../mainwindow.ui" line="2035"/>
        <source>USDT Markets</source>
        <translation>USDT市场</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="716"/>
        <source>↑ Volume</source>
        <translation>↑音量</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="721"/>
        <source>↓ Volume</source>
        <translation>↓音量</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="726"/>
        <source>↑ Change</source>
        <translation>↑变化</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="731"/>
        <source>↓ Change</source>
        <translation>↓变更</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="736"/>
        <source>↑ Last Price</source>
        <translation>↑最后价格</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="741"/>
        <source>↓ Last Price</source>
        <translation>↓最后价格</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="799"/>
        <source>Summary</source>
        <translation>摘要</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1005"/>
        <location filename="../mainwindow.ui" line="1392"/>
        <location filename="../mainwindow.ui" line="1637"/>
        <location filename="../mainwindow.ui" line="1882"/>
        <location filename="../mainwindow.ui" line="2133"/>
        <location filename="../mainwindow.ui" line="2378"/>
        <source>Last Price</source>
        <translation>最后价格</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1052"/>
        <source>Bid Price</source>
        <translation>竞价</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1093"/>
        <source>Ask Price</source>
        <translation>询问价格</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1134"/>
        <source>Mid Price</source>
        <translation>中间价</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="826"/>
        <source>Base Volume</source>
        <translation>基本卷</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="176"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.bittrexbot.io/&quot;&gt;&lt;img src=&quot;:/icons/light_mode/bittrexbot_logo.png&quot; width=&quot;200&quot; height=&quot;200&quot;/&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:36pt; font-weight:600; font-style:italic;&quot;&gt;BittrexBot&lt;/span&gt;&lt;br/&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;Bleeding edge, fully automated, &lt;br/&gt;A.I. powered trading bot for the Bittrex Exchange.&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.bittrexbot.io/terms-and-conditions&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Terms and Conditions&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.bittrexbot.io/usage-and-tutorials/&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Usage and Tutorials&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.facebook.com/bittrexbot/&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Follow as on Facebook&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://twitter.com/BotBittrex&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Follow as on Twitter&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.reddit.com/user/bittrexbot_software&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Join us on Reddit&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.youtube.com/channel/UCLwcXW6CLUpGFcj1UnBGHMg?view_as=subscriber&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Subscribe to Youtube&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://t.me/BittrexBotCommunityChat&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Join us on Telegram&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.bittrexbot.io/blog/&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;News and Blogs&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.bittrexbot.io/&quot;&gt;&lt;img src=&quot;:/icons/light_mode/bittrexbot_logo.png&quot; width=&quot;200&quot; height=&quot;200&quot;/&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:36pt; font-weight:600; font-style:italic;&quot;&gt;BittrexBot&lt;/span&gt;&lt;br/&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;出血缘、完全自动化的,&lt;br/&gt;A.i. 用于Bittrex交易的交易机器人。&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.bittrexbot.io/terms-and-conditions&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;条款及细则&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.bittrexbot.io/usage-and-tutorials/&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;用法和教程&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.facebook.com/bittrexbot/&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;Facebook上的关注&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://twitter.com/BotBittrex&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;按照作为在Twitter上&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.reddit.com/user/bittrexbot_software&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;加入我们的Reddit&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.youtube.com/channel/UCLwcXW6CLUpGFcj1UnBGHMg?view_as=subscriber&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;订阅Youtube&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://t.me/BittrexBotCommunityChat&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;加入我们的电报&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.bittrexbot.io/blog/&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600; text-decoration: underline; color:#007af4;&quot;&gt;新闻和博客&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="249"/>
        <source>Live Trading</source>
        <translation>实时交易</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="268"/>
        <source>Paper Trading</source>
        <translation>纸张交易</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="281"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;https://bittrex.com/Account/Register?referralCode=JRM-DEC-0IH&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#007af4;&quot;&gt;Create Bittrex Account&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;https://bittrex.com/Account/Register?referralCode=JRM-DEC-0IH&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#007af4;&quot;&gt;创建一个Bittrex帐户&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="300"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;https://bittrex.zendesk.com/hc/en-us/articles/360031921872-How-to-create-an-API-key-&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#007af4;&quot;&gt;How to create an API key?&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;https://bittrex.zendesk.com/hc/en-us/articles/360031921872-How-to-create-an-API-key-&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#007af4;&quot;&gt;如何创建API密钥？&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="683"/>
        <location filename="../mainwindow.ui" line="2286"/>
        <source>EUR Markets</source>
        <translation>EUR市场</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="867"/>
        <location filename="../mainwindow.ui" line="1370"/>
        <location filename="../mainwindow.ui" line="1615"/>
        <location filename="../mainwindow.ui" line="1860"/>
        <location filename="../mainwindow.ui" line="2111"/>
        <location filename="../mainwindow.ui" line="2356"/>
        <source>Volume</source>
        <translation>体积</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="908"/>
        <source>24H High</source>
        <translation>24小时最高</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="949"/>
        <source>24H Low</source>
        <translation>24小时最低</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1220"/>
        <location filename="../mainwindow.ui" line="1465"/>
        <location filename="../mainwindow.ui" line="1710"/>
        <location filename="../mainwindow.ui" line="1955"/>
        <location filename="../mainwindow.ui" line="2206"/>
        <source>Select All</source>
        <translation>全选</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1233"/>
        <location filename="../mainwindow.ui" line="1478"/>
        <location filename="../mainwindow.ui" line="1723"/>
        <location filename="../mainwindow.ui" line="1968"/>
        <location filename="../mainwindow.ui" line="2219"/>
        <source>Deselect All</source>
        <translation>全部取消选择</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1337"/>
        <location filename="../mainwindow.ui" line="1582"/>
        <location filename="../mainwindow.ui" line="1827"/>
        <location filename="../mainwindow.ui" line="2078"/>
        <location filename="../mainwindow.ui" line="2323"/>
        <source>Select</source>
        <translation>选择</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1348"/>
        <location filename="../mainwindow.ui" line="1593"/>
        <location filename="../mainwindow.ui" line="1838"/>
        <location filename="../mainwindow.ui" line="2089"/>
        <location filename="../mainwindow.ui" line="2334"/>
        <location filename="../mainwindow.ui" line="4025"/>
        <location filename="../mainwindow.ui" line="4233"/>
        <location filename="../mainwindow.ui" line="6016"/>
        <location filename="../mainwindow.ui" line="6288"/>
        <location filename="../mainwindow.ui" line="6873"/>
        <location filename="../mainwindow.ui" line="7145"/>
        <location filename="../mainwindow.ui" line="7700"/>
        <location filename="../mainwindow.ui" line="7972"/>
        <location filename="../mainwindow.ui" line="8533"/>
        <location filename="../mainwindow.ui" line="8805"/>
        <location filename="../mainwindow.ui" line="9360"/>
        <location filename="../mainwindow.ui" line="9632"/>
        <source>Market</source>
        <translation>市场</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1359"/>
        <location filename="../mainwindow.ui" line="1604"/>
        <location filename="../mainwindow.ui" line="1849"/>
        <location filename="../mainwindow.ui" line="2100"/>
        <location filename="../mainwindow.ui" line="2345"/>
        <source>Currency</source>
        <translation>货币</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1381"/>
        <location filename="../mainwindow.ui" line="1626"/>
        <location filename="../mainwindow.ui" line="1871"/>
        <location filename="../mainwindow.ui" line="2122"/>
        <location filename="../mainwindow.ui" line="2367"/>
        <source>Change %</source>
        <translation>变化％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1403"/>
        <location filename="../mainwindow.ui" line="1648"/>
        <location filename="../mainwindow.ui" line="1893"/>
        <location filename="../mainwindow.ui" line="2144"/>
        <location filename="../mainwindow.ui" line="2389"/>
        <source>24HR High</source>
        <translation>24小时最高</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1414"/>
        <location filename="../mainwindow.ui" line="1659"/>
        <location filename="../mainwindow.ui" line="1904"/>
        <location filename="../mainwindow.ui" line="2155"/>
        <location filename="../mainwindow.ui" line="2400"/>
        <source>24HR Low</source>
        <translation>24小时最低</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1425"/>
        <location filename="../mainwindow.ui" line="1670"/>
        <location filename="../mainwindow.ui" line="1915"/>
        <location filename="../mainwindow.ui" line="2166"/>
        <location filename="../mainwindow.ui" line="2411"/>
        <source>Spread %</source>
        <translation>传播％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1436"/>
        <location filename="../mainwindow.ui" line="1681"/>
        <location filename="../mainwindow.ui" line="1926"/>
        <location filename="../mainwindow.ui" line="2177"/>
        <location filename="../mainwindow.ui" line="2422"/>
        <source>Added</source>
        <translation>添加</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2471"/>
        <source>Strategy</source>
        <translation>战略</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2487"/>
        <location filename="../mainwindow.ui" line="2744"/>
        <source>Description</source>
        <translation>描述</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2503"/>
        <location filename="../mainwindow.cpp" line="2507"/>
        <location filename="../mainwindow.cpp" line="2551"/>
        <source>Filter Rule</source>
        <translation>筛选规则</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2510"/>
        <source>A filter rule is a trading strategy based on pre-determined price changes, typically quantified as a percentage</source>
        <translation>过滤规则是一种基于预定价格变化的交易策略，通常以百分比表示</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2514"/>
        <source>Trigger-buy at 0.01%</source>
        <translation>触发买入价为0.01％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2520"/>
        <source>Triggers a limit buy order at 0.01% momentum percentage increase</source>
        <translation>在动量百分比增加0.01％时触发限价买单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2525"/>
        <source>Trigger-buy at 0.02%</source>
        <translation>触发买入0.02％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2531"/>
        <source>Triggers a limit buy order at 0.02% momentum percentage increase</source>
        <translation>在动量百分比增加0.02％时触发限价买单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2536"/>
        <source>Trigger-buy at 0.03%</source>
        <translation>触发买入价为0.03％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2542"/>
        <source>Triggers a limit buy order at 0.03% momentum percentage increase</source>
        <translation>在动量百分比增加0.03％时触发限价买单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2547"/>
        <source>Trigger-buy at 0.04%</source>
        <translation>触发买入价为0.04％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2553"/>
        <source>Triggers a limit buy order at 0.04% momentum percentage increase</source>
        <translation>在动量百分比增加0.04％时触发限价买单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2558"/>
        <source>Trigger-buy at 0.05%</source>
        <translation>触发买入0.05％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2564"/>
        <source>Triggers a limit buy order at 0.05% momentum percentage increase</source>
        <translation>在动量百分比增加0.05％时触发限价买单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2569"/>
        <source>Trigger-buy at 0.06%</source>
        <translation>触发买入价为0.06％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2575"/>
        <source>Triggers a limit buy order at 0.06% momentum percentage increase</source>
        <translation>以0.06％的动量百分比增加触发限价买单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2580"/>
        <source>Trigger-buy at 0.07%</source>
        <translation>触发买入价为0.07％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2586"/>
        <source>Triggers a limit buy order at 0.07% momentum percentage increase</source>
        <translation>在动量百分比增加0.07％时触发限价买单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2591"/>
        <source>Trigger-buy at 0.08%</source>
        <translation>触发买入价为0.08％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2597"/>
        <source>Triggers a limit buy order at 0.08% momentum percentage increase</source>
        <translation>在动量百分比增加0.08％时触发限价买单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2602"/>
        <source>Trigger-buy at 0.09%</source>
        <translation>触发买入价为0.09％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2608"/>
        <source>Triggers a limit buy order at 0.09% momentum percentage increase</source>
        <translation>在动量百分比增加0.09％时触发限价买单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2613"/>
        <source>Trigger-buy at 0.1%</source>
        <translation>触发购买0.1％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2619"/>
        <source>Triggers a limit buy order at 0.1% momentum percentage increase</source>
        <translation>在动量百分比增加0.1％时触发限价买单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2624"/>
        <source>Trigger-buy at 0.2%</source>
        <translation>触发买入价为0.2％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2630"/>
        <source>Triggers a limit buy order at 0.2% momentum percentage increase</source>
        <translation>在动量百分比增加0.2％时触发限价买单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2635"/>
        <source>Trigger-buy at 0.3%</source>
        <translation>触发购买0.3％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2641"/>
        <source>Triggers a limit buy order at 0.3% momentum percentage increase</source>
        <translation>在动量百分比增加0.3％时触发限价买单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2646"/>
        <source>Trigger-buy at 0.4%</source>
        <translation>触发买入0.4％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2652"/>
        <source>Triggers a limit buy order at 0.4% momentum percentage increase</source>
        <translation>在动量百分比增加0.4％时触发限价买单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2657"/>
        <source>Trigger-buy at 0.5%</source>
        <translation>触发买入价为0.5％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2663"/>
        <source>Triggers a limit buy order at 0.5% momentum percentage increase</source>
        <translation>在动量百分比增加0.5％时触发限价买单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2668"/>
        <source>Trigger-buy at 0.6%</source>
        <translation>以0.6％的价格买进</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2674"/>
        <source>Triggers a limit buy order at 0.6% momentum percentage increase</source>
        <translation>在动量百分比增加0.6％时触发限价买单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2679"/>
        <source>Trigger-buy at 0.7%</source>
        <translation>触发购买0.7％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2685"/>
        <source>Triggers a  limit buy order at 0.7% momentum percentage increase</source>
        <translation>在动量百分比增加0.7％时触发限价买单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2690"/>
        <source>Trigger-buy at 0.8%</source>
        <translation>触发买入0.8％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2696"/>
        <source>Triggers a  limit buy order at 0.8% momentum percentage increase</source>
        <translation>在动量百分比增加0.8％时触发限价买单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2701"/>
        <source>Trigger-buy at 0.9%</source>
        <translation>触发买入0.9％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2707"/>
        <source>Triggers a  limit buy order at 0.9% momentum percentage increase</source>
        <translation>在动量百分比增加0.9％时触发限价买单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2729"/>
        <source>Risk</source>
        <translation>风险</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2759"/>
        <source>Risk Indicator</source>
        <translation>风险指标</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2774"/>
        <location filename="../mainwindow.cpp" line="2512"/>
        <location filename="../mainwindow.cpp" line="2556"/>
        <source>Stop Loss</source>
        <translation>止损</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2781"/>
        <source>A stop-loss is designed to limit an investor&apos;s loss on an asset position</source>
        <translation>止损旨在限制投资者在资产头寸上的损失</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2784"/>
        <location filename="../mainwindow.ui" line="2907"/>
        <source>-</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2788"/>
        <source>Stop-loss at 4.0%</source>
        <translation>止损为4.0％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2794"/>
        <source>Trigger a stop-loss order at 4.0% below the price at which you bought the asset</source>
        <translation>在低于您购买资产价格的4.0％处触发止损订单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2797"/>
        <location filename="../mainwindow.ui" line="2815"/>
        <location filename="../mainwindow.ui" line="2920"/>
        <location filename="../mainwindow.ui" line="2938"/>
        <location filename="../mainwindow.ui" line="2956"/>
        <location filename="../mainwindow.ui" line="2974"/>
        <location filename="../mainwindow.ui" line="2992"/>
        <location filename="../mainwindow.ui" line="3010"/>
        <location filename="../mainwindow.ui" line="3028"/>
        <location filename="../mainwindow.ui" line="3046"/>
        <location filename="../mainwindow.cpp" line="2515"/>
        <location filename="../mainwindow.cpp" line="2559"/>
        <source>Low Risk</source>
        <translation>低风险</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2806"/>
        <source>Stop-loss at 5.0%</source>
        <translation>止损为5.0％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2812"/>
        <source>Trigger a stop-loss order at 5.0% below the price at which you bought the asset</source>
        <translation>在低于您购买资产价格的5.0％处触发止损订单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2824"/>
        <source>Stop-loss at 6.0%</source>
        <translation>止损为6.0％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2830"/>
        <source>Trigger a stop-loss order at 6.0% below the price at which you bought the asset</source>
        <translation>在低于您购买资产价格的6.0％处触发止损订单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2833"/>
        <location filename="../mainwindow.ui" line="2851"/>
        <location filename="../mainwindow.ui" line="3064"/>
        <location filename="../mainwindow.ui" line="3082"/>
        <location filename="../mainwindow.ui" line="3100"/>
        <location filename="../mainwindow.ui" line="3118"/>
        <location filename="../mainwindow.cpp" line="2518"/>
        <location filename="../mainwindow.cpp" line="2562"/>
        <source>Medium Risk</source>
        <translation>中等风险</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2842"/>
        <source>Stop-loss at 7.0%</source>
        <translation>止损为7.0％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2848"/>
        <source>Trigger a stop-loss order at 7.0% below the price at which you bought the asset</source>
        <translation>在低于您购买资产价格的7.0％处触发止损订单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2860"/>
        <source>Stop-loss at 8.0%</source>
        <translation>止损为8.0％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2866"/>
        <source>Trigger a stop-loss order at 8.0% below the price at which you bought the asset</source>
        <translation>在低于您购买资产价格的8.0％处触发止损订单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2869"/>
        <location filename="../mainwindow.ui" line="2887"/>
        <location filename="../mainwindow.ui" line="3136"/>
        <location filename="../mainwindow.ui" line="3154"/>
        <location filename="../mainwindow.ui" line="3172"/>
        <location filename="../mainwindow.ui" line="3190"/>
        <location filename="../mainwindow.cpp" line="2521"/>
        <location filename="../mainwindow.cpp" line="2565"/>
        <source>High Risk</source>
        <translation>高风险</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2878"/>
        <source>Stop-loss at 9.0%</source>
        <translation>止损为9.0％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2884"/>
        <source>Trigger a stop-loss order at 9.0% below the price at which you bought the asset</source>
        <translation>以低于您购买资产价格9.0％的价格触发止损订单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2897"/>
        <location filename="../mainwindow.cpp" line="2513"/>
        <location filename="../mainwindow.cpp" line="2557"/>
        <source>Take Profit</source>
        <translation>从中受益</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2904"/>
        <source>A take-profit is designed to limit an investor&apos;s profit on an asset position</source>
        <translation>止盈旨在限制投资者在资产头寸上的利润</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2911"/>
        <source>Take-profit at 0.03%</source>
        <translation>获利0.03％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2917"/>
        <source>Trigger a take-profit order at 0.03% above the price at which you bought the asset</source>
        <translation>以高于您购买资产价格的0.03％的价格触发止盈订单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2929"/>
        <source>Take-profit at 0.04%</source>
        <translation>获利0.04％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2935"/>
        <source>Trigger a take-profit order at 0.04% above the price at which you bought the asset</source>
        <translation>以高于您购买资产价格0.04％的价格触发止盈订单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2947"/>
        <source>Take-profit at 0.05%</source>
        <translation>获利0.05％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2953"/>
        <source>Trigger a take-profit order at 0.05% above the price at which you bought the asset</source>
        <translation>以高于您购买资产价格的0.05％触发止盈订单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2965"/>
        <source>Take-profit at 0.06%</source>
        <translation>获利0.06％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2971"/>
        <source>Trigger a take-profit order at 0.06% above the price at which you bought the asset</source>
        <translation>以高于您购买资产价格0.06％的价格触发止盈订单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2983"/>
        <source>Take-profit at 0.07%</source>
        <translation>获利0.07％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2989"/>
        <source>Trigger a take-profit order at 0.07% above the price at which you bought the asset</source>
        <translation>以高于您购买资产价格0.07％的价格触发止盈订单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3001"/>
        <source>Take-profit at 0.08%</source>
        <translation>获利0.08％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3007"/>
        <source>Trigger a take-profit order at 0.08% above the price at which you bought the asset</source>
        <translation>以高于您购买资产价格0.08％的价格触发止盈订单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3019"/>
        <source>Take-profit at 0.09%</source>
        <translation>获利0.09％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3025"/>
        <source>Trigger a take-profit order at 0.09% above the price at which you bought the asset</source>
        <translation>以高于您购买资产价格0.09％的价格触发止盈订单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3037"/>
        <source>Take-profit at 0.1%</source>
        <translation>获利0.1％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3043"/>
        <source>Trigger a take-profit order at 0.1% above the price at which you bought the asset</source>
        <translation>以高于您购买资产价格0.1％的价格触发止盈订单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3055"/>
        <source>Take-profit at 0.2%</source>
        <translation>获利0.2％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3061"/>
        <source>Trigger a take-profit order at 0.2% above the price at which you bought the asset</source>
        <translation>以高于您购买资产价格0.2％的价格触发止盈订单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3073"/>
        <source>Take-profit at 0.3%</source>
        <translation>获利为0.3％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3079"/>
        <source>Trigger a take-profit order at 0.3% above the price at which you bought the asset</source>
        <translation>以高于您购买资产价格0.3％的价格触发止盈订单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3091"/>
        <source>Take-profit at 0.4%</source>
        <translation>获利为0.4％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3097"/>
        <source>Trigger a take-profit order at 0.4% above the price at which you bought the asset</source>
        <translation>以高于您购买资产价格0.4％的价格触发止盈订单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3109"/>
        <source>Take-profit at 0.5%</source>
        <translation>获利为0.5％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3115"/>
        <source>Trigger a take-profit order at 0.5% above the price at which you bought the asset</source>
        <translation>以高于您购买资产价格0.5％的价格触发止盈订单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3127"/>
        <source>Take-profit at 0.6%</source>
        <translation>获利为0.6％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3133"/>
        <source>Trigger a take-profit order at 0.6% above the price at which you bought the asset</source>
        <translation>以高于您购买资产价格0.6％的价格触发止盈订单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3145"/>
        <source>Take-profit at 0.7%</source>
        <translation>获利为0.7％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3151"/>
        <source>Trigger a take-profit order at 0.7% above the price at which you bought the asset</source>
        <translation>以高于您购买资产价格0.7％的价格触发止盈订单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3163"/>
        <source>Take-profit at 0.8%</source>
        <translation>获利为0.8％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3169"/>
        <source>Trigger a take-profit order at 0.8% above the price at which you bought the asset</source>
        <translation>以高于您购买资产价格0.8％的价格触发止盈订单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3181"/>
        <source>Take-profit at 0.9%</source>
        <translation>获利0.9％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3187"/>
        <source>Trigger a take-profit order at 0.9% above the price at which you bought the asset</source>
        <translation>以高于您购买资产价格0.9％的价格触发止盈订单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3225"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600; text-decoration: underline;&quot;&gt;What is BittrexBot?&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;&lt;br /&gt;BittrexBot is a bleeding edge, fully automated, A.I. powered trading bot for the Bittrex exchange. It is a desktop application that enables users to automate trades at the Bittrex exchange in real time. &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Built on top of a signal-driven, asynchronous, multithreaded trading engine, it strives to maximize profit and mitigate risk by providing efficient mechanisms for detecting market trends, triggering orders and managining positions.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;BittrexBot is powered by a form of artificial intelligence known as stacked generalization, a meta-learning technique that combines different predictive modelling algorithms to achieve greater degree of accuracy in regards to price prediction.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;BittrexBot comes with a powerful recovery mechanism which enables users to restore trading sessions that might have been cut abruptly due to a power failure, crash or an accidental quit by the user.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Designed with security in mind, the application runs locally on your laptop or desktop computer, removing the need to store your API keys into the cloud or to external third party entities.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;To properly use the application, a user must have a valid bittrex account and must generate a public and a private token that are going to be used by the bot in order to communicate with the exchange and execute orders.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600; text-decoration: underline;&quot;&gt;什么是BittrexBot？&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;&lt;br /&gt;BittrexBot是一个前沿技术，完全自动化。 比特币交易所的自动交易机器人。 它是一个桌面应用程序，使用户可以实时在Bittrex交易所自动进行交易。 &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;它建立在信号驱动的异步多线程交易引擎之上，通过提供有效的机制来检测市场趋势，触发订单和管理头寸，努力最大程度地提高利润并降低风险。&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;BittrexBot由一种称为堆叠概括的人工智能形式提供支持，这是一种元学习技术，该技术结合了不同的预测建模算法，可以在价格预测方面实现更高的准确性。&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;BittrexBot带有强大的恢复机制，使用户能够恢复由于电源故障，崩溃或用户意外退出而可能突然中断的交易时段。&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;在设计时考虑到安全性，该应用程序在便携式计算机或台式计算机上本地运行，从而无需将API密钥存储到云或外部第三方实体中。&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;为了正确使用该应用程序，用户必须具有有效的bittrex帐户，并且必须生成供机器人使用的公共令牌和私有令牌，以便与交易所进行通信并执行订单。&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3268"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; font-weight:600; text-decoration: underline;&quot;&gt;Trading Configuration Guideline:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;1. Navigate to the markets page to select or deselect markets.&lt;br/&gt;2. Navigate to the trader page to select or deselect strategies.&lt;br/&gt;3. Navigate to the trader page to select or deselect risks.&lt;br/&gt;4. You must select at least one strategy.&lt;br/&gt;5. You must select at least one take-profit risk.&lt;br/&gt;6. You must select at least one stop-loss risk.&lt;br/&gt;7. You cannot change configuration during trading session.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; font-weight:600; text-decoration: underline;&quot;&gt;Capital Allocation Guideline:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;1. Carefully allocate initial capital (if any) for each market.&lt;br/&gt;2. For each asset in a market provide the capital percentage.&lt;br/&gt;3. For each asset in a market provide the trading rounds.&lt;br/&gt;4. Allocate capital based on technical analysis.&lt;br/&gt;5. Thoroughly check your capital allocations.&lt;br/&gt;6. Click the check-up button for sanity check.&lt;br/&gt;7. You cannot change capital allocation during trading session.&lt;br/&gt;8. When ready, click the execute button to start the trading session.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;&amp;quot;Aut non rem temptes aut perfice&amp;quot; - Ovid, 43 BC-17 AD, Roman poet&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; font-weight:600; text-decoration: underline;&quot;&gt;交易配置指南：&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;1.导航到市场页面以选择或取消选择市场。&lt;br/&gt;2.导航至交易者页面以选择或取消选择策略。&lt;br/&gt;3.导航至交易者页面以选择或取消选择风险。&lt;br/&gt;4.您必须至少选择一种策略。&lt;br/&gt;5.您必须至少选择一种获利风险。&lt;br/&gt;6.您必须至少选择一种止损风险。&lt;br/&gt;7.您不能在交易时段中更改配置。&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; font-weight:600; text-decoration: underline;&quot;&gt;资本分配准则：&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;1.仔细分配每个市场的初始资本（如果有）。&lt;br/&gt;2.为市场中的每种资产提供资本百分比。&lt;br/&gt;3.为市场中的每种资产提供交易回合。&lt;br/&gt;4.根据技术分析分配资金。&lt;br/&gt;5.彻底检查您的资本分配。&lt;br/&gt;6.单击检查按钮进行健全性检查。&lt;br/&gt;7.您不能在交易时段更改资本分配。&lt;br/&gt;8.准备就绪后，单击执行按钮以开始交易时段。&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;&amp;quot;Aut non rem temptes aut perfice&amp;quot; - Ovid, 43 BC-17 AD, Roman poet&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; font-weight:600; text-decoration: underline;&quot;&gt;Trading Configuration Guideline:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;1. Navigate to the markets page to select or deselect markets.&lt;br/&gt;2. Navigate to the trader page to select or deselect strategies.&lt;br/&gt;3. Navigate to the trader page to select or deselect risks.&lt;br/&gt;4. You must select at least one strategy.&lt;br/&gt;5. You must select at least one take-profit risk.&lt;br/&gt;6. You must select at least one stop-loss risk.&lt;br/&gt;8. You cannot change configuration during trading session.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; font-weight:600; text-decoration: underline;&quot;&gt;Capital Allocation Guideline:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;1. Carefully allocate initial capital (if any) for each market.&lt;br/&gt;2. For each asset in a market provide the capital percentage.&lt;br/&gt;3. For each asset in a market provide the trading rounds.&lt;br/&gt;4. Allocate capital based on technical analysis.&lt;br/&gt;5. Thoroughly check your capital allocations.&lt;br/&gt;6. Click the check-up button for sanity check.&lt;br/&gt;7. You cannot change capital allocation during trading session.&lt;br/&gt;8. When ready, click the execute button to start the trading session.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;&amp;quot;Aut non rem temptes aut perfice&amp;quot; - Ovid, 43 BC-17 AD, Roman poet&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; font-weight:600; text-decoration: underline;&quot;&gt;交易配置指南：&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;1.导航到市场页面以选择或取消选择市场。&lt;br/&gt;2.导航至交易者页面以选择或取消选择策略。&lt;br/&gt;3.导航至交易者页面以选择或取消选择风险。&lt;br/&gt;4.您必须至少选择一种策略。&lt;br/&gt;5.您必须至少选择一种获利风险。&lt;br/&gt;6.您必须至少选择一种止损风险。&lt;br/&gt;7.您不能在交易时段中更改配置。&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; font-weight:600; text-decoration: underline;&quot;&gt;资本分配准则：&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;1.仔细分配每个市场的初始资本（如果有）。&lt;br/&gt;2.为市场中的每种资产提供资本百分比。&lt;br/&gt;3.为市场中的每种资产提供交易回合。&lt;br/&gt;4.根据技术分析分配资金。&lt;br/&gt;5.彻底检查您的资本分配。&lt;br/&gt;6.单击检查按钮进行健全性检查。&lt;br/&gt;7.您不能在交易时段更改资本分配。&lt;br/&gt;8.准备就绪后，单击执行按钮以开始交易时段。&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;&amp;quot;Aut non rem temptes aut perfice&amp;quot; - Ovid, 43 BC-17 AD, Roman poet&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3303"/>
        <source>CHECK-UP</source>
        <translation>检查</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3329"/>
        <source>EXECUTE</source>
        <translation>执行</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3355"/>
        <source>USD Capital ($)</source>
        <translation>USD资金（$）</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3410"/>
        <source>Selected USD Markets</source>
        <translation>部分USD市场</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3421"/>
        <location filename="../mainwindow.ui" line="3524"/>
        <location filename="../mainwindow.ui" line="3628"/>
        <location filename="../mainwindow.ui" line="3731"/>
        <source>Capital Percentage %</source>
        <translation>资本百分比</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3432"/>
        <location filename="../mainwindow.ui" line="3535"/>
        <location filename="../mainwindow.ui" line="3639"/>
        <location filename="../mainwindow.ui" line="3742"/>
        <location filename="../mainwindow.ui" line="3835"/>
        <source>Trading Rounds</source>
        <translation>交易回合</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3458"/>
        <source>BTC Capital (₿)</source>
        <translation>BTC资本（₿）</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3513"/>
        <source>Selected BTC Markets</source>
        <translation>选定的BTC市场</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3562"/>
        <source>ETH Capital (Ξ)</source>
        <translation>ETH资本（O）</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3617"/>
        <source>Selected ETH Markets</source>
        <translation>精选ETH市场</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3665"/>
        <source>USDT Capital (₮)</source>
        <translation>USDT资本（₮）</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3720"/>
        <source>Selected USDT Markets</source>
        <translation>部分USDT市场</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3771"/>
        <source>EUR Capital (€)</source>
        <translation>EUR资本（€）</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3813"/>
        <source>Selected EUR Markets</source>
        <translation>精选EUR市场</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3824"/>
        <source>Capital Percentage (%)</source>
        <translation>资本百分比 (%)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3902"/>
        <source>Cancel All</source>
        <translation>全部取消</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="3966"/>
        <source>Open Orders</source>
        <translation>未结订单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4003"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4014"/>
        <source>Opened</source>
        <translation>开了</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4036"/>
        <location filename="../mainwindow.ui" line="4244"/>
        <source>Side</source>
        <translation>侧</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4047"/>
        <location filename="../mainwindow.ui" line="4255"/>
        <source>Type</source>
        <translation>类型</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4058"/>
        <location filename="../mainwindow.ui" line="4266"/>
        <location filename="../mainwindow.ui" line="6093"/>
        <location filename="../mainwindow.ui" line="6365"/>
        <location filename="../mainwindow.ui" line="6950"/>
        <location filename="../mainwindow.ui" line="7222"/>
        <location filename="../mainwindow.ui" line="7777"/>
        <location filename="../mainwindow.ui" line="8049"/>
        <location filename="../mainwindow.ui" line="8610"/>
        <location filename="../mainwindow.ui" line="8882"/>
        <location filename="../mainwindow.ui" line="9437"/>
        <location filename="../mainwindow.ui" line="9709"/>
        <source>Price</source>
        <translation>价钱</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4069"/>
        <location filename="../mainwindow.ui" line="4277"/>
        <location filename="../mainwindow.ui" line="6082"/>
        <location filename="../mainwindow.ui" line="6354"/>
        <location filename="../mainwindow.ui" line="6939"/>
        <location filename="../mainwindow.ui" line="7211"/>
        <location filename="../mainwindow.ui" line="7766"/>
        <location filename="../mainwindow.ui" line="8038"/>
        <location filename="../mainwindow.ui" line="8599"/>
        <location filename="../mainwindow.ui" line="8871"/>
        <location filename="../mainwindow.ui" line="9426"/>
        <location filename="../mainwindow.ui" line="9698"/>
        <source>Quantity</source>
        <translation>数量</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4080"/>
        <location filename="../mainwindow.ui" line="4288"/>
        <source>Filled %</source>
        <translation>填充 ％</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4091"/>
        <source>Est. Total</source>
        <translation>估计总数</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4174"/>
        <source>Closed Orders</source>
        <translation>未结订单</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4211"/>
        <source>Closed Date</source>
        <translation>截止日期</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4222"/>
        <source>Opened Date</source>
        <translation>开馆日</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4299"/>
        <source>Total</source>
        <translation>总</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4427"/>
        <location filename="../mainwindow.ui" line="5735"/>
        <source>Initial Capital ($)</source>
        <translation>初始资金（$）</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4464"/>
        <location filename="../mainwindow.ui" line="5782"/>
        <source>Current Capital ($)</source>
        <translation>流动资金（$）</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4501"/>
        <source>Total Equity ($)</source>
        <translation>权益总额（$）</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4538"/>
        <location filename="../mainwindow.ui" line="5625"/>
        <source>Realised PnL ($)</source>
        <translation>已实现损益（$）</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4572"/>
        <location filename="../mainwindow.ui" line="4795"/>
        <location filename="../mainwindow.ui" line="5018"/>
        <location filename="../mainwindow.ui" line="5256"/>
        <location filename="../mainwindow.ui" line="5472"/>
        <source>ROI (%)</source>
        <translation>投资回报 (%)</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4625"/>
        <source>USD Portfolio Session Summary</source>
        <translation>USD投资组合会议摘要</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4653"/>
        <location filename="../mainwindow.ui" line="6592"/>
        <source>Initial Capital (₿)</source>
        <translation>初始资金（₿）</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4690"/>
        <location filename="../mainwindow.ui" line="6639"/>
        <source>Current Capital (₿)</source>
        <translation>流动资金（₿）</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4727"/>
        <source>Total Equity (₿)</source>
        <translation>总权益（₿）</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4761"/>
        <location filename="../mainwindow.ui" line="6482"/>
        <source>Realised PnL (₿)</source>
        <translation>已实现损益（₿）</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4848"/>
        <source>BTC Portfolio Session Summary</source>
        <translation>BTC投资组合会议摘要</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="5114"/>
        <location filename="../mainwindow.ui" line="8264"/>
        <source>Initial Capital (₮)</source>
        <translation>初始资金（₮）</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="5151"/>
        <location filename="../mainwindow.ui" line="8311"/>
        <source>Current Capital (₮)</source>
        <translation>流动资金（₮）</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="5188"/>
        <source>Total Equity (₮)</source>
        <translation>总权益（₮）</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="5222"/>
        <location filename="../mainwindow.ui" line="8166"/>
        <source>Realised PnL (₮)</source>
        <translation>已实现损益（₮）</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="5299"/>
        <source>USDT Portfolio Session Summary</source>
        <translation>USDT投资组合会议摘要</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4876"/>
        <location filename="../mainwindow.ui" line="7437"/>
        <source>Initial Capital (Ξ)</source>
        <translation>初始资金（Ξ）</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4913"/>
        <location filename="../mainwindow.ui" line="7478"/>
        <source>Current Capital (Ξ)</source>
        <translation>流动资金（Ξ）</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4950"/>
        <source>Total Equity (Ξ)</source>
        <translation>总权益（Ξ）</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="4984"/>
        <location filename="../mainwindow.ui" line="7339"/>
        <source>Realised PnL (Ξ)</source>
        <translation>已实现损益（Ξ）</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="5076"/>
        <source>ETH Portfolio Session Summary</source>
        <translation>ETH投资组合会议摘要</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="5327"/>
        <location filename="../mainwindow.ui" line="9091"/>
        <source>Initial Capital (€)</source>
        <translation>初始资金（€）</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="5364"/>
        <location filename="../mainwindow.ui" line="9138"/>
        <source>Current Capital (€)</source>
        <translation>流动资金（€）</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="5401"/>
        <source>Total Equity (€)</source>
        <translation>总权益（€）</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="5438"/>
        <location filename="../mainwindow.ui" line="8993"/>
        <source>Realised PnL (€)</source>
        <translation>已实现损益（€）</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="5525"/>
        <source>EUR Portfolio Session Summary</source>
        <translation>EUR投资组合会议摘要</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="5594"/>
        <source>USD Portfolio</source>
        <translation>USD投资组合</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="5678"/>
        <source>Unrealised PnL ($)</source>
        <translation>未实现损益（$）</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="5829"/>
        <source>Equity ($)</source>
        <translation>股本（$）</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="5881"/>
        <location filename="../mainwindow.ui" line="6738"/>
        <location filename="../mainwindow.ui" line="7565"/>
        <location filename="../mainwindow.ui" line="8398"/>
        <location filename="../mainwindow.ui" line="9225"/>
        <source>Liquify All</source>
        <translation>液化所有</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="5951"/>
        <location filename="../mainwindow.ui" line="6808"/>
        <location filename="../mainwindow.ui" line="7635"/>
        <location filename="../mainwindow.ui" line="8468"/>
        <location filename="../mainwindow.ui" line="9295"/>
        <source>Open Positions</source>
        <translation>空缺职位</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="5994"/>
        <location filename="../mainwindow.ui" line="6851"/>
        <location filename="../mainwindow.ui" line="7678"/>
        <location filename="../mainwindow.ui" line="8511"/>
        <location filename="../mainwindow.ui" line="9338"/>
        <source>Liquify</source>
        <translation>液化</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="6005"/>
        <location filename="../mainwindow.ui" line="6277"/>
        <location filename="../mainwindow.ui" line="6862"/>
        <location filename="../mainwindow.ui" line="7134"/>
        <location filename="../mainwindow.ui" line="7689"/>
        <location filename="../mainwindow.ui" line="7961"/>
        <location filename="../mainwindow.ui" line="8522"/>
        <location filename="../mainwindow.ui" line="8794"/>
        <location filename="../mainwindow.ui" line="9349"/>
        <location filename="../mainwindow.ui" line="9621"/>
        <source>Action</source>
        <translation>行动</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="6027"/>
        <location filename="../mainwindow.ui" line="6299"/>
        <location filename="../mainwindow.ui" line="6884"/>
        <location filename="../mainwindow.ui" line="7156"/>
        <location filename="../mainwindow.ui" line="7711"/>
        <location filename="../mainwindow.ui" line="7983"/>
        <location filename="../mainwindow.ui" line="8544"/>
        <location filename="../mainwindow.ui" line="8816"/>
        <location filename="../mainwindow.ui" line="9371"/>
        <location filename="../mainwindow.ui" line="9643"/>
        <source>Average Price</source>
        <translation>平均价格</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="6038"/>
        <location filename="../mainwindow.ui" line="6310"/>
        <location filename="../mainwindow.ui" line="6895"/>
        <location filename="../mainwindow.ui" line="7167"/>
        <location filename="../mainwindow.ui" line="7722"/>
        <location filename="../mainwindow.ui" line="7994"/>
        <location filename="../mainwindow.ui" line="8555"/>
        <location filename="../mainwindow.ui" line="8827"/>
        <location filename="../mainwindow.ui" line="9382"/>
        <location filename="../mainwindow.ui" line="9654"/>
        <source>Market Value</source>
        <translation>市场价值</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="6049"/>
        <location filename="../mainwindow.ui" line="6321"/>
        <location filename="../mainwindow.ui" line="6906"/>
        <location filename="../mainwindow.ui" line="7178"/>
        <location filename="../mainwindow.ui" line="7733"/>
        <location filename="../mainwindow.ui" line="8005"/>
        <location filename="../mainwindow.ui" line="8566"/>
        <location filename="../mainwindow.ui" line="8838"/>
        <location filename="../mainwindow.ui" line="9393"/>
        <location filename="../mainwindow.ui" line="9665"/>
        <source>Cost Basis</source>
        <translation>成本基础</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="6060"/>
        <location filename="../mainwindow.ui" line="6332"/>
        <location filename="../mainwindow.ui" line="6917"/>
        <location filename="../mainwindow.ui" line="7189"/>
        <location filename="../mainwindow.ui" line="7744"/>
        <location filename="../mainwindow.ui" line="8016"/>
        <location filename="../mainwindow.ui" line="8577"/>
        <location filename="../mainwindow.ui" line="8849"/>
        <location filename="../mainwindow.ui" line="9404"/>
        <location filename="../mainwindow.ui" line="9676"/>
        <source>Realised PnL</source>
        <translation>已实现损益</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="6071"/>
        <location filename="../mainwindow.ui" line="6343"/>
        <location filename="../mainwindow.ui" line="6928"/>
        <location filename="../mainwindow.ui" line="7200"/>
        <location filename="../mainwindow.ui" line="7755"/>
        <location filename="../mainwindow.ui" line="8027"/>
        <location filename="../mainwindow.ui" line="8588"/>
        <location filename="../mainwindow.ui" line="8860"/>
        <location filename="../mainwindow.ui" line="9415"/>
        <location filename="../mainwindow.ui" line="9687"/>
        <source>Unrealised PnL</source>
        <translation>未实现损益</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="6104"/>
        <location filename="../mainwindow.ui" line="6376"/>
        <location filename="../mainwindow.ui" line="6961"/>
        <location filename="../mainwindow.ui" line="7233"/>
        <location filename="../mainwindow.ui" line="7788"/>
        <location filename="../mainwindow.ui" line="8060"/>
        <location filename="../mainwindow.ui" line="8621"/>
        <location filename="../mainwindow.ui" line="8893"/>
        <location filename="../mainwindow.ui" line="9448"/>
        <location filename="../mainwindow.ui" line="9720"/>
        <source>Buys</source>
        <translation>购买</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="6115"/>
        <location filename="../mainwindow.ui" line="6387"/>
        <location filename="../mainwindow.ui" line="6972"/>
        <location filename="../mainwindow.ui" line="7244"/>
        <location filename="../mainwindow.ui" line="7799"/>
        <location filename="../mainwindow.ui" line="8071"/>
        <location filename="../mainwindow.ui" line="8632"/>
        <location filename="../mainwindow.ui" line="8904"/>
        <location filename="../mainwindow.ui" line="9459"/>
        <location filename="../mainwindow.ui" line="9731"/>
        <source>Sells</source>
        <translation>卖出</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="6126"/>
        <location filename="../mainwindow.ui" line="6398"/>
        <location filename="../mainwindow.ui" line="6983"/>
        <location filename="../mainwindow.ui" line="7255"/>
        <location filename="../mainwindow.ui" line="7810"/>
        <location filename="../mainwindow.ui" line="8082"/>
        <location filename="../mainwindow.ui" line="8643"/>
        <location filename="../mainwindow.ui" line="8915"/>
        <location filename="../mainwindow.ui" line="9470"/>
        <location filename="../mainwindow.ui" line="9742"/>
        <source>Net</source>
        <translation>净</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="6137"/>
        <location filename="../mainwindow.ui" line="6409"/>
        <location filename="../mainwindow.ui" line="6994"/>
        <location filename="../mainwindow.ui" line="7266"/>
        <location filename="../mainwindow.ui" line="7821"/>
        <location filename="../mainwindow.ui" line="8093"/>
        <location filename="../mainwindow.ui" line="8654"/>
        <location filename="../mainwindow.ui" line="8926"/>
        <location filename="../mainwindow.ui" line="9481"/>
        <location filename="../mainwindow.ui" line="9753"/>
        <source>Net Total</source>
        <translation>净总值</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="6148"/>
        <location filename="../mainwindow.ui" line="6420"/>
        <location filename="../mainwindow.ui" line="7005"/>
        <location filename="../mainwindow.ui" line="7277"/>
        <location filename="../mainwindow.ui" line="7832"/>
        <location filename="../mainwindow.ui" line="8104"/>
        <location filename="../mainwindow.ui" line="8665"/>
        <location filename="../mainwindow.ui" line="8937"/>
        <location filename="../mainwindow.ui" line="9492"/>
        <location filename="../mainwindow.ui" line="9764"/>
        <source>N.I.C</source>
        <translation>净包容性佣金</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="6234"/>
        <location filename="../mainwindow.ui" line="7091"/>
        <location filename="../mainwindow.ui" line="7918"/>
        <location filename="../mainwindow.ui" line="8751"/>
        <location filename="../mainwindow.ui" line="9578"/>
        <source>Closed Positions</source>
        <translation>平仓</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="6451"/>
        <source>BTC Portfolio</source>
        <translation>BTC投资组合</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="6535"/>
        <source>Unrealised PnL (₿)</source>
        <translation>未实现损益（₿）</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="6686"/>
        <source>Equity (₿)</source>
        <translation>权益（₿）</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="7308"/>
        <source>ETH Portfolio</source>
        <translation>ETH投资组合</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="7386"/>
        <source>Unrealised PnL (Ξ)</source>
        <translation>未实现损益（Ξ）</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="7519"/>
        <source>Equity (Ξ)</source>
        <translation>权益（Ξ）</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="8135"/>
        <source>USDT Portfolio</source>
        <translation>USDT投资组合</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="8213"/>
        <source>Unrealised PnL (₮)</source>
        <translation>未实现损益（₮）</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="8352"/>
        <source>Equity (₮)</source>
        <translation>权益（₮）</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="8962"/>
        <source>EUR Portfolio</source>
        <translation>EUR投资组合</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="9040"/>
        <source>Unrealised PnL (€)</source>
        <translation>未实现损益（€）</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="9179"/>
        <source>Equity (€)</source>
        <translation>权益（€）</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="9857"/>
        <source>Light Mode</source>
        <translation>灯光模式</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="9872"/>
        <source>Dark Mode</source>
        <translation>暗模式</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="9887"/>
        <source>Logger</source>
        <translation>记录仪</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="9902"/>
        <location filename="../mainwindow.ui" line="9905"/>
        <source>Resume Execution</source>
        <translation>恢复执行</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="9920"/>
        <source>Pause Execution</source>
        <translation>暂停执行</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="9935"/>
        <source>Stop Execution</source>
        <translation>停止执行</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="9948"/>
        <source>System Tray</source>
        <translation>系统托盘</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="9951"/>
        <source>Minimize to system tray</source>
        <translation>最小化到系统托盘</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="9960"/>
        <source>Language</source>
        <translation>语言</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="9963"/>
        <source>Language Translator</source>
        <translation>语言翻译</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1407"/>
        <source>The current trading session is still running!</source>
        <translation>当前交易时段仍在运行！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1408"/>
        <source>Are you sure you want to stop execution?</source>
        <translation>您确定要停止执行吗？</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1409"/>
        <source>Stopping the current session while executing </source>
        <translation>执行时停止当前会话</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1410"/>
        <source>can leave you exposed with open positions. It is highly </source>
        <translation>暴露和打开位置 </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1411"/>
        <source> advised to wait until the session completes.</source>
        <translation>建议等待会话完成。</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1893"/>
        <source>BittrexBot terminated while trading session was still active!</source>
        <translation>BittrexBot在交易时段仍处于活动状态时终止！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1894"/>
        <source>Would you like to restore the trading session?</source>
        <translation>您想恢复交易时段吗？</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1895"/>
        <source>The BittrexBot program either crashed or was terminated by the user before the trading session had completed.</source>
        <translation>在交易时段结束之前，BittrexBot程序崩溃或被用户终止。</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1896"/>
        <source> It is highly reccommended to restore your previous active trading session.</source>
        <translation>强烈建议您恢复以前的活跃交易时段。</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1897"/>
        <source> If you do not restore, you will be left with exposed positions.</source>
        <translation>如果不还原，您将处于暴露状态。</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3255"/>
        <source>An error was discovered regarding strategy selection!</source>
        <translation>发现有关策略选择的错误！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3256"/>
        <source>No strategy has been selected!</source>
        <translation>尚未选择任何策略！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3257"/>
        <source>You must select at least one trading strategy, </source>
        <translation>您必须至少选择一种交易策略，</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3258"/>
        <source>In order to do so navigate to the trader page and select a strategy from the strategies list.</source>
        <translation>为此，请导航至交易者页面，然后从策略列表中选择一个策略。</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3268"/>
        <source>An error was discovered regarding risk selection!</source>
        <translation>发现有关风险选择的错误！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3269"/>
        <source>No stop-loss or take-profit risk was selected!</source>
        <translation>没有选择止损或获利风险！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3270"/>
        <source>You must select at least one take-profit risk </source>
        <translation>您必须选择至少一项获利风险</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3271"/>
        <source>and at least one stop-loss risk, in order to do so </source>
        <translation>至少有一种止损风险</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3272"/>
        <source>navigate to the trader page and select the risks from the risks list.</source>
        <translation>导航至交易者页面，然后从风险列表中选择风险。</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3311"/>
        <location filename="../mainwindow.cpp" line="3333"/>
        <location filename="../mainwindow.cpp" line="3355"/>
        <location filename="../mainwindow.cpp" line="3377"/>
        <location filename="../mainwindow.cpp" line="3399"/>
        <location filename="../mainwindow.cpp" line="3415"/>
        <location filename="../mainwindow.cpp" line="3426"/>
        <location filename="../mainwindow.cpp" line="3437"/>
        <location filename="../mainwindow.cpp" line="3448"/>
        <location filename="../mainwindow.cpp" line="3459"/>
        <source>An inaccuracy was discovered regarding capital allocation!</source>
        <translation>发现有关资本分配的错误信息！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3312"/>
        <location filename="../mainwindow.cpp" line="3334"/>
        <location filename="../mainwindow.cpp" line="3356"/>
        <location filename="../mainwindow.cpp" line="3378"/>
        <location filename="../mainwindow.cpp" line="3400"/>
        <source>There was a capital allocation error for market </source>
        <translation>市场存在资本分配错误</translation>
    </message>
    <message>
        <source>The allocated capital for market </source>
        <translation type="vanished">市场分配资本</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3313"/>
        <location filename="../mainwindow.cpp" line="3335"/>
        <location filename="../mainwindow.cpp" line="3357"/>
        <location filename="../mainwindow.cpp" line="3379"/>
        <location filename="../mainwindow.cpp" line="3401"/>
        <source> </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3313"/>
        <location filename="../mainwindow.cpp" line="3335"/>
        <location filename="../mainwindow.cpp" line="3357"/>
        <location filename="../mainwindow.cpp" line="3379"/>
        <location filename="../mainwindow.cpp" line="3401"/>
        <source>The allocated quantity for market </source>
        <translation>市场分布</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3314"/>
        <location filename="../mainwindow.cpp" line="3336"/>
        <location filename="../mainwindow.cpp" line="3358"/>
        <location filename="../mainwindow.cpp" line="3380"/>
        <location filename="../mainwindow.cpp" line="3402"/>
        <source>was </source>
        <translation>原为</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3314"/>
        <location filename="../mainwindow.cpp" line="3336"/>
        <location filename="../mainwindow.cpp" line="3358"/>
        <location filename="../mainwindow.cpp" line="3380"/>
        <location filename="../mainwindow.cpp" line="3402"/>
        <source> while </source>
        <translation>而</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3315"/>
        <location filename="../mainwindow.cpp" line="3337"/>
        <location filename="../mainwindow.cpp" line="3359"/>
        <location filename="../mainwindow.cpp" line="3381"/>
        <location filename="../mainwindow.cpp" line="3403"/>
        <source>the minimum trading size for the market is </source>
        <translation>市场的最小交易量是</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3316"/>
        <location filename="../mainwindow.cpp" line="3338"/>
        <location filename="../mainwindow.cpp" line="3360"/>
        <location filename="../mainwindow.cpp" line="3382"/>
        <location filename="../mainwindow.cpp" line="3404"/>
        <source>, in order to deal with this issue either increase the allocated capital percentage or </source>
        <translation>，为了解决此问题，请增加分配的资本百分比或</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3317"/>
        <location filename="../mainwindow.cpp" line="3339"/>
        <location filename="../mainwindow.cpp" line="3361"/>
        <location filename="../mainwindow.cpp" line="3383"/>
        <location filename="../mainwindow.cpp" line="3405"/>
        <source>if there is insufficient capital navigate back to the markets page and deselect market </source>
        <translation>如果资金不足，请导航至市场页面并取消选择市场</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3318"/>
        <source> from the USD markets table.</source>
        <translation>从USD市场表中。</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3340"/>
        <source> from the BTC markets table.</source>
        <translation>从BTC市场表。</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3362"/>
        <source> from the ETH markets table.</source>
        <translation>从ETH市场表中。</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3384"/>
        <source> from the USDT markets table.</source>
        <translation>从USDT市场表中。</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3406"/>
        <source> from the EUR markets table.</source>
        <translation>从EUR市场表中。</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3416"/>
        <source>There was a capital allocation error for USD markets!</source>
        <translation>USD市场存在资本分配错误！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3417"/>
        <location filename="../mainwindow.cpp" line="3428"/>
        <location filename="../mainwindow.cpp" line="3439"/>
        <location filename="../mainwindow.cpp" line="3450"/>
        <location filename="../mainwindow.cpp" line="3461"/>
        <source>The sum of the allocated percentages exceeds the 100% upper bound limit, </source>
        <translation>分配的百分比之和超过了100％的上限，</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3418"/>
        <location filename="../mainwindow.cpp" line="3429"/>
        <location filename="../mainwindow.cpp" line="3440"/>
        <location filename="../mainwindow.cpp" line="3451"/>
        <location filename="../mainwindow.cpp" line="3462"/>
        <source>in order to deal with this issue either decrease the allocated percentages or </source>
        <translation>为了解决此问题，请降低分配的百分比或</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3419"/>
        <source> navigate back to the markets page and deselect a market from the USD markets table.</source>
        <translation>导航回到市场页面，然后从USD市场表中取消选择市场。</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3427"/>
        <source>There was a capital allocation error for BTC markets!</source>
        <translation>BTC市场存在资本分配错误！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3430"/>
        <source> navigate back to the markets page and deselect a market from the BTC markets table.</source>
        <translation>导航回到市场页面，然后从BTC市场表中取消选择市场。</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3438"/>
        <source>There was a capital allocation error for ETH markets!</source>
        <translation>ETH市场存在资本分配错误！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3441"/>
        <source> navigate back to the markets page and deselect a market from the ETH markets table.</source>
        <translation>导航回到市场页面，然后从ETH市场表中取消选择市场。</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3449"/>
        <source>There was a capital allocation error for USDT markets!</source>
        <translation>USDT市场存在资本分配错误！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3452"/>
        <source> navigate back to the markets page and deselect a market from the USDT markets table.</source>
        <translation>导航回到市场页面，然后从USDT市场表中取消选择市场。</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3460"/>
        <source>There was a capital allocation error for EUR markets!</source>
        <translation>EUR市场存在资本分配错误！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3463"/>
        <source> navigate back to the markets page and deselect a market from the EUR markets table.</source>
        <translation>导航回到市场页面，然后从EUR市场表中取消选择市场。</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3468"/>
        <source>Sanity check completed, everything looks good!</source>
        <translation>理智检查完成，一切看起来不错！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3469"/>
        <source>Click execute button to start the session!</source>
        <translation>Click the execute button to start the session!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="4060"/>
        <location filename="../mainwindow.cpp" line="4066"/>
        <source>Allocated capital for EUR-Portfolio</source>
        <translation>EUR投资组合的分配资金</translation>
    </message>
    <message>
        <source>Everything looks good!</source>
        <translation type="vanished">一切看起来都不错！</translation>
    </message>
    <message>
        <source>Click execute button!</source>
        <translation type="vanished">点击执行按钮！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3470"/>
        <source>BittrexBot performed a thorough sanity check </source>
        <translation>BittrexBot执行了彻底的健全性检查</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3471"/>
        <source>on the trading configurations and did not encounter any errors.</source>
        <translation>在交易配置上，没有遇到任何错误。</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="3979"/>
        <location filename="../mainwindow.cpp" line="3984"/>
        <location filename="../mainwindow.cpp" line="3989"/>
        <location filename="../mainwindow.cpp" line="3994"/>
        <location filename="../mainwindow.cpp" line="3999"/>
        <source>reserved</source>
        <translation>保留的</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="4016"/>
        <location filename="../mainwindow.cpp" line="4022"/>
        <source>Allocated capital for USD-Portfolio</source>
        <translation>USD投资组合的已分配资本</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="4027"/>
        <location filename="../mainwindow.cpp" line="4033"/>
        <source>Allocated capital for BTC-Portfolio</source>
        <translation>BTC投资组合的分配资金</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="4038"/>
        <location filename="../mainwindow.cpp" line="4044"/>
        <source>Allocated capital for ETH-Portfolio</source>
        <translation>ETH投资组合的分配资金</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="4049"/>
        <location filename="../mainwindow.cpp" line="4055"/>
        <source>Allocated capital for USDT-Portfolio</source>
        <translation>USDT投资组合的已分配资金</translation>
    </message>
</context>
<context>
    <name>MarketListItem</name>
    <message>
        <location filename="../marketlistitem.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>RiskTree</name>
    <message>
        <location filename="../risktree.cpp" line="23"/>
        <location filename="../risktree.cpp" line="38"/>
        <source>Stop Loss</source>
        <translation>止损</translation>
    </message>
    <message>
        <location filename="../risktree.cpp" line="24"/>
        <location filename="../risktree.cpp" line="38"/>
        <source>Take Profit</source>
        <translation>从中受益</translation>
    </message>
</context>
<context>
    <name>StopLossRiskFactory</name>
    <message>
        <location filename="../stoplossriskfactory.cpp" line="85"/>
        <source>Stop-loss at 4.0%</source>
        <translation>止损为4.0％</translation>
    </message>
    <message>
        <source>Trigger a stop-loss order at 4.0% below the price at which you bought the asset</source>
        <translation type="vanished">在低于您购买资产价格的4.0％处触发止损订单</translation>
    </message>
    <message>
        <location filename="../stoplossriskfactory.cpp" line="89"/>
        <source>Stop-loss at 5.0%</source>
        <translation>止损为5.0％</translation>
    </message>
    <message>
        <source>Trigger a stop-loss order at 5.0% below the price at which you bought the asset</source>
        <translation type="vanished">在低于您购买资产价格的5.0％处触发止损订单</translation>
    </message>
    <message>
        <location filename="../stoplossriskfactory.cpp" line="93"/>
        <source>Stop-loss at 6.0%</source>
        <translation>止损为6.0％</translation>
    </message>
    <message>
        <source>Trigger a stop-loss order at 6.0% below the price at which you bought the asset</source>
        <translation type="vanished">在低于您购买资产价格的6.0％处触发止损订单</translation>
    </message>
    <message>
        <location filename="../stoplossriskfactory.cpp" line="97"/>
        <source>Stop-loss at 7.0%</source>
        <translation>止损为7.0％</translation>
    </message>
    <message>
        <source>Trigger a stop-loss order at 7.0% below the price at which you bought the asset</source>
        <translation type="vanished">在低于您购买资产价格的7.0％处触发止损订单</translation>
    </message>
    <message>
        <location filename="../stoplossriskfactory.cpp" line="101"/>
        <source>Stop-loss at 8.0%</source>
        <translation>止损为8.0％</translation>
    </message>
    <message>
        <source>Trigger a stop-loss order at 8.0% below the price at which you bought the asset</source>
        <translation type="vanished">在低于您购买资产价格的8.0％处触发止损订单</translation>
    </message>
    <message>
        <location filename="../stoplossriskfactory.cpp" line="105"/>
        <source>Stop-loss at 9.0%</source>
        <translation>止损为9.0％</translation>
    </message>
    <message>
        <source>Trigger a stop-loss order at 9.0% below the price at which you bought the asset</source>
        <translation type="vanished">以低于您购买资产价格9.0％的价格触发止损订单</translation>
    </message>
</context>
<context>
    <name>StrategyTree</name>
    <message>
        <location filename="../strategytree.cpp" line="23"/>
        <source>Time Series</source>
        <translation>时间序列</translation>
    </message>
    <message>
        <location filename="../strategytree.cpp" line="24"/>
        <source>Neural Networks</source>
        <translation>神经网络</translation>
    </message>
    <message>
        <location filename="../strategytree.cpp" line="25"/>
        <source>Order Book Properties</source>
        <translation>订单簿属性</translation>
    </message>
    <message>
        <location filename="../strategytree.cpp" line="26"/>
        <source>Markov Models</source>
        <translation>马尔可夫模型</translation>
    </message>
    <message>
        <location filename="../strategytree.cpp" line="27"/>
        <location filename="../strategytree.cpp" line="41"/>
        <source>Filter Rule</source>
        <translation>筛选规则</translation>
    </message>
</context>
<context>
    <name>SystemTray</name>
    <message>
        <location filename="../systemtray.cpp" line="10"/>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="../systemtray.cpp" line="12"/>
        <source>Quit</source>
        <translation>放弃</translation>
    </message>
</context>
<context>
    <name>TakeProfitRiskFactory</name>
    <message>
        <location filename="../takeprofitriskfactory.cpp" line="172"/>
        <source>Take-profit at 0.03%</source>
        <translation>获利0.03％</translation>
    </message>
    <message>
        <source>Trigger a take-profit order at 0.03% above the price at which you bought the asset</source>
        <translation type="vanished">以高于您购买资产价格的0.03％的价格触发止盈订单</translation>
    </message>
    <message>
        <location filename="../takeprofitriskfactory.cpp" line="176"/>
        <source>Take-profit at 0.04%</source>
        <translation>获利0.04％</translation>
    </message>
    <message>
        <source>Trigger a take-profit order at 0.04% above the price at which you bought the asset</source>
        <translation type="vanished">以高于您购买资产价格0.04％的价格触发止盈订单</translation>
    </message>
    <message>
        <location filename="../takeprofitriskfactory.cpp" line="180"/>
        <source>Take-profit at 0.05%</source>
        <translation>获利0.05％</translation>
    </message>
    <message>
        <source>Trigger a take-profit order at 0.05% above the price at which you bought the asset</source>
        <translation type="vanished">以高于您购买资产价格的0.05％触发止盈订单</translation>
    </message>
    <message>
        <location filename="../takeprofitriskfactory.cpp" line="184"/>
        <source>Take-profit at 0.06%</source>
        <translation>获利0.06％</translation>
    </message>
    <message>
        <source>Trigger a take-profit order at 0.06% above the price at which you bought the asset</source>
        <translation type="vanished">以高于您购买资产价格0.06％的价格触发止盈订单</translation>
    </message>
    <message>
        <location filename="../takeprofitriskfactory.cpp" line="188"/>
        <source>Take-profit at 0.07%</source>
        <translation>获利0.07％</translation>
    </message>
    <message>
        <source>Trigger a take-profit order at 0.07% above the price at which you bought the asset</source>
        <translation type="vanished">以高于您购买资产价格0.07％的价格触发止盈订单</translation>
    </message>
    <message>
        <location filename="../takeprofitriskfactory.cpp" line="192"/>
        <source>Take-profit at 0.08%</source>
        <translation>获利0.08％</translation>
    </message>
    <message>
        <source>Trigger a take-profit order at 0.08% above the price at which you bought the asset</source>
        <translation type="vanished">以高于您购买资产价格0.08％的价格触发止盈订单</translation>
    </message>
    <message>
        <location filename="../takeprofitriskfactory.cpp" line="196"/>
        <source>Take-profit at 0.09%</source>
        <translation>获利0.09％</translation>
    </message>
    <message>
        <source>Trigger a take-profit order at 0.09% above the price at which you bought the asset</source>
        <translation type="vanished">以高于您购买资产价格0.09％的价格触发止盈订单</translation>
    </message>
    <message>
        <location filename="../takeprofitriskfactory.cpp" line="200"/>
        <source>Take-profit at 0.1%</source>
        <translation>获利0.1％</translation>
    </message>
    <message>
        <source>Trigger a take-profit order at 0.1% above the price at which you bought the asset</source>
        <translation type="vanished">以高于您购买资产价格0.1％的价格触发止盈订单</translation>
    </message>
    <message>
        <location filename="../takeprofitriskfactory.cpp" line="204"/>
        <source>Take-profit at 0.2%</source>
        <translation>获利0.2％</translation>
    </message>
    <message>
        <source>Trigger a take-profit order at 0.2% above the price at which you bought the asset</source>
        <translation type="vanished">以高于您购买资产价格0.2％的价格触发止盈订单</translation>
    </message>
    <message>
        <location filename="../takeprofitriskfactory.cpp" line="208"/>
        <source>Take-profit at 0.3%</source>
        <translation>获利为0.3％</translation>
    </message>
    <message>
        <source>Trigger a take-profit order at 0.3% above the price at which you bought the asset</source>
        <translation type="vanished">以高于您购买资产价格0.3％的价格触发止盈订单</translation>
    </message>
    <message>
        <location filename="../takeprofitriskfactory.cpp" line="212"/>
        <source>Take-profit at 0.4%</source>
        <translation>获利为0.4％</translation>
    </message>
    <message>
        <source>Trigger a take-profit order at 0.4% above the price at which you bought the asset</source>
        <translation type="vanished">以高于您购买资产价格0.4％的价格触发止盈订单</translation>
    </message>
    <message>
        <location filename="../takeprofitriskfactory.cpp" line="216"/>
        <source>Take-profit at 0.5%</source>
        <translation>获利为0.5％</translation>
    </message>
    <message>
        <source>Trigger a take-profit order at 0.5% above the price at which you bought the asset</source>
        <translation type="vanished">以高于您购买资产价格0.5％的价格触发止盈订单</translation>
    </message>
    <message>
        <location filename="../takeprofitriskfactory.cpp" line="220"/>
        <source>Take-profit at 0.6%</source>
        <translation>获利为0.6％</translation>
    </message>
    <message>
        <source>Trigger a take-profit order at 0.6% above the price at which you bought the asset</source>
        <translation type="vanished">以高于您购买资产价格0.6％的价格触发止盈订单</translation>
    </message>
    <message>
        <location filename="../takeprofitriskfactory.cpp" line="224"/>
        <source>Take-profit at 0.7%</source>
        <translation>获利为0.7％</translation>
    </message>
    <message>
        <source>Trigger a take-profit order at 0.7% above the price at which you bought the asset</source>
        <translation type="vanished">以高于您购买资产价格0.7％的价格触发止盈订单</translation>
    </message>
    <message>
        <location filename="../takeprofitriskfactory.cpp" line="228"/>
        <source>Take-profit at 0.8%</source>
        <translation>获利为0.8％</translation>
    </message>
    <message>
        <source>Trigger a take-profit order at 0.8% above the price at which you bought the asset</source>
        <translation type="vanished">以高于您购买资产价格0.8％的价格触发止盈订单</translation>
    </message>
    <message>
        <location filename="../takeprofitriskfactory.cpp" line="232"/>
        <source>Take-profit at 0.9%</source>
        <translation>获利0.9％</translation>
    </message>
    <message>
        <source>Trigger a take-profit order at 0.9% above the price at which you bought the asset</source>
        <translation type="vanished">以高于您购买资产价格0.9％的价格触发止盈订单</translation>
    </message>
    <message>
        <source>Take-profit at 1.0%</source>
        <translation type="vanished">获利0.9％ {1.0%?}</translation>
    </message>
    <message>
        <source>Trigger a take-profit order at 1.0% above the price at which you bought the asset</source>
        <translation type="vanished">以高于您购买资产价格0.9％的价格触发止盈订单 {1.0%?}</translation>
    </message>
</context>
</TS>
