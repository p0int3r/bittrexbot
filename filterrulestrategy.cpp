#include "filterrulestrategy.hpp"
#include "bittrexanalyser.hpp"
#include "order.hpp"
#include <QDebug>

FilterRuleStrategy::FilterRuleStrategy(): FilterRuleStrategy(QString(),QString(),trader::Strategy::FrequencyLevel::NONE)
{
    this->setFilterRule(0.0);
    this->setBackwardSteps(0);
}


FilterRuleStrategy::FilterRuleStrategy(QString name,QString description,trader::Strategy::FrequencyLevel frequencyLevel)
{
    this->setName(name);
    this->setDescription(description);
    this->setFrequencyLevel(frequencyLevel);
}

FilterRuleStrategy::~FilterRuleStrategy() {}

double FilterRuleStrategy::getFilterRule() const { return this->filterRule; }

void FilterRuleStrategy::setFilterRule(double value)
{
    this->filterRule=value;
    emit this->filterRuleChanged(this->filterRule);
}

int FilterRuleStrategy::getBackwardSteps() const { return this->backwardSteps; }

void FilterRuleStrategy::setBackwardSteps(int value)
{
    this->backwardSteps=value;
    emit this->backwardStepsChanged(this->backwardSteps);
}


void FilterRuleStrategy::preprocess(const QString &market,const QObject *object)
{
    BittrexAnalyser *analyser=const_cast<BittrexAnalyser *>(static_cast<const BittrexAnalyser *>(object));
    const boost::circular_buffer<double> prices=analyser->getPrices();
    double middle_price=analyser->getMicroEconomicAnalysis()->getMiddlePrice();
    int start=prices.size()-1;
    int end=prices.size()-this->getBackwardSteps();
    bool detected_filter_rule=false;

    for (int i=start;i>=end;i--)
    {
        double momentum=(middle_price-prices[i]);
        double momentumPercentage=(momentum/prices[i]);
        if (momentumPercentage>=this->getFilterRule()) { detected_filter_rule=true; break; }
    }

    if (detected_filter_rule==true) { emit this->preprocessed(market,object); }
}

void FilterRuleStrategy::strategise(const QString &market,const QObject *object)
{
    const BittrexAnalyser *analyser=static_cast<const BittrexAnalyser *>(object);
    double bidPrice=analyser->getMicroEconomicAnalysis()->getMiddlePrice();
    double averageBidVolume=analyser->getMicroEconomicAnalysis()->getAverageBidVolume();
    trader::Order *order=new trader::Order();
    order->setMarket(market);
    order->setQuantity(averageBidVolume);
    order->setPrice(bidPrice);
    order->setType(trader::Order::Type::LIMIT_BUY);
    order->setExchange(trader::Order::Exchange::BITTREX);
    order->setStatus(trader::Order::Status::SUGGESTED);
    emit this->strategised(market,order);
}
