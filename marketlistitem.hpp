#ifndef MARKETLISTITEM_HPP
#define MARKETLISTITEM_HPP

#include <QWidget>
#include <QJsonObject>
#include <QTimer>

namespace Ui { class MarketListItem; }

class MarketListItem: public QWidget
{
    Q_OBJECT

    private:
        QJsonObject marketData;
        QJsonObject marketSummaryData;
        QJsonObject marketTickerData;
        QString marketName;
        QString percentChange;
        QString quoteVolume;
        QString lastPrice;
        Ui::MarketListItem *ui=nullptr;

    public:
        explicit MarketListItem(QWidget *parent=nullptr);
        ~MarketListItem();
        Ui::MarketListItem *getUi() const;
        void setUi(Ui::MarketListItem *value);
        QJsonObject getMarketData() const;
        void setMarketData(const QJsonObject &value);
        QJsonObject getMarketSummaryData() const;
        void setMarketSummaryData(const QJsonObject &value);
        QJsonObject getMarketTickerData() const;
        void setMarketTickerData(const QJsonObject &value);
        void initSignalsAndSlots();
        QString getMarketName() const;
        void setMarketName(const QString &value);
        QString getPercentChange() const;
        void setPercentChange(const QString &value);
        QString getQuoteVolume() const;
        void setQuoteVolume(const QString &value);
        QString getLastPrice() const;
        void setLastPrice(const QString &value);
        void changeColor(const QString &color);

    signals:
        void marketNameChanged(const QString &marketName);
        void percentChangeChanged(const QString &percentChange);
        void quoteVolumeChanged(const QString &quoteVolume);
        void lastPriceChanged(const QString &lastPrice);
};

#endif // MARKETLISTITEM_HPP
