#include "openpositiontable.hpp"
#include "qtablenumericitem.hpp"
#include <QToolButton>


OpenPositionTable::OpenPositionTable(QWidget *parent): QTableWidget(parent)
{
    this->setMap(new QMap<QString,QList<QTableWidgetItem *>>());
    this->initializeSignalsAndSlots();
}

OpenPositionTable::~OpenPositionTable() { delete this->map; }

void OpenPositionTable::initializeSignalsAndSlots()
{
    // TODO: implement.
}

QMap<QString, QList<QTableWidgetItem *> > *OpenPositionTable::getMap() const { return this->map; }

void OpenPositionTable::setMap(QMap<QString, QList<QTableWidgetItem *> > *value) { this->map=value; }

OpenPositionTable::ColorMode OpenPositionTable::getColorMode() const { return this->colorMode; }

void OpenPositionTable::setColorMode(const OpenPositionTable::ColorMode &value) { this->colorMode=value; }

OpenPositionTable::BaseCurrency OpenPositionTable::getBaseCurrency() const { return this->baseCurrency; }

void OpenPositionTable::setBaseCurrency(const OpenPositionTable::BaseCurrency &value) { this->baseCurrency=value; }

void OpenPositionTable::changeToLightMode()
{
    this->setColorMode(OpenPositionTable::ColorMode::LIGHT);
    QMap<QString,QList<QTableWidgetItem *>>::iterator iterator;
    for (iterator=this->getMap()->begin();iterator!=this->getMap()->end();iterator++)
    {
        QString foregroundColor=QString();
        QList<QTableWidgetItem *> tableWidgetList=iterator.value();
        QTableWidgetItem *liquifyItem=tableWidgetList.at(0);
        QToolButton *liquifyButton=static_cast<QToolButton *>(this->cellWidget(liquifyItem->row(),liquifyItem->column()));
        if (this->getBaseCurrency()==OpenPositionTable::BaseCurrency::USD_CURRENCY) { liquifyButton->setIcon(QIcon(":/icons/light_mode/liquify_usd_icon.png")); }
        if (this->getBaseCurrency()==OpenPositionTable::BaseCurrency::BTC_CURRENCY) { liquifyButton->setIcon(QIcon(":/icons/light_mode/liquify_bitcoin_icon.png")); }
        if (this->getBaseCurrency()==OpenPositionTable::BaseCurrency::ETH_CURRENCY) { liquifyButton->setIcon(QIcon(":/icons/light_mode/liquify_ethereum_icon.png")); }
        if (this->getBaseCurrency()==OpenPositionTable::BaseCurrency::USDT_CURRENCY) { liquifyButton->setIcon(QIcon(":/icons/light_mode/liquify_usdt_icon.png")); }
        if (this->getBaseCurrency()==OpenPositionTable::BaseCurrency::EUR_CURRENCY) { liquifyButton->setIcon(QIcon(":/icons/light_mode/liquify_eur_icon.png")); }
        QTableWidgetItem *realisedPnLItem=tableWidgetList.at(6);
        double realisedPnL=realisedPnLItem->text().toDouble();
        if (realisedPnL>0.0) { foregroundColor=QString("green"); }
        if (qFuzzyIsNull(realisedPnL)==true) { foregroundColor=QString("orange"); }
        if (realisedPnL<0.0) { foregroundColor=QString("red"); }
        realisedPnLItem->setForeground(QColor(foregroundColor));
        QTableWidgetItem *unrealisedPnLItem=tableWidgetList.at(7);
        double unrealisedPnL=unrealisedPnLItem->text().toDouble();
        if (unrealisedPnL>0.0) { foregroundColor=QString("green"); }
        if (qFuzzyIsNull(unrealisedPnL)==true) { foregroundColor=QString("orange"); }
        if (unrealisedPnL<0.0) { foregroundColor=QString("red"); }
        unrealisedPnLItem->setForeground(QColor(foregroundColor));
        QTableWidgetItem *quantityItem=tableWidgetList.at(8);
        quantityItem->setForeground(QColor(0,85,127));
        QTableWidgetItem *priceItem=tableWidgetList.at(9);
        priceItem->setForeground(QColor("purple"));
    }
}


void OpenPositionTable::changeToDarkMode()
{
    this->setColorMode(OpenPositionTable::ColorMode::DARK);
    QMap<QString,QList<QTableWidgetItem *>>::iterator iterator;
    for (iterator=this->getMap()->begin();iterator!=this->getMap()->end();iterator++)
    {
        QString foregroundColor=QString();
        QList<QTableWidgetItem *> tableWidgetList=iterator.value();
        QTableWidgetItem *liquifyItem=tableWidgetList.at(0);
        QToolButton *liquifyButton=static_cast<QToolButton *>(this->cellWidget(liquifyItem->row(),liquifyItem->column()));
        if (this->getBaseCurrency()==OpenPositionTable::BaseCurrency::USD_CURRENCY) { liquifyButton->setIcon(QIcon(":/icons/dark_mode/liquify_usd_icon.png")); }
        if (this->getBaseCurrency()==OpenPositionTable::BaseCurrency::BTC_CURRENCY) { liquifyButton->setIcon(QIcon(":/icons/dark_mode/liquify_bitcoin_icon.png")); }
        if (this->getBaseCurrency()==OpenPositionTable::BaseCurrency::ETH_CURRENCY) { liquifyButton->setIcon(QIcon(":/icons/dark_mode/liquify_ethereum_icon.png")); }
        if (this->getBaseCurrency()==OpenPositionTable::BaseCurrency::USDT_CURRENCY) { liquifyButton->setIcon(QIcon(":/icons/dark_mode/liquify_usdt_icon.png")); }
        if (this->getBaseCurrency()==OpenPositionTable::BaseCurrency::EUR_CURRENCY) { liquifyButton->setIcon(QIcon(":/icons/dark_mode/liquify_eur_icon.png")); }
        QTableWidgetItem *realisedPnLItem=tableWidgetList.at(6);
        double realisedPnL=realisedPnLItem->text().toDouble();
        if (realisedPnL>0.0) { foregroundColor=QString("lime"); }
        if (qFuzzyIsNull(realisedPnL)==true) { foregroundColor=QString("yellow"); }
        if (realisedPnL<0.0) { foregroundColor=QString("orange"); }
        realisedPnLItem->setForeground(QColor(foregroundColor));
        QTableWidgetItem *unrealisedPnLItem=tableWidgetList.at(7);
        double unrealisedPnL=unrealisedPnLItem->text().toDouble();
        if (unrealisedPnL>0.0) { foregroundColor=QString("lime"); }
        if (qFuzzyIsNull(unrealisedPnL)==true) { foregroundColor=QString("yellow"); }
        if (unrealisedPnL<0.0) { foregroundColor=QString("orange"); }
        unrealisedPnLItem->setForeground(QColor(foregroundColor));
        QTableWidgetItem *quantityItem=tableWidgetList.at(8);
        quantityItem->setForeground(QColor("cyan"));
        QTableWidgetItem *priceItem=tableWidgetList.at(9);
        priceItem->setForeground(QColor("#ffddf4"));
    }
}


void OpenPositionTable::insertTableItems(const QString &market,const trader::Position *position)
{
    QString foregroundColor=QString();
    int currentRow=this->rowCount();
    this->insertRow(currentRow);
    QList<QTableWidgetItem *> tableWidgetList=QList<QTableWidgetItem *>();
    QToolButton *liquifyButton=new QToolButton();

    if (this->getColorMode()==OpenPositionTable::ColorMode::LIGHT)
    {
        if (this->getBaseCurrency()==OpenPositionTable::BaseCurrency::USD_CURRENCY) { liquifyButton->setIcon(QIcon(":/icons/light_mode/liquify_usd_icon.png")); }
        if (this->getBaseCurrency()==OpenPositionTable::BaseCurrency::BTC_CURRENCY) { liquifyButton->setIcon(QIcon(":/icons/light_mode/liquify_bitcoin_icon.png")); }
        if (this->getBaseCurrency()==OpenPositionTable::BaseCurrency::ETH_CURRENCY) { liquifyButton->setIcon(QIcon(":/icons/light_mode/liquify_ethereum_icon.png")); }
        if (this->getBaseCurrency()==OpenPositionTable::BaseCurrency::USDT_CURRENCY) { liquifyButton->setIcon(QIcon(":/icons/light_mode/liquify_usdt_icon.png")); }
        if (this->getBaseCurrency()==OpenPositionTable::BaseCurrency::EUR_CURRENCY) { liquifyButton->setIcon(QIcon(":/icons/light_mode/liquify_eur_icon.png")); }
    }

    if (this->getColorMode()==OpenPositionTable::ColorMode::DARK)
    {
        if (this->getBaseCurrency()==OpenPositionTable::BaseCurrency::USD_CURRENCY) { liquifyButton->setIcon(QIcon(":/icons/dark_mode/liquify_usd_icon.png")); }
        if (this->getBaseCurrency()==OpenPositionTable::BaseCurrency::BTC_CURRENCY) { liquifyButton->setIcon(QIcon(":/icons/dark_mode/liquify_bitcoin_icon.png")); }
        if (this->getBaseCurrency()==OpenPositionTable::BaseCurrency::ETH_CURRENCY) { liquifyButton->setIcon(QIcon(":/icons/dark_mode/liquify_ethereum_icon.png")); }
        if (this->getBaseCurrency()==OpenPositionTable::BaseCurrency::USDT_CURRENCY) { liquifyButton->setIcon(QIcon(":/icons/dark_mode/liquify_usdt_icon.png")); }
        if (this->getBaseCurrency()==OpenPositionTable::BaseCurrency::EUR_CURRENCY) { liquifyButton->setIcon(QIcon(":/icons/dark_mode/liquify_eur_icon.png")); }
    }

    this->connect(liquifyButton,&QToolButton::clicked,this,[=]() { this->liquifyPosition(market,position); });
    QTableWidgetItem *liquifyItem=new QTableWidgetItem();
    liquifyItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    this->setItem(currentRow,0,liquifyItem);
    this->setCellWidget(currentRow,0,liquifyButton);

    QTableWidgetItem *actionItem=new QTableWidgetItem();
    actionItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    trader::Position::Action action=position->getAction();
    QString actionStr=QString();
    if (action==trader::Position::Action::SOLD) { actionStr=QString("SOLD"); }
    if (action==trader::Position::Action::BOUGHT) { actionStr=QString("BOUGHT"); }
    actionItem->setText(actionStr);
    actionItem->setToolTip(actionStr);
    this->setItem(currentRow,1,actionItem);

    QTableWidgetItem *marketItem=new QTableWidgetItem();
    marketItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    QString marketStr=position->getMarket();
    marketItem->setText(marketStr);
    marketItem->setToolTip(marketStr);
    this->setItem(currentRow,2,marketItem);

    QTableNumericItem *averagePriceItem=new QTableNumericItem();
    averagePriceItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double averagePrice=position->getAveragePrice();
    averagePriceItem->setText(QString::number(averagePrice,'g'));
    averagePriceItem->setToolTip(QString::number(averagePrice,'f',8));
    this->setItem(currentRow,3,averagePriceItem);

    QTableNumericItem *marketValueItem=new QTableNumericItem();
    marketValueItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double marketValue=position->getMarketValue();
    marketValueItem->setText(QString::number(marketValue,'g'));
    marketValueItem->setToolTip(QString::number(marketValue,'f',8));
    this->setItem(currentRow,4,marketValueItem);

    QTableNumericItem *costBasisItem=new QTableNumericItem();
    costBasisItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double costBasis=position->getCostBasis();
    costBasisItem->setText(QString::number(costBasis,'g'));
    costBasisItem->setToolTip(QString::number(costBasis,'f',8));
    this->setItem(currentRow,5,costBasisItem);

    QTableNumericItem *realisedPnLItem=new QTableNumericItem();
    realisedPnLItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double realisedPnL=position->getRealisedPnL();
    if (this->getColorMode()==OpenPositionTable::ColorMode::LIGHT)
    {
        if (realisedPnL>0.0) { foregroundColor=QString("green"); }
        if (qFuzzyIsNull(realisedPnL)==true) { foregroundColor=QString("orange"); }
        if (realisedPnL<0.0) { foregroundColor=QString("red"); }
    }
    if (this->getColorMode()==OpenPositionTable::ColorMode::DARK)
    {
        if (realisedPnL>0.0) { foregroundColor=QString("lime"); }
        if (qFuzzyIsNull(realisedPnL)==true) { foregroundColor=QString("yellow"); }
        if (realisedPnL<0.0) { foregroundColor=QString("orange"); }
    }
    realisedPnLItem->setForeground(QColor(foregroundColor));
    realisedPnLItem->setText(QString::number(realisedPnL,'g'));
    realisedPnLItem->setToolTip(QString::number(realisedPnL,'f',8));
    this->setItem(currentRow,6,realisedPnLItem);

    QTableNumericItem *unrealisedPnLItem=new QTableNumericItem();
    unrealisedPnLItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double unrealisedPnL=position->getUnrealisedPnL();
    if (this->getColorMode()==OpenPositionTable::ColorMode::LIGHT)
    {
        if (unrealisedPnL>0.0) { foregroundColor=QString("green"); }
        if (qFuzzyIsNull(unrealisedPnL)==true) { foregroundColor=QString("orange"); }
        if (unrealisedPnL<0.0) { foregroundColor=QString("red"); }
    }
    if (this->getColorMode()==OpenPositionTable::ColorMode::DARK)
    {
        if (unrealisedPnL>0.0) { foregroundColor=QString("lime"); }
        if (qFuzzyIsNull(unrealisedPnL)==true) { foregroundColor=QString("yellow"); }
        if (unrealisedPnL<0.0) { foregroundColor=QString("orange"); }
    }
    unrealisedPnLItem->setForeground(QColor(foregroundColor));
    unrealisedPnLItem->setText(QString::number(unrealisedPnL,'g'));
    unrealisedPnLItem->setToolTip(QString::number(unrealisedPnL,'f',8));
    this->setItem(currentRow,7,unrealisedPnLItem);

    QTableNumericItem *quantityItem=new QTableNumericItem();
    quantityItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double quantity=position->getQuantity();
    if (this->getColorMode()==OpenPositionTable::ColorMode::LIGHT) { quantityItem->setForeground(QColor(0,85,127)); }
    if (this->getColorMode()==OpenPositionTable::ColorMode::DARK) { quantityItem->setForeground(QColor("cyan")); }
    quantityItem->setText(QString::number(quantity,'g'));
    quantityItem->setToolTip(QString::number(quantity,'f',8));
    this->setItem(currentRow,8,quantityItem);

    QTableNumericItem *priceItem=new QTableNumericItem();
    priceItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double price=position->getPrice();
    if (this->getColorMode()==OpenPositionTable::ColorMode::LIGHT) { priceItem->setForeground(QColor("purple")); }
    if (this->getColorMode()==OpenPositionTable::ColorMode::DARK) { priceItem->setForeground(QColor("#ffddf4")); }
    priceItem->setText(QString::number(price,'g'));
    priceItem->setToolTip(QString::number(price,'f',8));
    this->setItem(currentRow,9,priceItem);

    QTableNumericItem *buysItem=new QTableNumericItem();
    buysItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double buys=position->getBuys();
    buysItem->setText(QString::number(buys,'g'));
    buysItem->setToolTip(QString::number(buys,'f',8));
    this->setItem(currentRow,10,buysItem);

    QTableNumericItem *sellsItem=new QTableNumericItem();
    sellsItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double sells=position->getSells();
    sellsItem->setText(QString::number(sells,'g'));
    sellsItem->setToolTip(QString::number(sells,'f',8));
    this->setItem(currentRow,11,sellsItem);

    QTableNumericItem *netItem=new QTableNumericItem();
    netItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double net=position->getNet();
    netItem->setText(QString::number(net,'g'));
    netItem->setToolTip(QString::number(net,'f',8));
    this->setItem(currentRow,12,netItem);

    QTableNumericItem *netTotalItem=new QTableNumericItem();
    netTotalItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double netTotal=position->getNetTotal();
    netTotalItem->setText(QString::number(netTotal,'g'));
    netTotalItem->setToolTip(QString::number(netTotal,'f',8));
    this->setItem(currentRow,13,netTotalItem);

    QTableNumericItem *netInclusiveCommissionItem=new QTableNumericItem();
    netInclusiveCommissionItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double netInclusiveCommission=position->getNetInclusiveCommission();
    netInclusiveCommissionItem->setText(QString::number(netInclusiveCommission,'g'));
    netInclusiveCommissionItem->setToolTip(QString::number(netInclusiveCommission,'f',8));
    this->setItem(currentRow,14,netInclusiveCommissionItem);

    tableWidgetList.insert(0,liquifyItem);
    tableWidgetList.insert(1,actionItem);
    tableWidgetList.insert(2,marketItem);
    tableWidgetList.insert(3,averagePriceItem);
    tableWidgetList.insert(4,marketValueItem);
    tableWidgetList.insert(5,costBasisItem);
    tableWidgetList.insert(6,realisedPnLItem);
    tableWidgetList.insert(7,unrealisedPnLItem);
    tableWidgetList.insert(8,quantityItem);
    tableWidgetList.insert(9,priceItem);
    tableWidgetList.insert(10,buysItem);
    tableWidgetList.insert(11,sellsItem);
    tableWidgetList.insert(12,netItem);
    tableWidgetList.insert(13,netTotalItem);
    tableWidgetList.insert(14,netInclusiveCommissionItem);
    this->getMap()->insert(market,tableWidgetList);

    emit this->tableItemsInserted(market,position);
}


void OpenPositionTable::cancelTableItems(const QString &market,const trader::Position *position)
{
    QList<QTableWidgetItem *> tableWidgetList=this->getMap()->value(market);
    if (tableWidgetList.length()==0) { return; }
    int current_row=tableWidgetList.at(0)->row();
    this->removeRow(current_row);
    this->getMap()->remove(market);
    emit this->tableItemsCanceled(market,position);
}



void OpenPositionTable::removeTableItems(const QString &market,const trader::Position *position)
{
    QList<QTableWidgetItem *> tableWidgetList=this->getMap()->value(market);
    if (tableWidgetList.length()==0) { return; }
    int current_row=tableWidgetList.at(0)->row();
    this->removeRow(current_row);
    this->getMap()->remove(market);
    emit this->tableItemsRemoved(market,position);
}


void OpenPositionTable::modifyTableItems(const QString &market,const trader::Position *position)
{
    QList<QTableWidgetItem *> tableWidgetList=this->getMap()->value(market);
    if (tableWidgetList.length()==0) { return; }
    QString actionStr=QString();
    QString foregroundColor=QString();
    QTableWidgetItem *actionItem=tableWidgetList.at(1);
    trader::Position::Action action=position->getAction();
    if (action==trader::Position::Action::SOLD) { actionStr=QString("SOLD"); }
    if (action==trader::Position::Action::BOUGHT) { actionStr=QString("BOUGHT"); }
    actionItem->setText(actionStr);
    actionItem->setToolTip(actionStr);

    QTableWidgetItem *averagePriceItem=tableWidgetList.at(3);
    double averagePrice=position->getAveragePrice();
    averagePriceItem->setText(QString::number(averagePrice,'g'));
    averagePriceItem->setToolTip(QString::number(averagePrice,'f',8));

    QTableWidgetItem *marketValueItem=tableWidgetList.at(4);
    double marketValue=position->getMarketValue();
    marketValueItem->setText(QString::number(marketValue,'g'));
    marketValueItem->setToolTip(QString::number(marketValue,'f',8));

    QTableWidgetItem *costBasisItem=tableWidgetList.at(5);
    costBasisItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double costBasis=position->getCostBasis();
    costBasisItem->setText(QString::number(costBasis,'g'));
    costBasisItem->setToolTip(QString::number(costBasis,'f',8));

    QTableWidgetItem *realisedPnLItem=tableWidgetList.at(6);
    double realisedPnL=position->getRealisedPnL();
    if (this->getColorMode()==OpenPositionTable::ColorMode::LIGHT)
    {
        if (realisedPnL>0.0) { foregroundColor=QString("green"); }
        if (qFuzzyIsNull(realisedPnL)==true) { foregroundColor=QString("orange"); }
        if (realisedPnL<0.0) { foregroundColor=QString("red"); }
    }
    if (this->getColorMode()==OpenPositionTable::ColorMode::DARK)
    {
        if (realisedPnL>0.0) { foregroundColor=QString("lime"); }
        if (qFuzzyIsNull(realisedPnL)==true) { foregroundColor=QString("yellow"); }
        if (realisedPnL<0.0) { foregroundColor=QString("orange"); }
    }
    realisedPnLItem->setForeground(QColor(foregroundColor));
    realisedPnLItem->setText(QString::number(realisedPnL,'g'));
    realisedPnLItem->setToolTip(QString::number(realisedPnL,'f',8));

    QTableWidgetItem *unrealisedPnLItem=tableWidgetList.at(7);
    double unrealisedPnL=position->getUnrealisedPnL();
    if (this->getColorMode()==OpenPositionTable::ColorMode::LIGHT)
    {
        if (unrealisedPnL>0.0) { foregroundColor=QString("green"); }
        if (qFuzzyIsNull(unrealisedPnL)==true) { foregroundColor=QString("orange"); }
        if (unrealisedPnL<0.0) { foregroundColor=QString("red"); }
    }
    if (this->getColorMode()==OpenPositionTable::ColorMode::DARK)
    {
        if (unrealisedPnL>0.0) { foregroundColor=QString("lime"); }
        if (qFuzzyIsNull(unrealisedPnL)==true) { foregroundColor=QString("yellow"); }
        if (unrealisedPnL<0.0) { foregroundColor=QString("orange"); }
    }
    unrealisedPnLItem->setForeground(QColor(foregroundColor));
    unrealisedPnLItem->setText(QString::number(unrealisedPnL,'g'));
    unrealisedPnLItem->setToolTip(QString::number(unrealisedPnL,'f',8));

    QTableWidgetItem *quantityItem=tableWidgetList.at(8);
    double quantity=position->getQuantity();
    if (this->getColorMode()==OpenPositionTable::ColorMode::LIGHT) { quantityItem->setForeground(QColor(0,85,127)); }
    if (this->getColorMode()==OpenPositionTable::ColorMode::DARK) { quantityItem->setForeground(QColor("cyan")); }
    quantityItem->setText(QString::number(quantity,'g'));
    quantityItem->setToolTip(QString::number(quantity,'f',8));

    QTableWidgetItem *priceItem=tableWidgetList.at(9);
    double price=position->getPrice();
    if (this->getColorMode()==OpenPositionTable::ColorMode::LIGHT) { priceItem->setForeground(QColor("purple")); }
    if (this->getColorMode()==OpenPositionTable::ColorMode::DARK) { priceItem->setForeground(QColor("#ffddf4")); }
    priceItem->setText(QString::number(price,'g'));
    priceItem->setToolTip(QString::number(price,'f',8));

    QTableWidgetItem *buysItem=tableWidgetList.at(10);
    double buys=position->getBuys();
    buysItem->setText(QString::number(buys,'g'));
    buysItem->setToolTip(QString::number(buys,'f',8));

    QTableWidgetItem *sellsItem=tableWidgetList.at(11);
    double sells=position->getSells();
    sellsItem->setText(QString::number(sells,'g'));
    sellsItem->setToolTip(QString::number(sells,'f',8));

    QTableWidgetItem *netItem=tableWidgetList.at(12);
    netItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double net=position->getNet();
    netItem->setText(QString::number(net,'g'));
    netItem->setToolTip(QString::number(net,'f',8));

    QTableWidgetItem *netTotalItem=tableWidgetList.at(13);
    netTotalItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double netTotal=position->getNetTotal();
    netTotalItem->setText(QString::number(netTotal,'g'));
    netTotalItem->setToolTip(QString::number(netTotal,'f',8));

    QTableWidgetItem *netInclusiveCommissionItem=tableWidgetList.at(14);
    netInclusiveCommissionItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double netInclusiveCommission=position->getNetInclusiveCommission();
    netInclusiveCommissionItem->setText(QString::number(netInclusiveCommission,'g'));
    netInclusiveCommissionItem->setToolTip(QString::number(netInclusiveCommission,'f',8));

    emit this->tableItemsModified(market,position);
}


void OpenPositionTable::liquifyPosition(const QString &market,const trader::Position *position)
{
    double askPrice=position->getMiddlePrice();
    double volume=position->getQuantity();
    trader::Order *order=new trader::Order();
    order->setMarket(market);
    order->setQuantity(volume);
    order->setPrice(askPrice);
    order->setType(trader::Order::Type::LIMIT_SELL);
    order->setExchange(trader::Order::Exchange::BITTREX);
    order->setStatus(trader::Order::Status::SUGGESTED);
    emit this->positionLiquified(market,order);
}
