 #include "bittrexfilterer.hpp"
#include "bittrexasset.hpp"


BittrexFilterer::BittrexFilterer(): BittrexFilterer(nullptr) {}

BittrexFilterer::BittrexFilterer(trader::Trader *trader) { this->setTrader(trader); }

BittrexFilterer::~BittrexFilterer() { this->setTrader(nullptr); }

trader::Trader *BittrexFilterer::getTrader() const { return this->trader; }

void BittrexFilterer::setTrader(trader::Trader *value) { this->trader=value; }

void BittrexFilterer::filter(const QString &market,const QObject *object)
{
    const trader::Order *order=static_cast<const trader::Order *>(object);
    trader::Order::Type order_type=order->getType();

    if (order_type==trader::Order::Type::LIMIT_BUY)
    {
        trader::Trader *trader=this->getTrader();
        bool contains_open_order=false;
        bool contains_suggested_order=false;
        bool contains_open_position=false;
        BittrexAsset *bittrex_asset=static_cast<BittrexAsset *>(trader->getAssetManager()->getAsset(market));
        int current_round=bittrex_asset->getCurrentRound();
        int trading_rounds=bittrex_asset->getTradingRounds();
        bool exceeded_trading_rounds=(current_round>=trading_rounds);
        if (exceeded_trading_rounds==true) { emit this->declined(market,object); return; }
        trader::OrderManager *orderManager=this->getTrader()->getOrderManager();

        contains_open_order=orderManager->containsOpenMarketOrder(market);
        if (contains_open_order==true) { emit this->declined(market,object); return; }

        contains_suggested_order=orderManager->containsSuggestedMarketOrder(market);
        if (contains_suggested_order==true) { emit this->declined(market,object); return; }

        trader::Portfolio *usd_portfolio=trader->getPortfolioManager()->getPortfolio("USD-Portfolio");
        trader::Portfolio *btc_portfolio=trader->getPortfolioManager()->getPortfolio("BTC-Portfolio");
        trader::Portfolio *eth_portfolio=trader->getPortfolioManager()->getPortfolio("ETH-Portfolio");
        trader::Portfolio *usdt_portfolio=trader->getPortfolioManager()->getPortfolio("USDT-Portfolio");
        trader::Portfolio *eur_portfolio=trader->getPortfolioManager()->getPortfolio("EUR-Portfolio");

        contains_open_position=usd_portfolio->getOpenPositions()->contains(market);
        if (contains_open_position==true) { emit this->declined(market,object); return; }

        contains_open_position=btc_portfolio->getOpenPositions()->contains(market);
        if (contains_open_position==true) { emit this->declined(market,object); return; }

        contains_open_position=eth_portfolio->getOpenPositions()->contains(market);
        if (contains_open_position==true) { emit this->declined(market,object); return; }

        contains_open_position=usdt_portfolio->getOpenPositions()->contains(market);
        if (contains_open_position==true) { emit this->declined(market,object); return; }

        contains_open_position=eur_portfolio->getOpenPositions()->contains(market);
        if (contains_open_position==true) { emit this->declined(market,object); return; }

        emit this->accepted(market,object);
    }

    if (order_type==trader::Order::Type::LIMIT_SELL)
    {
        bool contains_open_order=false;
        bool contains_suggested_order=false;
        trader::OrderManager *orderManager=this->getTrader()->getOrderManager();
        contains_open_order=orderManager->containsOpenMarketOrder(market);
        if (contains_open_order==true) { emit this->declined(market,object); return; }
        contains_suggested_order=orderManager->containsSuggestedMarketOrder(market);
        if (contains_suggested_order==true) { emit this->declined(market,object); return; }
        emit this->accepted(market,object);
    }
}
