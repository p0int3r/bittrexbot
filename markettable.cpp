#include <QTableWidgetItem>
#include "qtablenumericitem.hpp"
#include "markettable.hpp"


MarketTable::MarketTable(QWidget *parent): QTableWidget(parent)
{
    this->setMap(new QMap<QString,QList<QTableWidgetItem *>>());
    this->initializeSignalsAndSlots();
}

MarketTable::~MarketTable() { delete this->map; }

MarketTable::ColorMode MarketTable::getColorMode() const { return this->colorMode; }

void MarketTable::setColorMode(const MarketTable::ColorMode &value) { this->colorMode=value; }

QMap<QString,QList<QTableWidgetItem *>> *MarketTable::getMap() const { return this->map; }

void MarketTable::setMap(QMap<QString,QList<QTableWidgetItem *>> *value) { this->map=value; }


void MarketTable::initializeSignalsAndSlots()
{
    this->connect(this,&MarketTable::cellClicked,this,[=](int row,int column)
    {
        if (column!=0) { return; }
        QTableWidgetItem *selectionItem=this->item(row,column);
        QTableWidgetItem *marketItem=this->item(row,column+1);
        QString market=marketItem->text();
        if (selectionItem->checkState()==Qt::Checked) { emit this->marketSelected(market); }
        if (selectionItem->checkState()==Qt::Unchecked) { emit this->marketDeselected(market); }
    });
}


void MarketTable::changeToLightMode()
{
    this->setColorMode(MarketTable::ColorMode::LIGHT);
    QMap<QString,QList<QTableWidgetItem *>>::iterator iterator;
    for (iterator=this->getMap()->begin();iterator!=this->getMap()->end();iterator++)
    {
        QList<QTableWidgetItem *> tableWidgetList=iterator.value();
        QTableWidgetItem *volumeItem=tableWidgetList.at(3);
        QTableWidgetItem *changeItem=tableWidgetList.at(4);
        QTableWidgetItem *lastPriceItem=tableWidgetList.at(5);
        QTableWidgetItem *hr24highItem=tableWidgetList.at(6);
        QTableWidgetItem *hr24lowItem=tableWidgetList.at(7);
        QTableWidgetItem *spreadItem=tableWidgetList.at(8);
        volumeItem->setForeground(QColor(0,85,127));
        lastPriceItem->setForeground(QColor("purple"));
        spreadItem->setForeground(QColor("SaddleBrown"));
        hr24lowItem->setForeground(QColor("blue"));
        hr24highItem->setForeground(QColor("blue"));
        if (changeItem->text().toDouble()<0.0) { changeItem->setForeground(QColor("red")); }
        if (changeItem->text().toDouble()>0.0) { changeItem->setForeground(QColor("green")); }
        if (qFuzzyIsNull(changeItem->text().toDouble())==true) { changeItem->setForeground(QColor("orange")); }
    }
}


void MarketTable::changeToDarkMode()
{
    this->setColorMode(MarketTable::ColorMode::DARK);
    QMap<QString,QList<QTableWidgetItem *>>::iterator iterator;
    for (iterator=this->getMap()->begin();iterator!=this->getMap()->end();iterator++)
    {
        QList<QTableWidgetItem *> tableWidgetList=iterator.value();
        QTableWidgetItem *volumeItem=tableWidgetList.at(3);
        QTableWidgetItem *changeItem=tableWidgetList.at(4);
        QTableWidgetItem *lastPriceItem=tableWidgetList.at(5);
        QTableWidgetItem *hr24highItem=tableWidgetList.at(6);
        QTableWidgetItem *hr24lowItem=tableWidgetList.at(7);
        QTableWidgetItem *spreadItem=tableWidgetList.at(8);
        volumeItem->setForeground(QColor("Cyan"));
        lastPriceItem->setForeground(QColor("#ffddf4"));
        spreadItem->setForeground(QColor("YellowGreen"));
        hr24highItem->setForeground(QColor("blue"));
        hr24lowItem->setForeground(QColor("blue"));
        if (changeItem->text().toDouble()<0.0) { changeItem->setForeground(QColor("orange")); }
        if (changeItem->text().toDouble()>0.0) { changeItem->setForeground(QColor("lime")); }
        if (qFuzzyIsNull(changeItem->text().toDouble())==true) { changeItem->setForeground(QColor("yellow")); }
    }
}


void MarketTable::insertTableItems(const QString &market,const QJsonObject &data)
{
    int currentRow=this->rowCount();
    this->insertRow(currentRow);
    QTableWidgetItem *checkedItem=new QTableWidgetItem();
    checkedItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    checkedItem->setCheckState(Qt::CheckState::Unchecked);
    this->setItem(currentRow,0,checkedItem);

    QTableWidgetItem *marketItem=new QTableWidgetItem();
    marketItem->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    marketItem->setText(market);
    marketItem->setToolTip(market);
    this->setItem(currentRow,1,marketItem);

    QTableWidgetItem *currencyItem=new QTableWidgetItem();
    currencyItem->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    QStringList market_split=market.split("-");
    if (market_split.count()>0)
    {
        currencyItem->setText(market_split[0]);
        currencyItem->setToolTip(market_split[0]);
    }
    this->setItem(currentRow,2,currencyItem);

    QTableNumericItem *volumeItem=new QTableNumericItem();
    volumeItem->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    volumeItem->setText(QString::number(0.0));
    volumeItem->setToolTip(QString::number(0.0));
    this->setItem(currentRow,3,volumeItem);

    QTableNumericItem *changeItem=new QTableNumericItem();
    changeItem->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    changeItem->setText(QString::number(0.0));
    changeItem->setToolTip(QString::number(0.0));
    if (this->getColorMode()==MarketTable::ColorMode::LIGHT) { changeItem->setForeground(QColor("orange")); }
    if (this->getColorMode()==MarketTable::ColorMode::DARK) { changeItem->setForeground(QColor("yellow")); }
    this->setItem(currentRow,4,changeItem);

    QTableNumericItem *lastPriceItem=new QTableNumericItem();
    lastPriceItem->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    lastPriceItem->setText(QString::number(0.0));
    lastPriceItem->setToolTip(QString::number(0.0));
    this->setItem(currentRow,5,lastPriceItem);

    QTableNumericItem *hr24highItem=new QTableNumericItem();
    hr24highItem->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    hr24highItem->setText(QString::number(0.0));
    hr24highItem->setToolTip(QString::number(0.0));
    this->setItem(currentRow,6,hr24highItem);

    QTableNumericItem *hr24lowItem=new QTableNumericItem();
    hr24lowItem->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    hr24lowItem->setText(QString::number(0.0));
    hr24lowItem->setToolTip(QString::number(0.0));
    this->setItem(currentRow,7,hr24lowItem);

    QTableNumericItem *spreadItem=new QTableNumericItem();
    spreadItem->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    spreadItem->setText(QString::number(0.0));
    spreadItem->setToolTip(QString::number(0.0));
    this->setItem(currentRow,8,spreadItem);

    QTableWidgetItem *addedItem=new QTableWidgetItem();
    addedItem->setTextAlignment(Qt::AlignHCenter |Qt::AlignVCenter);
    addedItem->setText(data.value("createdAt").toString());
    addedItem->setToolTip(data.value("createdAt").toString());
    this->setItem(currentRow,9,addedItem);

    QList<QTableWidgetItem *> tableWidgetList=QList<QTableWidgetItem *>();

    if (this->getColorMode()==MarketTable::ColorMode::LIGHT)
    {
        volumeItem->setForeground(QColor(0,85,127));
        lastPriceItem->setForeground(QColor("purple"));
        spreadItem->setForeground(QColor("SaddleBrown"));
        hr24lowItem->setForeground(QColor("blue"));
        hr24highItem->setForeground(QColor("blue"));
    }

    if (this->getColorMode()==MarketTable::ColorMode::DARK)
    {
        volumeItem->setForeground(QColor("Cyan"));
        lastPriceItem->setForeground(QColor("#ffddf4"));
        spreadItem->setForeground(QColor("YellowGreen"));
        hr24highItem->setForeground(QColor("blue"));
        hr24lowItem->setForeground(QColor("blue"));
    }

    tableWidgetList.insert(0,checkedItem);
    tableWidgetList.insert(1,marketItem);
    tableWidgetList.insert(2,currencyItem);
    tableWidgetList.insert(3,volumeItem);
    tableWidgetList.insert(4,changeItem);
    tableWidgetList.insert(5,lastPriceItem);
    tableWidgetList.insert(6,hr24highItem);
    tableWidgetList.insert(7,hr24lowItem);
    tableWidgetList.insert(8,spreadItem);
    tableWidgetList.insert(9,addedItem);
    this->getMap()->insert(market,tableWidgetList);
    emit this->tableItemsInserted(market,data);
}

void MarketTable::removeTableItems(const QString &market,const QJsonObject &data)
{
    QList<QTableWidgetItem *> tableWidgetList=this->getMap()->value(market);
    int current_row=tableWidgetList.at(0)->row();
    this->removeRow(current_row);
    this->getMap()->remove(market);
    emit this->tableItemsRemoved(market,data);
}

void MarketTable::modifyTableItems(const QString &market,const QJsonObject &data)
{
    QList<QTableWidgetItem *> tableWidgetList=this->getMap()->value(market);

    if (data.contains("quoteVolume")==true)
    {
        QTableWidgetItem *quote_volume_item=tableWidgetList.at(3);
        double quote_volume=data.value("quoteVolume").toString().toDouble();
        quote_volume_item->setText(QString::number(quote_volume,'g'));
        quote_volume_item->setToolTip(QString::number(quote_volume,'f',8));
    }

    if (data.contains("percentChange")==true)
    {
        QTableWidgetItem *percent_change_item=tableWidgetList.at(4);
        double percent_change=data.value("percentChange").toString().toDouble();

        if (this->getColorMode()==MarketTable::ColorMode::LIGHT)
        {
            if (percent_change<0.0) { percent_change_item->setForeground(QColor("red")); }
            if (percent_change>0.0) { percent_change_item->setForeground(QColor("green")); }
            if (qFuzzyIsNull(percent_change)==true) { percent_change_item->setForeground(QColor("orange")); }
        }

        if (this->getColorMode()==MarketTable::ColorMode::DARK)
        {
            if (percent_change<0.0) { percent_change_item->setForeground(QColor("orange")); }
            if (percent_change>0.0) { percent_change_item->setForeground(QColor("lime")); }
            if (qFuzzyIsNull(percent_change)==true) { percent_change_item->setForeground(QColor("yellow")); }
        }

        percent_change_item->setText(QString::number(percent_change,'f'));
        percent_change_item->setToolTip(QString::number(percent_change,'f',8));
    }

    if (data.contains("lastTradeRate")==true)
    {
        QTableWidgetItem *last_price_item=tableWidgetList.at(5);
        double last_price=data.value("lastTradeRate").toString().toDouble();
        last_price_item->setText(QString::number(last_price,'g'));
        last_price_item->setToolTip(QString::number(last_price,'f',8));
    }

    if (data.contains("high")==true)
    {
        QTableWidgetItem *high_item=tableWidgetList.at(6);
        double high=data.value("high").toString().toDouble();
        high_item->setText(QString::number(high,'g'));
        high_item->setToolTip(QString::number(high,'f',8));
    }

    if (data.contains("low")==true)
    {
        QTableWidgetItem *low_item=tableWidgetList.at(7);
        double low=data.value("low").toString().toDouble();
        low_item->setText(QString::number(low,'g'));
        low_item->setToolTip(QString::number(low,'f',8));
    }

    if (data.contains("bidRate")==true && data.contains("askRate")==true)
    {
        double spread=0.0;
        QTableWidgetItem *spread_item=tableWidgetList.at(8);
        double bid=data.value("bidRate").toString().toDouble();
        double ask=data.value("askRate").toString().toDouble();
        if (ask>0.0) { spread=qAbs(ask-bid)/ask*100; }
        if (qFuzzyIsNull(ask)==true) { spread=0.0; }
        spread_item->setText(QString::number(spread,'g'));
        spread_item->setToolTip(QString::number(spread,'f',8));
    }

    emit this->tableItemsModified(market,data);
}
