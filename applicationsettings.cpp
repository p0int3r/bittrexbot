#include "applicationsettings.hpp"
#include <QJsonObject>
#include <QJsonDocument>
#include <QFile>
#include <QDir>

ApplicationSettings::ApplicationSettings(QObject *parent) : QObject(parent)
{
    this->initializeSignalsAndSlots();
    bool exists_application_settings_file=this->existsApplicationSettingsFile();
    if (exists_application_settings_file==false) { this->createApplicationSettingsFile(); }
    if (exists_application_settings_file==true) { this->loadApplicationSettingsFile(); }
}

void ApplicationSettings::initializeSignalsAndSlots()
{
    this->connect(this,&ApplicationSettings::lightModeChanged,this,&ApplicationSettings::save);
    this->connect(this,&ApplicationSettings::darkModeChanged,this,&ApplicationSettings::save);
    this->connect(this,&ApplicationSettings::loggerChanged,this,&ApplicationSettings::save);
    this->connect(this,&ApplicationSettings::languageChanged,this,&ApplicationSettings::save);
}

bool ApplicationSettings::getLightMode() const { return this->lightMode; }

void ApplicationSettings::setLightMode(bool value)
{
    this->lightMode=value;
    emit this->lightModeChanged(this->lightMode);
}

bool ApplicationSettings::getDarkMode() const { return this->darkMode; }

void ApplicationSettings::setDarkMode(bool value)
{
    this->darkMode=value;
    emit this->darkModeChanged(this->darkMode);
}

bool ApplicationSettings::getLogger() const { return this->logger; }

void ApplicationSettings::setLogger(bool value)
{
    this->logger=value;
    emit this->loggerChanged(this->logger);
}

QString ApplicationSettings::getLanguage() const { return this->language; }

void ApplicationSettings::setLanguage(const QString &value)
{
    this->language=value;
    emit this->languageChanged(this->language);
}

QString ApplicationSettings::getApplicationSettingsFilePath() const { return this->applicationSettingsFilePath; }

void ApplicationSettings::setApplicationSettingsFilePath(const QString &value) { this->applicationSettingsFilePath=value; }


bool ApplicationSettings::existsApplicationSettingsFile()
{
    QDir home_directory=QDir::home();
    QString application_settings_file_path=home_directory.path()+QString("/")+QString(".bittrexbot/application_settings.json");
    bool exists_application_settings_file=QFile::exists(application_settings_file_path);
    return exists_application_settings_file;
}

bool ApplicationSettings::createApplicationSettingsFile()
{
    QDir home_directory=QDir::home();
    home_directory.mkdir(".bittrexbot");
    QString application_settings_file_path=home_directory.path()+QString("/")+QString(".bittrexbot/application_settings.json");
    this->setApplicationSettingsFilePath(application_settings_file_path);
    this->defaults(); this->save();
    return true;
}


bool ApplicationSettings::loadApplicationSettingsFile()
{
    QDir home_directory=QDir::home();
    QString application_settings_file_path=home_directory.path()+QString("/")+QString(".bittrexbot/application_settings.json");
    this->setApplicationSettingsFilePath(application_settings_file_path);
    return true;
}

void ApplicationSettings::defaults()
{
    this->setLightMode(true);
    this->setDarkMode(false);
    this->setLogger(false);
    this->setLanguage(QString("English"));
}

void ApplicationSettings::save()
{
    QJsonObject object;
    object.insert("lightMode",this->getLightMode());
    object.insert("darkMode",this->getDarkMode());
    object.insert("logger",this->getLogger());
    object.insert("language",this->getLanguage());
    QJsonDocument document(object);
    QFile json_file=QFile(this->getApplicationSettingsFilePath());
    json_file.open(QFile::WriteOnly);
    json_file.write(document.toJson());
    json_file.close();
}

void ApplicationSettings::restore()
{
    QFile json_file=QFile(this->getApplicationSettingsFilePath());
    json_file.open(QFile::ReadOnly);
    QJsonDocument document=QJsonDocument::fromJson(json_file.readAll());
    json_file.close();
    QJsonObject object=document.object();
    this->lightMode=object.value("lightMode").toBool();
    this->darkMode=object.value("darkMode").toBool();
    this->logger=object.value("logger").toBool();
    this->language=object.value("language").toString();
}
