#ifndef SELECTEDMARKETTABLE_HPP
#define SELECTEDMARKETTABLE_HPP

#include <QWidget>
#include <QMap>
#include <QTableWidget>

class SelectedMarketTable: public QTableWidget
{
    Q_OBJECT

    private:
        QMap<QString,QList<QTableWidgetItem *>> *widgetMap=nullptr;
        QMap<QString,int> *rowMap=nullptr;

    public:
        explicit SelectedMarketTable(QWidget *parent=nullptr);
        ~SelectedMarketTable();
        QMap<QString,QList<QTableWidgetItem *> > *getWidgetMap() const;
        void setWidgetMap(QMap<QString,QList<QTableWidgetItem *> > *value);
        QMap<QString,int> *getRowMap() const;
        void setRowMap(QMap<QString,int> *value);

    signals:
        void marketInserted(const QString &market);
        void marketRemoved(const QString &market);
        void tradingRoundsSpecified(const QString &market,int tradingRounds);
        void capitalPercentageSpecified(const QString &market,double capitalPercentage);

    public slots:
        void insertMarket(const QString &market);
        void removeMarket(const QString &market);
        void specifyTradingRounds(const QString &market,const int &tradingRounds);
        void specifyCapitalPercentage(const QString &market,const double &capitalPercentage);
};

#endif // SELECTEDMARKETTABLE_HPP
