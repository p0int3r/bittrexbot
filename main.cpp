#include "mainwindow.hpp"
#include <QApplication>
#include <QTranslator>

int main(int argc, char *argv[])
{
    QTranslator translator;
    QApplication application(argc, argv);
    QApplication::setStyle(QStyleFactory::create("Fusion"));
    MainWindow window; window.show();
    return application.exec();
}
