#include "openordertable.hpp"
#include <QToolButton>
#include <QTableWidgetItem>
#include "qtablenumericitem.hpp"


OpenOrderTable::OpenOrderTable(QWidget *parent): QTableWidget(parent)
{
    this->setMap(new QMap<QString,QList<QTableWidgetItem *>>());
    this->initializeSignalsAndSlots();
}

OpenOrderTable::~OpenOrderTable() { delete this->map; }


void OpenOrderTable::initializeSignalsAndSlots()
{
    // TODO: implement.
}

OpenOrderTable::ColorMode OpenOrderTable::getColorMode() const { return this->colorMode; }

void OpenOrderTable::setColorMode(const OpenOrderTable::ColorMode &value) { this->colorMode=value; }

QMap<QString, QList<QTableWidgetItem *> > *OpenOrderTable::getMap() const { return this->map; }

void OpenOrderTable::setMap(QMap<QString, QList<QTableWidgetItem *> > *value) { this->map=value; }


void OpenOrderTable::changeToLightMode()
{
    this->setColorMode(OpenOrderTable::ColorMode::LIGHT);
    QMap<QString,QList<QTableWidgetItem *>>::iterator iterator;
    for (iterator=this->getMap()->begin();iterator!=this->getMap()->end();iterator++)
    {
        QList<QTableWidgetItem *> tableWidgetList=iterator.value();
        QTableWidgetItem *cancelItem=tableWidgetList.at(0);
        QTableWidgetItem *sideItem=tableWidgetList.at(3);
        QTableWidgetItem *priceItem=tableWidgetList.at(5);
        QTableWidgetItem *quantityItem=tableWidgetList.at(6);
        QTableWidgetItem *filledItem=tableWidgetList.at(7);
        QToolButton *cancelButton=static_cast<QToolButton *>(this->cellWidget(cancelItem->row(),cancelItem->column()));
        cancelButton->setIcon(QIcon(":/icons/light_mode/cancel_order_icon.png"));
        if (sideItem->text()==QString("BUY")) { sideItem->setForeground(QColor("green")); }
        if (sideItem->text()==QString("SELL")) { sideItem->setForeground(QColor("red")); }
        priceItem->setForeground(QColor("purple"));
        quantityItem->setForeground(QColor(0,85,127));
        filledItem->setForeground(QColor("SaddleBrown"));
    }
}

void OpenOrderTable::changeToDarkMode()
{
    this->setColorMode(OpenOrderTable::ColorMode::DARK);
    QMap<QString,QList<QTableWidgetItem *>>::iterator iterator;
    for (iterator=this->getMap()->begin();iterator!=this->getMap()->end();iterator++)
    {
        QList<QTableWidgetItem *> tableWidgetList=iterator.value();
        QTableWidgetItem *cancelItem=tableWidgetList.at(0);
        QTableWidgetItem *sideItem=tableWidgetList.at(3);
        QTableWidgetItem *priceItem=tableWidgetList.at(5);
        QTableWidgetItem *quantityItem=tableWidgetList.at(6);
        QTableWidgetItem *filledItem=tableWidgetList.at(7);
        QToolButton *cancelButton=static_cast<QToolButton *>(this->cellWidget(cancelItem->row(),cancelItem->column()));
        cancelButton->setIcon(QIcon(":/icons/dark_mode/cancel_order_icon.png"));
        if (sideItem->text()==QString("BUY")) { sideItem->setForeground(QColor("lime")); }
        if (sideItem->text()==QString("SELL")) { sideItem->setForeground(QColor("orange")); }
        priceItem->setForeground(QColor("#ffddf4"));
        quantityItem->setForeground(QColor("cyan"));
        filledItem->setForeground(QColor("YellowGreen"));
    }
}


void OpenOrderTable::insertTableItems(const QString &orderId,const trader::Order *order)
{
    int currentRow=0;
    this->insertRow(currentRow);
    QList<QTableWidgetItem *> tableWidgetList=QList<QTableWidgetItem *>();
    QToolButton *cancelButton=new QToolButton();
    cancelButton->setEnabled(false);
    if (this->getColorMode()==OpenOrderTable::ColorMode::LIGHT) { cancelButton->setIcon(QIcon(":/icons/light_mode/cancel_order_icon.png")); }
    if (this->getColorMode()==OpenOrderTable::ColorMode::DARK) { cancelButton->setIcon(QIcon(":/icons/dark_mode/cancel_order_icon.png")); }
    this->connect(cancelButton,&QToolButton::clicked,this,[=](){ this->cancelTableItems(orderId,order); });
    QTableWidgetItem *cancelItem=new QTableWidgetItem();
    cancelItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    this->setItem(currentRow,0,cancelItem);
    this->setCellWidget(currentRow,0,cancelButton);

    QTableWidgetItem *openedItem=new QTableWidgetItem();
    openedItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    QString opened=order->getOpened().toString();
    openedItem->setText(opened);
    openedItem->setToolTip(opened);
    this->setItem(currentRow,1,openedItem);

    QTableWidgetItem *marketItem=new QTableWidgetItem();
    marketItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    QString market=order->getMarket();
    marketItem->setText(market);
    marketItem->setToolTip(market);
    this->setItem(currentRow,2,marketItem);

    QTableWidgetItem *sideItem=new QTableWidgetItem();
    sideItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    QString side=QString();
    QString foregroundColor=QString();
    if (order->getType()==trader::Order::Type::LIMIT_BUY)
    {
        side=QString("BUY");
        if (this->getColorMode()==OpenOrderTable::ColorMode::LIGHT) { foregroundColor=QString("green"); }
        if (this->getColorMode()==OpenOrderTable::ColorMode::DARK) { foregroundColor=QString("lime"); }
    }
    if (order->getType()==trader::Order::Type::LIMIT_SELL)
    {
        side=QString("SELL");
        if (this->getColorMode()==OpenOrderTable::ColorMode::LIGHT) { foregroundColor=QString("red"); }
        if (this->getColorMode()==OpenOrderTable::ColorMode::DARK) { foregroundColor=QString("orange"); }
    }
    sideItem->setForeground(QColor(foregroundColor));
    sideItem->setText(side);
    sideItem->setToolTip(side);
    this->setItem(currentRow,3,sideItem);

    QTableWidgetItem *typeItem=new QTableWidgetItem();
    typeItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    QString type=QString("Limit");
    typeItem->setText(type);
    typeItem->setToolTip(type);
    this->setItem(currentRow,4,typeItem);

    QTableNumericItem *priceItem=new QTableNumericItem();
    priceItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double price=order->getPrice();
    if (this->getColorMode()==OpenOrderTable::ColorMode::LIGHT) { priceItem->setForeground(QColor("purple")); }
    if (this->getColorMode()==OpenOrderTable::ColorMode::DARK) { priceItem->setForeground(QColor("#ffddf4")); }
    priceItem->setText(QString::number(price,'g'));
    priceItem->setToolTip(QString::number(price,'f',8));
    this->setItem(currentRow,5,priceItem);

    QTableNumericItem *quantityItem=new QTableNumericItem();
    quantityItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double quantity=order->getQuantity();
    if (this->getColorMode()==OpenOrderTable::ColorMode::LIGHT) { quantityItem->setForeground(QColor(0,85,127)); }
    if (this->getColorMode()==OpenOrderTable::ColorMode::DARK) { quantityItem->setForeground(QColor("cyan")); }
    quantityItem->setText(QString::number(quantity));
    quantityItem->setToolTip(QString::number(quantity,'f',8));
    this->setItem(currentRow,6,quantityItem);

    QTableNumericItem *filledItem=new QTableNumericItem();
    filledItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double fillQuantity=order->getData().value("fillQuantity").toString().toDouble();
    double filled=fillQuantity/quantity;
    filledItem->setText(QString::number(filled*100.0,'g'));
    filledItem->setToolTip(QString::number(filled*100.0,'f',8));
    if (this->getColorMode()==OpenOrderTable::ColorMode::LIGHT) { filledItem->setForeground(QColor("SaddleBrown")); }
    if (this->getColorMode()==OpenOrderTable::ColorMode::DARK) { filledItem->setForeground(QColor("YellowGreen")); }
    this->setItem(currentRow,7,filledItem);

    QTableNumericItem *estTotalItem=new QTableNumericItem();
    estTotalItem->setTextAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    double commissionPaid=order->getData().value("commission").toString().toDouble();
    double estTotal=(quantity*price)+commissionPaid;
    if (order->getType()==trader::Order::Type::LIMIT_BUY) { estTotal=-1.0*estTotal; }
    if (order->getType()==trader::Order::Type::LIMIT_SELL) { estTotal=1.0*estTotal; }
    estTotalItem->setText(QString::number(estTotal,'g'));
    estTotalItem->setToolTip(QString::number(estTotal,'f',8));
    this->setItem(currentRow,8,estTotalItem);

    tableWidgetList.insert(0,cancelItem);
    tableWidgetList.insert(1,openedItem);
    tableWidgetList.insert(2,marketItem);
    tableWidgetList.insert(3,sideItem);
    tableWidgetList.insert(4,typeItem);
    tableWidgetList.insert(5,priceItem);
    tableWidgetList.insert(6,quantityItem);
    tableWidgetList.insert(7,filledItem);
    tableWidgetList.insert(8,estTotalItem);
    this->getMap()->insert(orderId,tableWidgetList);

    emit this->tableItemsInserted(orderId,order);
}

void OpenOrderTable::cancelTableItems(const QString &orderId,const trader::Order *order)
{
    QList<QTableWidgetItem *> tableWidgetList=this->getMap()->value(orderId);
    if (tableWidgetList.length()==0) { return; }
    int current_row=tableWidgetList.at(0)->row();
    this->removeRow(current_row);
    this->getMap()->remove(orderId);
    emit this->tableItemsCanceled(orderId,order);
}

void OpenOrderTable::removeTableItems(const QString &orderId,const trader::Order *order)
{
    QList<QTableWidgetItem *> tableWidgetList=this->getMap()->value(orderId);
    if (tableWidgetList.length()==0) { return; }
    int current_row=tableWidgetList.at(0)->row();
    this->removeRow(current_row);
    this->getMap()->remove(orderId);
    emit this->tableItemsRemoved(orderId,order);
}


void OpenOrderTable::modifyTableItems(const QString &orderId,const trader::Order *order)
{
    QList<QTableWidgetItem *> tableWidgetList=this->getMap()->value(orderId);
    if (tableWidgetList.length()==0) { return; }
    int current_row=tableWidgetList.at(0)->row();
    QTableWidgetItem *filledItem=static_cast<QTableNumericItem *>(tableWidgetList.at(7));
    QJsonObject order_object=order->getData();
    if (order_object.isEmpty()==true) { return; }
    double quantity=order_object.value("quantity").toString().toDouble();
    double fill_quantity=order_object.value("fillQuantity").toString().toDouble();
    double filled=fill_quantity/quantity;
    filledItem->setText(QString::number(filled*100.0,'f',3));
    filledItem->setToolTip(QString::number(filled*100.0,'f',8));
    QToolButton *cancelButton=static_cast<QToolButton *>(this->cellWidget(current_row,0));
    if (qFuzzyIsNull(fill_quantity)==true) { cancelButton->setEnabled(true); }
    else if (fill_quantity<quantity) { cancelButton->setEnabled(false); }
    emit this->tableItemsModified(orderId,order);
}
